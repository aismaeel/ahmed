//=================================================================================================
Geometry.Tolerance = 1.0e-4;
Mesh.ElementOrder = 1;

//=================================================================================================
Dia   = 2.5; // cm
Thick = 0.2; // cm
Len   = 12.5; // cm

NELEM_PERIPH = 5;
NELEM_AXIAL  = 20;

NELEM_PERIPH = 8;
NELEM_AXIAL  = 50;

NELEM_PERIPH = 8;
NELEM_AXIAL  = 10;

//=================================================================================================

Point(1)  = {Dia/2, 0, 0};
Extrude {Thick, 0, 0} { Point{1}; Layers{1}; }
Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} { Line{ 1}; Layers{NELEM_PERIPH}; Recombine; }
Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} { Line{ 2}; Layers{NELEM_PERIPH}; Recombine; }
Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} { Line{ 6}; Layers{NELEM_PERIPH}; Recombine; }
Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} { Line{10}; Layers{NELEM_PERIPH}; Recombine; }
Extrude {0, 0, Len} { Surface{5,9,13,17}; Layers{NELEM_AXIAL}; Recombine; }

Physical Volume(301) = {1,2,3,4};
Physical Surface(200) = {5,9,13,17};
Physical Surface(201) = {39,61,83,105};
Physical Surface(202) = {38,60,82,104};
Physical Surface(203) = {30,52,74,96};

//=================================================================================================
