//=================================================================================================
Merge "aortic_arch-input.stl";
CreateTopology;
Merge "aortic_arch-radius.bgm";

out[] = Extrude{ Surface{1}; Layers{1,  0.5}; Using Index[0]; Using View[0]; Recombine; };

//=================================================================================================
Physical Volume (301) = {1}; // arterial wall volume

Physical Surface(200) = { 1}; // external surface
Physical Surface(201) = {31}; // brachiocephalic artery
Physical Surface(202) = {27}; // left common carotid artery
Physical Surface(203) = {15}; // left subclavian artery
Physical Surface(204) = {23}; // asceding aorta (inlet)
Physical Surface(205) = {19}; // desceding aorta (outlet)
Physical Surface(206) = {32}; // internal surface (lumen)
//=================================================================================================
