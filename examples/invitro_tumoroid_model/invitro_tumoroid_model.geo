//=================================================================================================
LENGTH_SCALE = 1.0;
DIA = 15.0*LENGTH_SCALE;     // diameter of the tumoroid (tumour spheroid) sample
LX = 4.0*LENGTH_SCALE;       // tumoroid length in the X-axis
LY = 3.0*LENGTH_SCALE;       // tumoroid length in the Y-axis
LZ = 3.0*LENGTH_SCALE;       // tumoroid length in the Z-axis
H_ABOVE = 2.6*LENGTH_SCALE;  // distance from the center of the tumoroid to the top surface
H_BELOW = 2.6*LENGTH_SCALE;  // distance from the center of the tumoroid to the bottom surface

Geometry.Tolerance = 1.0e-6*LENGTH_SCALE;
Mesh.ElementOrder = 1;
Mesh.CharacteristicLengthMin = DIA/31;
Mesh.CharacteristicLengthMax = DIA/30;

//=================================================================================================
Point(1) = {0.0, 0.0, -H_BELOW};
Point(2) = {0.0, 0.0, +H_ABOVE};
Extrude {DIA/2, 0.0, 0.0} { Point{1,2}; }
Extrude {{0, 0, +1}, {0.0, 0.0, 0.0}, +Pi/2} { Line{ 1, 2}; }
Extrude {{0, 0, +1}, {0.0, 0.0, 0.0}, +Pi/2} { Line{ 3, 6}; }
Extrude {{0, 0, +1}, {0.0, 0.0, 0.0}, +Pi/2} { Line{ 9,12}; }
Extrude {{0, 0, +1}, {0.0, 0.0, 0.0}, +Pi/2} { Line{15,18}; }
Line(27) = { 3,  4};
Line(28) = { 6,  8};
Line(29) = {10, 12};
Line(30) = {14, 16};
Line Loop(31) = { 4, 28,  -7, -27};  Ruled Surface(32) = {31};
Line Loop(33) = {10, 29, -13, -28};  Ruled Surface(34) = {33};
Line Loop(35) = {16, 30, -19, -29};  Ruled Surface(36) = {35};
Line Loop(37) = {22, 27, -25, -30};  Ruled Surface(38) = {37};

//Surface Loop(39) = {20, 14, 8, 26, 38, 23, 17, 11, 5, 32, 34, 36};  Volume(40) = {39};

Point(50) = {0.0, 0.0, 0.0};
Point(51) = {+LX/2, 0.0, 0.0};
Point(52) = {-LX/2, 0.0, 0.0};
Point(53) = {0.0, +LY/2, 0.0};
Point(54) = {0.0, -LY/2, 0.0};
Point(55) = {0.0, 0.0, +LZ/2};
Point(56) = {0.0, 0.0, -LZ/2};

Ellipse(39) = {51, 50, 50, 53};
Ellipse(40) = {53, 50, 50, 52};
Ellipse(41) = {52, 50, 50, 54};
Ellipse(42) = {54, 50, 50, 51};

Ellipse(43) = {51, 50, 50, 55};
Ellipse(44) = {55, 50, 50, 52};
Ellipse(45) = {52, 50, 50, 56};
Ellipse(46) = {56, 50, 50, 51};

Ellipse(47) = {53, 50, 50, 55};
Ellipse(48) = {55, 50, 50, 54};
Ellipse(49) = {54, 50, 50, 56};
Ellipse(50) = {56, 50, 50, 53};

Line Loop(51) = {39, 47, -43};  Ruled Surface(52) = {51};
Line Loop(53) = {40, -44, -47}; Ruled Surface(54) = {53};
Line Loop(55) = {41, -48, 44};  Ruled Surface(56) = {55};
Line Loop(57) = {42, 43, 48};   Ruled Surface(58) = {57};
Line Loop(59) = {39, -50, 46};  Ruled Surface(60) = {59};
Line Loop(61) = {40, 45, 50};   Ruled Surface(62) = {61};
Line Loop(63) = {41, 49, -45};  Ruled Surface(64) = {63};
Line Loop(65) = {42, -46, -49}; Ruled Surface(66) = {65};

// tumoroid region
Surface Loop(67) = {64, 56, 58, 66, 60, 52, 54, 62};                Volume(68) = {67};
// matrigel region
Surface Loop(69) = {36, 17, 11, 5, 23, 38, 32, 34, 14, 8, 26, 20};  Volume(70) = {67, 69};

//=================================================================================================
//Transfinite Line {1,2,3,6,9,12,15,18} = 5 Using Progression 1.0;  // radial lines
//Transfinite Line {4:25:3} = 10 Using Progression 1.0;             // circumference/4
//Transfinite Line {27:30} = 9 Using Progression 1.0;               // vertical lines
//Transfinite Line {39:42} = 6 Using Progression 1.0;               // quarter ellipse lines X
//Transfinite Line {47:50} = 6 Using Progression 1.0;               // quarter ellipse lines Y
//Transfinite Line {43:46} = 6 Using Progression 1.0;               // quarter ellipse lines Y

//=================================================================================================
Physical Surface(2001) = {5:23:6}; // bottom surface
Physical Surface(2002) = {8:26:6}; // top surface
Physical Surface(2003) = {32:38:2}; // side surface
Physical Volume(3001) = {68}; // tumoroid
Physical Volume(3002) = {70}; // matrigel

//=================================================================================================






