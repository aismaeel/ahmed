//=================================================================================================
Geometry.Tolerance = 1.0e-6;
Mesh.ElementOrder = 1;
Mesh.Algorithm = 1;

R = 10.0;
r = 8.0;
D = R - r;

NN_Def = 3;
NE_R = 2;
NE_S = 5;

//=================================================================================================
Point(1) = {+r+D, 0, 0};
Extrude {{0, 0, 1}, {+r+D/2, 0, 0}, Pi/4} { Point{1}; Layers{NE_R}; }
Extrude {{0, 0, 1}, {+r+D/2, 0, 0}, Pi/4} { Point{2}; Layers{NE_R}; }
Extrude {{0, 0, 1}, {+r+D/2, 0, 0}, Pi/4} { Point{4}; Layers{NE_R}; }
Extrude {{0, 0, 1}, {+r+D/2, 0, 0}, Pi/4} { Point{5}; Layers{NE_R}; }
Extrude {{0, 0, 1}, {+r+D/2, 0, 0}, Pi/4} { Point{6}; Layers{NE_R}; }
Extrude {{0, 0, 1}, {+r+D/2, 0, 0}, Pi/4} { Point{7}; Layers{NE_R}; }
Extrude {{0, 0, 1}, {+r+D/2, 0, 0}, Pi/4} { Point{8}; Layers{NE_R}; }
Extrude {{0, 0, 1}, {+r+D/2, 0, 0}, Pi/4} { Point{9}; Layers{NE_R}; }
Line( 9) = {1, 3};
Line(10) = {3, 4};
Line(11) = {3, 6};
Line(12) = {3, 8};
Line Loop(13) = {1, 2, -10, -9}; Plane Surface(14) = {13};
Line Loop(15) = {3, 4, -11, 10}; Plane Surface(16) = {15};
Line Loop(17) = {5, 6, -12, 11}; Plane Surface(18) = {17};
Line Loop(19) = {7, 8,   9, 12}; Plane Surface(20) = {19};
Extrude {{0, -1, 0}, {0, 0, 0}, Pi/8} { Surface{ 14: 20: 2}; Layers{NE_S}; Recombine; }
Extrude {{0, -1, 0}, {0, 0, 0}, Pi/8} { Surface{ 42:108:22}; Layers{NE_S}; Recombine; }
Extrude {{0, -1, 0}, {0, 0, 0}, Pi/8} { Surface{130:196:22}; Layers{NE_S}; Recombine; }
Extrude {{0, -1, 0}, {0, 0, 0}, Pi/8} { Surface{218:284:22}; Layers{NE_S}; Recombine; }

Transfinite Line "*" = NN_Def;

//=================================================================================================
Physical Volume(301) = {1:16};
Physical Surface(2000) = {306:372:22}; // UX = 0
Physical Surface(2002) = { 14: 20: 2}; // UZ = 0
Physical Surface(2004) = {29,33,51,55,73,77,95,99, 117,121,139,143,161,165,183,187, 205,209,227,231,249,253,271,275, 297,315,319,337,341,359}; // T = 0
Physical Surface(2005) = {293,363}; // Tz = 0

//=================================================================================================
