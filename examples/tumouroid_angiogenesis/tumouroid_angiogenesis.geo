//=================================================================================================
Geometry.Tolerance = 1.0e-8;
Mesh.ElementOrder = 1;

//=================================================================================================
// Geometrical parameters
RAD = 0.500e-3;
NFL = 1.5*RAD;
LEN = 12.0*RAD;
// Scale parameters
scale__1 = 0.65;
scale__2 = 0.65;

DIA = 2.0*LEN/16;

//=================================================================================================
// Tumour domain
Point(1) = {0.0, 0.0, 0.0};
Extrude{RAD/2, 0.0, 0.0} { Point{1}; }
Extrude{0.0, RAD/2, 0.0} { Point{1}; }
Extrude{0.0, 0.0, RAD/2} { Point{1}; }
Point(5) = {0.85*RAD/2, 0.85*RAD/2, 0.0};
Point(6) = {0.85*RAD/2, 0.0, 0.85*RAD/2};
Point(7) = {0.0, 0.85*RAD/2, 0.85*RAD/2};
Point(8) = {0.85*RAD/2, 0.85*RAD/2, 0.85*RAD/2};
Line(4) = {2, 5};
Line(5) = {3, 5};
Line(6) = {2, 6};
Line(7) = {4, 6};
Line(8) = {3, 7};
Line(9) = {4, 7};
Line(10) = {5, 8};
Line(11) = {6, 8};
Line(12) = {7, 8};
Line Loop(13) = {1, 4, -5, -2};   Plane Surface(14) = {13};
Line Loop(15) = {1, 6, -7, -3};   Plane Surface(16) = {15};
Line Loop(17) = {2, 8, -9, -3};   Plane Surface(18) = {17};
Line Loop(19) = {4, 10, -11, -6}; Plane Surface(20) = {19};
Line Loop(21) = {5, 10, -12, -8}; Plane Surface(22) = {21};
Line Loop(23) = {7, 11, -12, -9}; Plane Surface(24) = {23};
Surface Loop(25) = {16, 14, 20, 22, 24, 18}; Volume(26) = {25};
Extrude{RAD/2, 0.0, 0.0} { Point{2}; }
Extrude{0.0, RAD/2, 0.0} { Point{3}; }
Extrude{0.0, 0.0, RAD/2} { Point{4}; }
Extrude {{0, 0, 1}, {0, 0, 0}, +Pi/4} { Point{ 9}; }
Extrude {{0, 0, 1}, {0, 0, 0}, +Pi/4} { Point{12}; }
Extrude {{1, 0, 0}, {0, 0, 0}, +Pi/4} { Point{10}; }
Extrude {{1, 0, 0}, {0, 0, 0}, +Pi/4} { Point{13}; }
Extrude {{0, 1, 0}, {0, 0, 0}, +Pi/4} { Point{11}; }
Extrude {{0, 1, 0}, {0, 0, 0}, +Pi/4} { Point{14}; }
Line(36) = {5, 12};
Line(37) = {7, 13};
Line(38) = {6, 14};
Line Loop(39) = {4, 36, -30, -27};  Plane Surface(40) = {39};
Line Loop(41) = {5, 36, 31, -28};   Plane Surface(42) = {41};
Line Loop(43) = {28, 32, -37, -8};  Plane Surface(44) = {43};
Line Loop(45) = {29, -33, -37, -9}; Plane Surface(46) = {45};
Line Loop(47) = {29, 34, -38, -7};  Plane Surface(48) = {47};
Line Loop(49) = {38, 35, -27, 6};   Plane Surface(50) = {49};
Point(15) = {RAD/Sqrt(3.0), RAD/Sqrt(3.0), RAD/Sqrt(3.0)};
Line(51) = {8, 15};
Circle(52) = {12, 1, 15};
Circle(53) = {13, 1, 15};
Circle(54) = {14, 1, 15};
Line Loop(55) = {36, 52, -51, -10};  Ruled Surface(56) = {55};
Line Loop(57) = {12, 51, -53, -37};  Ruled Surface(58) = {57};
Line Loop(59) = {11, 51, -54, -38};  Ruled Surface(60) = {59};
Line Loop(61) = {30, 52, -54, 35}; Ruled Surface(62) = {61};
Line Loop(63) = {31, 32, 53, -52}; Ruled Surface(64) = {63};
Line Loop(65) = {33, 34, 54, -53}; Ruled Surface(66) = {65};
Surface Loop(67) = {40, 62, 50, 56, 20, 60}; Volume(68) = {67};
Surface Loop(69) = {42, 64, 44, 58, 56, 22}; Volume(70) = {69};
Surface Loop(71) = {66, 46, 48, 24, 58, 60}; Volume(72) = {71};

//=================================================================================================
// Peri-tumoural domain
Extrude{NFL, 0.0, 0.0} { Point{ 9}; }
Extrude{0.0, NFL, 0.0} { Point{10}; }
Extrude{0.0, 0.0, NFL} { Point{11}; }
Extrude {{0, 0, 1}, {0, 0, 0}, +Pi/4} { Point{16}; }
Extrude {{0, 0, 1}, {0, 0, 0}, +Pi/4} { Point{19}; }
Extrude {{1, 0, 0}, {0, 0, 0}, +Pi/4} { Point{17}; }
Extrude {{1, 0, 0}, {0, 0, 0}, +Pi/4} { Point{20}; }
Extrude {{0, 1, 0}, {0, 0, 0}, +Pi/4} { Point{18}; }
Extrude {{0, 1, 0}, {0, 0, 0}, +Pi/4} { Point{21}; }
Line(82) = {12, 19};
Line(83) = {13, 20};
Line(84) = {14, 21};
Line Loop(85) = {73, 76, -82, -30}; Plane Surface(86) = {85};
Line Loop(87) = {82, 77, -74, -31}; Plane Surface(88) = {87};
Line Loop(89) = {74, 78, -83, -32}; Plane Surface(90) = {89};
Line Loop(91) = {79, -75, -33, 83}; Plane Surface(92) = {91};
Line Loop(93) = {80, -84, -34, 75}; Plane Surface(94) = {93};
Line Loop(95) = {84, 81, -73, -35}; Plane Surface(96) = {95};
Point(22) = {(RAD+NFL)/Sqrt(3.0), (RAD+NFL)/Sqrt(3.0), (RAD+NFL)/Sqrt(3.0)};
Line(97) = {15, 22};
Circle( 98) = {19, 1, 22};
Circle( 99) = {20, 1, 22};
Circle(100) = {21, 1, 22};
Line Loop(101) = {82, 98, -97, -52};  Plane Surface(102) = {101};
Line Loop(103) = {97, -99, -83, 53};  Plane Surface(104) = {103};
Line Loop(105) = {54, 97, -100, -84}; Plane Surface(106) = {105};
Line Loop(107) = {76, 98, -100, 81};   Ruled Surface(108) = {107};
Line Loop(109) = {77, 78, 99, -98};    Ruled Surface(110) = {109};
Line Loop(111) = {99, -100, -80, -79}; Ruled Surface(112) = {111};
Surface Loop(113) = {86, 96, 108, 62, 102, 106}; Volume(114) = {113};
Surface Loop(115) = {88, 110, 90, 102, 104, 64}; Volume(116) = {115};
Surface Loop(117) = {112, 94, 92, 106, 104, 66}; Volume(118) = {117};

//=================================================================================================
// ECM domain
Extrude{LEN-NFL-RAD, 0.0, 0.0} { Point{16}; }
Extrude{0.0, LEN-NFL-RAD, 0.0} { Point{17}; }
Extrude{0.0, 0.0, LEN-NFL-RAD} { Point{18}; }
Extrude{0.0, RAD+scale__1*NFL, 0.0} { Point{23}; }
Extrude{0.0, 0.0, RAD+scale__1*NFL} { Line{122}; }
Extrude{RAD+scale__1*NFL, 0.0, 0.0} { Point{24}; }
Extrude{0.0, 0.0, RAD+scale__1*NFL} { Line{127}; }
Extrude{RAD+scale__1*NFL, 0.0, 0.0} { Point{25}; }
Extrude{0.0, RAD+scale__1*NFL, 0.0} { Line{132}; }
Line(137) = {21, 27};
Line(138) = {21, 32};
Line(139) = {20, 30};
Line(140) = {20, 33};
Line(141) = {22, 28};
Line(142) = {22, 31};
Line(143) = {22, 34};
Line(144) = {19, 26};
Line(145) = {19, 29};
Line Loop(146) = {119, 124, -137, 81};   Plane Surface(147) = {146};
Line Loop(148) = {144, 125, -141, -98};  Plane Surface(149) = {148};
Line Loop(150) = {76, 144, -122, -119};  Plane Surface(151) = {150};
Line Loop(152) = {77, 120, 127, -145};   Plane Surface(153) = {152};
Line Loop(154) = {120, 129, -139, -78};  Plane Surface(155) = {154};
Line Loop(156) = {145, 130, -142, -98};  Plane Surface(157) = {156};
Line Loop(158) = {100, 141, -123, -137}; Plane Surface(159) = {158};
Line Loop(160) = {99, 142, -128, -139};  Plane Surface(161) = {160};
Line Loop(162) = {80, 138, -132, -121};  Plane Surface(163) = {162};
Line Loop(164) = {121, 134, -140, 79};   Plane Surface(165) = {164};
Line Loop(166) = {99, 143, -133, -140};  Plane Surface(167) = {166};
Line Loop(168) = {100, 143, -135, -138}; Plane Surface(169) = {168};
Surface Loop(170) = {131, 153, 155, 161, 157, 110}; Volume(171) = {170};
Surface Loop(172) = {165, 163, 169, 167, 136, 112}; Volume(173) = {172};
Surface Loop(174) = {149, 151, 126, 159, 147, 108}; Volume(175) = {174};
Point(35) = {LEN, LEN, 0.0};
Point(36) = {LEN, 0.0, LEN};
Point(37) = {0.0, LEN, LEN};
Point(38) = {LEN, LEN, LEN};
Extrude{0.0, 0.0, RAD+scale__2*NFL} { Point{35}; }
Extrude{0.0, RAD+scale__2*NFL, 0.0} { Point{36}; }
Extrude{RAD+scale__2*NFL, 0.0, 0.0} { Point{37}; }
Line(179) = {26, 35};
Line(180) = {29, 35};
Line(181) = {27, 36};
Line(182) = {32, 36};
Line(183) = {30, 37};
Line(184) = {33, 37};
Line(185) = {28, 39};
Line(186) = {31, 39};
Line(187) = {28, 40};
Line(188) = {34, 40};
Line(189) = {31, 41};
Line(190) = {34, 41};
Line(191) = {39, 38};
Line(192) = {40, 38};
Line(193) = {41, 38};
Line Loop(194) = {137, 181, -182, -138}; Plane Surface(195) = {194};
Line Loop(196) = {144, 179, -180, -145}; Plane Surface(197) = {196};
Line Loop(198) = {139, 183, -184, -140}; Plane Surface(199) = {198};
Line Loop(200) = {141, 185, -186, -142}; Plane Surface(201) = {200};
Line Loop(202) = {142, 189, -190, -143}; Plane Surface(203) = {202};
Line Loop(204) = {141, 187, -188, -143}; Plane Surface(205) = {204};
Line Loop(206) = {185, 191, -192, -187}; Plane Surface(207) = {206};
Line Loop(208) = {186, 191, -193, -189}; Plane Surface(209) = {208};
Line Loop(210) = {192, -193, -190, 188}; Plane Surface(211) = {210};
Surface Loop(212) = {207, 201, 209, 211, 203, 205}; Volume(213) = {212};
Line Loop(214) = {123, 187, -177, -181}; Plane Surface(215) = {214};
Line Loop(216) = {182, 177, -188, -135}; Plane Surface(217) = {216};
Line Loop(218) = {125, 185, -176, -179}; Plane Surface(219) = {218};
Line Loop(220) = {186, -176, -180, 130}; Plane Surface(221) = {220};
Line Loop(222) = {128, 189, -178, -183}; Plane Surface(223) = {222};
Line Loop(224) = {178, -190, -133, 184}; Plane Surface(225) = {224};
Surface Loop(226) = {197, 219, 221, 149, 157, 201}; Volume(227) = {226};
Surface Loop(228) = {199, 223, 225, 161, 167, 203}; Volume(229) = {228};
Surface Loop(230) = {195, 215, 217, 169, 159, 205}; Volume(231) = {230};

Symmetry {-1, 0, 0, 1.0e-12} { Duplicata{ Volume{26,68:72:2, 114:118:2, 171:175:2, 213, 227:231:2}; } }
Symmetry {0, -1, 0, 1.0e-12} { Duplicata{ Volume{26,68:72:2, 114:118:2, 171:175:2, 213, 227:231:2, 232:325:31, 356:418:31, 449:511:31, 542, 573:635:31}; } }
Symmetry {0, 0, -1, 1.0e-12} { Duplicata{ Volume{26,68:72:2, 114:118:2, 171:175:2, 213, 227:231:2, 232:325:31, 356:418:31, 449:511:31, 542, 573:635:31, 647:740:31, 771:833:31, 864:926:31, 957, 988:1050:31, 1081:1174:31, 1205:1267:31, 1298:1360:31, 1391, 1422:1484:31}; } }

// ================= coarsest mesh ================================================================
// Transfinite Line "*" = 3;
// Transfinite Line {546,544,576,2242,561,2244,467,459,184,184,2259,2165,139,120,1727,190,142,142,1872,1872,145,1731,192,1825,185,179,1808,1810,1395,1393,1425,3110,3112,1410,1316,1308,1308,3033,3127,1023,878,872,2595,2740,976,882,874,2599,2693,961,959,991,2676,2678,977,930,928,2647,2688,2683,966,992,971,2694,182,119,137,1795,1903,188,141,144,1779,1826,193,186,180,1815,1820,3122,3117,1426,1405,1400,1405,3128,3081,1362,1364,1411,2229,2337,521,531,639,2260,2213,513,515,562,2254,2249,556,551,577,191,187,181,962,960,189,138,908,143,972,183,140,899,121,1022,493,557,488,1342,1406,545,547,638,1396,1394,1809,1811,1902,2679,2677,1821,1757,2625,1752,2689,1871,1748,1746,2616,2739,2255,2191,2186,3059,3123,2243,2245,2336,3113,3111} = 6;
// Transfinite Line {-360,-2078,2061,-380,358,363,-1209,75,-816,-2946,-795,74,-806,-775,-2533,-2512,84,1627,-1665,-1644,1692,-1229,82,97,73,83} = 5 Using Progression 1.2;
// Transfinite Surface "*";
// Transfinite Volume "*";
// Recombine Surface "*";
// Recombine Volume "*";

// ================= finer mesh ===================================================================
Transfinite Line "*" = 4;
Transfinite Line {546,544,576,2242,561,2244,467,459,184,184,2259,2165,139,120,1727,190,142,142,1872,1872,145,1731,192,1825,185,179,1808,1810,1395,1393,1425,3110,3112,1410,1316,1308,1308,3033,3127,1023,878,872,2595,2740,976,882,874,2599,2693,961,959,991,2676,2678,977,930,928,2647,2688,2683,966,992,971,2694,182,119,137,1795,1903,188,141,144,1779,1826,193,186,180,1815,1820,3122,3117,1426,1405,1400,1405,3128,3081,1362,1364,1411,2229,2337,521,531,639,2260,2213,513,515,562,2254,2249,556,551,577,191,187,181,962,960,189,138,908,143,972,183,140,899,121,1022,493,557,488,1342,1406,545,547,638,1396,1394,1809,1811,1902,2679,2677,1821,1757,2625,1752,2689,1871,1748,1746,2616,2739,2255,2191,2186,3059,3123,2243,2245,2336,3113,3111} = 10;
Transfinite Line {-360,-2078,2061,-380,358,363,-1209,75,-816,-2946,-795,74,-806,-775,-2533,-2512,84,1627,-1665,-1644,1692,-1229,82,97,73,83} = 5 Using Progression 1.3;
Transfinite Surface "*";
Transfinite Volume "*";
Recombine Surface "*";
Recombine Volume "*";

//=================================================================================================
// Physical volumes and surfaces
Physical Volume(3001) = {26,68:72:2, 232:325:31, 647:740:31, 1081:1174:31, 1496:1589:31, 1930:2023:31, 2364:2457:31, 2798:2891:31}; // Tumour physical domain
Physical Volume(3002) = {114:118:2, 356:418:31, 771:833:31, 1205:1267:31, 1620:1682:31, 2054:2116:31, 2488:2550:31, 2922:2984:31}; // Peri-tumoural physical domain
Physical Volume(3003) = {171:175:2, 213, 227:231:2, 449:511:31, 542, 573:635:31, 864:926:31, 957, 988:1050:31, 1298:1360:31, 1391, 1422:1484:31, 1713:1775:31, 1806, 1837:1899:31, 2147:2209:31, 2240, 2271:2333:31, 2581:2643:31, 2674, 2705:2767:31, 3015:3077:31, 3108, 3139:3201:31}; // ECM physical domain

Physical Surface(2001) = {207,215,1056,958,219,126,937,994,1843,1786,2654,2711,1807,1905,2773,2675}; // External plane normal to X (+1)
Physical Surface(2002) = {1817,1874,2308,2251,1848,1714,2148,2282,221,131,450,584,209,223,610,553}; // External plane normal to Y (+1)
Physical Surface(2003) = {558,646,1495,1407,615,501,1350,1464,225,136,916,1030,211,217,1061,973}; // External plane normal to Z (+1)
Physical Surface(2004) = {543,641,1490,1392,579,522,1371,1428,2277,2220,3088,3145,2241,2339,3207,3109}; // External plane normal to X (-1)
Physical Surface(2005) = {3119,3176,2742,2685,3150,3016,2582,2716,1433,1299,865,999,1402,1459,1025,968}; // External plane normal to Y (-1)
Physical Surface(2006) = {2256,2344,3212,3124,2313,2199,3067,3181,1879,1765,2633,2747,1822,1910,2778,2690}; // External plane normal to Z (-1)

//=================================================================================================
