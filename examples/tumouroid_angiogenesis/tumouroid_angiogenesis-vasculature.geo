//=================================================================================================
Geometry.Tolerance = 1.0e-8;
Mesh.ElementOrder = 1;

//=================================================================================================
// Geometrical parameters
RAD = 0.500e-3;
NFL = 1.5*RAD;
LEN = 12.0*RAD;
// Scale parameters
scale__1 = 0.65;
scale__2 = 0.65;

DIA = 2.0*LEN/16;

NS = 2; // coarse mesh
NS = 7; // fine mesh

//=================================================================================================
// Total domain
// // // Point(1) = {-LEN, -LEN, -LEN};
// // // Extrude{2*LEN, 0.0, 0.0} { Point{1}; }
// // // Extrude{0.0, 2*LEN, 0.0} { Line{1}; }
// // // Extrude{0.0, 0.0, 2*LEN} { Surface{5}; }
// // // Point(15) = {RAD, 0, 0};
// // // Extrude {{0, 0, 1}, {0, 0, 0}, +Pi/2} { Point{15}; }
// // // Extrude {{0, 0, 1}, {0, 0, 0}, +Pi/2} { Point{16}; }
// // // Extrude {{1, 0, 0}, {0, 0, 0}, +Pi/2} { Line{28,29}; }
// // // Extrude {{1, 0, 0}, {0, 0, 0}, +Pi/2} { Line{30,33}; }
// // // Extrude {{1, 0, 0}, {0, 0, 0}, +Pi/2} { Line{36,39}; }
// // // Extrude {{1, 0, 0}, {0, 0, 0}, +Pi/2} { Line{42,45}; }

//=================================================================================================
Point(31) = {0.60*LEN, 0.46*LEN, LEN- 0*DIA};
// Point(32) = {0.55*LEN, 0.45*LEN, LEN- 1*DIA};
Point(33) = {0.45*LEN, 0.49*LEN, LEN- 2*DIA};
Point(34) = {0.62*LEN, 0.57*LEN, LEN- 3*DIA};
Point(35) = {0.65*LEN, 0.47*LEN, LEN- 4*DIA};
Point(36) = {0.70*LEN, 0.69*LEN, LEN- 5*DIA};
Point(37) = {0.70*LEN, 0.69*LEN, LEN- 6*DIA};
Point(38) = {0.68*LEN, 0.60*LEN, LEN- 7*DIA};
Point(39) = {0.60*LEN, 0.55*LEN, LEN- 8*DIA};
Spline(50) = {31,33:39};
Point(40) = {0.55*LEN, 0.49*LEN, LEN- 9*DIA};
Point(41) = {0.59*LEN, 0.43*LEN, LEN-10*DIA};
Point(42) = {0.53*LEN, 0.41*LEN, LEN-11*DIA};
Point(43) = {0.58*LEN, 0.57*LEN, LEN-12*DIA};
Point(44) = {0.66*LEN, 0.61*LEN, LEN-13*DIA};
Point(45) = {0.59*LEN, 0.43*LEN, LEN-14*DIA};
// Point(46) = {0.59*LEN, 0.49*LEN, LEN-15*DIA};
Point(47) = {0.66*LEN, 0.58*LEN, LEN-16*DIA};
Spline(51) = {39:45,47};

Point(51) = {-0.78*LEN, -0.71*LEN, LEN-16*DIA};
Point(53) = {-0.65*LEN, -0.63*LEN, LEN-14*DIA};
Point(55) = {-0.65*LEN, -0.68*LEN, LEN-12*DIA};
Point(57) = {-0.71*LEN, -0.79*LEN, LEN-10*DIA};
Point(59) = {-0.58*LEN, -0.68*LEN, LEN- 8*DIA};
Spline(52) = {51:59:2};
Point(61) = {-0.59*LEN, -0.61*LEN, LEN- 6*DIA};
Point(63) = {-0.58*LEN, -0.67*LEN, LEN- 4*DIA};
Point(65) = {-0.61*LEN, -0.63*LEN, LEN- 2*DIA};
Point(67) = {-0.67*LEN, -0.78*LEN, LEN- 0*DIA};
Spline(53) = {59:67:2};

Point(70) = { 0.21*LEN, -0.51*LEN, LEN-16*DIA};
Point(71) = { 0.23*LEN, -0.41*LEN, LEN-15*DIA};
Point(72) = { 0.12*LEN, -0.39*LEN, LEN-14*DIA};
Point(73) = { 0.03*LEN, -0.22*LEN, LEN-13*DIA};
Point(74) = {-0.09*LEN, -0.37*LEN, LEN-12*DIA};
Point(75) = {-0.19*LEN, -0.47*LEN, LEN-11*DIA};
Point(76) = {-0.39*LEN, -0.37*LEN, LEN-10*DIA};
Spline(54) = {70:76:1,59};

Point(77) = { 0.19*LEN, -1.00*LEN, -0.53*LEN};
Point(78) = {-0.03*LEN, -0.89*LEN, -0.26*LEN};
Point(79) = { 0.22*LEN, -0.71*LEN, -0.04*LEN};
Point(80) = { 0.34*LEN, -0.39*LEN, +0.12*LEN};
Spline(55) = {77:80};

Point(93) = {-0.31*LEN, 0.52*LEN, -0.01*LEN};
Point(94) = {-0.31*LEN, 0.36*LEN, -0.21*LEN};
Point(95) = {-0.55*LEN, 0.22*LEN, -0.13*LEN};
Point(96) = {-0.78*LEN, 0.51*LEN, -0.04*LEN};
Point(97) = {-1.00*LEN, 0.52*LEN,  0.07*LEN};
Spline(57) = {93:97};

Point( 98) = {-0.32*LEN, 0.66*LEN, -0.01*LEN+1*DIA};
Point( 99) = {-0.51*LEN, 0.72*LEN, -0.01*LEN+3*DIA};
Point(100) = {-0.76*LEN, 0.85*LEN, -0.01*LEN+6*DIA};
Point(101) = {-0.48*LEN, 0.95*LEN, LEN};
Spline(58) = {93,98:101};

Point(81)  = { 0.23*LEN, -0.39*LEN, +0.30*LEN};
Point(82)  = { 0.41*LEN, -0.42*LEN, +0.82*LEN};
Point(102) = { 0.24*LEN, -0.52*LEN, +1.00*LEN};
Spline(59) = {80:82,102};

Point(103) = { 0.44*LEN, -0.39*LEN, +0.12*LEN};
Point(104) = { 0.86*LEN, -0.44*LEN, -0.21*LEN};
Point(105) = { 0.91*LEN, -0.51*LEN, -0.62*LEN};
Point(106) = { 1.00*LEN, -0.41*LEN, -1.00*LEN};
Spline(60) = {80,103:106};

Point(107) = { 0.58*LEN, 0.74*LEN,  0.02*LEN};
Point(108) = { 0.66*LEN, 1.00*LEN, -0.79*LEN};
Spline(61) = {39,107:108};

Point(109) = {-0.44*LEN, 0.48*LEN, -0.38*LEN};
Point(110) = {-0.43*LEN, 0.78*LEN, -0.58*LEN};
Point(111) = {-0.63*LEN, 1.00*LEN, -1.00*LEN};
Spline(62) = {93,109:111};



Point(121) = {-0.51*LEN, -0.21*LEN, LEN-16*DIA};
Point(122) = {-0.49*LEN, -0.18*LEN, LEN-15*DIA};
Point(123) = {-0.52*LEN, -0.13*LEN, LEN-14*DIA};
Point(124) = {-0.61*LEN, -0.23*LEN, LEN-13*DIA};
Point(125) = {-0.58*LEN, -0.18*LEN, LEN-12*DIA};
Point(126) = {-0.60*LEN, -0.21*LEN, LEN-11*DIA};
Point(127) = {-0.54*LEN, -0.19*LEN, LEN-10*DIA};
Point(128) = {-0.45*LEN, -0.22*LEN, LEN- 9*DIA};
Point(129) = {-0.39*LEN, -0.10*LEN, LEN- 8*DIA};
Point(130) = {-0.38*LEN, -0.13*LEN, LEN- 7*DIA};
Spline(63) = {121:130};
Point(131) = {-0.31*LEN, -0.11*LEN, LEN- 6*DIA};
Point(132) = {-0.30*LEN, -0.02*LEN, LEN- 5*DIA};
Point(133) = {-0.29*LEN, -0.17*LEN, LEN- 4*DIA};
Point(134) = {-0.36*LEN, -0.21*LEN, LEN- 3*DIA};
Point(135) = {-0.39*LEN, -0.33*LEN, LEN- 2*DIA};
Point(136) = {-0.32*LEN, -0.29*LEN, LEN- 1*DIA};
Point(137) = {-0.46*LEN, -0.38*LEN, LEN- 0*DIA};
Spline(64) = {130:137};
Point(138) = {-0.68*LEN, -0.13*LEN, LEN- 7*DIA};
Point(139) = {-0.88*LEN,  0.13*LEN, LEN- 9*DIA};
Point(140) = {-1.00*LEN,  0.02*LEN, LEN-16*DIA};
Spline(65) = {130,138:140};

Point(150) = {-0.62*LEN, 0.12*LEN, LEN-16*DIA};
Point(151) = {-0.16*LEN, 0.38*LEN, LEN-11*DIA};
Point(152) = {-0.09*LEN, 0.31*LEN, LEN- 6*DIA};
Point(153) = {-0.19*LEN, 0.27*LEN, LEN- 0*DIA};
Spline(66) = {150:153};

Point(154) = { 0.39*LEN, -0.02*LEN, LEN-16*DIA};
Point(155) = { 0.51*LEN,  0.21*LEN, LEN-14*DIA};
Point(156) = { 0.42*LEN,  0.32*LEN, LEN-11*DIA};
Point(157) = { 0.73*LEN,  0.29*LEN, LEN- 9*DIA};
Spline(67) = {154:157};
Point(158) = { 0.90*LEN,  0.29*LEN, LEN-12*DIA};
Point(159) = { 1.00*LEN,  0.35*LEN, LEN-16*DIA};
Spline(68) = {157:159};
Point(160) = { 0.69*LEN, 0.32*LEN, LEN-4*DIA};
Point(161) = { 1.00*LEN, 0.49*LEN, LEN-0*DIA};
Spline(120) = {157,160:161};

Point(162) = { 0.97*LEN, -0.42*LEN, LEN- 0*DIA};
Point(163) = { 0.72*LEN, -0.47*LEN, LEN- 4*DIA};
Point(164) = { 0.90*LEN, -0.69*LEN, LEN-12*DIA};
Point(165) = { 1.00*LEN, -0.84*LEN, LEN-14*DIA};
Spline(121) = {162:165};

Point(170) = { 0.09*LEN,  1.00*LEN, -0.44*LEN};
Point(171) = {-0.09*LEN,  0.82*LEN,  0.22*LEN};
Point(172) = { 0.21*LEN,  0.52*LEN,  0.22*LEN};
Point(173) = { 0.15*LEN,  0.28*LEN,  0.33*LEN};
Spline(69) = {170:173};
Point(174) = {-0.09*LEN,  0.09*LEN,  0.44*LEN};
Point(175) = { 0.09*LEN,  0.29*LEN,  0.74*LEN};
Point(176) = { 0.12*LEN,  0.07*LEN,  1.00*LEN};
Spline(56) = {173:176};
Point(177) = { 0.15*LEN,  0.28*LEN, -0.03*LEN};
Point(178) = { 0.45*LEN,  0.68*LEN, -0.23*LEN};
Point(179) = { 0.33*LEN,  1.00*LEN, -0.78*LEN};
Spline(70) = {173,177:179};

Point(181) = { 0.10*LEN, -0.87*LEN, LEN- 0*DIA};
Point(182) = {-0.05*LEN, -0.62*LEN, LEN- 2*DIA};
Point(183) = {-0.22*LEN, -0.57*LEN, LEN- 3*DIA};
Point(184) = {-0.07*LEN, -0.69*LEN, LEN- 5*DIA};
Point(185) = {-0.11*LEN, -0.56*LEN, LEN- 8*DIA};
Spline(71) = {181:185};
Point(186) = {-0.19*LEN, -0.83*LEN, LEN-10*DIA};
Point(187) = {-0.08*LEN, -0.27*LEN, LEN-16*DIA};
Spline(72) = {185:187};

Point(190) = {-0.01*LEN, -0.26*LEN, LEN- 9*DIA};
Point(191) = {+0.11*LEN, -0.50*LEN, LEN-11*DIA};
Point(192) = {+0.34*LEN, -0.61*LEN, LEN-10*DIA};
Point(193) = {+0.41*LEN, -1.00*LEN, LEN-13*DIA};
Spline(73) = {185,190:193};

Point(195) = { 0.09*LEN,  1.00*LEN, LEN- 2*DIA};
Point(196) = { 0.18*LEN,  0.77*LEN, LEN- 7*DIA};
Point(197) = { 0.22*LEN,  0.72*LEN, LEN- 9*DIA};
Spline(74) = {195:197};
Point(198) = { 0.27*LEN,  0.77*LEN, LEN-13*DIA};
Point(200) = { 0.29*LEN,  0.61*LEN, LEN-13*DIA};
Point(201) = { 0.25*LEN,  0.33*LEN, LEN-16*DIA};
Spline(75) = {197,198,200:201};
Point(259) = { 0.05*LEN,  0.73*LEN, LEN-14*DIA};
Point(260) = {-0.17*LEN,  0.81*LEN, LEN-16*DIA};
Spline(85) = {197,259:260};

Point(202) = {-0.77*LEN, +0.62*LEN, LEN- 0*DIA};
Point(203) = {-0.72*LEN, +0.29*LEN, LEN- 7*DIA};
Point(204) = {-0.79*LEN, +0.36*LEN, LEN-10*DIA};
Point(205) = {-0.68*LEN, -0.39*LEN, LEN-12*DIA};
Point(206) = {-1.00*LEN, -0.68*LEN, LEN-16*DIA};
Spline(76) = {202:206};

Point(210) = { 0.90*LEN, +0.05*LEN, LEN- 0*DIA};
Point(211) = { 0.81*LEN, +0.15*LEN, LEN- 5*DIA};
Point(212) = { 0.85*LEN, +0.15*LEN, LEN- 7*DIA};
Point(213) = { 0.65*LEN, +0.04*LEN, LEN-10*DIA};
Spline(77) = {210:213};
Point(214) = { 0.42*LEN, -0.22*LEN, LEN-13*DIA};
Point(215) = { 0.41*LEN, -0.22*LEN, LEN-16*DIA};
Spline(78) = {213:215};
Point(216) = { 0.86*LEN, +0.09*LEN, LEN-15*DIA};
Point(217) = { 0.92*LEN, -0.19*LEN, LEN-16*DIA};
Spline(79) = {213,216:217};

Point(220) = {-0.00*LEN, -0.10*LEN, LEN- 0*DIA};
Point(221) = {-0.15*LEN, -0.15*LEN, LEN- 1*DIA};
Point(222) = {-0.12*LEN, -0.12*LEN, LEN- 3*DIA};
Point(223) = {+0.08*LEN, -0.15*LEN, LEN- 4*DIA};
Point(224) = {-0.17*LEN, -0.07*LEN, LEN- 6*DIA};
Point(225) = {-0.21*LEN, -0.11*LEN, LEN- 9*DIA};
Point(226) = {-0.15*LEN, +0.15*LEN, LEN-11*DIA};
Point(227) = {-0.18*LEN, -0.08*LEN, LEN-12*DIA};
Point(228) = {-0.16*LEN, +0.04*LEN, LEN-14*DIA};
Point(229) = {-0.16*LEN, +0.16*LEN, LEN-16*DIA};
Spline(80) = {220:229};

Point(230) = {-0.80*LEN, -0.50*LEN, LEN- 0*DIA};
Point(231) = {-0.95*LEN, -0.55*LEN, LEN- 1*DIA};
Point(232) = {-0.82*LEN, -0.52*LEN, LEN- 3*DIA};
Point(233) = {-0.79*LEN, -0.55*LEN, LEN- 4*DIA};
Point(234) = {-0.93*LEN, -0.47*LEN, LEN- 6*DIA};
Point(235) = {-0.81*LEN, -0.51*LEN, LEN- 9*DIA};
Spline(81) = {230:235};
Point(236) = {-0.85*LEN, -0.48*LEN, LEN-10*DIA};
Point(237) = {-0.88*LEN, -0.55*LEN, LEN-12*DIA};
Point(238) = {-0.81*LEN, -0.44*LEN, LEN-14*DIA};
Point(239) = {-0.97*LEN, -0.36*LEN, LEN-16*DIA};
Spline(82) = {235:239};
Point(240) = {-0.85*LEN, -0.78*LEN, LEN-10*DIA};
Point(241) = {-0.92*LEN, -0.88*LEN, LEN-14*DIA};
Point(242) = {-0.78*LEN, -1.00*LEN, LEN-14*DIA};
Spline(83) = {235,240:242};

Point(251) = {+0.35*LEN, +0.15*LEN, LEN- 0*DIA};
Point(252) = {+0.42*LEN, +0.12*LEN, LEN- 3*DIA};
Point(253) = {+0.35*LEN, +0.15*LEN, LEN- 4*DIA};
Point(254) = {+0.17*LEN, +0.07*LEN, LEN- 6*DIA};
Point(255) = {+0.30*LEN, +0.11*LEN, LEN- 9*DIA};
Point(256) = {+0.15*LEN, +0.05*LEN, LEN-10*DIA};
Point(257) = {+0.18*LEN, +0.11*LEN, LEN-12*DIA};
Point(258) = {+0.16*LEN, -0.04*LEN, LEN-16*DIA};
Spline(84) = {251:258};




Point(261) = {+0.66*LEN, -0.77*LEN, LEN- 0*DIA};
Point(262) = {+0.55*LEN, -0.88*LEN, LEN- 2*DIA};
Point(263) = {+0.73*LEN, -0.61*LEN, LEN- 4*DIA};
Point(264) = {+0.73*LEN, -0.82*LEN, LEN- 8*DIA};
Point(265) = {+0.81*LEN, -0.86*LEN, LEN-10*DIA};
Spline(86) = {261:265};
Point(266) = {+0.91*LEN, -0.90*LEN, LEN-12*DIA};
Point(267) = {+1.00*LEN, -1.00*LEN, LEN-16*DIA};
Spline(87) = {265:267};
Point(268) = {+0.52*LEN, -0.89*LEN, LEN-14*DIA};
Point(269) = {+0.38*LEN, -1.00*LEN, LEN-16*DIA};
Spline(88) = {265,268:269};
Point(270) = {+0.77*LEN, -0.42*LEN, LEN-16*DIA};
Point(271) = {+0.68*LEN, -0.58*LEN, LEN-15*DIA};
Point(272) = {+0.46*LEN, -0.81*LEN, LEN-11*DIA};
Point(273) = {+0.40*LEN, -0.69*LEN, LEN-14*DIA};
Point(274) = {+0.33*LEN, -0.59*LEN, LEN-14*DIA};
Point(275) = {+0.07*LEN, -0.66*LEN, LEN-16*DIA};
Spline(89) = {270:272};
Spline(90) = {272:275};

Point(280) = {0.87*LEN, 0.80*LEN, LEN- 0*DIA};
Point(281) = {0.95*LEN, 0.95*LEN, LEN- 1*DIA};
Point(282) = {0.82*LEN, 0.82*LEN, LEN- 3*DIA};
Point(283) = {0.92*LEN, 0.95*LEN, LEN- 4*DIA};
Point(284) = {0.91*LEN, 0.97*LEN, LEN- 6*DIA};
Point(285) = {0.86*LEN, 0.81*LEN, LEN- 9*DIA};
Spline(93) = {280:285};
Point(286) = {0.85*LEN, 0.88*LEN, LEN-10*DIA};
Point(287) = {0.88*LEN, 0.85*LEN, LEN-12*DIA};
Point(288) = {0.81*LEN, 0.90*LEN, LEN-14*DIA};
Point(289) = {1.00*LEN, 1.00*LEN, LEN-16*DIA};
Spline(94) = {285:289};
Point(290) = {0.75*LEN, 0.68*LEN, LEN-10*DIA};
Point(291) = {0.92*LEN, 0.51*LEN, LEN-14*DIA};
Point(292) = {1.00*LEN, 0.65*LEN, LEN-13*DIA};
Spline(95) = {285,290:292};

Point(300) = {-0.57*LEN, 0.40*LEN, LEN- 0*DIA};
Point(301) = {-0.55*LEN, 0.45*LEN, LEN- 1*DIA};
Point(302) = {-0.52*LEN, 0.42*LEN, LEN- 3*DIA};
Point(303) = {-0.52*LEN, 0.25*LEN, LEN- 4*DIA};
Point(304) = {-0.51*LEN, 0.37*LEN, LEN- 6*DIA};
Point(305) = {-0.56*LEN, 0.41*LEN, LEN- 9*DIA};
Spline(96) = {300:305};
Point(306) = {-0.55*LEN, 0.64*LEN, LEN-10*DIA};
Point(307) = {-0.58*LEN, 0.69*LEN, LEN-12*DIA};
Point(308) = {-0.51*LEN, 0.60*LEN, LEN-14*DIA};
Point(309) = {-0.62*LEN, 0.76*LEN, LEN-16*DIA};
Spline(97) = {305:309};
Point(310) = {-0.55*LEN, 0.28*LEN, LEN-10*DIA};
Point(311) = {-0.61*LEN, 0.48*LEN, LEN-14*DIA};
Point(312) = {-0.90*LEN, 0.58*LEN, LEN-16*DIA};
Spline(98) = {305,310:312};

Point(320) = {-0.47*LEN, -0.00*LEN, LEN- 0*DIA};
Point(321) = {-0.45*LEN, -0.05*LEN, LEN- 1*DIA};
Point(322) = {-0.42*LEN, -0.02*LEN, LEN- 3*DIA};
Point(323) = {-0.42*LEN,  0.05*LEN, LEN- 4*DIA};
Point(324) = {-0.41*LEN,  0.07*LEN, LEN- 6*DIA};
Point(325) = {-0.46*LEN,  0.10*LEN, LEN- 9*DIA};
Spline(99) = {320:325};
Point(326) = {-0.35*LEN,  0.14*LEN, LEN-10*DIA};
Point(327) = {-0.48*LEN,  0.09*LEN, LEN-12*DIA};
Point(328) = {-0.31*LEN,  0.10*LEN, LEN-14*DIA};
Point(329) = {-0.32*LEN,  0.13*LEN, LEN-16*DIA};
Spline(100) = {325:329};
Point(330) = {-0.55*LEN,-0.02*LEN, LEN-12*DIA};
Point(331) = {-0.71*LEN, 0.07*LEN, LEN-14*DIA};
Point(332) = {-0.88*LEN,-0.12*LEN, LEN-16*DIA};
Spline(101) = {325,330:332};

Point(333) = {-0.07*LEN,  0.50*LEN, LEN- 0*DIA};
Point(334) = {-0.05*LEN,  0.55*LEN, LEN- 1*DIA};
Point(335) = {-0.02*LEN,  0.52*LEN, LEN- 3*DIA};
Point(336) = {-0.02*LEN,  0.55*LEN, LEN- 4*DIA};
Point(337) = {-0.01*LEN,  0.57*LEN, LEN- 6*DIA};
Point(338) = {-0.06*LEN,  0.50*LEN, LEN- 9*DIA};
Point(339) = {-0.05*LEN,  0.54*LEN, LEN-10*DIA};
Point(340) = {-0.08*LEN,  0.59*LEN, LEN-12*DIA};
Point(341) = {-0.01*LEN,  0.50*LEN, LEN-14*DIA};
Point(342) = {-0.02*LEN,  0.53*LEN, LEN-16*DIA};
Point(343) = { 0.05*LEN,  0.32*LEN, LEN-12*DIA};
Point(344) = { 0.11*LEN,  0.22*LEN, LEN-13*DIA};
Point(345) = { 0.01*LEN,  0.32*LEN, LEN-16*DIA};
Spline(102) = {333:337};
Spline(103) = {337:342};
Spline(104) = {337,343:345};


Point(346) = {-0.17*LEN, -0.78*LEN, LEN- 0*DIA};
Point(347) = {-0.35*LEN, -0.75*LEN, LEN- 1*DIA};
Point(348) = {-0.32*LEN, -0.71*LEN, LEN- 3*DIA};
Point(349) = {-0.32*LEN, -0.85*LEN, LEN- 4*DIA};
Point(350) = {-0.31*LEN, -0.77*LEN, LEN- 6*DIA};
Point(351) = {-0.36*LEN, -0.76*LEN, LEN- 9*DIA};
Point(352) = {-0.35*LEN, -0.74*LEN, LEN-10*DIA};
Point(353) = {-0.38*LEN, -0.81*LEN, LEN-12*DIA};
Point(354) = {-0.31*LEN, -0.97*LEN, LEN-14*DIA};
Point(355) = {-0.32*LEN, -1.00*LEN, LEN-16*DIA};
Point(356) = {-0.15*LEN, -0.79*LEN, LEN-14*DIA};
Point(357) = {-0.01*LEN, -0.92*LEN, LEN-14*DIA};
Point(358) = { 0.13*LEN, -0.92*LEN, LEN-16*DIA};
Spline(105) = {346:352};
Spline(106) = {352:355};
Spline(107) = {352,356:358};

Point(360) = {0.77*LEN, -0.08*LEN, LEN- 0*DIA};
Point(361) = {0.72*LEN, -0.11*LEN, LEN- 3*DIA};
Point(362) = {0.71*LEN, -0.17*LEN, LEN- 6*DIA};
Point(363) = {0.85*LEN, -0.14*LEN, LEN-10*DIA};
Point(364) = {0.72*LEN, -0.02*LEN, LEN-16*DIA};
Spline(108) = {360:364};
Point(365) = {-0.83*LEN, -0.08*LEN, LEN- 0*DIA};
Point(366) = {-0.62*LEN, -0.41*LEN, LEN- 3*DIA};
Point(367) = {-0.51*LEN, -0.45*LEN, LEN- 6*DIA};
Point(368) = {-0.65*LEN, -0.44*LEN, LEN-10*DIA};
Point(369) = {-0.46*LEN, -0.42*LEN, LEN-16*DIA};
Spline(109) = {365:369};
Point(370) = {-0.97*LEN, 0.98*LEN, LEN- 0*DIA};
Point(371) = {-0.99*LEN, 0.91*LEN, LEN- 3*DIA};
Point(372) = {-0.99*LEN, 0.77*LEN, LEN- 6*DIA};
Point(373) = {-0.95*LEN, 0.94*LEN, LEN-10*DIA};
Point(374) = {-0.90*LEN, 0.92*LEN, LEN-16*DIA};
Spline(110) = {370:374};
Point(375) = {0.57*LEN, -0.58*LEN, LEN- 0*DIA};
Point(376) = {0.59*LEN, -0.51*LEN, LEN- 3*DIA};
Point(377) = {0.59*LEN, -0.57*LEN, LEN- 6*DIA};
Point(378) = {0.55*LEN, -0.54*LEN, LEN-10*DIA};
Point(379) = {0.50*LEN, -0.52*LEN, LEN-16*DIA};
Spline(111) = {375:379};
Point(380) = {-0.85*LEN, 0.32*LEN, LEN- 0*DIA};
Point(381) = {-0.95*LEN, 0.38*LEN, LEN- 3*DIA};
Point(382) = {-0.81*LEN, 0.39*LEN, LEN- 6*DIA};
Point(383) = {-0.90*LEN, 0.31*LEN, LEN-10*DIA};
Point(384) = {-0.84*LEN, 0.30*LEN, LEN-16*DIA};
Spline(112) = {380:384};

Point(385) = {+0.31*LEN, -0.88*LEN, LEN- 8*DIA};
Point(386) = {+0.46*LEN, -0.78*LEN, LEN- 6*DIA};
Point(387) = {+0.29*LEN, -0.89*LEN, LEN- 4*DIA};
Point(388) = {+0.25*LEN, -0.91*LEN, LEN- 1*DIA};
Point(389) = {+0.38*LEN, -0.81*LEN, LEN- 0*DIA};
Spline(113) = {272,385:389};


Point(390) = {-0.26*LEN, -0.58*LEN, LEN-16*DIA};
Point(391) = {-0.26*LEN, -0.43*LEN, LEN-15*DIA};
Point(392) = {-0.19*LEN, -0.23*LEN, LEN-11*DIA};
Point(393) = {-0.11*LEN, -0.23*LEN, LEN-14*DIA};
Point(394) = {-0.02*LEN,  0.01*LEN, LEN-16*DIA};
Spline(91) = {390:392};
Spline(92) = {392:394};
Point(395) = {-0.19*LEN, -0.28*LEN, LEN- 9*DIA};
Point(396) = {-0.19*LEN, -0.28*LEN, LEN- 7*DIA};
Point(397) = {-0.19*LEN, -0.28*LEN, LEN- 5*DIA};
Point(398) = {-0.01*LEN, -0.38*LEN, LEN- 0*DIA};
Spline(114) = {392,395:398};

Point(400) = {0.67*LEN, -0.38*LEN, LEN- 0*DIA};
Point(401) = {0.51*LEN, -0.21*LEN, LEN- 3*DIA};
Point(402) = {0.69*LEN, -0.37*LEN, LEN- 6*DIA};
Point(403) = {0.60*LEN, -0.27*LEN, LEN-10*DIA};
Point(404) = {0.60*LEN, -0.32*LEN, LEN-16*DIA};
Spline(115) = {400:404};
Point(405) = {0.41*LEN, -0.18*LEN, LEN- 0*DIA};
Point(406) = {0.29*LEN, -0.11*LEN, LEN- 3*DIA};
Point(407) = {0.21*LEN, -0.17*LEN, LEN- 6*DIA};
Point(408) = {0.25*LEN, -0.14*LEN, LEN-10*DIA};
Point(409) = {0.16*LEN, -0.23*LEN, LEN-16*DIA};
Spline(116) = {405:409};

Point(52) = {-0.55*LEN, -0.78*LEN, LEN-16*DIA};
Point(54) = {-0.62*LEN, -0.83*LEN, LEN-13*DIA};
Point(56) = {-0.72*LEN, -0.91*LEN, LEN-11*DIA};
Point(58) = {-0.68*LEN, -0.92*LEN, LEN- 9*DIA};
Point(60) = {-0.55*LEN, -0.83*LEN, LEN- 7*DIA};
Point(62) = {-0.56*LEN, -0.89*LEN, LEN- 5*DIA};
Point(64) = {-0.66*LEN, -0.95*LEN, LEN- 3*DIA};
Point(66) = {-0.48*LEN, -0.99*LEN, LEN- 0*DIA};
Spline(117) = {52:66:2};

Point(410) = {-0.31*LEN, 0.78*LEN, LEN- 0*DIA};
Point(411) = {-0.27*LEN, 0.89*LEN, LEN- 3*DIA};
Point(412) = {-0.19*LEN, 0.69*LEN, LEN- 8*DIA};
Point(413) = {-0.28*LEN, 0.78*LEN, LEN-14*DIA};
Point(414) = {-0.31*LEN, 0.98*LEN, LEN-16*DIA};
Spline(118) = {410:414};
Point(415) = {0.87*LEN, -1.00*LEN, LEN- 0*DIA};
Point(416) = {0.59*LEN, -0.91*LEN, LEN- 3*DIA};
Point(417) = {0.59*LEN, -0.89*LEN, LEN- 6*DIA};
Point(418) = {0.55*LEN, -0.94*LEN, LEN-10*DIA};
Point(419) = {0.76*LEN, -1.00*LEN, LEN-16*DIA};
Spline(119) = {415:419};

Point(420) = {0.57*LEN, 0.98*LEN, LEN- 0*DIA};
Point(421) = {0.49*LEN, 0.81*LEN, LEN- 3*DIA};
Point(422) = {0.39*LEN, 0.87*LEN, LEN- 6*DIA};
Point(423) = {0.45*LEN, 0.94*LEN, LEN-14*DIA};
Point(424) = {0.50*LEN, 1.00*LEN, LEN-16*DIA};
Spline(122) = {420:424};

// NOTE that lines with IDs: 120-121 are reserved already

Transfinite Line "*" = 2;
Transfinite Line {50} = 11*NS;
Transfinite Line {51} = 7*NS;
Transfinite Line {52} = 8*NS;
Transfinite Line {53} = 8*NS;
Transfinite Line {54} = 10*NS;
Transfinite Line {55} = 9*NS;
Transfinite Line {56,57} = 8*NS;
Transfinite Line {58} = 8*NS;
Transfinite Line {59} = 7*NS;
Transfinite Line {60} = 10*NS;
Transfinite Line {61} = 6*NS;
Transfinite Line {62} = 8*NS;
Transfinite Line {63} = 8*NS;
Transfinite Line {64} = 7*NS;
Transfinite Line {65} = 10*NS;
Transfinite Line {66} = 12*NS;
Transfinite Line {67} = 7*NS;
Transfinite Line {68} = 7*NS;
Transfinite Line {69} = 11*NS;
Transfinite Line {70} = 11*NS;
Transfinite Line {71} = 11*NS;
Transfinite Line {72} = 7*NS;
Transfinite Line {73} = 13*NS;
Transfinite Line {74} = 9*NS;
Transfinite Line {75} = 7*NS;
Transfinite Line {76} = 18*NS;
Transfinite Line {77} = 8*NS;
Transfinite Line {78,79} = 6*NS;
Transfinite Line {80} = 16*NS;
Transfinite Line {81} = 9*NS;
Transfinite Line {82,83} = 5*NS;
Transfinite Line {84} = 16*NS;
Transfinite Line {85} = 6*NS;

Transfinite Line {86} = 11*NS;
Transfinite Line {87} = 5*NS;
Transfinite Line {88} = 6*NS;
Transfinite Line {89} = 4*NS;
Transfinite Line {90} = 7*NS;

Transfinite Line {91,92} = 4*NS;
Transfinite Line {114}   = 11*NS;

Transfinite Line {93,96,99} = 9*NS;
Transfinite Line {94,97} = 5*NS;
Transfinite Line {95} = 7*NS;
Transfinite Line {98} = 10*NS;
Transfinite Line {100} = 6*NS;
Transfinite Line {101} = 6*NS;

Transfinite Line {102} = 6*NS;
Transfinite Line {103} = 9*NS;
Transfinite Line {104} = 9*NS;

Transfinite Line {105} = 11*NS;
Transfinite Line {106} = 6*NS;
Transfinite Line {107} = 6*NS;
Transfinite Line {108} = 12*NS;
Transfinite Line {109} = 15*NS;
Transfinite Line {110} = 13*NS;
Transfinite Line {111} = 13*NS;
Transfinite Line {112} = 15*NS;
Transfinite Line {113} = 9*NS;
Transfinite Line {114} = 11*NS;
Transfinite Line {115} = 12*NS;
Transfinite Line {116} = 13*NS;
Transfinite Line {117} = 14*NS;
Transfinite Line {118} = 14*NS;
Transfinite Line {119} = 17*NS;

Transfinite Line {120} = 7*NS;
Transfinite Line {121} = 11*NS;

Transfinite Line {122} = 16*NS;

//=================================================================================================
// parent capillary vessels = {50:116,120:121};
Physical Line(1001) = {98,97,96,82,83,81,65,63,64,107,106,105,52,53,54,101,99,100,72,71,73,59,55,60,78,79,77,68,67,120,51,61,50,95,93,94,86,88,87,89,113,90,69,70,56,75,74,85,103,104,102,62,58,57,114,91,92};
Physical Line(1002) = {66,76,80,84,108,109,110,111,112,115,116,117,118,119,121,122};

// Physical Point(101) = {66, 67,    137,     102,    176,     31,     101,    153, 161,     162, 181,     398,     195,     202, 210,     220, 230,     251, 261,     389,     280,     300,     320,     333,     346,     360, 365, 370, 375, 380, 400, 405, 410, 415, 420};
// Physical Point(102) = {52, 51,70, 121,140, 77,106, 170,179, 47,108, 97,111, 150, 154,159, 165, 187,193, 390,394, 201,260, 206, 215,217, 229, 239,242, 258, 267,269, 270,275, 289,292, 309,312, 329,332, 342,345, 355,358, 364, 369, 374, 379, 384, 404, 409, 414, 419, 424};

// parent capillary vessels: inlets/outlets
Physical Point(101) = {67,    137,     102,    176,     31,     101,    161,     181,     398,     195,     210,     230,     261,     389,     280,     300,     320,     333,     346,      52, 150, 165, 206, 229, 258, 364, 369, 374, 379, 384, 404, 409, 414, 419, 424};
Physical Point(102) = {51,70, 121,140, 77,106, 170,179, 47,108, 97,111, 154,159, 187,193, 390,394, 201,260, 215,217, 239,242, 267,269, 270,275, 289,292, 309,312, 329,332, 342,345, 355,358,  66, 153, 162, 202, 220, 251, 360, 365, 370, 375, 380, 400, 405, 410, 415, 420};

//=================================================================================================

