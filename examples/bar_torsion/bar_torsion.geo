//=================================================================================================
Geometry.Tolerance = 1.0e-6;
Mesh.ElementOrder = 1;
Mesh.Algorithm = 1;
NX = 4;
NY = 4;
NZ = 20;

//=================================================================================================
Point(1)  = {-(NX*1.0e-3)/2.0, -(NY*1.0e-3)/2.0, 0.0};
Extrude {NX*1.0e-3, 0, 0} { Point{1};   Layers{1+NX/2}; Recombine; }
Extrude {0, NY*1.0e-3, 0} { Line{1};    Layers{1+NY/2}; Recombine; }
Extrude {0, 0, NZ*1.0e-3} { Surface{5}; Layers{1+NZ/3}; Recombine; }

//=================================================================================================
Physical Volume(301) = {1};

Physical Surface(201) = {14,18,22,26}; // traction-free surfaces
Physical Surface(202) = {5}; // fixed base
Physical Surface(203) = {27}; // top-surface

//=================================================================================================
