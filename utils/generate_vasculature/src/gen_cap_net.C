/**************************************************************************************************
Filename:    gen_cap_net.C
Description: Generate a random vascular network in Gmsh file format.
Author:      Peter Wijeratne (p.wijeratne@ucl.ac.uk)
**************************************************************************************************/
#include <iostream>
#include <cmath>
#include <random>
#include <sstream>
#include <fstream>
#include <map>

void _gen_cap_net(const double& L, const double& R, const unsigned int& N, const bool& exclude, const bool& mix);

int main(void)
{
  //////////////////////////////////////
  // USER INPUT BEGIN
  /////
  // cube dimensions / total length!
  const double L = 11.9e-3;
  // tumour radius
  const double R = 0.5e-3;
  // number of seeds per side
  // const unsigned int N = 8;
  // const unsigned int N = 11;
  const unsigned int N = 21;
  // set to true if you want to exclude the tumour
  const bool exclude = true;
  // set to true if you want a uniform mix of inlets and outlets on each surface
  // else it will set all nodes on one surface as inlets and the other as outlets
  const bool mix = true;
  /////
  // USER INPUT END
  /////////////////////////////////////

  // generate and write capillary network
  _gen_cap_net(L, R, N, exclude, mix);

  return 0;
}

void _gen_cap_net(const double& L, const double& R, const unsigned int& N, const bool& exclude, const bool& mix)
{

  // write data to Gmsh .geo file format
  std::stringstream filename;
  filename << "cap_net.geo" << std::flush;
  std::ofstream omsh(filename.str().c_str());
  //
  omsh << "Geometry.Tolerance = 1.0e-8;" << std::endl;
  omsh << "Mesh.ElementOrder = 1;" << std::endl;
  //
  // output a list of parameters: radius of tumour, length of domain, nodal multiplier
  omsh << "RAD = " << R << ";" << std::endl;
  omsh << "LEN = " << L << ";" << std::endl;
  omsh << "NS = 2;" << std::endl;

  // first determine seed point grid
  const double lx = L/N, ly = L/N;

  // spline index
  unsigned int count = 0;

  // keep track of the bottom and top Point IDs
  std::vector<std::string> bot_ids;
  std::vector<std::string> top_ids;

  // keep track of the spline number and number of points / spline
  std::map<int, int > ind_map;

  // loop over grid elements
  for(unsigned int i=0; i<N; i++){
    for(unsigned int j=0; j<N; j++){

      double xmin = -L/2.0 + i*lx;
      double xmax = -L/2.0 + (i+1)*lx;
      double ymin = -L/2.0 + j*ly;
      double ymax = -L/2.0 + (j+1)*ly;

      if(exclude){
        // don't generate a capillary if this element is entirely enclosed by a square around the tumour
        if(xmin > -R && xmax < R && ymin > -R && ymax < R)
          continue;
      }

      count++;

      // initialise a random number generator
      std::random_device rd;
      std::default_random_engine rgen(rd());
      // initialise uniform distribution for the x-coordinate
      std::uniform_real_distribution<double> udist_x(xmin, xmax);
      // initialise uniform distribution for the y-coordinate
      std::uniform_real_distribution<double> udist_y(ymin, ymax);
      // initialise uniform distribution for the z-coordinate
      std::uniform_real_distribution<double> udist_z(0.0, L);

      // seed point coordinates
      double x0 = udist_x(rgen);
      double y0 = udist_y(rgen);
      double z0 = -L/2.0;

      omsh << "Point(" << count << "00) = {" << x0 << ", " << y0 << ", " << z0 << "};" << std::endl;

      // save the bottom Point ID
      std::stringstream bot_str;
      bot_str << count << "00";
      bot_ids.push_back(bot_str.str().c_str());

      // now generate another point along the z-axis
      const unsigned int N_z = 10;
      // keep moving up the z-axis until we hit the boundary
      bool propagate = true;
      const double tol = 1E-6;
      unsigned int k = 0;

      double xk = x0, yk = y0, zk = z0;

      while(propagate){

	k++;

	// move within a defined radius of the last point
	const double rad = 1.0;
	// generate a new displacement
	double ux = 0.0, uy = 0.0, uz = 0.0;

	bool reroll = true;

	while(reroll){
	  ux = udist_x(rgen)*rad;
	  uy = udist_y(rgen)*rad;
	  uz = udist_z(rgen)*0.5;

	  double xtemp = xk + ux;
	  double ytemp = yk + uy;

	  // if both x and y coordinates are within the boundary, we can exit the loop
	  if( (std::fabs(xtemp - L/2.0) > tol && std::fabs(ytemp - L/2.0) > tol) )
	    reroll = false;

	  // however, if we're too close to the last point in z then reroll; this prevents kinked and out-of-bounds splines
	  if( uz / L < 0.1)
	    reroll = true;

	  if(exclude){
	    // if either x-y coordinate is now within a square of half length R about the origin, i.e. too close to the tumour, reroll
	    if( std::fabs(xtemp) < R && std::fabs(ytemp) < R)
	      reroll = true;
	  }

	}

	xk = ux;
	yk = uy;
	zk += uz;

	// stop propagating along the z-axis when the vessel approaches the boundary
	if(zk > 0.8*L/2.0){
	  zk = L/2.0;
	  propagate = false;
	}

	// write new coordinates
	omsh << "Point(" << count << "0" << k << ") = {" << xk << ", " << yk << ", " << zk << "};" << std::endl;

	// save last point's ID
	if(!propagate){
	  std::stringstream top_str;
	  top_str << count << "0" << k;
	  top_ids.push_back(top_str.str().c_str());
	}

      }

      // write a spline with these points
      omsh << "Spline(" << count << "00) = {" << count << 0 << 0 << ":" << count << 0 << k << "};" << std::endl;

      ind_map.insert(std::make_pair(count, k+1));

    }
  }

  omsh << "Transfinite Line \"*\" = 2;" << std::endl;

  // write a transfinite line with each spline
  for(std::map<int, int>::iterator it = ind_map.begin(); it!= ind_map.end(); it++)
    omsh << "Transfinite Line {" << it->first << "00} = " << it->second << "*NS;" << std::endl;

  // set physical numbers for the inlet and outlet vessels
  // for now split equally... could also do it randomly
  unsigned int N_inl = (int)ind_map.size()/2;
  std::map<int, int>::iterator it = ind_map.begin();
  unsigned int ind = 0;
  // inlets...
  omsh << "Physical Line(1001) = {";
  for(it; it!= ind_map.end(); it++){
    ind++;
    //
    omsh << it->first << "00";
    if(ind > N_inl){
      it++;
      break;
    }
    omsh << ",";
  }
  omsh << "};" << std::endl;
  // outlets...
  omsh << "Physical Line(1002) = {";
  for(it; it!= ind_map.end(); it++){
    omsh << it->first << "00";
    // some insane acrobatics to determine if we're at the last entry...
    std::map<int, int>::iterator dum_it = it;
    dum_it++;
    if(dum_it == ind_map.end())
      break;
    omsh << ",";
  }
  omsh << "};" << std::endl;

  // check that we've counted the number of inlets and outlets correctly
  if( bot_ids.size() != top_ids.size() )
    std::cout << "Odd number of inlets and outlets: " << (bot_ids.size() + top_ids.size()) << ". If there are no bifurcations, this shouldn't happen..." << std::endl;

  // set physical numbers for the inlet and outlet points

  if(mix){
    // use a random generator to get a mix of inlets and outlets on each surface
    std::random_device rd;
    std::default_random_engine rgen(rd());
    std::uniform_real_distribution<double> udist(0.0, 1.0);
    // sort the inlets and outlets into vectors
    std::vector<std::string> inl_ids;
    std::vector<std::string> out_ids;
    for(unsigned int i=0; i<bot_ids.size(); i++){
      if(udist(rgen) > 0.5){
	inl_ids.push_back(bot_ids[i]);
	out_ids.push_back(top_ids[i]);
      }
      else{
	inl_ids.push_back(top_ids[i]);
	out_ids.push_back(bot_ids[i]);
      }
    }
    // now print the inlets...
    omsh << "Physical Point(101) = {";
    for(unsigned int i=0; i<inl_ids.size(); i++){

      omsh << inl_ids[i];

      if(i+1 == inl_ids.size())
        break;

      omsh << ",";
    }
    omsh << "};" << std::endl;

    // outlets...
    omsh << "Physical Point(102) = {";
    for(unsigned int i=0; i<out_ids.size(); i++){

    omsh << out_ids[i];

    if(i+1 == out_ids.size())
      break;

    omsh << ",";
    }
    omsh << "};" << std::endl;

  }
  else{
    // set all points on one surface as inlets and the other as outlets
    // inlets...
    omsh << "Physical Point(101) = {";
    for(unsigned int i=0; i<bot_ids.size(); i++){

      omsh << bot_ids[i];

      if(i+1 == bot_ids.size())
        break;

      omsh << ",";
    }
    omsh << "};" << std::endl;

    // outlets...
    omsh << "Physical Point(102) = {";
    for(unsigned int i=0; i<top_ids.size(); i++){

    omsh << top_ids[i];

    if(i+1 == top_ids.size())
      break;

    omsh << ",";
    }
    omsh << "};" << std::endl;
  }

  // close readout
  omsh.close();
}
