Vasilis list:
=============

* The sparsity function for the IGA_Context does not work properly when multiple patches are inserted.

* Perform more validation tests for the mixed (u/p) formulation of the IGA solid mechanics solver.

* Edit the 'ParaviewIO::write_quadrature' routine in lines 524-531 and delete from the string the full path of the output file.

* Mind to enhance/change the way that the input reads BC-type for different types of solvers (biomech/biochem).

* Clean or/and reduce the code by doing the following things:
    a. remove the intermediate_solution systems in the solid mechanics (FEM/IGA) solvers; application-wise it is obsolete.
    b. remove from the wound-healing solver the wound-stitching case; no longer functioning.
    c. create a branch version of FEB3 and remove from the master the membrane elements; significantly reduce computations.

* In the tumour angiogenesis solver, should I just split the auxiliary solution into more systems; will this improve computational efficiency?

* Replace the (node & element) IDs in all containers for class: 'DomainData_SolidCoupledWithVascularNetwork' into pointer to Node or Elem respectively.
  For now you can skip doing this task for: 'NodalForceInfo', 'RegionInfo' and 'DomainData' since the 3D mesh is not expected to change in the current modelling framework.
  Likewise for classes: 'VascularInfo' and 'DomainData_VascularNetwork' because the 1D mesh is in serial and local mode.

* I need to add sub-iterations in the implicit scheme of the reaction-diffusion solver.
