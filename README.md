README
======

Welcome to FEB3 (pronounced Phoebe), a C++ Finite Element and Isogeometric Analysis software for modelling 3D biomechanical problems. FEB3 is designed as a modular framework, permitting the user to plug in their own constitutive relations and boundary conditions with ease. It incorporates both forward and inverse finite deformation analysis methods, and implements both implicit and explicit Total Lagrangian solvers. 

FEB3 is capable of simulating static and dynamic solid mechanics, parabolic equations, multiphase materials, and mechanics at multiple length and time scales. Currently it features the following analyses:

* Nonlinear elasticity
  *  Compressible solids
  *  Nearly-incompressible solids
  *  Isotropic, transversely isotropic & orthotropic elasticity
  *  Continuum scale fibre reinforced materials
* Multiphase mechanics
  *  Biphasic and poroelastic deformation and fluid flow
* Growth mechanics
  *  Decomposition of deformation gradient into elastic and plastic components
* Coupled micro-macro nonlinear solid mechanics
  * Micro scale incompressible fibre networks
* Reaction-convection-diffusion dynamics

Developed biomedical applications are:

* Tumour growth
* Angiogenesis
* Wound healing

These applications feature coupled multiphysics solvers, permitting a physiological description of the chosen process with user-defined constitutive relations, material properties and boundary conditions.

FEB3 is an open-source software intended to be used for academic or research purposes. Its core is founded on various open-source libraries (e.g. libMesh, PETSc, GSL, MPICH). Compilation and utilisation of FEB3 necessitates installation of the open-source libraries listed below, which are freely available online.


Installation steps
------------------

* Libraries to install: [MPICH](https://www.mpich.org/), [GSL](http://www.gnu.org/software/gsl/), [blitz](http://sourceforge.net/projects/blitz/), [FLANN](http://www.cs.ubc.ca/research/flann/), [PETSc](www.mcs.anl.gov/petsc/) and [libMesh](http://libmesh.github.io/). Guidelines on how to install all these libraries are provided below.
* Assume also `$LIP` the local installation path of all libraries.
* Please do consider a configuration check before compiling, i.e. `./configure --help 1>configure_help.out`, and confirm you have the correct configuration setup.
* Note that PETSc will install a copy of the up-to-date version of MPICH locally.

### GSL

    ./configure --disable-shared \
                --prefix=$LIP/gsl-2.1
    make
    make install

### blitz++

    ./configure --disable-shared --enable-64bit --enable-optimize \
                --prefix=$LIP/blitz-0.10
    make lib
    make check-testsuite
    make check-examples
    make install
    make install-pdf

### PETSc

This is an example of the configuration setup and "make" needed to compile/install/test the library.
   
    ./configure COPTFLAGS=-O3 CXXOPTFLAGS=-O3 FOPTFLAGS=-O3 \
                --disable-shared --with-debugging=0 \
                --with-fblaslapack=1 --download-f2cblaslapack=yes \
                --with-metis=1 --download-metis=yes \
                --with-parmetis=1 --download-parmetis=yes \
                --with-superlu=1 --download-superlu=yes \
                --with-superlu_dist=1 --download-superlu_dist=yes \
                --with-mpi=1 --download-mpich=yes \
                --prefix=$LIP/petsc-3.7.2
    make PETSC_ARCH=arch-linux2-c-opt all
    make PETSC_ARCH=arch-linux2-c-opt install
    make PETSC_DIR=$LIP/petsc-3.7.2 test
    make PETSC_DIR=$LIP/petsc-3.7.2 streams

### libMesh

    ./configure PETSC_DIR=$LIP/petsc-3.7.2 PETSC_ARCH=arch-linux2-c-opt \
                --enable-everything --disable-shared --with-methods="opt dbg" \
                --disable-1D-only --disable-2D-only --disable-periodic --disable-complex \
                --disable-openmp --disable-eigen --disable-ifem --disable-slepc --disable-trilinos \
                --enable-netcdf --enable-netcdf-4 --enable-exodus \
                --disable-vtk --disable-tecio --disable-tecplot \
                --disable-tbb --disable-pthreads --disable-cppthreads --disable-qhull \
                --enable-metis --with-metis=PETSc \
                --prefix=$LIP/libmesh-1.1.0
    make
    make install

Note that this installation assumes that libMesh compilation and execution of FEB3 calls the PETSc-internally installed MPICH version.

Testing
------------------