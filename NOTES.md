Some Useful Notes
=================

Execution related notes
-----------------------

Use the following command to run **Phoebe** remotely-safe. Make sure that path and filename of the executable is correct, as well as the output-streams used by *nohup*.

    nohup mpiexec -n  [INTEGER]  ./feb3-opt.exe -i  [STRING]  >feb3-opt.out  2>feb3-opt.err  </dev/null  &

Paraview related notes
----------------------

Adopt the follow strings when using Paraview's calculator filter to post-process various variables. Confirm that labels used for each solver (see input file) agree with the ones used below.

    1. for the 3D (solid) domain:

        displacement vector:

            NC3D__u0*iHat+NC3D__u1*jHat+NC3D__u2*kHat

        mean solid stress or hydrostatic pressure (positive is compressive):

            (-(NC3D__S00+NC3D__S11+NC3D__S22)/3)

        deformation gradient determinant: NC3D__J

            NC3D__F00*(NC3D__F11*NC3D__F22-NC3D__F12*NC3D__F21)-NC3D__F01*(NC3D__F10*NC3D__F22-NC3D__F12*NC3D__F20)+NC3D__F02*(NC3D__F10*NC3D__F21-NC3D__F11*NC3D__F20)

        principal-stress vectors: NC3D__S0_v, NC3D__1_v and NC3D__S2_v

            NC3D__S0_v0*iHat+NC3D__S0_v1*jHat+NC3D__S0_v2*kHat

            NC3D__S1_v0*iHat+NC3D__S1_v1*jHat+NC3D__S1_v2*kHat

            NC3D__S2_v0*iHat+NC3D__S2_v1*jHat+NC3D__S2_v2*kHat

        Von Mises stress:

            sqrt(0.5*(mag(NC3D__S0_v)-mag(NC3D__S1_v))^2+0.5*(mag(NC3D__S1_v)-mag(NC3D__S2_v))^2+0.5*(mag(NC3D__S0_v)-mag(NC3D__S2_v))^2)

        Cauchy stress (only for forward analysis): NC3D__Sc00, NC3D__Sc01, NC3D__Sc02, NC3D__Sc11, NC3D__Sc12 and NC3D__Sc22, also you need to compute the deformation gradient determinant: NC3D__J

            (NC3D__F00*(NC3D__F00*NC3D__S00+NC3D__F01*NC3D__S01+NC3D__F02*NC3D__S02)+NC3D__F01*(NC3D__F00*NC3D__S01+NC3D__F01*NC3D__S11+NC3D__F02*NC3D__S12)+NC3D__F02*(NC3D__F00*NC3D__S02+NC3D__F01*NC3D__S12+NC3D__F02*NC3D__S22))/NC3D__J

            (NC3D__F10*(NC3D__F00*NC3D__S00+NC3D__F01*NC3D__S01+NC3D__F02*NC3D__S02)+NC3D__F11*(NC3D__F00*NC3D__S01+NC3D__F01*NC3D__S11+NC3D__F02*NC3D__S12)+NC3D__F12*(NC3D__F00*NC3D__S02+NC3D__F01*NC3D__S12+NC3D__F02*NC3D__S22))/NC3D__J

            (NC3D__F20*(NC3D__F00*NC3D__S00+NC3D__F01*NC3D__S01+NC3D__F02*NC3D__S02)+NC3D__F21*(NC3D__F00*NC3D__S01+NC3D__F01*NC3D__S11+NC3D__F02*NC3D__S12)+NC3D__F22*(NC3D__F00*NC3D__S02+NC3D__F01*NC3D__S12+NC3D__F02*NC3D__S22))/NC3D__J

            (NC3D__F10*(NC3D__F10*NC3D__S00+NC3D__F11*NC3D__S01+NC3D__F12*NC3D__S02)+NC3D__F11*(NC3D__F10*NC3D__S01+NC3D__F11*NC3D__S11+NC3D__F12*NC3D__S12)+NC3D__F12*(NC3D__F10*NC3D__S02+NC3D__F11*NC3D__S12+NC3D__F12*NC3D__S22))/NC3D__J

            (NC3D__F20*(NC3D__F10*NC3D__S00+NC3D__F11*NC3D__S01+NC3D__F12*NC3D__S02)+NC3D__F21*(NC3D__F10*NC3D__S01+NC3D__F11*NC3D__S11+NC3D__F12*NC3D__S12)+NC3D__F22*(NC3D__F10*NC3D__S02+NC3D__F11*NC3D__S12+NC3D__F12*NC3D__S22))/NC3D__J

            (NC3D__F20*(NC3D__F20*NC3D__S00+NC3D__F21*NC3D__S01+NC3D__F22*NC3D__S02)+NC3D__F21*(NC3D__F20*NC3D__S01+NC3D__F21*NC3D__S11+NC3D__F22*NC3D__S12)+NC3D__F22*(NC3D__F20*NC3D__S02+NC3D__F21*NC3D__S12+NC3D__F22*NC3D__S22))/NC3D__J
            
        Tumour Angiogenesis coupled problem:
             
             interstitial fluid velocity
                 
                 NC3D_TA3D__v0_INTE*iHat + NC3D_TA3D__v1_INTE*jHat + NC3D_TA3D__v2_INTE*kHat
                 
             transvascular (plasma) velocity
                 
                 NC3D_TA3D__v0_TRAN*iHat + NC3D_TA3D__v1_TRAN*jHat + NC3D_TA3D__v2_TRAN*kHat
                 

    2. for the vascular network domain (tumour-angiogenesis solver):

        displacement vector:

            TA3D__u0*iHat+TA3D__u1*jHat+TA3D__u2*kHat


    3. for the solid tissue domain (tumour-angiogenesis solver):

        tumour volume increase (percentage):

            ((Volume / XXX) - 1) * 100.0
