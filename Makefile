###############################################################################
include ./Make.common
###############################################################################
SRC_PATH = ./src
srcfiles := $(wildcard $(SRC_PATH)/*.C)
objects	:= $(patsubst %.C, %.$(obj-suffix), $(srcfiles))
target = ./feb3-$(METHOD).exe
libmesh_INCLUDE += $(OTHER_INCLUDES) -I./include \
                   -I$(BLITZ_INCLUDE_DIR) \
                   -I$(GSL_INCLUDE_DIR)
libmesh_LIBS += $(OTHER_LIBS) \
                -L$(BLITZ_LIB_DIR) -lblitz \
                -L$(GSL_LIB_DIR) -lgsl -lgslcblas
###############################################################################
include ./Make.rules
###############################################################################
