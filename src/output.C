/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#include "feb3/quadrature.h"
#include "libmesh/getpot.h"
#include "libmesh/libmesh_logging.h"
#include <iomanip>
#include "feb3/nurbs.h"
#include "libmesh/mesh.h"
#include "libmesh/dof_map.h"
#include "libmesh/system.h"
#include "libmesh/equation_systems.h"
#include "feb3/iga-context.h"
//=================================================================================================
// external variables
extern
OFStream flog, ferr;
//=================================================================================================
void check_and_fix_vascular_network (const char* name) {
    // local definition of a vascular network node data structure
    struct vn_node {
        int id;
        Point xyz;
        int bc_label;
        bool is_active;
    };
    // local definition of a vascular network segment (element) data structure
    struct vn_elem {
        int id;
        vn_node* conn[2];
        int flow_label;
    };

    // start reading the Gmsh-formatted file
    float version_number;
    int file_type, data_size;
    std::vector<vn_node> nodes_list;
    std::vector<vn_elem> elements_list;
    int n_boundary_nodes = 0;
    int n_segments = 0;
    do {
        std::ifstream fin(name);
        // read the header
        {
            std::string begin_header;
            fin >> begin_header;
            ASSERT_("$MeshFormat" == begin_header, "critical reading error");
            //
            fin >> version_number >> file_type >> data_size;
            //
            std::string end_header;
            fin >> end_header;
            ASSERT_("$EndMeshFormat" == end_header, "critical reading error");
        }
        // read the nodes
        {
            std::string begin_header;
            fin >> begin_header;
            ASSERT_("$Nodes" == begin_header, "critical reading error");
            // read the number of nodes of the mesh
            unsigned int n_nodes = 0;
            fin >> n_nodes;
            ASSERT_(n_nodes >= 3, "critical reading error");
            // read all nodes
            nodes_list.resize(n_nodes);
            for (unsigned int i=0; i<n_nodes; i++) {
                // read the ID of the node
                fin >> nodes_list[i].id;
                nodes_list[i].id -= 1;
                // read the Cartesian coordinates
                fin >> nodes_list[i].xyz(0)
                    >> nodes_list[i].xyz(1)
                    >> nodes_list[i].xyz(2);
                // set these flags to default value
                nodes_list[i].is_active = false;
                nodes_list[i].bc_label = -1;
            }
            //
            std::string end_header;
            fin >> end_header;
            ASSERT_("$EndNodes" == end_header, "critical reading error");
        }
        // read the elements
        {
            std::string begin_header;
            fin >> begin_header;
            ASSERT_("$Elements" == begin_header, "critical reading error");
            // read the number of elements of the mesh
            unsigned int n_elems = 0;
            fin >> n_elems;
            ASSERT_(n_elems >= 3, "critical reading error");
            // read all segments and BC nodes
            for (unsigned int i=0; i<n_elems; i++) {
                // read the ID of the element
                unsigned int id;
                fin >> id;
                // read the type of the element
                int type;
                fin >> type;
                unsigned int n_nodes = 0;
                if      ( 15 == type ) n_nodes = 1;
                else if (  1 == type ) n_nodes = 2;
                else ERROR_("critical reading error");
                // read the number of tabs of the element
                int n_tabs;
                fin >> n_tabs;
                ASSERT_(n_tabs == 2 || n_tabs == 3, "critical reading error");
                std::vector<unsigned int> tab(n_tabs);
                for (int l=0; l<n_tabs; l++) {
                    fin >> tab[l];
                }
                const unsigned int physical = tab[0];
                // read the connectivity of the element
                std::vector<unsigned int> conn(n_nodes);
                for (int l=0; l<n_nodes; l++) {
                    fin >> conn[l];
                    //
                    conn[l] -= 1;
                }
                //
                if ( 15 == type ) {
                    nodes_list[conn[0]].bc_label = physical;
                    // nodes_list[conn[0]].is_active = true;
                    // increment this index
                    ++n_boundary_nodes;
                } else {
                    vn_elem this_element;
                    this_element.id = n_segments;
                    for (int l=0; l<n_nodes; l++) {
                        nodes_list[conn[l]].is_active = true;
                        this_element.conn[l] = &(nodes_list[conn[l]]);
                    }
                    this_element.flow_label = physical;
                    //
                    elements_list.push_back(this_element);
                    // increment this index
                    ++n_segments;
                }
            }
            //
            std::string end_header;
            fin >> end_header;
            ASSERT_("$EndElements" == end_header, "critical reading error");
        }
    } while ( false );
    //
    int n_active_nodes = 0;
    for (unsigned int i=0; i<nodes_list.size(); i++) {
        if ( ! nodes_list[i].is_active ) continue;
        //
        nodes_list[i].id = n_active_nodes;
        // increment this index
        ++n_active_nodes;
    }
    //
    do {
        std::ofstream fout(name);
        unsigned int index;
        // write the header
        fout << "$MeshFormat" << std::endl;
        fout << ' ' << version_number << ' ' << file_type << ' ' << data_size << std::endl;
        fout << "$EndMeshFormat" << std::endl;
        // write the nodes
        fout << "$Nodes" << std::endl;
        fout << n_active_nodes << std::endl;
        index = 0;
        for (unsigned int i=0; i<nodes_list.size(); i++) {
            if ( ! nodes_list[i].is_active ) continue;
            //
            fout << index+1
                 << ' ' << nodes_list[i].xyz(0)
                 << ' ' << nodes_list[i].xyz(1)
                 << ' ' << nodes_list[i].xyz(2)
                 << std::endl;
            // increment this index
            ++index;
        }
        fout << "$EndNodes" << std::endl;
        // write the elements
        fout << "$Elements" << std::endl;
        fout << n_boundary_nodes+elements_list.size() << std::endl;
        index = 0;
        for (unsigned int i=0; i<nodes_list.size(); i++) {
            if ( nodes_list[i].bc_label < 0 ) continue;
            //
            fout << index+1
                 << " 15 2 "
                 << ' ' << nodes_list[i].bc_label
                 << ' ' << 0
                 << ' ' << nodes_list[i].id+1
                 << std::endl;
            // increment this index
            ++index;
        }
        for (unsigned int i=0; i<elements_list.size(); i++) {
            fout << index+1
                 << " 1 2 "
                 << ' ' << elements_list[i].flow_label
                 << ' ' << 0
                 << ' ' << elements_list[i].conn[0]->id+1
                 << ' ' << elements_list[i].conn[1]->id+1
                 << std::endl;
            // increment this index
            ++index;
        }
        fout << "$EndElements" << std::endl;
    } while ( false );
}
//=================================================================================================
#define PRINT_POINTS
#define PRINT_SHAPE
#define PRINT_D_SHAPE
#define PRINT_DD_SHAPE
#define PRINT_DDD_SHAPE
#define PRINT_SPANS
//
// #undef  PRINT_POINTS
// #undef  PRINT_SHAPE
// #undef  PRINT_D_SHAPE
// #undef  PRINT_DD_SHAPE
// #undef  PRINT_DDD_SHAPE
// #undef  PRINT_SPANS
//=================================================================================================
void print_nurbs (const NURBS& nurbs, unsigned int nsample) {
    const int DIM = nurbs.dimension();
    libmesh_assert( DIM >= SP1D && DIM <= SP3D );
    std::ofstream fout;
    char name[buffer_len];
    //
    const unsigned int NS_X = DIM>=SP1D ? nsample : 1;
    const unsigned int NS_Y = DIM>=SP2D ? nsample : 1;
    const unsigned int NS_Z = DIM>=SP3D ? nsample : 1;
    const Real ZP_X = DIM>=SP1D ? nurbs.nonrepeated_knot(0,0,0)(0) : 0.0;
    const Real ZP_Y = DIM>=SP2D ? nurbs.nonrepeated_knot(0,0,0)(1) : 0.0;
    const Real ZP_Z = DIM>=SP3D ? nurbs.nonrepeated_knot(0,0,0)(2) : 0.0;
    const Real DS_X = DIM>=SP1D ? (nurbs.nonrepeated_knot(nurbs.n_knots(0)-1,0,0)(0)-ZP_X)/(NS_X-1) : 0.0;
    const Real DS_Y = DIM>=SP2D ? (nurbs.nonrepeated_knot(0,nurbs.n_knots(1)-1,0)(1)-ZP_Y)/(NS_Y-1) : 0.0;
    const Real DS_Z = DIM>=SP3D ? (nurbs.nonrepeated_knot(0,0,nurbs.n_knots(2)-1)(2)-ZP_Z)/(NS_Z-1) : 0.0;
    //
    #ifdef PRINT_SHAPE
    sprintf(name, "NURBS%d_basis_F.dat", nurbs.id());
    fout.open(name);
    if ( DIM >= SP1D ) fout << " ξ";
    if ( DIM >= SP2D ) fout << " η";
    if ( DIM >= SP3D ) fout << " ζ";
    for (unsigned int A=0; A<nurbs.size(); A++) {
        fout << " F_" << (A+1);
    }
    fout << std::endl;
    for (unsigned int ii=0; ii<NS_X; ii++) {
        const Real xi = ZP_X + ii * DS_X;
        for (unsigned int jj=0; jj<NS_Y; jj++) {
            const Real eta = ZP_Y + jj * DS_Y;
            for (unsigned int kk=0; kk<NS_Z; kk++) {
                const Real zeta = ZP_Z + kk * DS_Z;
                //
                const Point Z = Point(xi, eta, zeta);
                const std::vector<Real>& phi = nurbs.get_phi();
                nurbs.reinit(Z);
                //
                for (unsigned int D=0; D<DIM; D++) {
                    fout << ' ' << Z(D);
                }
                for (unsigned int A=0; A<nurbs.size(); A++) {
                    fout << ' ' << phi[A];
                }
                fout << std::endl;
                //
            }
        }
    }
    fout.close();
    #endif
    //
    #ifdef PRINT_D_SHAPE
    sprintf(name, "NURBS%d_basis_dF.dat", nurbs.id());
    fout.open(name);
    if ( DIM >= SP1D ) fout << " ξ";
    if ( DIM >= SP2D ) fout << " η";
    if ( DIM >= SP3D ) fout << " ζ";
    for (unsigned int A=0; A<nurbs.size(); A++) {
        if ( DIM >= SP1D ) fout << " dFdξ_" << (A+1);
        if ( DIM >= SP2D ) fout << " dFdη_" << (A+1);
        if ( DIM >= SP3D ) fout << " dFdζ_" << (A+1);
    }
    fout << std::endl;
    for (unsigned int ii=0; ii<NS_X; ii++) {
        const Real xi = ZP_X + ii * DS_X;
        for (unsigned int jj=0; jj<NS_Y; jj++) {
            const Real eta = ZP_Y + jj * DS_Y;
            for (unsigned int kk=0; kk<NS_Z; kk++) {
                const Real zeta = ZP_Z + kk * DS_Z;
                //
                const Point Z = Point(xi, eta, zeta);
                const std::vector<Real>& dphidxi   = nurbs.get_dphidz<0>();
                const std::vector<Real>& dphideta  = nurbs.get_dphidz<1>();
                const std::vector<Real>& dphidzeta = nurbs.get_dphidz<2>();
                nurbs.reinit(Z);
                //
                for (unsigned int D=0; D<DIM; D++) {
                    fout << ' ' << Z(D);
                }
                for (unsigned int A=0; A<nurbs.size(); A++) {
                    if ( DIM >= SP1D ) fout << ' ' << dphidxi[A];
                    if ( DIM >= SP2D ) fout << ' ' << dphideta[A];
                    if ( DIM >= SP3D ) fout << ' ' << dphidzeta[A];
                }
                fout << std::endl;
                //
            }
        }
    }
    fout.close();
    #endif
    //
    #ifdef PRINT_DD_SHAPE
    sprintf(name, "NURBS%d_basis_ddF.dat", nurbs.id());
    fout.open(name);
    if ( DIM >= SP1D ) fout << " ξ";
    if ( DIM >= SP2D ) fout << " η";
    if ( DIM >= SP3D ) fout << " ζ";
    for (unsigned int A=0; A<nurbs.size(); A++) {
        if ( DIM >= SP1D ) fout << " d2Fdξ2_" << (A+1);
        if ( DIM >= SP2D ) fout << " d2Fdη2_" << (A+1);
        if ( DIM >= SP3D ) fout << " d2Fdζ2_" << (A+1);
    }
    fout << std::endl;
    for (unsigned int ii=0; ii<NS_X; ii++) {
        const Real xi = ZP_X + ii * DS_X;
        for (unsigned int jj=0; jj<NS_Y; jj++) {
            const Real eta = ZP_Y + jj * DS_Y;
            for (unsigned int kk=0; kk<NS_Z; kk++) {
                const Real zeta = ZP_Z + kk * DS_Z;
                //
                const Point Z = Point(xi, eta, zeta);
                const std::vector<Real>& d2phidxi2   = nurbs.get_d2phidz2<0,0>();
                const std::vector<Real>& d2phideta2  = nurbs.get_d2phidz2<1,1>();
                const std::vector<Real>& d2phidzeta2 = nurbs.get_d2phidz2<2,2>();
                nurbs.reinit(Z);
                //
                for (unsigned int D=0; D<DIM; D++) {
                    fout << ' ' << Z(D);
                }
                for (unsigned int A=0; A<nurbs.size(); A++) {
                    if ( DIM >= SP1D ) fout << ' ' << d2phidxi2[A];
                    if ( DIM >= SP2D ) fout << ' ' << d2phideta2[A];
                    if ( DIM >= SP3D ) fout << ' ' << d2phidzeta2[A];
                }
                fout << std::endl;
                //
            }
        }
    }
    fout.close();
    #endif
    //
    #ifdef PRINT_DDD_SHAPE
    sprintf(name, "NURBS%d_basis_dddF.dat", nurbs.id());
    fout.open(name);
    if ( DIM >= SP1D ) fout << " ξ";
    if ( DIM >= SP2D ) fout << " η";
    if ( DIM >= SP3D ) fout << " ζ";
    for (unsigned int A=0; A<nurbs.size(); A++) {
        if ( DIM >= SP1D ) fout << " d3Fdξ3_" << (A+1);
        if ( DIM >= SP2D ) fout << " d3Fdη3_" << (A+1);
        if ( DIM >= SP3D ) fout << " d3Fdζ3_" << (A+1);
    }
    fout << std::endl;
    for (unsigned int ii=0; ii<NS_X; ii++) {
        const Real xi = ZP_X + ii * DS_X;
        for (unsigned int jj=0; jj<NS_Y; jj++) {
            const Real eta = ZP_Y + jj * DS_Y;
            for (unsigned int kk=0; kk<NS_Z; kk++) {
                const Real zeta = ZP_Z + kk * DS_Z;
                //
                const Point Z = Point(xi, eta, zeta);
                const std::vector<Real>& d3phidxi3   = nurbs.get_d3phidz3<0,0,0>();
                const std::vector<Real>& d3phideta3  = nurbs.get_d3phidz3<1,1,1>();
                const std::vector<Real>& d3phidzeta3 = nurbs.get_d3phidz3<2,2,2>();
                nurbs.reinit(Z);
                //
                for (unsigned int D=0; D<DIM; D++) {
                    fout << ' ' << Z(D);
                }
                for (unsigned int A=0; A<nurbs.size(); A++) {
                    if ( DIM >= SP1D ) fout << ' ' << d3phidxi3[A];
                    if ( DIM >= SP2D ) fout << ' ' << d3phideta3[A];
                    if ( DIM >= SP3D ) fout << ' ' << d3phidzeta3[A];
                }
                fout << std::endl;
                //
            }
        }
    }
    fout.close();
    #endif
    //
    #ifdef PRINT_POINTS
    // print-out the sample points of the NURBS
    sprintf(name, "NURBS%d_points.dat", nurbs.id());
    fout.open(name);
    for (unsigned int ii=0; ii<NS_X; ii++) {
        const Real xi = ZP_X + ii * DS_X;
        for (unsigned int jj=0; jj<NS_Y; jj++) {
            const Real eta = ZP_Y + jj * DS_Y;
            for (unsigned int kk=0; kk<NS_Z; kk++) {
                const Real zeta = ZP_Z + kk * DS_Z;
                //
                const Point Z = Point(xi, eta, zeta);
                const Point& p = nurbs.get_xyz();
                nurbs.reinit(Z);
                //
                for (int dim=0; dim<SP3D; dim++)
                    fout << ' ' << p(dim);
                fout << std::endl;
                //
            }
        }
    }
    fout.close();
    // print-out the control points of the net
    sprintf(name, "NURBS%d_control.dat", nurbs.id());
    fout.open(name);
    for (unsigned int A=0; A<nurbs.size(); A++) {
        //
        for (int dim=0; dim<SP3D; dim++)
            fout << ' ' << nurbs.ctrl_pnt_indexed(A)(dim);
        fout << std::endl;
        //
    }
    fout.close();
    #endif
    // print-out the non-repeated knot vectors of the net
    #ifdef PRINT_SPANS
    sprintf(name, "NURBS%d_knots.msh", nurbs.id());
    fout.open(name);
    fout << "$MeshFormat\n" << std::flush;
    fout << "2.0 0 8\n" << std::flush;
    fout << "$EndMeshFormat\n" << std::flush;
    fout << "$Nodes\n" << std::flush;
    fout << nurbs.n_knots(0)*nurbs.n_knots(1)*nurbs.n_knots(2) << '\n' << std::flush;
    int inode = 0;
    for (int K=0; K<nurbs.n_knots(2); K++) {
        for (int J=0; J<nurbs.n_knots(1); J++) {
            for (int I=0; I<nurbs.n_knots(0); I++) {
                //
                const Point Z = nurbs.nonrepeated_knot(I, J, K);
                const Point& p = nurbs.get_xyz();
                nurbs.reinit(Z);
                //
                fout << (inode+1) << ' ';
                for (int dim=0; dim<SP3D; dim++)
                    fout << ' ' << p(dim);
                fout << std::endl;
                //
                ++inode;
            }
        }
    }
    fout << "$EndNodes\n" << std::flush;
    fout << "$Elements\n" << std::flush;
    if ( nurbs.dimension() == SP1D ) {
        //
        blitz::Array<int, SP1D> net(nurbs.n_knots(0));
        int inode = 1;
        for (int I=0; I<nurbs.n_knots(0); I++) {
            net(I) = inode;
            ++inode;
        }
        //
        fout << nurbs.span(0) << '\n' << std::flush;
        //
        int I = 0;
        for (int ielem=1; ielem<=nurbs.span(0); ielem++) {
            fout << ielem << ' ';
            fout << " 1 3 0 0 0  ";
            fout << net(I+0) << ' ';
            fout << net(I+1) << ' ';
            ++I;
            fout << std::endl;
        }
        //
    } else if ( nurbs.dimension() == SP2D ) {
        //
        blitz::Array<int, SP2D> net(nurbs.n_knots(0), nurbs.n_knots(1));
        int inode = 1;
        for (int J=0; J<nurbs.n_knots(1); J++) {
            for (int I=0; I<nurbs.n_knots(0); I++) {
                net(I,J) = inode;
                ++inode;
            }
        }
        //
        fout << nurbs.span(0)*nurbs.span(1) << '\n' << std::flush;
        //
        int I = 0;
        int J = 0;
        for (int ielem=1; ielem<=nurbs.span(0)*nurbs.span(1); ielem++) {
            fout << ielem << ' ';
            fout << " 3 3 0 0 0  ";
            fout << net(I+0,J+0) << ' ';
            fout << net(I+1,J+0) << ' ';
            fout << net(I+1,J+1) << ' ';
            fout << net(I+0,J+1) << ' ';
            ++I;
            if ( ielem%nurbs.span(0) == 0 ) {
                I = 0;
                ++J;
            }
            fout << std::endl;
        }
        //
    } else if ( nurbs.dimension() == SP3D ) {
        //
        blitz::Array<int, SP3D> net(nurbs.n_knots(0), nurbs.n_knots(1), nurbs.n_knots(2));
        int inode = 1;
        for (int K=0; K<nurbs.n_knots(2); K++) {
            for (int J=0; J<nurbs.n_knots(1); J++) {
                for (int I=0; I<nurbs.n_knots(0); I++) {
                    std::cout << ' ' << I << ' ' << J << ' ' << K << std::endl;
                    net(I,J,K) = inode;
                    ++inode;
                }
            }
        }
        //
        fout << nurbs.span(0)*nurbs.span(1)*nurbs.span(2) << '\n' << std::flush;
        //
        int I = 0;
        int J = 0;
        int K = 0;
        for (int ielem=1; ielem<=nurbs.span(0)*nurbs.span(1)*nurbs.span(2); ielem++) {
            fout << ielem << ' ';
            fout << " 5 3 0 0 0  ";
            fout << net(I+0,J+0,K+0) << ' ';
            fout << net(I+1,J+0,K+0) << ' ';
            fout << net(I+1,J+1,K+0) << ' ';
            fout << net(I+0,J+1,K+0) << ' ';
            fout << net(I+0,J+0,K+1) << ' ';
            fout << net(I+1,J+0,K+1) << ' ';
            fout << net(I+1,J+1,K+1) << ' ';
            fout << net(I+0,J+1,K+1) << ' ';
            ++I;
            if ( ielem%nurbs.span(0) == 0 ) {
                I = 0;
                ++J;
                if ( ielem%(nurbs.span(0)*nurbs.span(1)) == 0 ) {
                    J = 0;
                    ++K;
                }
            }
            fout << std::endl;
        }
        //
    } else {
        libmesh_error();
    }
    fout << "$EndElements\n" << std::flush;
    fout.close();
    #endif
}
//=================================================================================================
#undef  PRINT_POINTS
#undef  PRINT_SHAPE
#undef  PRINT_D_SHAPE
#undef  PRINT_DD_SHAPE
#undef  PRINT_DDD_SHAPE
#undef  PRINT_SPANS
//=================================================================================================
void inspect_system (const System& system, std::ofstream& fout) {
    WARNING_("system inspection routine has been overridden");
    return;

    const MeshBase& msh = system.get_mesh();
    const unsigned int nvars = system.n_vars();

    const DofMap& dof_map = system.get_dof_map();
    const std::vector<dof_id_type>& send_list = dof_map.get_send_list();
    //const std::vector<dof_id_type>& n_nz = dof_map.get_n_nz();
    //const std::vector<dof_id_type>& n_oz = dof_map.get_n_oz();
    std::map<dof_id_type, dof_id_type> dof_2_node_map;
    int index;

    // print the system name first
    fout << "\n ****";
    for (unsigned int i=0; i<system.name().size(); i++) fout << '*';
    fout << "**** \n";
    fout << " *** " << system.name().c_str() << " *** ";
    fout << "\n ****";
    for (unsigned int i=0; i<system.name().size(); i++) fout << '*';
    fout << "**** \n";

    fout << " All_Nodes (" << msh.n_nodes() << ") = \n" << std::endl;
    index = 0;
    for (MeshBase::const_node_iterator ci=msh.nodes_begin(); ci!=msh.nodes_end(); ci++) {
        const Node* node = (*ci);
        //
        fout << " " << std::setw(3) << index << " -> ";
        fout << " (NODE: " << std::setw(3) << (node->id()+1) << ") : ";
        for (unsigned int ivar=0; ivar<system.n_vars(); ivar++) {
            const dof_id_type idof = node->dof_number(system.number(), ivar, 0);
            fout << " " << std::setw(3) << idof;
            dof_2_node_map.insert( std::make_pair(idof, node->id()) );
        }
        fout << std::endl;
        ++index;
    }
    fout << std::endl;

    fout << " Local_DoFs (" << dof_map.n_local_dofs() << ") = \n" << std::endl;
    fout << "            {" << dof_map.first_dof() << "," << dof_map.end_dof()-1 << "}\n" << std::endl;

    fout << " Local_Nodes (" << msh.n_nodes_on_proc(libMesh::global_processor_id()) << ") = \n" << std::endl;
    index = 0;
    for (MeshBase::const_node_iterator ci=msh.local_nodes_begin(); ci!=msh.local_nodes_end(); ci++) {
        const Node* node = (*ci);
        //
        fout << " " << std::setw(3) << index << " -> ";
        fout << " (NODE: " << std::setw(3) << (node->id()+1) << ") : ";
        // for (unsigned int ivar=0; ivar<system.n_vars(); ivar++) {
        //     fout << " " << std::setw(3) << node->dof_number(system.number(), ivar, 0);
        // }
        fout << std::endl;
        //
        ++index;
    }
    fout << std::endl;

    // index = 0;
    // for ( ; index<dof_map.n_local_dofs(); ) {
    //     const dof_id_type res = dof_map.first_dof() + index;
    //     //
    //     fout << " " << std::setw(3) << index << " -> ";
    //     fout << " (NODE: " << std::setw(3) << (dof_2_node_map[res]+1) << ") : ";
    //     fout << " " << std::setw(3) << res;
    //     if ( (index+1)%nvars == 0 ) fout << std::endl;
    //     //
    //     ++index;
    // }
    // fout << std::endl;

    fout << " Send_List (" << send_list.size() << ") = \n" << std::endl;
    index = 0;
    for (unsigned i=0; i<send_list.size(); i++) {
        const dof_id_type res = send_list[i];
        //
        fout << " " << std::setw(3) << index << " -> ";
        fout << " (NODE: " << std::setw(3) << (dof_2_node_map[res]+1) << ") : ";
        fout << " " << std::setw(3) << res;
        if ( (index+1)%nvars == 0 ) fout << std::endl;
        //
        ++index;
    }
    fout << std::endl;

    // fout << " N_NonZeros (" << n_nz.size() << ") = \n" << std::endl;
    // index = 0;
    // for (unsigned i=0; i<n_nz.size(); i++) {
    //     const dof_id_type res = n_nz[i];
    //     //
    //     fout << " " << std::setw(3) << index << " -> ";
    //     fout << " (NODE: " << std::setw(3) << (dof_2_node_map[dof_map.first_dof()+index]+1) << ") : ";
    //     fout << " " << std::setw(3) << res;
    //     if ( (index+1)%nvars == 0 ) fout << std::endl;
    //     //
    //     ++index;
    // }
    // fout << std::endl;

    // fout << " N_OverZeros (" << n_oz.size() << ") = \n" << std::endl;
    // index = 0;
    // for (unsigned i=0; i<n_oz.size(); i++) {
    //     const dof_id_type res = n_oz[i];
    //     //
    //     fout << " " << std::setw(3) << index << " -> ";
    //     fout << " (NODE: " << std::setw(3) << (dof_2_node_map[dof_map.first_dof()+index]+1) << ") : ";
    //     fout << " " << std::setw(3) << res;
    //     if ( (index+1)%nvars == 0 ) fout << std::endl;
    //     //
    //     ++index;
    // }
    // fout << std::endl;

    dof_map.print_info(fout);
    fout << std::endl;
}
//=================================================================================================
void inspect_all_systems (const EquationSystems& es, std::ofstream& fout) {
    for (unsigned int i=0; i<es.n_systems(); i++) {
        const System& system = es.get_system<System>(i);
        inspect_system(system, fout);
    }
}
//=================================================================================================
void plot_results_iga (const IGA_Context& iga_ctx, const char* name, const unsigned int nsample) {
    const System& system = iga_ctx.get_system();
    const MeshIGA& msh_iga = iga_ctx.get_mesh_IGA();
    const std::vector<NURBS>& nurbs_list = msh_iga.get_nurbs();

    std::vector<Number> solution;
    system.solution->localize_to_one(solution);

    if ( libMesh::global_processor_id() ) return;

    std::map<unsigned int, Point> points_map;
    std::map<unsigned int, std::pair<unsigned int, int> > points_nurbs_side_map;
    std::map<unsigned int, Point> points_prm_coord_map;
    std::map<unsigned int, std::vector<unsigned int> > cells_conn_map;
    std::map<unsigned int, boundary_id_type > cells_bcid_map;
    //
    unsigned int n_points = 0;
    unsigned int n_cells  = 0;
    const MeshBase::const_element_iterator begin_el = msh_iga.get_parametric_mesh().active_elements_begin();
    const MeshBase::const_element_iterator end_el   = msh_iga.get_parametric_mesh().active_elements_end();
    // loop over all active (parametric) elements
    for (MeshBase::const_element_iterator citer_el=begin_el; citer_el!=end_el; citer_el++) {
        const Elem* elem_prm = *citer_el;
        // access the NURBS
        const NURBS* elem = &nurbs_list[elem_prm->subdomain_id()];
        libmesh_assert( elem->dimension() == SP3D );
        //
        // run for all sides of NURBS
        for (unsigned int s=0; s<elem_prm->n_sides(); s++) {
            if ( elem_prm->neighbor(s) != NULL ) continue;
            // get the ID of the boundary
            const boundary_id_type bc_id = elem->boundary_id(s);
            // create a side NURBS
            UniquePtr<NURBS> side = elem->build_side(s);
            //
            Point alpha, beta;
            iga_transf_3d(elem_prm, alpha, beta, s);
            //
            blitz::Array<unsigned int, 2> pnt_array(nsample, nsample);
            //
            const Real step = 2.0 / (nsample-1.0);
            const Real zeta = 0.0;
            for (unsigned int j=0; j<nsample; j++) {
                const Real eta = -1.0 + j * step;
                for (unsigned int i=0; i<nsample; i++) {
                    const Real xi = -1.0 + i * step;
                    //
                    const Point Z = self_scale(alpha, Point(xi, eta, zeta)) + beta;
                    // initialize and get access to the IGA stuff
                    const Point& P = side->get_xyz();
                    side->reinit(Z);
                    //
                    std::pair<unsigned int, int> D(elem->id(), s);
                    //
                    points_map.insert( std::make_pair(n_points, P) );
                    points_nurbs_side_map.insert( std::make_pair(n_points, D) );
                    points_prm_coord_map.insert( std::make_pair(n_points, Z) );
                    //
                    pnt_array((int)i,(int)j) = n_points;
                    // increment this index
                    ++n_points;
                }
            }
            //
            for (int j=0; j<nsample-1; j++) {
                for (int i=0; i<nsample-1; i++) {
                    std::vector<unsigned int> conn(4);
                    //
                    conn[0] = pnt_array(i+0,j+0);
                    conn[1] = pnt_array(i+1,j+0);
                    conn[2] = pnt_array(i+1,j+1);
                    conn[3] = pnt_array(i+0,j+1);
                    //
                    cells_conn_map.insert( std::make_pair(n_cells, conn) );
                    cells_bcid_map.insert( std::make_pair(n_cells, bc_id) );
                    // increment this index
                    ++n_cells;
                }
            }
            // end of (parametric) element sides loop...
        }
        // end of active (parametric) elements loop...
    }

    // store results in a legacy VTK file
    std::ofstream fout(name);

    const unsigned int n_entries = n_cells + 4*n_cells;
    int index = 0;

    fout << "# vtk DataFile Version 2.0" << std::endl;
    fout << "Unstructured Grid Example" << std::endl;
    fout << "ASCII" << std::endl;
    fout << "DATASET UNSTRUCTURED_GRID" << std::endl;
    fout << std::endl;

    fout << "POINTS " << n_points << " float" << std::endl;
    index = 1;
    for ( std::map<unsigned int, Point>::const_iterator
          citer=points_map.begin(); citer!=points_map.end(); citer++ ) {
        const Point& p = citer->second;
        for (int dim=0; dim<SP3D; dim++)
            fout << ' ' << p(dim);
        //
        if ( index%10 == 0 ) fout << std::endl;
        else if ( index == n_points ) fout << std::endl;
        ++index;
        // end of points loop...
    }
    fout << std::endl;

    fout << "CELLS " << n_cells << " " << n_entries << std::endl;
    index = 1;
    for ( std::map<unsigned int, std::vector<unsigned int> >::const_iterator
          citer=cells_conn_map.begin(); citer!=cells_conn_map.end(); citer++ ) {
        const std::vector<unsigned int>& conn = citer->second;
        fout << ' ' << conn.size() << ' ';
        for (unsigned int i=0; i<conn.size(); i++)
            fout << ' ' << conn[i];
        fout << std::endl;
        ++index;
        // end of cells loop...
    }
    fout << std::endl;

    fout << "CELL_TYPES " << n_cells << std::endl;
    index = 1;
    for (unsigned int icell=0; icell<n_cells; icell++ ) {
        fout << ' ' << 9 << std::endl;
        //
        if ( index%10 == 0 ) fout << std::endl;
        else if ( index == n_cells ) fout << std::endl;
        ++index;
        // end of cells loop...
    }
    fout << std::endl;

    fout << "CELL_DATA " << n_cells << std::endl;
    index = 1;
    fout << "SCALARS boundary_id int 1" << std::endl;
    fout << "LOOKUP_TABLE default" << std::endl;
    for ( std::map<unsigned int, boundary_id_type >::const_iterator
          citer=cells_bcid_map.begin(); citer!=cells_bcid_map.end(); citer++ ) {
        fout << ' ' << citer->second << std::endl;
        //
        if ( index%10 == 0 ) fout << std::endl;
        else if ( index == n_cells ) fout << std::endl;
        ++index;
        // end of cells loop...
    }
    fout << std::endl;

    fout << "POINT_DATA " << n_points << std::endl;
    for (unsigned int ivar=0; ivar<system.n_vars(); ivar++) {
        fout << "SCALARS " << system.variable_name(ivar) << " float 1" << std::endl;
        fout << "LOOKUP_TABLE default" << std::endl;
        index = 1;
        std::map<unsigned int, std::pair<unsigned int, int> >::const_iterator citer = points_nurbs_side_map.begin();
        std::map<unsigned int, Point>::const_iterator                         cjter = points_prm_coord_map.begin();
        for ( ; citer!=points_nurbs_side_map.end() && cjter!=points_prm_coord_map.end(); citer++, cjter++ ) {
            // NURBS element
            const NURBS* elem = &nurbs_list[citer->second.first];
            //
            const int se = citer->second.second;
            if ( se == -1 ) {
                const Point& Z = cjter->second;
                const std::vector<Real>& phi = elem->get_phi();
                elem->reinit(Z);
                //
                Real field = 0.0;
                for (unsigned int A=0; A<elem->size(); A++) {
                    const Node* node = elem->get_ctrl_node_indexed(A);
                    const unsigned int idof = node->dof_number(system.number(), ivar, 0);
                    field += phi[A] * solution[idof];
                }
                fout << ' ' << field;
            } else {
                // side NURBS element
                UniquePtr<NURBS> side = elem->build_side(se);
                //
                const Point& Z = cjter->second;
                const std::vector<Real>& phi = side->get_phi();
                side->reinit(Z);
                //
                Real field = 0.0;
                for (unsigned int A=0; A<side->size(); A++) {
                    const Node* node = side->get_ctrl_node_indexed(A);
                    const unsigned int idof = node->dof_number(system.number(), ivar, 0);
                    field += phi[A] * solution[idof];
                }
                fout << ' ' << field;
            }
            //
            if ( index%10 == 0 ) fout << std::endl;
            else if ( index == n_points ) fout << std::endl;
            ++index;
        }
        fout << std::endl;
    }
    fout << std::endl;
}
//=================================================================================================
