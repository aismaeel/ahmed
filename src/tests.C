/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#include "feb3/utils.h"
#include "feb3/domain_data.h"
#include "libmesh/getpot.h"
#include "libmesh/libmesh_logging.h"
#include "libmesh/mesh.h"
#include "feb3/utils.h"
#include "feb3/reaction_diffusion_solver_Xd.h"
#include "feb3/navier_cauchy_solver_3d.h"
#include "feb3/navier_cauchy_iga_solver_3d.h"
#include "feb3/wound_healing_solver.h"
#include "feb3/mesh_iga.h"
#include "feb3/quadrature.h"
//=================================================================================================
// external variables
extern
OFStream flog, ferr;
extern
GlobalTolerance tol;
extern
std::string tmp_dir;
//=================================================================================================
// external routines
extern
void inspect_system (const System&, std::ofstream& );
extern
void inspect_all_systems (const EquationSystems&, std::ofstream& );
//=================================================================================================
static
void eigenproblem_test () {
    Dyadic3D A;
    A.comp = 40, -20,   1e-12,
            -20,  40, -20,
              0, -20,  40;
    if ( true ) {
        std::cout << "                                      \n"
                  << A.comp << std::endl;
    }
    if ( true ) {
        std::cout << std::endl;
        //
        std::vector<Real> l;
        std::vector<Vector3D> v;
        eigen_solve(A, l, v);
        for (int i=0; i<3; i++) {
            std::cout << l[i] << ' ' << magnitude(v[i]) << ' ' << v[i].comp << std::endl;
        }
        //
        std::cout << std::endl;
    }
    if ( true ) {
        std::cout << std::endl;
        //
        Real l;
        Vector3D v;
        l = min_eigen(A, v);
        std::cout << l << ' ' << magnitude(v) << ' ' << v.comp << std::endl;
        v = min_eigen(A);
        std::cout << magnitude(v) << ' ' << unit(v).comp << std::endl;
        l = max_eigen(A, v);
        std::cout << l << ' ' << magnitude(v) << ' ' << v.comp << std::endl;
        v = max_eigen(A);
        std::cout << magnitude(v) << ' ' << unit(v).comp << std::endl;
        //
        Point p = vec(unit(v));
        Dyadic3D a = tensor(p, p);
        std::cout << a.comp << std::endl;
        //
        std::cout << std::endl;
    }
}
//=================================================================================================
static
void IGA_test (const DomainData& dmn) {
    const EquationSystems& es = dmn.get_equation_systems();
    const std::map<std::string, MeshIGA>* all_meshes_iga =
        es.parameters.get<std::map<std::string, MeshIGA>*>("pointer to IGA parametric mesh(es)");
    //
    const std::string label = es.parameters.get<std::string>("IGA parametric mesh label #0");
    //
    const MeshIGA& msh_iga = all_meshes_iga->find(label)->second;
    //
    const std::vector<NURBS>& nurbs_list = msh_iga.get_nurbs();

    char name[buffer_len];
    sprintf(name, "%s/test_results", tmp_dir.c_str());
    std::ofstream fout(name);

    es.get_mesh().print_info(fout);

    for ( std::vector<NURBS>::const_iterator
          n_ci=nurbs_list.begin(); n_ci!=nurbs_list.end(); n_ci++ ) {
        const NURBS* nurbs = &(*n_ci);

        nurbs->print_info(fout);

        for (int s=0; s<nurbs->n_sides(); s++) {
            if ( ! nurbs->neighbor(s) ) {
                char txt[buffer_len];
                sprintf( txt, " > NURBS (ID:%d) has no neighbors (Boundary ID:%d) on side %d \n",
                         nurbs->id(), nurbs->boundary_id(s), s );
                fout << txt;
                //
                UniquePtr<NURBS> side_nurbs = nurbs->build_side(s);
                side_nurbs->print_info(fout);

            } else {
                char txt[buffer_len];
                sprintf( txt, " > NURBS (ID:%d) neighbors with NURBS (ID:%d) on side %d \n",
                         nurbs->id(), nurbs->neighbor(s)->id(), s );
                fout << txt;
                //
            }
        }

        // end of NURBS loop...
    }

    const MeshBase::const_element_iterator begin_el = msh_iga.get_parametric_mesh().active_local_elements_begin();
    const MeshBase::const_element_iterator end_el   = msh_iga.get_parametric_mesh().active_local_elements_end();
    msh_iga.print_info(fout);

    for (MeshBase::const_element_iterator citer_el=begin_el; citer_el!=end_el; citer_el++) {
        const Elem* p_elem = *citer_el;
        const NURBS* nurbs = &nurbs_list[p_elem->subdomain_id()];

        fout << std::endl;
        p_elem->print_info(fout);
        for (unsigned int s=0; s<p_elem->n_sides(); s++) {
            UniquePtr<Elem>  p_elem_side = p_elem->build_side(s);
            UniquePtr<NURBS> nurbs_side = nurbs->build_side(s);

            if (p_elem->neighbor(s) != NULL) continue;

            fout << std::endl;
            p_elem_side->print_info(fout);
        }
        fout << std::endl;

        // end of active local (parametric) elements loop...
    }
}
//=================================================================================================
void run_a_test () {
    std::cout << "\n *** Running a test with FEB3...\r" << std::flush;

    eigenproblem_test();

    std::cout << " *** Running a test with FEB3... [  OK  ]\n" << std::endl;
}
//=================================================================================================
