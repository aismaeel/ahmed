/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#include "feb3/domain_data.h"
//=================================================================================================
// external variables
extern
OFStream flog, ferr;
//=================================================================================================
namespace damaged {
//=================================================================================================
#ifdef _LOCAL_ARGUMENTS_2D_
    #error "an unexpected error occurred!"
#else
    #define _LOCAL_ARGUMENTS_2D_ \
        const MaterialInfo& mat, \
        const FieldInfo& fld,    \
        Dyadic2D& S2pk,          \
        Quindic2D& Dtsm
#endif
#ifdef _LOCAL_ARGUMENTS_3D_
    #error "an unexpected error occurred!"
#else
    #define _LOCAL_ARGUMENTS_3D_ \
        const MaterialInfo& mat, \
        const FieldInfo& fld,    \
        Dyadic3D& S2pk,          \
        Quindic3D& Dtsm_uu,      \
        Dyadic3D& Dtsm_up,       \
        Real& Ph,                \
        Dyadic3D& Dtsm_pu,       \
        Real& Dtsm_pp
#endif
//=================================================================================================
#define DMG  (1.0-fld.biomech[0])
//=================================================================================================
// strain-energy function defined in healthy::polynomial_2d_forward() routine
//=================================================================================================
void polynomial_2d_forward ( _LOCAL_ARGUMENTS_2D_ ) {
    MaterialInfo mat_dmg(9);
    mat_dmg.set_biomech_param(0) = mat.get_biomech_param(0)*DMG;
    mat_dmg.set_biomech_param(1) = mat.get_biomech_param(1);
    mat_dmg.set_biomech_param(2) = mat.get_biomech_param(2)*DMG;
    mat_dmg.set_biomech_param(3) = mat.get_biomech_param(3)*DMG;
    mat_dmg.set_biomech_param(4) = mat.get_biomech_param(4)*DMG;
    mat_dmg.set_biomech_param(5) = mat.get_biomech_param(5)*DMG;
    mat_dmg.set_biomech_param(6) = mat.get_biomech_param(6)*DMG;
    mat_dmg.set_biomech_param(7) = mat.get_biomech_param(7)*DMG;
    mat_dmg.set_biomech_param(8) = mat.get_biomech_param(8)*DMG;
    healthy::polynomial_2d_forward(mat_dmg, fld, S2pk, Dtsm);
}
//=================================================================================================
// strain-energy function defined in healthy::polynomial_3d_forward_std() routine
//=================================================================================================
void polynomial_3d_forward_std ( _LOCAL_ARGUMENTS_3D_ ) {
    MaterialInfo mat_dmg(30);
    mat_dmg.set_biomech_param(0) = mat.get_biomech_param(0)*DMG;
    mat_dmg.set_biomech_param(1) = mat.get_biomech_param(1);
    mat_dmg.set_biomech_param(2) = mat.get_biomech_param(2)*DMG;
    mat_dmg.set_biomech_param(3) = mat.get_biomech_param(3)*DMG;
    mat_dmg.set_biomech_param(4) = mat.get_biomech_param(4)*DMG;
    mat_dmg.set_biomech_param(5) = mat.get_biomech_param(5)*DMG;
    mat_dmg.set_biomech_param(6) = mat.get_biomech_param(6)*DMG;
    mat_dmg.set_biomech_param(7) = mat.get_biomech_param(7);
    mat_dmg.set_biomech_param(8) = mat.get_biomech_param(8);
    mat_dmg.set_biomech_param(9) = mat.get_biomech_param(9);
    mat_dmg.set_biomech_param(10) = mat.get_biomech_param(10);
    healthy::polynomial_3d_forward_std(mat_dmg, fld, S2pk, Dtsm_uu, Dtsm_up, Ph, Dtsm_pu, Dtsm_pp);
}
//=================================================================================================
// strain-energy function defined in healthy::polynomial_3d_forward_mod() routine
//=================================================================================================
void polynomial_3d_forward_mod ( _LOCAL_ARGUMENTS_3D_ ) {
    MaterialInfo mat_dmg(30);
    mat_dmg.set_biomech_param(0) = mat.get_biomech_param(0)*DMG;
    mat_dmg.set_biomech_param(1) = mat.get_biomech_param(1);
    mat_dmg.set_biomech_param(2) = mat.get_biomech_param(2)*DMG;
    mat_dmg.set_biomech_param(3) = mat.get_biomech_param(3)*DMG;
    mat_dmg.set_biomech_param(4) = mat.get_biomech_param(4)*DMG;
    mat_dmg.set_biomech_param(5) = mat.get_biomech_param(5)*DMG;
    mat_dmg.set_biomech_param(6) = mat.get_biomech_param(6)*DMG;
    mat_dmg.set_biomech_param(7) = mat.get_biomech_param(7);
    mat_dmg.set_biomech_param(8) = mat.get_biomech_param(8);
    mat_dmg.set_biomech_param(9) = mat.get_biomech_param(9);
    mat_dmg.set_biomech_param(10) = mat.get_biomech_param(10);
    mat_dmg.set_biomech_param(11) = mat.get_biomech_param(11)*DMG;
    mat_dmg.set_biomech_param(12) = mat.get_biomech_param(12)*DMG;
    mat_dmg.set_biomech_param(13) = mat.get_biomech_param(13)*DMG;
    mat_dmg.set_biomech_param(14) = mat.get_biomech_param(14)*DMG;
    mat_dmg.set_biomech_param(15) = mat.get_biomech_param(15)*DMG;
    mat_dmg.set_biomech_param(16) = mat.get_biomech_param(16)*DMG;
    mat_dmg.set_biomech_param(17) = mat.get_biomech_param(17)*DMG;
    mat_dmg.set_biomech_param(18) = mat.get_biomech_param(18)*DMG;
    mat_dmg.set_biomech_param(19) = mat.get_biomech_param(19)*DMG;
    mat_dmg.set_biomech_param(20) = mat.get_biomech_param(20)*DMG;
    mat_dmg.set_biomech_param(21) = mat.get_biomech_param(21)*DMG;
    mat_dmg.set_biomech_param(22) = mat.get_biomech_param(22)*DMG;
    mat_dmg.set_biomech_param(23) = mat.get_biomech_param(23)*DMG;
    mat_dmg.set_biomech_param(24) = mat.get_biomech_param(24)*DMG;
    mat_dmg.set_biomech_param(25) = mat.get_biomech_param(25)*DMG;
    mat_dmg.set_biomech_param(26) = mat.get_biomech_param(26)*DMG;
    healthy::polynomial_3d_forward_mod(mat_dmg, fld, S2pk, Dtsm_uu, Dtsm_up, Ph, Dtsm_pu, Dtsm_pp);
}
//=================================================================================================
// strain-energy function defined in healthy::polynomial_3d_forward_2f() routine
//=================================================================================================
void polynomial_3d_forward_2f ( _LOCAL_ARGUMENTS_3D_ ) {
    MaterialInfo mat_dmg(30);
    mat_dmg.set_biomech_param(0) = mat.get_biomech_param(0)*DMG;
    mat_dmg.set_biomech_param(1) = mat.get_biomech_param(1);
    mat_dmg.set_biomech_param(2) = mat.get_biomech_param(2)*DMG;
    mat_dmg.set_biomech_param(3) = mat.get_biomech_param(3)*DMG;
    mat_dmg.set_biomech_param(4) = mat.get_biomech_param(4)*DMG;
    mat_dmg.set_biomech_param(5) = mat.get_biomech_param(5)*DMG;
    mat_dmg.set_biomech_param(6) = mat.get_biomech_param(6)*DMG;
    mat_dmg.set_biomech_param(7) = mat.get_biomech_param(7);
    mat_dmg.set_biomech_param(8) = mat.get_biomech_param(8);
    mat_dmg.set_biomech_param(9) = mat.get_biomech_param(9);
    mat_dmg.set_biomech_param(10) = mat.get_biomech_param(10);
    mat_dmg.set_biomech_param(11) = mat.get_biomech_param(11)*DMG;
    mat_dmg.set_biomech_param(12) = mat.get_biomech_param(12)*DMG;
    mat_dmg.set_biomech_param(13) = mat.get_biomech_param(13)*DMG;
    mat_dmg.set_biomech_param(14) = mat.get_biomech_param(14)*DMG;
    mat_dmg.set_biomech_param(15) = mat.get_biomech_param(15)*DMG;
    mat_dmg.set_biomech_param(16) = mat.get_biomech_param(16)*DMG;
    mat_dmg.set_biomech_param(17) = mat.get_biomech_param(17)*DMG;
    mat_dmg.set_biomech_param(18) = mat.get_biomech_param(18)*DMG;
    mat_dmg.set_biomech_param(19) = mat.get_biomech_param(19)*DMG;
    mat_dmg.set_biomech_param(20) = mat.get_biomech_param(20)*DMG;
    mat_dmg.set_biomech_param(21) = mat.get_biomech_param(21)*DMG;
    mat_dmg.set_biomech_param(22) = mat.get_biomech_param(22)*DMG;
    mat_dmg.set_biomech_param(23) = mat.get_biomech_param(23)*DMG;
    mat_dmg.set_biomech_param(24) = mat.get_biomech_param(24)*DMG;
    mat_dmg.set_biomech_param(25) = mat.get_biomech_param(25)*DMG;
    mat_dmg.set_biomech_param(26) = mat.get_biomech_param(26)*DMG;
    healthy::polynomial_3d_forward_2f(mat_dmg, fld, S2pk, Dtsm_uu, Dtsm_up, Ph, Dtsm_pu, Dtsm_pp);
}
//=================================================================================================
#undef DMG
//=================================================================================================
#undef _LOCAL_ARGUMENTS_2D_
#undef _LOCAL_ARGUMENTS_3D_
//=================================================================================================
} // end of namespace damaged...
//=================================================================================================
