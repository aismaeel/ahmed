/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#include "feb3/utils.h"
//=================================================================================================
// local variable
static
const Dyadic3D& I = identity_matrix;
//=================================================================================================
namespace wound_healing {
//=================================================================================================
void damage_model_511 (const MaterialInfo& mat, FieldInfo& fld) {
    const unsigned int nvars = 5;
    //
    const Real& cell = fld.biochem[0],
                ecgf = fld.biochem[1],
                oxyg = fld.biochem[2],
                mdgf = fld.biochem[3],
                vasc = fld.biochem[4];
    //
    fld.biomech[0] = material__max_damage_factor
                   - apply_bounds(0.0, cell, material__max_damage_factor);
}
//=================================================================================================
void damage_model_611 (const MaterialInfo& mat, FieldInfo& fld) {
    const unsigned int nvars = 6;
    //
    const Real& cell = fld.biochem[0],
                ecgf = fld.biochem[1],
                oxyg = fld.biochem[2],
                mdgf = fld.biochem[3],
                vasc = fld.biochem[4],
                ecm  = fld.biochem[5];
    //
    fld.biomech[0] = material__max_damage_factor
                   - apply_bounds(0.0, ecm, material__max_damage_factor);
}
//=================================================================================================
} // end of namespace wound_healing...
//=================================================================================================
//
//
//
//=================================================================================================
namespace wound_healing {
//=================================================================================================
#ifdef _LOCAL_ARGUMENTS_2D_
    #error "an unexpected error occurred!"
#else
    #define _LOCAL_ARGUMENTS_2D_ \
        const MaterialInfo& mat, \
        const FieldInfo& fld,    \
        Dyadic2D& S2pk,          \
        Quindic2D& Dtsm
#endif
#ifdef _LOCAL_ARGUMENTS_3D_
    #error "an unexpected error occurred!"
#else
    #define _LOCAL_ARGUMENTS_3D_ \
        const MaterialInfo& mat, \
        const FieldInfo& fld,    \
        Dyadic3D& S2pk,          \
        Quindic3D& Dtsm_uu,      \
        Dyadic3D& Dtsm_up,       \
        Real& Ph,                \
        Dyadic3D& Dtsm_pu,       \
        Real& Dtsm_pp
#endif
//=================================================================================================
void contraction_membrane_511 ( _LOCAL_ARGUMENTS_2D_ ) {
    const unsigned int nvars = 5;
    //
    libmesh_error();
}
//=================================================================================================
void contraction_solid_511 ( _LOCAL_ARGUMENTS_3D_ ) {
    const unsigned int nvars = 5;
    //
    const Real& cell = fld.biochem[0],
                ecgf = fld.biochem[1],
                oxyg = fld.biochem[2],
                mdgf = fld.biochem[3],
                vasc = fld.biochem[4];
    //
    const Real cell_dens = mat.get_biochem_param(8) * apply_bounds(1.0e-5, cell, 1.0), // cell / m^3
               ecm_dens  = apply_bounds(1.0e-5, vasc, 1.0),
               cell_max_press = mat.get_biochem_param(9), // N * m / cell [= (N/m^2) / (cell/m^3)]
               psi = mat.get_biochem_param(21);
    //
    const Real P_cell = psi
                      ? cell_dens * cell_max_press * (ecm_dens/(pow2(psi)+pow2(ecm_dens)))
                      : cell_dens * cell_max_press;
    //
    S2pk.comp += scale(P_cell, I).comp;
}
//=================================================================================================
void contraction_membrane_611 ( _LOCAL_ARGUMENTS_2D_ ) {
    const unsigned int nvars = 6;
    //
    libmesh_error();
}
//=================================================================================================
void contraction_solid_611 ( _LOCAL_ARGUMENTS_3D_ ) {
    const unsigned int nvars = 6;
    //
    const Real& cell = fld.biochem[0],
                ecgf = fld.biochem[1],
                oxyg = fld.biochem[2],
                mdgf = fld.biochem[3],
                vasc = fld.biochem[4],
                ecm  = fld.biochem[5];
    //
    const Real cell_dens = mat.get_biochem_param(8) * apply_bounds(1.0e-5, cell, 1.0), // cell / m^3
               ecm_dens  = apply_bounds(1.0e-5, ecm, 1.0),
               cell_max_press = mat.get_biochem_param(9), // N * m / cell [= (N/m^2) / (cell/m^3)]
               psi = mat.get_biochem_param(21);
    //
    const Real P_cell = psi
                      ? cell_dens * cell_max_press * (pow2(ecm_dens)/(pow2(psi)+pow2(ecm_dens)))
                      : cell_dens * cell_max_press;
    //
    S2pk.comp += scale(P_cell, I).comp;
}
//=================================================================================================
#undef _LOCAL_ARGUMENTS_2D_
#undef _LOCAL_ARGUMENTS_3D_
//=================================================================================================
} // end of namespace wound_healing...
//=================================================================================================
