/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#include "feb3/domain_data.h"
//=================================================================================================
// external variables
extern
OFStream flog, ferr;
// local variables
static
const Dyadic3D& I = identity_matrix;
static
const Quindic3D& I_s = identity_tensor;
//=================================================================================================
namespace healthy {
//=================================================================================================
#ifdef _LOCAL_ARGUMENTS_2D_
    #error "an unexpected error occurred!"
#else
    #define _LOCAL_ARGUMENTS_2D_ \
        const MaterialInfo& mat, \
        const FieldInfo& fld,    \
        Dyadic2D& S2pk,          \
        Quindic2D& Dtsm
#endif
#ifdef _LOCAL_ARGUMENTS_3D_
    #error "an unexpected error occurred!"
#else
    #define _LOCAL_ARGUMENTS_3D_ \
        const MaterialInfo& mat, \
        const FieldInfo& fld,    \
        Dyadic3D& S2pk,          \
        Quindic3D& Dtsm_uu,      \
        Dyadic3D& Dtsm_up,       \
        Real& Ph,                \
        Dyadic3D& Dtsm_pu,       \
        Real& Dtsm_pp
#endif
//=================================================================================================
// W = alpha_1 * (I1-3) + beta_1 * (I1-3)^2 + alpha_2 * (I2-3) + beta_2 * (I2-3)^2
//   + gamma_1 * (I1-3) * (I2-3)
//   + alpha_3 * (exp[beta_3*(I1-1)]-1)
//=================================================================================================
void polynomial_2d_forward ( _LOCAL_ARGUMENTS_2D_ ) {
    const Real& alpha_1 = mat.get_biomech_param(2), beta_1 = mat.get_biomech_param(3),
                alpha_2 = mat.get_biomech_param(4), beta_2 = mat.get_biomech_param(5),
                gamma_1 = mat.get_biomech_param(6),
                alpha_3 = mat.get_biomech_param(7), beta_3 = mat.get_biomech_param(8);
    //
    Vector3D G_[SP2D];
    covariant_basis(fld.grad_space_reference, G_);
    Vector3D g_[SP2D];
    covariant_basis(fld.grad_space_current, g_);
    //
    const Dyadic2D G_cova = metric_tensor(G_), G_cont = inverse(G_cova),
                   g_cova = metric_tensor(g_), g_cont = inverse(g_cova);
    // invariants
    const Real I1 = outer_product(g_cova, G_cont),
               I2 = determinant(g_cova)/determinant(G_cova),
               J = sqrt(I2);
    //
    const Real dW_dI1 = alpha_1+2.0*beta_1*I1-6.0*beta_1+gamma_1*I2-3.0*gamma_1
                      + alpha_3*beta_3*exp(beta_3*(I1-3));
    const Real dW_dI2 = alpha_2+2.0*beta_2*I2-6.0*beta_2+gamma_1*I1-3.0*gamma_1;
    const Real d2W_dI1dI1 = 2.0*beta_1
                          + alpha_3*pow2(beta_3)*exp(beta_3*(I1-3));
    const Real d2W_dI1dI2 = gamma_1;
    const Real d2W_dI2dI1 = gamma_1;
    const Real d2W_dI2dI2 = 2.0*beta_2;
    //
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    //
    Dyadic2D dI1_dg,
             dI2_dg;
    dI1_dg.comp = G_cont.comp(i,j);
    dI2_dg.comp = I2*g_cont.comp(i,j);
    //
    Quindic2D d2I1_dgdg,
              d2I2_dgdg;
    d2I1_dgdg.comp = 0.0;
    d2I2_dgdg.comp = g_cont.comp(i,j)*dI2_dg.comp(k,l)
                   - (0.5*I2)*g_cont.comp(i,k)*g_cont.comp(j,l)
                   - (0.5*I2)*g_cont.comp(i,l)*g_cont.comp(j,k);
    //
    Dyadic2D d2W_dI1dg,
             d2W_dI2dg;
    d2W_dI1dg.comp = d2W_dI1dI1*dI1_dg.comp(i,j) + d2W_dI1dI2*dI2_dg.comp(i,j);
    d2W_dI2dg.comp = d2W_dI2dI1*dI1_dg.comp(i,j) + d2W_dI2dI2*dI2_dg.comp(i,j);
    //
    S2pk.comp += 2.0*dW_dI1*dI1_dg.comp(i,j)
               + 2.0*dW_dI2*dI2_dg.comp(i,j);
    //
    Dtsm.comp = 4.0*d2W_dI1dg.comp(k,l)*dI1_dg.comp(i,j) + 4.0*dW_dI1*d2I1_dgdg.comp(i,j,k,l)
              + 4.0*d2W_dI2dg.comp(k,l)*dI2_dg.comp(i,j) + 4.0*dW_dI2*d2I2_dgdg.comp(i,j,k,l);
}
//=================================================================================================
#define C    inv_inf.C
#define Ci   inv_inf.Ci
#define C2   inv_inf.C2
#define Ci_s inv_inf.Ci_s
#define J1   inv_inf.J1
#define J2   inv_inf.J2
#define J3   inv_inf.J3
#define J4   inv_inf.J4
#define J6   inv_inf.J6
#define J8   inv_inf.J8
#define dJ1_dC   inv_inf.dJ1_dC
#define dJ2_dC   inv_inf.dJ2_dC
#define dJ3_dC   inv_inf.dJ3_dC
#define dJ4_dC   inv_inf.dJ4_dC
#define dJ6_dC   inv_inf.dJ6_dC
#define dJ8_dC   inv_inf.dJ8_dC
#define d2J1_dCdC   inv_inf.d2J1_dCdC
#define d2J2_dCdC   inv_inf.d2J2_dCdC
#define d2J3_dCdC   inv_inf.d2J3_dCdC
#define d2J4_dCdC   inv_inf.d2J4_dCdC
#define d2J6_dCdC   inv_inf.d2J6_dCdC
#define d2J8_dCdC   inv_inf.d2J8_dCdC
//=================================================================================================
// W = alpha_1 * (I1-3) + beta_1 * (I1-3)^2 ...
//   + alpha_2 * (I2-3) + beta_2 * (I2-3)^2 ...
//   + gamma_1 * (I1-3) * (I2-3) ...
//   + kappa_0 * (J3-1)^2 + kappa_1 * (J3^2-1) - lambda_1 * log(J3) + lambda_2 * (log(J3))^2 ...
//=================================================================================================
void polynomial_3d_forward_std ( _LOCAL_ARGUMENTS_3D_ ) {
    const Real& alpha_1 = mat.get_biomech_param(2), beta_1 = mat.get_biomech_param(3),
                alpha_2 = mat.get_biomech_param(4), beta_2 = mat.get_biomech_param(5),
                gamma_1 = mat.get_biomech_param(6),
                kappa_0  = mat.get_biomech_param(7), kappa_1  = mat.get_biomech_param(8),
                lambda_1 = mat.get_biomech_param(9), lambda_2 = mat.get_biomech_param(10);
    //
    const InvariantsInfo inv_inf(right_CauchyGreen_deformation(fld.deform_grad_elas), fld.fibre_ref);
    //
    const Real dW_dJ1 = alpha_1*pow(J3,frac_2o3)+2.0*beta_1*pow(J3,frac_4o3)*J1-6.0*beta_1*pow(J3,frac_2o3)+gamma_1*J3*J3*J2-3.0*gamma_1*pow(J3,frac_2o3);
    const Real dW_dJ2 = alpha_2*pow(J3,frac_4o3)+2.0*beta_2*pow(J3,frac_8o3)*J2-6.0*beta_2*pow(J3,frac_4o3)+gamma_1*J3*J3*J1-3.0*gamma_1*pow(J3,frac_4o3);
    const Real dW_dJ3 = 2.0*kappa_1*J3+(2.0*alpha_1*J1*J3+4.0*beta_1*J1*J1*pow(J3,frac_5o3)-12.0*beta_1*J1*J3+4.0*alpha_2*J2*pow(J3,frac_5o3)+8.0*beta_2*J2*J2*J3*J3*J3-24.0*beta_2*J2*pow(J3,frac_5o3)+6.0*gamma_1*J1*pow(J3,frac_7o3)*J2-6.0*gamma_1*J1*J3-12.0*gamma_1*J2*pow(J3,frac_5o3)+6.0*kappa_0*pow(J3,frac_7o3)-6.0*kappa_0*pow(J3,frac_4o3)-3.0*lambda_1*pow(J3,frac_1o3)+6.0*lambda_2*log(J3)*pow(J3,frac_1o3))/pow(J3,frac_4o3)*frac_1o3;
    const Real d2W_dJ1dJ1 = 2.0*beta_1*pow(J3,frac_4o3);
    const Real d2W_dJ1dJ2 = gamma_1*J3*J3;
    const Real d2W_dJ1dJ3 = frac_2o3*(alpha_1+4.0*beta_1*J1*pow(J3,frac_2o3)-6.0*beta_1+3.0*gamma_1*J2*pow(J3,frac_4o3)-3.0*gamma_1)/pow(J3,frac_1o3);
    const Real d2W_dJ2dJ1 = gamma_1*J3*J3;
    const Real d2W_dJ2dJ2 = 2.0*beta_2*pow(J3,frac_8o3);
    const Real d2W_dJ2dJ3 = frac_4o3*alpha_2*pow(J3,frac_1o3)+frac_16o3*beta_2*J2*pow(J3,frac_5o3)-8.0*beta_2*pow(J3,frac_1o3)+2.0*gamma_1*J1*J3-4.0*gamma_1*pow(J3,frac_1o3);
    const Real d2W_dJ3dJ1 = frac_2o3*(alpha_1+4.0*beta_1*J1*pow(J3,frac_2o3)-6.0*beta_1+3.0*gamma_1*J2*pow(J3,frac_4o3)-3.0*gamma_1)/pow(J3,frac_1o3);
    const Real d2W_dJ3dJ2 = frac_4o3*alpha_2*pow(J3,frac_1o3)+frac_16o3*beta_2*J2*pow(J3,frac_5o3)-8.0*beta_2*pow(J3,frac_1o3)+2.0*gamma_1*J1*J3-4.0*gamma_1*pow(J3,frac_1o3);
    const Real d2W_dJ3dJ3 = 2.0*kappa_1-1.0/pow(J3,frac_7o3)*(2.0*alpha_1*J1*J3-4.0*beta_1*J1*J1*pow(J3,frac_5o3)-12.0*beta_1*J1*J3-4.0*alpha_2*J2*pow(J3,frac_5o3)-40.0*beta_2*J2*J2*J3*J3*J3+24.0*beta_2*J2*pow(J3,frac_5o3)-18.0*gamma_1*J1*pow(J3,frac_7o3)*J2-6.0*gamma_1*J1*J3+12.0*gamma_1*J2*pow(J3,frac_5o3)-18.0*kappa_0*pow(J3,frac_7o3)-9.0*lambda_1*pow(J3,frac_1o3)-18.0*lambda_2*pow(J3,frac_1o3)+18.0*lambda_2*log(J3)*pow(J3,frac_1o3))*frac_1o9;
    //
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    //
    Dyadic3D d2W_dJ1dC,
             d2W_dJ2dC,
             d2W_dJ3dC;
    d2W_dJ1dC.comp = d2W_dJ1dJ1*dJ1_dC.comp(i,j) + d2W_dJ1dJ2*dJ2_dC.comp(i,j) + d2W_dJ1dJ3*dJ3_dC.comp(i,j);
    d2W_dJ2dC.comp = d2W_dJ2dJ1*dJ1_dC.comp(i,j) + d2W_dJ2dJ2*dJ2_dC.comp(i,j) + d2W_dJ2dJ3*dJ3_dC.comp(i,j);
    d2W_dJ3dC.comp = d2W_dJ3dJ1*dJ1_dC.comp(i,j) + d2W_dJ3dJ2*dJ2_dC.comp(i,j) + d2W_dJ3dJ3*dJ3_dC.comp(i,j);
    //
    S2pk.comp += (2.0*dW_dJ1)*dJ1_dC.comp(i,j)
               + (2.0*dW_dJ2)*dJ2_dC.comp(i,j)
               + (2.0*dW_dJ3)*dJ3_dC.comp(i,j);
    //
    Dtsm_uu.comp = 4.0*d2W_dJ1dC.comp(k,l)*dJ1_dC.comp(i,j) + (4.0*dW_dJ1)*d2J1_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ2dC.comp(k,l)*dJ2_dC.comp(i,j) + (4.0*dW_dJ2)*d2J2_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ3dC.comp(k,l)*dJ3_dC.comp(i,j) + (4.0*dW_dJ3)*d2J3_dCdC.comp(i,j,k,l);
    //
    if ( ! are_equal(fld.deform_grad_inel, I) ) {
        const Dyadic3D Fi_p = inverse(fld.deform_grad_inel);
        //
        Quindic3D T_p;
        T_p.comp = 0.5*Fi_p.comp(k,i)*Fi_p.comp(j,l)
                 + 0.5*Fi_p.comp(l,i)*Fi_p.comp(k,j);
        //
        Dtsm_uu = mul_ijmn_mnkl(Dtsm_uu, T_p);
        //
        Dtsm_up = mul_mn_mnij(Dtsm_up, T_p);
        //
        Dtsm_pu = Dtsm_up;
    }
}
//=================================================================================================
// W = alpha_1 * (J1-3) + beta_1 * (J1-3)^2 ...
//   + alpha_2 * (J2-3) + beta_2 * (J2-3)^2 ...
//   + gamma_1 * (J1-3) * (J2-3) ...
//   + kappa_0 * (J3-1)^2 + kappa_1 * (J3^2-1) - lambda_1 * log(J3) + lambda_2 * (log(J3))^2 ...
//   + ksi4_0 * (J4-3) + ksi4_1 * (J4-3)^2 ...
//   + ksi6_0 * (J6-3) + ksi6_1 * (J6-3)^2 ...
//   + ksi8_0 * (J8-3) + ksi8_1 * (J8-3)^2 ...
//   + delta1_0 * (exp[delta1_1*(J1-3)] - 1) ...
//   + delta2_0 * (exp[delta2_1*(J2-3)] - 1) ...
//   + epsilon4_0 * (exp[epsilon4_1*(J4-1)^2] - 1) ...
//   + epsilon6_0 * (exp[epsilon6_1*(J6-1)^2] - 1) ...
//   + epsilon8_0 * (exp[epsilon8_1*(J8-1)^2] - 1) ...
//=================================================================================================
void polynomial_3d_forward_mod ( _LOCAL_ARGUMENTS_3D_ ) {
    const Real& alpha_1 = mat.get_biomech_param(2), beta_1 = mat.get_biomech_param(3),
                alpha_2 = mat.get_biomech_param(4), beta_2 = mat.get_biomech_param(5),
                gamma_1 = mat.get_biomech_param(6),
                kappa_0  = mat.get_biomech_param(7), kappa_1  = mat.get_biomech_param(8),
                lambda_1 = mat.get_biomech_param(9), lambda_2 = mat.get_biomech_param(10);
    const Real& ksi4_0   = mat.get_biomech_param(11), ksi4_1  = mat.get_biomech_param(12),
                ksi6_0   = mat.get_biomech_param(13), ksi6_1  = mat.get_biomech_param(14),
                ksi8_0   = mat.get_biomech_param(15), ksi8_1  = mat.get_biomech_param(16),
                delta1_0 = mat.get_biomech_param(17), delta1_1 = mat.get_biomech_param(18),
                delta2_0 = mat.get_biomech_param(19), delta2_1 = mat.get_biomech_param(20),
                epsilon4_0 = mat.get_biomech_param(21), epsilon4_1 = mat.get_biomech_param(22),
                epsilon6_0 = mat.get_biomech_param(23), epsilon6_1 = mat.get_biomech_param(24),
                epsilon8_0 = mat.get_biomech_param(25), epsilon8_1 = mat.get_biomech_param(26);
    //
    const InvariantsInfo inv_inf(right_CauchyGreen_deformation(fld.deform_grad_elas), fld.fibre_ref);
    //
    const Real dW_dJ1 = alpha_1+2.0*beta_1*J1-6.0*beta_1+gamma_1*J2-3.0*gamma_1+delta1_0*delta1_1*exp(delta1_1*(J1-3.0));
    const Real dW_dJ2 = alpha_2+2.0*beta_2*J2-6.0*beta_2+gamma_1*J1-3.0*gamma_1+delta2_0*delta2_1*exp(delta2_1*(J2-3.0));
    const Real dW_dJ3 = (2.0*kappa_0*J3-2.0*kappa_0+2.0*kappa_1*J3)-(lambda_1-2.0*lambda_2*log(J3))/J3;
    const Real d2W_dJ1dJ1 = 2.0*beta_1+delta1_0*delta1_1*delta1_1*exp(delta1_1*(J1-3.0));
    const Real d2W_dJ1dJ2 = gamma_1;
    const Real d2W_dJ2dJ1 = gamma_1;
    const Real d2W_dJ2dJ2 = 2.0*beta_2+delta2_0*delta2_1*delta2_1*exp(delta2_1*(J2-3.0));
    const Real d2W_dJ3dJ3 = (2.0*kappa_0+2.0*kappa_1)+(lambda_1+2.0*lambda_2-2.0*lambda_2*log(J3))/(J3*J3);
    const Real dW_dJ4 = ksi4_0+2.0*ksi4_1*(J4-3.0)+2.0*epsilon4_0*epsilon4_1*(J4-1.0)*exp(epsilon4_1*pow2(J4-1.0));
    const Real dW_dJ6 = ksi6_0+2.0*ksi6_1*(J6-3.0)+2.0*epsilon6_0*epsilon6_1*(J6-1.0)*exp(epsilon6_1*pow2(J6-1.0));
    const Real dW_dJ8 = ksi8_0+2.0*ksi8_1*(J8-3.0)+2.0*epsilon8_0*epsilon8_1*(J8-1.0)*exp(epsilon8_1*pow2(J8-1.0));
    const Real d2W_dJ4dJ4 = 2.0*ksi4_1+2.0*epsilon4_0*epsilon4_1*exp(epsilon4_1*pow2(J4-1.0))+4.0*epsilon4_0*pow2(epsilon4_1*(J4-1.0))*exp(epsilon4_1*pow2(J4-1.0));
    const Real d2W_dJ6dJ6 = 2.0*ksi6_1+2.0*epsilon6_0*epsilon6_1*exp(epsilon6_1*pow2(J6-1.0))+4.0*epsilon6_0*pow2(epsilon6_1*(J6-1.0))*exp(epsilon6_1*pow2(J6-1.0));
    const Real d2W_dJ8dJ8 = 2.0*ksi8_1+2.0*epsilon8_0*epsilon8_1*exp(epsilon8_1*pow2(J8-1.0))+4.0*epsilon8_0*pow2(epsilon8_1*(J8-1.0))*exp(epsilon8_1*pow2(J8-1.0));
    //
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    //
    Dyadic3D d2W_dJ1dC,
             d2W_dJ2dC,
             d2W_dJ3dC;
    d2W_dJ1dC.comp = d2W_dJ1dJ1*dJ1_dC.comp(i,j) + d2W_dJ1dJ2*dJ2_dC.comp(i,j);
    d2W_dJ2dC.comp = d2W_dJ2dJ1*dJ1_dC.comp(i,j) + d2W_dJ2dJ2*dJ2_dC.comp(i,j);
    d2W_dJ3dC.comp = d2W_dJ3dJ3*dJ3_dC.comp(i,j);
    Dyadic3D d2W_dJ4dC,
             d2W_dJ6dC,
             d2W_dJ8dC;
    d2W_dJ4dC.comp = d2W_dJ4dJ4*dJ4_dC.comp(i,j);
    d2W_dJ6dC.comp = d2W_dJ6dJ6*dJ6_dC.comp(i,j);
    d2W_dJ8dC.comp = d2W_dJ8dJ8*dJ8_dC.comp(i,j);
    //
    S2pk.comp += (2.0*dW_dJ1)*dJ1_dC.comp(i,j)
               + (2.0*dW_dJ2)*dJ2_dC.comp(i,j)
               + (2.0*dW_dJ3)*dJ3_dC.comp(i,j)
               + (2.0*dW_dJ4)*dJ4_dC.comp(i,j)
               + (2.0*dW_dJ6)*dJ6_dC.comp(i,j)
               + (2.0*dW_dJ8)*dJ8_dC.comp(i,j);
    //
    Dtsm_uu.comp = 4.0*d2W_dJ1dC.comp(k,l)*dJ1_dC.comp(i,j) + (4.0*dW_dJ1)*d2J1_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ2dC.comp(k,l)*dJ2_dC.comp(i,j) + (4.0*dW_dJ2)*d2J2_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ3dC.comp(k,l)*dJ3_dC.comp(i,j) + (4.0*dW_dJ3)*d2J3_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ4dC.comp(k,l)*dJ4_dC.comp(i,j) + (4.0*dW_dJ4)*d2J4_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ6dC.comp(k,l)*dJ6_dC.comp(i,j) + (4.0*dW_dJ6)*d2J6_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ8dC.comp(k,l)*dJ8_dC.comp(i,j) + (4.0*dW_dJ8)*d2J8_dCdC.comp(i,j,k,l);
    //
    if ( ! are_equal(fld.deform_grad_inel, I) ) {
        const Dyadic3D Fi_p = inverse(fld.deform_grad_inel);
        //
        Quindic3D T_p;
        T_p.comp = 0.5*Fi_p.comp(k,i)*Fi_p.comp(j,l)
                 + 0.5*Fi_p.comp(l,i)*Fi_p.comp(k,j);
        //
        Dtsm_uu = mul_ijmn_mnkl(Dtsm_uu, T_p);
        //
        Dtsm_up = mul_mn_mnij(Dtsm_up, T_p);
        //
        Dtsm_pu = Dtsm_up;
    }
}
//=================================================================================================
// W = alpha_1 * (J1-3) + beta_1 * (J1-3)^2 ...
//   + alpha_2 * (J2-3) + beta_2 * (J2-3)^2 ...
//   + gamma_1 * (J1-3) * (J2-3) ...
//   + kappa_0 * (J3-1)^2 + kappa_1 * (J3^2-1) - lambda_1 * log(J3) + lambda_2 * (log(J3))^2 ...
//   + ksi4_0 * (J4-3) + ksi4_1 * (J4-3)^2 ...
//   + ksi6_0 * (J6-3) + ksi6_1 * (J6-3)^2 ...
//   + ksi8_0 * (J8-3) + ksi8_1 * (J8-3)^2 ...
//   + delta1_0 * (exp[delta1_1*(J1-3)] - 1) ...
//   + delta2_0 * (exp[delta2_1*(J2-3)] - 1) ...
//   + epsilon4_0 * (exp[epsilon4_1*(J4-1)^2] - 1) ...
//   + epsilon6_0 * (exp[epsilon6_1*(J6-1)^2] - 1) ...
//   + epsilon8_0 * (exp[epsilon8_1*(J8-1)^2] - 1) ...
//=================================================================================================
void polynomial_3d_forward_2f ( _LOCAL_ARGUMENTS_3D_ ) {
    const Real& alpha_1 = mat.get_biomech_param(2), beta_1 = mat.get_biomech_param(3),
                alpha_2 = mat.get_biomech_param(4), beta_2 = mat.get_biomech_param(5),
                gamma_1 = mat.get_biomech_param(6),
                kappa_0  = mat.get_biomech_param(7), kappa_1  = mat.get_biomech_param(8),
                lambda_1 = mat.get_biomech_param(9), lambda_2 = mat.get_biomech_param(10);
    const Real& ksi4_0   = mat.get_biomech_param(11), ksi4_1  = mat.get_biomech_param(12),
                ksi6_0   = mat.get_biomech_param(13), ksi6_1  = mat.get_biomech_param(14),
                ksi8_0   = mat.get_biomech_param(15), ksi8_1  = mat.get_biomech_param(16),
                delta1_0 = mat.get_biomech_param(17), delta1_1 = mat.get_biomech_param(18),
                delta2_0 = mat.get_biomech_param(19), delta2_1 = mat.get_biomech_param(20),
                epsilon4_0 = mat.get_biomech_param(21), epsilon4_1 = mat.get_biomech_param(22),
                epsilon6_0 = mat.get_biomech_param(23), epsilon6_1 = mat.get_biomech_param(24),
                epsilon8_0 = mat.get_biomech_param(25), epsilon8_1 = mat.get_biomech_param(26);
    //
    const InvariantsInfo inv_inf(right_CauchyGreen_deformation(fld.deform_grad_elas), fld.fibre_ref);
    //
    const Real& p_t = fld.pressure;
    //
    const Real dW_dJ1 = alpha_1+2.0*beta_1*J1-6.0*beta_1+gamma_1*J2-3.0*gamma_1+delta1_0*delta1_1*exp(delta1_1*(J1-3.0));
    const Real dW_dJ2 = alpha_2+2.0*beta_2*J2-6.0*beta_2+gamma_1*J1-3.0*gamma_1+delta2_0*delta2_1*exp(delta2_1*(J2-3.0));
    const Real dW_dJ3 = (2.0*kappa_0*J3-2.0*kappa_0+2.0*kappa_1*J3)-(lambda_1-2.0*lambda_2*log(J3))/J3;
    const Real d2W_dJ1dJ1 = 2.0*beta_1+delta1_0*delta1_1*delta1_1*exp(delta1_1*(J1-3.0));
    const Real d2W_dJ1dJ2 = gamma_1;
    const Real d2W_dJ2dJ1 = gamma_1;
    const Real d2W_dJ2dJ2 = 2.0*beta_2+delta2_0*delta2_1*delta2_1*exp(delta2_1*(J2-3.0));
    const Real d2W_dJ3dJ3 = (2.0*kappa_0+2.0*kappa_1)+(lambda_1+2.0*lambda_2-2.0*lambda_2*log(J3))/(J3*J3);
    const Real dW_dJ4 = ksi4_0+2.0*ksi4_1*(J4-3.0)+2.0*epsilon4_0*epsilon4_1*(J4-1.0)*exp(epsilon4_1*pow2(J4-1.0));
    const Real dW_dJ6 = ksi6_0+2.0*ksi6_1*(J6-3.0)+2.0*epsilon6_0*epsilon6_1*(J6-1.0)*exp(epsilon6_1*pow2(J6-1.0));
    const Real dW_dJ8 = ksi8_0+2.0*ksi8_1*(J8-3.0)+2.0*epsilon8_0*epsilon8_1*(J8-1.0)*exp(epsilon8_1*pow2(J8-1.0));
    const Real d2W_dJ4dJ4 = 2.0*ksi4_1+2.0*epsilon4_0*epsilon4_1*exp(epsilon4_1*pow2(J4-1.0))+4.0*epsilon4_0*pow2(epsilon4_1*(J4-1.0))*exp(epsilon4_1*pow2(J4-1.0));
    const Real d2W_dJ6dJ6 = 2.0*ksi6_1+2.0*epsilon6_0*epsilon6_1*exp(epsilon6_1*pow2(J6-1.0))+4.0*epsilon6_0*pow2(epsilon6_1*(J6-1.0))*exp(epsilon6_1*pow2(J6-1.0));
    const Real d2W_dJ8dJ8 = 2.0*ksi8_1+2.0*epsilon8_0*epsilon8_1*exp(epsilon8_1*pow2(J8-1.0))+4.0*epsilon8_0*pow2(epsilon8_1*(J8-1.0))*exp(epsilon8_1*pow2(J8-1.0));
    //
    const Real d3W_dJ3dJ3dJ3 = -2.0*(lambda_1+3.0*lambda_2-2.0*lambda_2*log(J3))/(J3*J3*J3);
    const Real d4W_dJ3dJ3dJ3dJ3 = 2.0*(3.0*lambda_1+11.0*lambda_2-6.0*lambda_2*log(J3))/(J3*J3*J3*J3);
    //
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    //
    Dyadic3D d2W_dJ1dC,
             d2W_dJ2dC,
             d2W_dJ3dC;
    d2W_dJ1dC.comp = d2W_dJ1dJ1*dJ1_dC.comp(i,j) + d2W_dJ1dJ2*dJ2_dC.comp(i,j);
    d2W_dJ2dC.comp = d2W_dJ2dJ1*dJ1_dC.comp(i,j) + d2W_dJ2dJ2*dJ2_dC.comp(i,j);
    d2W_dJ3dC.comp = d2W_dJ3dJ3*dJ3_dC.comp(i,j);
    Dyadic3D d2W_dJ4dC,
             d2W_dJ6dC,
             d2W_dJ8dC;
    d2W_dJ4dC.comp = d2W_dJ4dJ4*dJ4_dC.comp(i,j);
    d2W_dJ6dC.comp = d2W_dJ6dJ6*dJ6_dC.comp(i,j);
    d2W_dJ8dC.comp = d2W_dJ8dJ8*dJ8_dC.comp(i,j);
    //
    const Real s_p = -1.0,
               s_Q = +1.0;
    const Real p = s_p*dW_dJ3,
               Q = s_Q*d2W_dJ3dJ3; // dp_dJ
    const Real dp_dJ3 = s_p*d2W_dJ3dJ3,
               dQ_dJ3 = s_Q*d3W_dJ3dJ3dJ3;
    const Real d2p_dJ3dJ3 = s_p*d3W_dJ3dJ3dJ3,
               d2Q_dJ3dJ3 = s_Q*d4W_dJ3dJ3dJ3dJ3;
    //
    Dyadic3D dp_dC,
             dQ_dC;
    dp_dC.comp = dp_dJ3*dJ3_dC.comp(i,j);
    dQ_dC.comp = dQ_dJ3*dJ3_dC.comp(i,j); //###
    //
    Quindic3D d2p_dCdC,
              d2Q_dCdC;
    d2p_dCdC.comp = dp_dJ3*d2J3_dCdC.comp(i,j,k,l) + d2p_dJ3dJ3*dJ3_dC.comp(i,j)*dJ3_dC.comp(k,l); //### 2
    d2Q_dCdC.comp = dQ_dJ3*d2J3_dCdC.comp(i,j,k,l) + d2Q_dJ3dJ3*dJ3_dC.comp(i,j)*dJ3_dC.comp(k,l); //###
    //
    S2pk.comp += (2.0*dW_dJ1)*dJ1_dC.comp(i,j)
               + (2.0*dW_dJ2)*dJ2_dC.comp(i,j)
               + (2.0*dW_dJ3)*dJ3_dC.comp(i,j)
               + (2.0*dW_dJ4)*dJ4_dC.comp(i,j)
               + (2.0*dW_dJ6)*dJ6_dC.comp(i,j)
               + (2.0*dW_dJ8)*dJ8_dC.comp(i,j)
               - (2.0*(p-p_t)/Q)*dp_dC.comp(i,j)
               + (pow2(p-p_t)/pow2(Q))*dQ_dC.comp(i,j); //###
    //
    Ph = (1.0/Q)*(p-p_t);
    //
    Dtsm_uu.comp = 4.0*d2W_dJ1dC.comp(k,l)*dJ1_dC.comp(i,j) + (4.0*dW_dJ1)*d2J1_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ2dC.comp(k,l)*dJ2_dC.comp(i,j) + (4.0*dW_dJ2)*d2J2_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ3dC.comp(k,l)*dJ3_dC.comp(i,j) + (4.0*dW_dJ3)*d2J3_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ4dC.comp(k,l)*dJ4_dC.comp(i,j) + (4.0*dW_dJ4)*d2J4_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ6dC.comp(k,l)*dJ6_dC.comp(i,j) + (4.0*dW_dJ6)*d2J6_dCdC.comp(i,j,k,l)
                 + 4.0*d2W_dJ8dC.comp(k,l)*dJ8_dC.comp(i,j) + (4.0*dW_dJ8)*d2J8_dCdC.comp(i,j,k,l)
                 - (4.0/Q)*dp_dC.comp(i,j)*dp_dC.comp(k,l)
                 + (4.0*(p-p_t)/pow2(Q))*(dp_dC.comp(i,j)*dQ_dC.comp(k,l)+dQ_dC.comp(i,j)*dp_dC.comp(k,l)) //### 1+2
                 - (4.0*(p-p_t)/Q)*d2p_dCdC.comp(i,j,k,l) //###
                 - (4.0*pow2(p-p_t)/pow3(Q))*dQ_dC.comp(i,j)*dQ_dC.comp(k,l) //###
                 + (2.0*pow2(p-p_t)/pow2(Q))*d2Q_dCdC.comp(i,j,k,l); //###
    //
    Dtsm_up.comp = (2.0/Q)*dp_dC.comp(i,j)
                 - (2.0*(p-p_t)/pow2(Q))*dQ_dC.comp(i,j); //###
    //
    Dtsm_pu.comp = Dtsm_up.comp;
    //
    Dtsm_pp = (-1.0/Q);
    //
    if ( ! are_equal(fld.deform_grad_inel, I) ) {
        const Dyadic3D Fi_p = inverse(fld.deform_grad_inel);
        //
        Quindic3D T_p;
        T_p.comp = 0.5*Fi_p.comp(k,i)*Fi_p.comp(j,l)
                 + 0.5*Fi_p.comp(l,i)*Fi_p.comp(k,j);
        //
        Dtsm_uu = mul_ijmn_mnkl(Dtsm_uu, T_p);
        //
        Dtsm_up = mul_mn_mnij(Dtsm_up, T_p);
        //
        Dtsm_pu = Dtsm_up;
    }
}
//=================================================================================================
#undef C
#undef Ci
#undef C2
#undef Ci_s
#undef J1
#undef J2
#undef J3
#undef J4
#undef J6
#undef J8
#undef dJ1_dC
#undef dJ2_dC
#undef dJ3_dC
#undef dJ4_dC
#undef dJ6_dC
#undef dJ8_dC
#undef d2J1_dCdC
#undef d2J2_dCdC
#undef d2J3_dCdC
#undef d2J4_dCdC
#undef d2J6_dCdC
#undef d2J8_dCdC
//=================================================================================================
#undef _LOCAL_ARGUMENTS_2D_
#undef _LOCAL_ARGUMENTS_3D_
//=================================================================================================
} // end of namespace healthy...
//=================================================================================================
