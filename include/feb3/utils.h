/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __UTILS_H__
#define __UTILS_H__
//=================================================================================================
#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include "feb3/auxiliary.h"
#include "libmesh/point.h"
#include "libmesh/getpot.h"
#include "libmesh/parallel.h"
#include "libmesh/vector_value.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/elem.h"
#include "libmesh/point.h"
#include "libmesh/tensor_value.h"
#include "gsl/gsl_math.h"
#include "gsl/gsl_errno.h"
#include "gsl/gsl_poly.h"
#include "gsl/gsl_eigen.h"
#include "libmesh/system.h"
#include <functional>
#include <random>
#include <stdio.h>
//=================================================================================================
// forward class declarations
template<class X> class Dual;
template<class X> class Triple;
template<class X, int N> class Vector;
template<class X, int N> class Dyadic;
template<class X, int N> class Triadic;
template<class X, int N> class Quindic;
template<class X, int N> class Sextic;
class Vector3DGradient;
//-------------------------------------------------------------------------------------------------
// compact type definitions
typedef Vector<Real, VOIGT_SP3D>  VectorVoigt3D;
typedef Dyadic<Real, VOIGT_SP3D>  DyadicVoigt3D;
typedef Vector<Real, SP3D>        Vector3D;
typedef Dyadic<Real, SP3D>        Dyadic3D;
typedef Triadic<Real, SP3D>       Triadic3D;
typedef Quindic<Real, SP3D>       Quindic3D;
typedef Sextic<Real, SP3D>        Sextic3D;
typedef Vector<Real, VOIGT_SP2D>  VectorVoigt2D;
typedef Dyadic<Real, VOIGT_SP2D>  DyadicVoigt2D;
typedef Vector<Real, SP2D>        Vector2D;
typedef Dyadic<Real, SP2D>        Dyadic2D;
typedef Triadic<Real, SP2D>       Triadic2D;
typedef Quindic<Real, SP2D>       Quindic2D;
typedef Sextic<Real, SP2D>        Sextic2D;
typedef blitz::Array<Point, 1>          Blitz_VectorPoint;
typedef blitz::Array<Real, 1>           Blitz_VectorReal;
typedef blitz::Array<Real, 2>           Blitz_DyadicReal;
typedef blitz::Array<RealGradient, 1>   Blitz_VectorGradient;
typedef blitz::Array<RealGradient, 2>   Blitz_DyadicGradient;
typedef blitz::Array<int, 1>            Blitz_VectorInt;
typedef blitz::Array<int, 2>            Blitz_DyadicInt;
typedef Dual<unsigned int>      DualUInt;
typedef Triple<unsigned int>    TripleUInt;
typedef std::set<unsigned int>  SetUInt;
typedef TensorValue<Real>       Tensor2o;
typedef VectorValue<Real>       Displacement;
typedef VectorValue<Real>       Velocity;
typedef Vector3DGradient        DisplacementGradient;
typedef Vector3DGradient        VelocityGradient;
typedef dof_id_type             material_id_type;
typedef subdomain_id_type       region_id_type;
typedef std::pair<dof_id_type, dof_id_type>  pair_dof_id_type;
//=================================================================================================
// damage factor margins
const Real material__min_damage_factor = 0.0000,
           material__max_damage_factor = 0.9999;
//=================================================================================================
enum ProblemCase {
    NO_SOLVER                =    0,
    FEM_BIOMECHANICS_SOLVER  = 1001, IGA_BIOMECHANICS_SOLVER  = 2001,
    FEM_BIOCHEMICS_SOLVER    = 1002, FEM_INVITRO_TUMOROID_SOLVER = 2002, FEM_RT_TUMOUR_FIBROSIS_SOLVER = 3002,
    FEM_WOUND_HEALING_SOLVER       = 1003,
    FEM_TUMOUR_ANGIOGENESIS_SOLVER = 1004
};
//=================================================================================================
struct Pointer2Node_Comparison {
    bool operator() (const Node* lhs, const Node* rhs) const {
        return lhs->id() < rhs->id();
    }
};
//-------------------------------------------------------------------------------------------------
struct Pointer2Elem_Comparison {
    bool operator() (const Elem* lhs, const Elem* rhs) const {
        return lhs->id() < rhs->id();
    }
};
//=================================================================================================
template<class X>
class Dual {
public:
    Dual () : _first(X()), _second(X()) {}
    Dual (const X& a, const X& b) : _first(a), _second(b) {}
    Dual (const Dual<X>& in) { _first=in._first; _second=in._second; }
    ~Dual () {}
    //
    inline
    Dual& operator= (const Dual<X>& in) { this->_first=in.get_first(); this->_second=in.get_second(); return *this; }
    inline
    const X& operator() (int i) const { if (i==0) return this->_first; else if (i==1) return this->_second; else libmesh_error(); }
    inline
    X&       operator() (int i)       { if (i==0) return this->_first; else if (i==1) return this->_second; else libmesh_error(); }
    inline
    const X& get_first () const { return this->_first; }
    inline
    const X& get_second () const { return this->_second; }
private:
    X _first, _second;
};
//-------------------------------------------------------------------------------------------------
template<class X>
class Triple {
public:
    Triple () : _first(X()), _second(X()), _third(X()) {}
    Triple (const X& a, const X& b, const X& c) : _first(a), _second(b), _third(c) {}
    Triple (const Triple<X>& in) { _first=in._first; _second=in._second; _third=in._third; }
    ~Triple () {}
    //
    inline
    Triple& operator= (const Triple<X>& in) { this->_first=in.get_first(); this->_second=in.get_second(); this->_third=in.get_third(); return *this; }
    inline
    const X& operator() (int i) const { if (i==0) return this->_first; else if (i==1) return this->_second; else if (i==2) return this->_third; else libmesh_error(); }
    inline
    X&       operator() (int i)       { if (i==0) return this->_first; else if (i==1) return this->_second; else if (i==2) return this->_third; else libmesh_error(); }
    inline
    const X& get_first () const { return this->_first; }
    inline
    const X& get_second () const { return this->_second; }
    inline
    const X& get_third () const { return this->_third; }
private:
    X _first, _second, _third;
};
//-------------------------------------------------------------------------------------------------
template<class X, class Y>
class my_pair {
public:
    my_pair () : _first(X()), _second(Y()) {}
    my_pair (const X& a, const Y& b) : _first(a), _second(b) {}
    my_pair (const my_pair<X,Y>& in) { _first=in._first; _second=in._second; }
    ~my_pair () {}
    //
    inline
    my_pair& operator= (const my_pair<X,Y>& in) { this->_first=in.get_first(); this->_second=in.get_second(); return *this; }
    inline
    const X& get_first () const { return this->_first; }
    inline
    const Y& get_second () const { return this->_second; }
private:
    X _first;
    Y _second;
};
//-------------------------------------------------------------------------------------------------
template<class X, class Y, class Z>
class my_triple {
public:
    my_triple () : _first(X()), _second(Y()), _third(Z()) {}
    my_triple (const X& a, const Y& b, const Z& c) : _first(a), _second(b), _third(c) {}
    my_triple (const my_triple<X,Y,Z>& in) { _first=in._first; _second=in._second; _third=in._third; }
    ~my_triple () {}
    //
    inline
    my_triple& operator= (const my_triple<X,Y,Z>& in) { this->_first=in.get_first(); this->_second=in.get_second(); this->_third=in.get_third(); return *this; }
    inline
    const X& get_first () const { return this->_first; }
    inline
    const Y& get_second () const { return this->_second; }
    inline
    const Z& get_third () const { return this->_third; }
private:
    X _first;
    Y _second;
    Z _third;
};
//-------------------------------------------------------------------------------------------------
template<class X, int N>
class Vector {
public:
    Vector () { comp.resize(N); comp=X(0); }
    Vector (const blitz::Array<X, 1>& a) { comp.resize(N); comp = a; }
    Vector (const Vector<X, N>& v) { comp.resize(N); comp = v.comp; }
    ~Vector () {}
    //
    inline
    const X& operator() (int i) const { return this->comp(i); }
    inline
    Vector<X, N>& operator= (const Vector<X, N>& in) { this->comp = in.comp; return *this; }
public:
    blitz::Array<X, 1> comp;
};
//-------------------------------------------------------------------------------------------------
template<class X, int N>
class Dyadic {
public:
    Dyadic () { comp.resize(N, N); comp=X(0); }
    Dyadic (const blitz::Array<X, 2>& a) { comp.resize(N, N); comp = a; }
    Dyadic (const Dyadic<X, N>& d) { comp.resize(N, N); comp = d.comp; }
    ~Dyadic () {}
    //
    inline
    const X& operator() (int i, int j) const { return this->comp(i,j); }
    inline
    Dyadic<X, N>& operator= (const Dyadic<X, N>& in) { this->comp = in.comp; return *this; }
public:
    blitz::Array<X, 2> comp;
};
//-------------------------------------------------------------------------------------------------
template<class X, int N>
class Triadic {
public:
    Triadic () { comp.resize(N, N, N); comp=X(0); }
    Triadic (const blitz::Array<X, 3>& a) { comp.resize(N, N, N); comp = a; }
    Triadic (const Triadic<X, N>& d) { comp.resize(N, N, N); comp = d.comp; }
    ~Triadic () {}
    //
    inline
    const X& operator() (int i, int j, int k) const { return this->comp(i,j,k); }
    inline
    Triadic<X, N>& operator= (const Triadic<X, N>& in) { this->comp = in.comp; return *this; }
public:
    blitz::Array<X, 3> comp;
};
//-------------------------------------------------------------------------------------------------
template<class X, int N>
class Quindic {
public:
    Quindic () { comp.resize(N, N, N, N); comp=X(0); }
    Quindic (const blitz::Array<X, 4>& a) { comp.resize(N, N, N, N); comp = a; }
    Quindic (const Quindic<X, N>& d) { comp.resize(N, N, N, N); comp = d.comp; }
    ~Quindic () {}
    //
    inline
    const X& operator() (int i, int j, int k, int l) const { return this->comp(i,j,k,l); }
    inline
    Quindic<X, N>& operator= (const Quindic<X, N>& in) { this->comp = in.comp; return *this; }
public:
    blitz::Array<X, 4> comp;
};
//-------------------------------------------------------------------------------------------------
template<class X, int N>
class Sextic {
public:
    Sextic () { comp.resize(N, N, N, N, N, N); comp=X(0); }
    Sextic (const blitz::Array<X, 6>& a) { comp.resize(N, N, N, N, N, N); comp = a; }
    Sextic (const Sextic<X, N>& d) { comp.resize(N, N, N, N, N, N); comp = d.comp; }
    ~Sextic () {}
    //
    inline
    const X& operator() (int i, int j, int k, int l, int r, int s) const { return this->comp(i,j,k,l,r,s); }
    inline
    Sextic<X, N>& operator= (const Sextic<X, N>& in) { this->comp = in.comp; return *this; }
public:
    blitz::Array<X, 6> comp;
};
//-------------------------------------------------------------------------------------------------
class Vector3DGradient {
public:
    Vector3DGradient () { _grad[0] = _grad[1] = _grad[2] = RealGradient(0.0, 0.0, 0.0); }
    ~Vector3DGradient () {}
    //
    inline
    Real operator() (unsigned int i, unsigned int Dx) const { return _grad[i](Dx); }
    inline
    Vector3D     vector   (unsigned int Dx) const { Vector3D v; v.comp=_grad[0](Dx),_grad[1](Dx),_grad[2](Dx); return v; }
    inline
    RealGradient gradient (unsigned int i) const { return _grad[i]; }
    //
    inline
    Real& operator() (unsigned int i, unsigned int Dx) { return _grad[i](Dx); }
    inline
    RealGradient& operator() (unsigned int i) { return _grad[i]; }
private:
    RealGradient _grad[SP3D];
};
//-------------------------------------------------------------------------------------------------
class FieldInfo {
public:
    FieldInfo (const unsigned int n_bc, const unsigned int n_bm, const unsigned int n_aux);
    ~FieldInfo () {}
public:
    int a_flag;
    Real current_time, time_step, refence_volume;
    std::vector<Real> biochem;
    std::vector<RealGradient> grad_biochem;
    std::vector<Real> biochem_rate;
    Displacement         displacement;
    DisplacementGradient grad_displacement;
    Velocity             velocity;
    VelocityGradient     grad_velocity;
    Real                 div_velocity;
    std::vector<Real> biomech;
    Dyadic3D         deform_gradient;
    mutable Dyadic3D deform_grad_elas, deform_grad_inel;
    Dyadic3D strain,      stress,
             strain_incr, stress_incr;
    Real pressure;
    Vector3D fluid_velocity;
    Vector3DGradient grad_space_reference,
                     grad_space_current;
    Vector3D fibre_ref[SP3D], fibre_curr[SP3D];
    mutable std::vector<Real> auxiliary;
    mutable std::vector<RealGradient> grad_auxiliary;
    mutable std::vector<Real> auxiliary_rate;
};
//-------------------------------------------------------------------------------------------------
class BodyForceInfo {
public:
    BodyForceInfo () {}
    ~BodyForceInfo () {}
    //
    inline
    void set_time_function  (Real (*f_ptr)(const std::vector<Real>&, const Real)) { this->_time_function = f_ptr; }
    inline
    Real calc_time_function (const Real t) const { return this->_time_function(this->time_params, t); }
private:
    Real (*_time_function) (const std::vector<Real>&, const Real);
public:
    Vector3D value;
    std::vector<Real> time_params;
};
//-------------------------------------------------------------------------------------------------
class NodalForceInfo {
public:
    NodalForceInfo () : load_vec(0) {}
    ~NodalForceInfo () {}
    //
    inline
    void set_time_function__employ  (Real (*f_ptr)(const std::vector<Real>&, const Real)) { this->_time_function__employ = f_ptr; }
    inline
    Real calc_time_function__employ (const Real t) const { return this->_time_function__employ(this->time_params__employ, t); }
    inline
    void set_time_function__evaluate  (Real (*f_ptr)(const std::vector<Real>&, const Real)) { this->_time_function__evaluate = f_ptr; }
    inline
    bool calc_time_function__evaluate (const Real t) const { return (bool)this->_time_function__evaluate(this->time_params__evaluate, t); }
private:
    Real (*_time_function__employ)   (const std::vector<Real>&, const Real);
    Real (*_time_function__evaluate) (const std::vector<Real>&, const Real);
public:
    std::set<unsigned int> node_local;
    NumericVector<Number>* load_vec;
    std::vector<Real> time_params__employ,
                      time_params__evaluate;
};
//-------------------------------------------------------------------------------------------------
class PressureLoadInfo {
public:
    PressureLoadInfo () {}
    ~PressureLoadInfo () {}
    //
    inline
    void set_time_function__employ  (Real (*f_ptr)(const std::vector<Real>&, const Real)) { this->_time_function__employ = f_ptr; }
    inline
    Real calc_time_function__employ (const Real t) const { return this->_time_function__employ(this->time_params__employ, t); }
    inline
    void set_time_function__evaluate  (Real (*f_ptr)(const std::vector<Real>&, const Real)) { this->_time_function__evaluate = f_ptr; }
    inline
    bool calc_time_function__evaluate (const Real t) const { return (bool)this->_time_function__evaluate(this->time_params__evaluate, t); }
private:
    Real (*_time_function__employ)   (const std::vector<Real>&, const Real);
    Real (*_time_function__evaluate) (const std::vector<Real>&, const Real);
public:
    blitz::Array<Real, 1> load_vec;
    std::vector<Real> time_params__employ,
                      time_params__evaluate;
};
//-------------------------------------------------------------------------------------------------
class BoundaryConditionInfo {
public:
    enum Type {
        DIRICHLET_PRESCRIBED_X  = 100,
        DIRICHLET_PRESCRIBED_Y  = 101,
        DIRICHLET_PRESCRIBED_Z  = 102,
        DIRICHLET_PRESCRIBED    = 103,
        DIRICHLET_PRESCRIBED_RD = 104,
        DIRICHLET_PRESCRIBED_PL = 105,
        DIRICHLET_PRESCRIBED_CY = 106,
        DIRICHLET_PRESCRIBED_PR = 107,
        DIRICHLET_PRESCRIBED_XY = 110,
        DIRICHLET_PRESCRIBED_YZ = 111,
        DIRICHLET_PRESCRIBED_XZ = 112,
        NEUMANN_NORMAL          = 203,
        NEUMANN_VECTOR          = 204
    };
    static constexpr Real generic_coord = 1.0e+22;
public:
    BoundaryConditionInfo () : type(NEUMANN_NORMAL) {}
    ~BoundaryConditionInfo () {}
    //
    inline
    void set_active_function  (Real (*f_ptr)(const std::vector<Real>&, const Real)) { this->_active_function = f_ptr; }
    inline
    void set_time_function    (Real (*f_ptr)(const std::vector<Real>&, const Real)) { this->_time_function = f_ptr; }
    inline
    bool calc_active_function (const Real t) const { return static_cast<bool>(this->_active_function(this->time_params, t)); }
    inline
    Real calc_time_function   (const Real t) const { return this->_time_function(this->time_params, t); }
private:
    Real (*_active_function) (const std::vector<Real>&, const Real);
    Real (*_time_function)   (const std::vector<Real>&, const Real);
public:
    Type type;
    std::vector<Real> active_params;
    std::vector<Real> time_params;
    Vector3D value;
    Point center, normal;
};
//-------------------------------------------------------------------------------------------------
class MaterialInfo {
public:
    explicit MaterialInfo (int nbmech =0, int nbchem =0) : _title("a_material")
        { if (nbmech) _biomech.assign(nbmech, 0.0); if (nbchem) _biochem.assign(nbchem, 0.0); }
    ~MaterialInfo () {}
    //
    inline
    std::string&       set_title () { return this->_title; }
    inline
    const std::string& get_title () const { return this->_title; }
    inline
    std::vector<Real>&       set_biomech_param () { return this->_biomech; }
    inline
    const std::vector<Real>& get_biomech_param () const { return this->_biomech; }
    inline
    Real& set_biomech_param (unsigned int i) { return this->_biomech.at(i); }
    inline
    Real  get_biomech_param (unsigned int i) const { return this->_biomech[i]; }
    inline
    std::vector<Real>&       set_biochem_param () { return this->_biochem; }
    inline
    const std::vector<Real>& get_biochem_param () const { return this->_biochem; }
    inline
    Real& set_biochem_param (unsigned int i) { return this->_biochem.at(i); }
    inline
    Real  get_biochem_param (unsigned int i) const { return this->_biochem[i]; }
private:
    std::string _title;
    std::vector<Real> _biomech;
    std::vector<Real> _biochem;
};
//-------------------------------------------------------------------------------------------------
class RegionInfo {
public:
    RegionInfo ();
    ~RegionInfo () {}
    //
    void set_biomech_2d  (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&));
    void set_biomech_3d  (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&));
    void set_biochem     (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&));
    void set_damage      (void (*f_ptr)(const MaterialInfo&, FieldInfo&));
    void set_non_damage  (void (*f_ptr)(const MaterialInfo&, FieldInfo&));
    void set_coupled_2d  (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&));
    void set_coupled_3d  (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&));
    void set_plastic_deform (void (*f_ptr)(const MaterialInfo&, const FieldInfo&));
    void calc_biomech_2d  (const FieldInfo& fld, Dyadic2D& S, Quindic2D& D) const;
    void calc_biomech_3d  (const FieldInfo& fld, Dyadic3D& S, Quindic3D& Duu, Dyadic3D& Dup, Real& P, Dyadic3D& Dpu, Real& Dpp) const;
    void calc_biochem     (const FieldInfo& fld, Blitz_VectorReal& F, Blitz_VectorGradient& G, Blitz_DyadicReal& K, Blitz_VectorPoint& V, Blitz_DyadicReal& D, Blitz_DyadicGradient& X) const;
    void calc_damage      (FieldInfo& fld) const;
    void calc_coupled_2d  (const FieldInfo& fld, Dyadic2D& S, Quindic2D& D) const;
    void calc_coupled_3d  (const FieldInfo& fld, Dyadic3D& S, Quindic3D& Duu, Dyadic3D& Dup, Real& P, Dyadic3D& Dpu, Real& Dpp) const;
    void calc_plastic_deform (const FieldInfo& fld) const;
private:
    void (*_biomech_2d)  (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
    void (*_biomech_3d)  (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
    void (*_biochem)     (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
    void (*_damage)      (const MaterialInfo&, FieldInfo&);
    void (*_non_damage)  (const MaterialInfo&, FieldInfo&);
    void (*_coupled_2d)  (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
    void (*_coupled_3d)  (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
    void (*_plastic_deform) (const MaterialInfo&, const FieldInfo&);
public:
    bool is_membrane, is_damaged;
    MaterialInfo* mat;
    // list of nodes of the region (global and local ones)
    std::set<unsigned int> node, node_local;
    // damage factor (constant value)
    Real damage_factor_const;
};
//-------------------------------------------------------------------------------------------------
class VascularInfo {
public:
    enum Flow {
        INVALID_FLOW = 1000,
        BLOOD = 1001,
        LYMPH = 1002
    };
    enum Type {
        INVALID_TYPE = 100,
        INLET   = 101,
        OUTLET  = 102,
        SPROUT  = 103,
        MATURE  = 104,
        BRANCH  = 105,
        TIP     = 106
    };
    enum Status {
        NORMAL     = 0,
        COMPRESSED = 1,
        COLLAPSED  = 2
    };
public:
    VascularInfo () : flow(VascularInfo::INVALID_FLOW), type(VascularInfo::INVALID_TYPE), status(VascularInfo::NORMAL) {
        this->level = 0;
        this->time_generate = this->time_branch = this->time_mature = 0.0;
        this->elem3d_id = DofObject::invalid_id;
        this->length = this->total_length = 0.0;
        this->radius = this->thickness = this->pore_radius = this->pores_fraction = 0.0;
        this->branch_id = this->parent_branch_id = 0;
        this->press_ratio = 0.0; this->press_ratio_cr = 1.0; this->compression_scale = 1.0;
    }
    ~VascularInfo () {}
public:
    int level;
    // basic features of the vascular network node
    Flow flow;
    Type type;
    Status status;
    // original time of generation and branching, time to become mature
    Real time_generate, time_branch, time_mature;
    // ID of the three-dimensional finite element (containing this vascular node)
    dof_id_type elem3d_id;
    // length of vascular segment (beginning from foregoing branch/bifurcation) and from parent vessel
    Real length, total_length;
    // blood vessel radius and thickness at the node, mean pore radius and pores fraction of vessel
    Real radius, thickness, pore_radius, pores_fraction;
    // ID of the corresponding network branch and ID of parent branch
    unsigned int branch_id, parent_branch_id;
    // full list of vascular elements (segments) associated to this node
    std::set<dof_id_type> vsc_elem;
    // pressure loading ratio, critical load for collapse, and scale of compression (stretch ratio)
    Real press_ratio, press_ratio_cr, compression_scale;
};
//-------------------------------------------------------------------------------------------------
class IndexPair {
public:
    IndexPair () {}
    ~IndexPair () {}
    //
    void clear () { _index.clear(); _id_map.clear(); }
    inline
    void insert (unsigned int id) { _index.push_back(id); _id_map.insert( std::make_pair(id, _index.size()-1) ); }
    inline
    unsigned int get_id (unsigned int k) const { libmesh_assert( k<_index.size() ); return _index[k]; }
    inline
    unsigned int get_index (unsigned int id) const { libmesh_assert( _id_map.end()!=_id_map.find(id) ); return _id_map.find(id)->second; }
    inline
    bool confirm_id (unsigned int id) const { return ( _id_map.end() != _id_map.find(id) ) ? true : false; }
private:
    std::vector<unsigned int> _index;
    std::map<unsigned int, unsigned int> _id_map;
};
//-------------------------------------------------------------------------------------------------
struct InvariantsInfo {
public:
    InvariantsInfo (const Dyadic3D& C_in, const Vector3D* N);
    ~InvariantsInfo () {}
public:
    // invariants and pseudo-invariants of tensor 'C' with 'N[3]' being prefered directions
    Real I1, I2, I3, J1, J2, J3,
         I4, I5,     J4, J5,
         I6, I7,     J6, J7,
         I8, I9,     J8, J9;
    Dyadic3D dI1_dC, dI2_dC, dI3_dC, dJ1_dC, dJ2_dC, dJ3_dC,
             dI4_dC, dI5_dC,         dJ4_dC, dJ5_dC,
             dI6_dC, dI7_dC,         dJ6_dC, dJ7_dC,
             dI8_dC, dI9_dC,         dJ8_dC, dJ9_dC;
    Quindic3D d2I1_dCdC, d2I2_dCdC, d2I3_dCdC, d2J1_dCdC, d2J2_dCdC, d2J3_dCdC,
              d2I4_dCdC, d2I5_dCdC,            d2J4_dCdC, d2J5_dCdC,
              d2I6_dCdC, d2I7_dCdC,            d2J6_dCdC, d2J7_dCdC,
              d2I8_dCdC, d2I9_dCdC,            d2J8_dCdC, d2J9_dCdC;
    // right_Cauchy-Green deformation tensor 'C'
    Dyadic3D C;
    // inverse of 'C', square or 'C' and tensor derivative of inverse with respect to 'C'
    Dyadic3D Ci, C2;
    Quindic3D Ci_s;
    // tensor product of self-fibre orientation vectors
    Dyadic3D N_N[SP3D];
    // ...more items wrt matrix 'C' and its inverse
    Vector3D CN[SP3D], NC[SP3D];
    Vector3D CiN[SP3D], NCi[SP3D];
    Dyadic3D N_CN[SP3D], NC_N[SP3D];
    Dyadic3D N_CiN[SP3D], NCi_N[SP3D];
};
//
struct InvariantsEulerInfo {
public:
    InvariantsEulerInfo (const Dyadic3D& b_in, const Vector3D* n);
    ~InvariantsEulerInfo () {}
public:
    // invariants, pseudo-invariants of tensor 'b' (Finger tensor)
    Real I1, I2, J1, J2, J;
    // ...and their derivatives
    Dyadic3D     FF__dI1dC,      FF__dI2dC,      FF__dJ1dC,      FF__dJ2dC,      FF__dJdC;
    Quindic3D FFFF__d2I1dCdC, FFFF__d2I2dCdC, FFFF__d2J1dCdC, FFFF__d2J2dCdC, FFFF__d2JdCdC;
    // left_Cauchy-Green deformation tensor 'b' and its squared form
    Dyadic3D b, b2;
};
//-------------------------------------------------------------------------------------------------
class BinomialBasis {
public:
    BinomialBasis () { this->_set_table(); }
    ~BinomialBasis () {}
    //
    inline
    unsigned int operator() (int I, int N) const {
        return ( ( I < 0 || I > N ) ? 0 : this->_bb[N][I] );
    }
private:
    // fundamental function to evaluate the binomial basis
    unsigned int _calc (int I, int N);
    // initialize the table
    void _set_table ();
private:
    // maximum number of bionomials to evaluate
    static constexpr unsigned int max_binom = 500;
    // array containing the binomial basis components
    std::vector< std::vector<unsigned int> > _bb;
};
//=================================================================================================
#include "feb3/utils-calcs0_inl.h"
#include "feb3/utils-calcs1_inl.h"
#include "feb3/utils-calcs2_inl.h"
#include "feb3/utils-calcs3_inl.h"
#include "feb3/utils-calcs4_inl.h"
#include "feb3/utils-calcs5_inl.h"
//=================================================================================================
#endif // __UTILS_H__
//=================================================================================================
