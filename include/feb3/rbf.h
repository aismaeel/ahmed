/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __RBF_H__
#define __RBF_H__
//=================================================================================================
#include "libmesh/libmesh.h"
#include "feb3/utils.h"
#include "libmesh/parameters.h"
#include "libmesh/mesh.h"
#include "libmesh/vector_value.h"
#include "libmesh/dense_vector.h"
#include "libmesh/dense_matrix.h"
//=================================================================================================
enum RBFunction {
    // global radial basis functions
    RBF_MQ          =6101,
    RBF_EG          =6102,
    // compactly supported radial basis functions
    RBF_WU_C2       =6111,
    RBF_WU_C4       =6112,
    RBF_WENDLAND_C2 =6113,
    RBF_WENDLAND_C4 =6114,
    RBF_WENDLAND_C6 =6115
};
//-------------------------------------------------------------------------------------------------
enum PolynomialOrder {
    // various polynomial order bases
    PO_LINEAR      =1001,
    PO_BILINEAR    =1002,
    PO_QUADRATIC   =1003,
    PO_BIQUADRATIC =1004,
    PO_CUBIC       =1005
};
//=================================================================================================
class RBFmap3D {
public:
    explicit RBFmap3D (const RBFunction f =RBF_MQ, const PolynomialOrder o =PO_LINEAR);
    ~RBFmap3D () {}
    //
    void init (const Real* coeff, const std::vector<Point>& pnt, const std::vector<Real> soln[]);
    void calc (const Point& pnt, Real soln[]) const;
private:
    void (*_polynomial)  (const Point&, Blitz_VectorReal&);
    Real (*_radial_func) (const Real, const Real*);
private:
    bool _is_init;
    const Real* _coeff;
    const std::vector<Point>* _pnt;
    unsigned int _mbasis;
    DenseVector<Number> _alpha[SP3D];
};
//=================================================================================================
#endif // __RBF_H__
//=================================================================================================
