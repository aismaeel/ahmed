/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __INPUT_INLINE_H__
#define __INPUT_INLINE_H__
//=================================================================================================
template <class X>
inline
unsigned int get_par_size (GetPot& in, const char* name) {
    unsigned int n;
    if ( in.have_variable(name) ) {
        in_pars.insert(std::string(name));
        n = in.vector_variable_size(name);
    } else {
        std::stringstream msg;
        msg << "could not retrieve variable \"" << name << "\"";
        ERROR_(msg.str());
    }
    return n;
}
//-------------------------------------------------------------------------------------------------
template <class X>
inline
X get_par (GetPot& in, const char* name) {
    X par;
    if ( in.have_variable(name) ) {
        in_pars.insert(std::string(name));
        par = in(name, X());
    } else {
        std::stringstream msg;
        msg << "could not retrieve variable \"" << name << "\"";
        ERROR_(msg.str());
    }
    return par;
}
template <class X>
inline
X get_par (GetPot& in, const char* name, const X def ) {
    X par;
    if ( in.have_variable(name) ) {
        in_pars.insert(std::string(name));
        par = in(name, def);
    } else {
        std::stringstream msg;
        msg << "could not retrieve variable \"" << name << "\"" << " ...using default value \"" << def << "\"";
        WARNING_(msg.str());
        par = def;
    }
    return par;
}
//-------------------------------------------------------------------------------------------------
template <class X>
inline
X get_par_comp (GetPot& in, const char* name, unsigned int index) {
    X par;
    if ( in.have_variable(name) ) {
        in_pars.insert(std::string(name));
        par = in(name, X(), index);
    } else {
        std::stringstream msg;
        msg << "could not retrieve variable \"" << name << "\"";
        ERROR_(msg.str());
    }
    return par;
}
template <class X>
inline
X get_par_comp (GetPot& in, const char* name, unsigned int index, const X def ) {
    X par;
    if ( in.have_variable(name) ) {
        in_pars.insert(std::string(name));
        par = in(name, def, index);
    } else {
        std::stringstream msg;
        msg << "could not retrieve variable \"" << name << "\"" << " ...using default value \"" << def << "\"";
        WARNING_(msg.str());
        par = def;
    }
    return par;
}
//=================================================================================================
#endif // __INPUT_INLINE_H__
//=================================================================================================
