/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __TUMOUR_ANGIOGENESIS_SOLVER_H__
#define __TUMOUR_ANGIOGENESIS_SOLVER_H__
//=================================================================================================
#include "feb3/domain_data.h"
#include "feb3/reaction_diffusion_solver_Xd.h"
#include "feb3/navier_cauchy_solver_3d.h"
#include "libmesh/getpot.h"
#include "feb3/drug_delivery_solver.h"
//=================================================================================================
class TumourAngiogenesisSolver : public NavierCauchySolver3D, public ReactionDiffusionSolverXD,
    public DrugDeliverySolver {
public:
    TumourAngiogenesisSolver ( DomainData& dmn, DomainData_VascularNetwork& dmn_vn,
                               DomainData_SolidCoupledWithVascularNetwork& dmn_coupled );
    ~TumourAngiogenesisSolver () {}
    //
    const std::string& get_label () const;
    const std::string get_log () const;
    //
    void init ();
    void run ();
protected:
    void _set_initial ();
    void _update_network ();
    void _fluid_solver ();
    void _remodel_vasculature ();
    void _set_other_systems ();
    void _inspect_network ();
    void _check_network ();
    void _save_restart (std::ofstream& fout) const;
    void _load_restart ();
private:
    void _prepare_vascular_update ();
    void _distribute_results ();
    void _update_domain_data ();
    void _prepare_meshes ();
    void _reinit_systems ();
    void _find_anastomoses ();
    void _perform_sprouting ();
    void _project_displacement_solution ();
    void _project_drug_solution ();
private:
    std::string _label;
    mutable PerfLog _log;
protected:
    DomainData& _dmn;
    DomainData_VascularNetwork& _dmn_vn;
    DomainData_SolidCoupledWithVascularNetwork& _dmn_coupled;
};
//=================================================================================================
#include "feb3/tumour_angiogenesis_solver_inl.h"
//=================================================================================================
#endif // __TUMOUR_ANGIOGENESIS_SOLVER_H__
//=================================================================================================
