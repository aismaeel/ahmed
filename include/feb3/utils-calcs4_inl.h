/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __UTILS_CALCS4_INLINE_H__
#define __UTILS_CALCS4_INLINE_H__
//=================================================================================================
inline
void damage_model__nothing (const MaterialInfo& mat, FieldInfo& fld) {
    ; // do nothing here...
}
inline
void damage_model__max (const MaterialInfo& mat, FieldInfo& fld) {
    fld.biomech[0] = material__max_damage_factor;
}
inline
void damage_model__min (const MaterialInfo& mat, FieldInfo& fld) {
    fld.biomech[0] = material__min_damage_factor;
}
//
inline
void zero_permanent_strain (const MaterialInfo& mat, const FieldInfo& fld) {
    // deformation gradient: inelastic (plastic)
    fld.deform_grad_inel = identity_matrix;
    // deformation gradient: elastic
    fld.deform_grad_elas = fld.deform_gradient;
}
//=================================================================================================
inline
FieldInfo::FieldInfo (const unsigned int n_bc, const unsigned int n_bm, const unsigned int n_aux) {
    Vector3D fibre_default;
    fibre_default.comp = sqrt(frac_1o3);
    //
    this->a_flag = 0;
    //
    this->current_time = 0.0;
    this->time_step = 1.0;
    this->refence_volume = 0.0;
    if ( n_bc ) {
        this->biochem.assign(n_bc, 0.0);
        this->grad_biochem.assign(n_bc, RealGradient(0.0, 0.0, 0.0));
        this->biochem_rate.assign(n_bc, 0.0);
    }
    this->div_velocity = 0.0;
    if ( n_bm ) {
        this->biomech.assign(n_bm, 0.0);
    }
    this->deform_gradient = identity_matrix;
    this->deform_grad_elas = identity_matrix;
    this->deform_grad_inel = identity_matrix;
    this->pressure = 0.0;
    for (int ispdm=0; ispdm<SP3D; ispdm++) {
        this->fibre_ref[ispdm] = fibre_default;
        this->fibre_curr[ispdm] = fibre_default;
    }
    if ( n_aux ) {
        this->auxiliary.assign(n_aux, 0.0);
        this->grad_auxiliary.assign(n_aux, RealGradient(0.0, 0.0, 0.0));
        this->auxiliary_rate.assign(n_aux, 0.0);
    }
}
//=================================================================================================
inline
RegionInfo::RegionInfo () : is_membrane(false), is_damaged(false) {
    this->damage_factor_const = material__min_damage_factor;
    this->_damage = &damage_model__max;
    this->_non_damage = &damage_model__min;
    // default pointer to function initialisation
    this->set_plastic_deform(zero_permanent_strain);
}
//
inline
void RegionInfo::set_biomech_2d (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&)) {
    libmesh_assert( this->is_membrane );
    this->_biomech_2d = f_ptr;
}
inline
void RegionInfo::set_biomech_3d (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&)) {
    libmesh_assert( !this->is_membrane );
    this->_biomech_3d = f_ptr;
}
inline
void RegionInfo::set_biochem (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&)) {
    this->_biochem = f_ptr;
}
inline
void RegionInfo::set_damage (void (*f_ptr)(const MaterialInfo&, FieldInfo&)) {
    this->_damage = f_ptr;
}
inline
void RegionInfo::set_non_damage (void (*f_ptr)(const MaterialInfo&, FieldInfo&)) {
    this->_non_damage = f_ptr;
}
inline
void RegionInfo::set_coupled_2d (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&)) {
    libmesh_assert( this->is_membrane );
    this->_coupled_2d = f_ptr;
}
inline
void RegionInfo::set_coupled_3d (void (*f_ptr)(const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&)) {
    libmesh_assert( !this->is_membrane );
    this->_coupled_3d = f_ptr;
}
inline
void RegionInfo::set_plastic_deform (void (*f_ptr)(const MaterialInfo&, const FieldInfo&)) {
    this->_plastic_deform = f_ptr;
}
//
inline
void RegionInfo::calc_biomech_2d (const FieldInfo& fld, Dyadic2D& S, Quindic2D& D) const {
    this->_biomech_2d(*(this->mat), fld, S, D);
}
inline
void RegionInfo::calc_biomech_3d (const FieldInfo& fld, Dyadic3D& S, Quindic3D& Duu, Dyadic3D& Dup, Real& P, Dyadic3D& Dpu, Real& Dpp) const {
    this->_biomech_3d(*(this->mat), fld, S, Duu, Dup, P, Dpu, Dpp);
}
inline
void RegionInfo::calc_biochem (const FieldInfo& fld, Blitz_VectorReal& F, Blitz_VectorGradient& G, Blitz_DyadicReal& K, Blitz_VectorPoint& V, Blitz_DyadicReal& D, Blitz_DyadicGradient& X) const {
    this->_biochem(*(this->mat), fld, F, G, K, V, D, X);
}
inline
void RegionInfo::calc_damage (FieldInfo& fld) const {
    if ( this->is_damaged ) this->_damage(*(this->mat), fld);
    else this->_non_damage(*(this->mat), fld);
}
inline
void RegionInfo::calc_coupled_2d (const FieldInfo& fld, Dyadic2D& S, Quindic2D& D) const {
    this->_coupled_2d(*(this->mat), fld, S, D);
}
inline
void RegionInfo::calc_coupled_3d (const FieldInfo& fld, Dyadic3D& S, Quindic3D& Duu, Dyadic3D& Dup, Real& P, Dyadic3D& Dpu, Real& Dpp) const {
    this->_coupled_3d(*(this->mat), fld, S, Duu, Dup, P, Dpu, Dpp);
}
inline
void RegionInfo::calc_plastic_deform (const FieldInfo& fld) const {
    this->_plastic_deform(*(this->mat), fld);
}
//-------------------------------------------------------------------------------------------------
inline
unsigned int BinomialBasis::_calc (int I, int N) {
    if      ( I <  0 || I >  N ) return 0;
    else if ( I == 0 || I == N ) return 1;
    else {
        Real res = 1.0;
        for (int j=1; j<=I; j++)
            res *= (N-I+j)/(j+0.0);
        return static_cast<unsigned int>(res);
    }
}
inline
void BinomialBasis::_set_table () {
    this->_bb.resize(max_binom);
    for (unsigned int N=0; N<max_binom; N++) {
        this->_bb[N].assign(N+1, 0);
        for (unsigned int I=0; I<N+1; I++)
            this->_bb[N][I] = this->_calc(I, N);
    }
}
//-------------------------------------------------------------------------------------------------
inline
InvariantsInfo::InvariantsInfo (const Dyadic3D& C_in, const Vector3D* N) {
    libmesh_assert(N != 0);
    //
    const Dyadic3D& I = identity_matrix;
    const Quindic3D& I_s = identity_tensor;
    //
    C = C_in;
    Ci = inverse(C);
    C2 = squared(C);
    Ci_s = op_symm_tensor(Ci);
    // NOTE to comment lines below to reduce computations (if necessary)
    N_N[0] = tensor(N[0]);
    N_N[1] = tensor(N[1]);
    N_N[2] = tensor(N[2]);
    CN[0] = mul_im_m(C, N[0]); CiN[0] = mul_im_m(Ci, N[0]);
    CN[1] = mul_im_m(C, N[1]); CiN[1] = mul_im_m(Ci, N[1]);
    CN[2] = mul_im_m(C, N[2]); CiN[2] = mul_im_m(Ci, N[2]);
    NC[0] = mul_m_mi(N[0], C); NCi[0] = mul_m_mi(N[0], Ci);
    NC[1] = mul_m_mi(N[1], C); NCi[1] = mul_m_mi(N[1], Ci);
    NC[2] = mul_m_mi(N[2], C); NCi[2] = mul_m_mi(N[2], Ci);
    N_CN[0] = tensor(N[0], CN[0]); N_CiN[0] = tensor(N[0], CiN[0]);
    N_CN[1] = tensor(N[1], CN[1]); N_CiN[1] = tensor(N[1], CiN[1]);
    N_CN[2] = tensor(N[2], CN[2]); N_CiN[2] = tensor(N[2], CiN[2]);
    NC_N[0] = tensor(NC[0], N[0]); NCi_N[0] = tensor(NCi[0], N[0]);
    NC_N[1] = tensor(NC[1], N[1]); NCi_N[1] = tensor(NCi[1], N[1]);
    NC_N[2] = tensor(NC[2], N[2]); NCi_N[2] = tensor(NCi[2], N[2]);
    //
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    //
    I1 = first_invariant(C);
    I2 = second_invariant(C);
    I3 = third_invariant(C);
    // NOTE to comment lines below to reduce computations (if necessary)
    I4 = product(N[0], C, N[0]);
    I6 = product(N[1], C, N[1]);
    I8 = product(N[2], C, N[2]);
    I5 = product(N[0], C2, N[0]);
    I7 = product(N[1], C2, N[1]);
    I9 = product(N[2], C2, N[2]);
    //
    dI1_dC.comp = I.comp(i,j);
    dI2_dC.comp = I1*I.comp(i,j) - C.comp(i,j);
    dI3_dC.comp = I3*Ci.comp(i,j);
    // NOTE to comment lines below to reduce computations (if necessary)
    dI4_dC.comp = N_N[0].comp(i,j);
    dI6_dC.comp = N_N[1].comp(i,j);
    dI8_dC.comp = N_N[2].comp(i,j);
    dI5_dC.comp = N_CN[0].comp(i,j) + NC_N[0].comp(i,j);
    dI7_dC.comp = N_CN[1].comp(i,j) + NC_N[1].comp(i,j);
    dI9_dC.comp = N_CN[2].comp(i,j) + NC_N[2].comp(i,j);
    //
    d2I1_dCdC.comp = 0.0;
    d2I2_dCdC.comp = I.comp(i,j)*I.comp(k,l) - I_s.comp(i,j,k,l);
    d2I3_dCdC.comp = I3*Ci.comp(i,j)*Ci.comp(k,l) + I3*Ci_s.comp(i,j,k,l);
    // NOTE to comment lines below to reduce computations (if necessary)
    d2I4_dCdC.comp = 0.0;
    d2I6_dCdC.comp = 0.0;
    d2I8_dCdC.comp = 0.0;
    d2I5_dCdC.comp = 0.5*(N[0].comp(i)*I.comp(j,k)*N[0].comp(l)+N[0].comp(i)*I.comp(j,l)*N[0].comp(k)
                         +N[0].comp(j)*I.comp(i,k)*N[0].comp(l)+N[0].comp(j)*I.comp(i,l)*N[0].comp(k));
    d2I7_dCdC.comp = 0.5*(N[1].comp(i)*I.comp(j,k)*N[1].comp(l)+N[1].comp(i)*I.comp(j,l)*N[1].comp(k)
                         +N[1].comp(j)*I.comp(i,k)*N[1].comp(l)+N[1].comp(j)*I.comp(i,l)*N[1].comp(k));
    d2I9_dCdC.comp = 0.5*(N[2].comp(i)*I.comp(j,k)*N[2].comp(l)+N[2].comp(i)*I.comp(j,l)*N[2].comp(k)
                         +N[2].comp(j)*I.comp(i,k)*N[2].comp(l)+N[2].comp(j)*I.comp(i,l)*N[2].comp(k));
    //
    const Real recip_I3p3o2 = 1.0/pow(I3, frac_3o2),
               recip_I3p1o2 = 1.0/pow(I3, frac_1o2),
               recip_I3p1o3 = 1.0/pow(I3, frac_1o3),
               recip_I3p2o3 = 1.0/pow(I3, frac_2o3),
               recip_I3p4o3 = 1.0/pow(I3, frac_4o3),
               recip_I3p5o3 = 1.0/pow(I3, frac_5o3),
               recip_I3p7o3 = 1.0/pow(I3, frac_7o3),
               recip_I3p8o3 = 1.0/pow(I3, frac_8o3);
    //
    J1 = I1*recip_I3p1o3;
    J2 = I2*recip_I3p2o3;
    J3 = sqrt(I3);
    // NOTE to comment lines below to reduce computations (if necessary)
    J4 = I4*recip_I3p1o3;
    J6 = I6*recip_I3p1o3;
    J8 = I8*recip_I3p1o3;
    J5 = I5*recip_I3p2o3;
    J7 = I7*recip_I3p2o3;
    J9 = I9*recip_I3p2o3;
    //
    dJ1_dC.comp = recip_I3p1o3*dI1_dC.comp(i,j) - (frac_1o3*I1*recip_I3p4o3)*dI3_dC.comp(i,j);
    dJ2_dC.comp = recip_I3p2o3*dI2_dC.comp(i,j) - (frac_2o3*I2*recip_I3p5o3)*dI3_dC.comp(i,j);
    dJ3_dC.comp = (frac_1o2*recip_I3p1o2)*dI3_dC.comp(i,j);
    // NOTE to comment lines below to reduce computations (if necessary)
    dJ4_dC.comp = recip_I3p1o3*dI4_dC.comp(i,j) - (frac_1o3*I4*recip_I3p4o3)*dI3_dC.comp(i,j);
    dJ6_dC.comp = recip_I3p1o3*dI6_dC.comp(i,j) - (frac_1o3*I6*recip_I3p4o3)*dI3_dC.comp(i,j);
    dJ8_dC.comp = recip_I3p1o3*dI8_dC.comp(i,j) - (frac_1o3*I8*recip_I3p4o3)*dI3_dC.comp(i,j);
    dJ5_dC.comp = recip_I3p2o3*dI5_dC.comp(i,j) - (frac_2o3*I5*recip_I3p5o3)*dI3_dC.comp(i,j);
    dJ7_dC.comp = recip_I3p2o3*dI7_dC.comp(i,j) - (frac_2o3*I7*recip_I3p5o3)*dI3_dC.comp(i,j);
    dJ9_dC.comp = recip_I3p2o3*dI9_dC.comp(i,j) - (frac_2o3*I9*recip_I3p5o3)*dI3_dC.comp(i,j);
    //
    d2J1_dCdC.comp = recip_I3p1o3*d2I1_dCdC.comp(i,j,k,l)
                   - (frac_1o3*recip_I3p4o3)*(dI1_dC.comp(i,j)*dI3_dC.comp(k,l)+dI3_dC.comp(i,j)*dI1_dC.comp(k,l))
                   - (frac_1o3*I1*recip_I3p4o3)*d2I3_dCdC.comp(i,j,k,l)
                   + (frac_4o9*I1*recip_I3p7o3)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
    d2J2_dCdC.comp = recip_I3p2o3*d2I2_dCdC.comp(i,j,k,l)
                   - (frac_2o3*recip_I3p5o3)*(dI2_dC.comp(i,j)*dI3_dC.comp(k,l)+dI3_dC.comp(i,j)*dI2_dC.comp(k,l))
                   - (frac_2o3*I2*recip_I3p5o3)*d2I3_dCdC.comp(i,j,k,l)
                   + (frac_10o9*I2*recip_I3p8o3)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
    d2J3_dCdC.comp = (frac_1o2*recip_I3p1o2)*d2I3_dCdC.comp(i,j,k,l)
                   - (frac_1o4*recip_I3p3o2)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
    // NOTE to comment lines below to reduce computations (if necessary)
    d2J4_dCdC.comp = recip_I3p1o3*d2I4_dCdC.comp(i,j,k,l)
                   - (frac_1o3*recip_I3p4o3)*(dI4_dC.comp(i,j)*dI3_dC.comp(k,l)+dI3_dC.comp(i,j)*dI4_dC.comp(k,l))
                   - (frac_1o3*I4*recip_I3p4o3)*d2I3_dCdC.comp(i,j,k,l)
                   + (frac_4o9*I4*recip_I3p7o3)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
    d2J6_dCdC.comp = recip_I3p1o3*d2I6_dCdC.comp(i,j,k,l)
                   - (frac_1o3*recip_I3p4o3)*(dI6_dC.comp(i,j)*dI3_dC.comp(k,l)+dI3_dC.comp(i,j)*dI6_dC.comp(k,l))
                   - (frac_1o3*I6*recip_I3p4o3)*d2I3_dCdC.comp(i,j,k,l)
                   + (frac_4o9*I6*recip_I3p7o3)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
    d2J8_dCdC.comp = recip_I3p1o3*d2I8_dCdC.comp(i,j,k,l)
                   - (frac_1o3*recip_I3p4o3)*(dI8_dC.comp(i,j)*dI3_dC.comp(k,l)+dI3_dC.comp(i,j)*dI8_dC.comp(k,l))
                   - (frac_1o3*I8*recip_I3p4o3)*d2I3_dCdC.comp(i,j,k,l)
                   + (frac_4o9*I8*recip_I3p7o3)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
    d2J5_dCdC.comp = recip_I3p2o3*d2I5_dCdC.comp(i,j,k,l)
                   - (frac_2o3*recip_I3p5o3)*(dI5_dC.comp(i,j)*dI3_dC.comp(k,l)+dI3_dC.comp(i,j)*dI5_dC.comp(k,l))
                   - (frac_2o3*I5*recip_I3p5o3)*d2I3_dCdC.comp(i,j,k,l)
                   + (frac_10o9*I5*recip_I3p8o3)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
    d2J7_dCdC.comp = recip_I3p2o3*d2I7_dCdC.comp(i,j,k,l)
                   - (frac_2o3*recip_I3p5o3)*(dI7_dC.comp(i,j)*dI3_dC.comp(k,l)+dI3_dC.comp(i,j)*dI7_dC.comp(k,l))
                   - (frac_2o3*I7*recip_I3p5o3)*d2I3_dCdC.comp(i,j,k,l)
                   + (frac_10o9*I7*recip_I3p8o3)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
    d2J9_dCdC.comp = recip_I3p2o3*d2I9_dCdC.comp(i,j,k,l)
                   - (frac_2o3*recip_I3p5o3)*(dI9_dC.comp(i,j)*dI3_dC.comp(k,l)+dI3_dC.comp(i,j)*dI9_dC.comp(k,l))
                   - (frac_2o3*I9*recip_I3p5o3)*d2I3_dCdC.comp(i,j,k,l)
                   + (frac_10o9*I9*recip_I3p8o3)*dI3_dC.comp(i,j)*dI3_dC.comp(k,l);
}
//
inline
InvariantsEulerInfo::InvariantsEulerInfo (const Dyadic3D& b_in, const Vector3D* n) {
    libmesh_assert(n != 0);
    //
    const Dyadic3D& I = identity_matrix;
    const Quindic3D& I_s = identity_tensor;
    //
    b = b_in;
    b2 = squared(b);
    const Quindic3D B_s = symm_tensor(b);
    //
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    //
    I1 = trace(b);
    I2 = inner_product(b, b);
    J  = sqrt(determinant(b));
    const Real Jp2o3 = pow(J, frac_2o3),
               Jp4o3 = pow(J, frac_4o3);
    J1 = I1/Jp2o3;
    J2 = I2/Jp4o3;
    //
    FF__dJdC.comp = (0.5*J)*I.comp(i,j);
    FF__dI1dC.comp = b.comp(i,j);
    FF__dI2dC.comp = 2.0*b2.comp(i,j);
    FF__dJ1dC.comp = (1.0/Jp2o3)*b.comp(i,j)-(frac_1o3*J1)*I.comp(i,j);
    FF__dJ2dC.comp = (2.0/Jp4o3)*b2.comp(i,j)-(frac_2o3*J2)*I.comp(i,j);
    //
    FFFF__d2JdCdC.comp = (frac_1o4*J)*I.comp(i,j)*I.comp(k,l)-(frac_1o2*J)*I_s.comp(i,j,k,l);
    FFFF__d2I1dCdC.comp = 0.0;
    FFFF__d2I2dCdC.comp = 2.0*B_s.comp(i,j,k,l);
    FFFF__d2J1dCdC.comp = (frac_1o3*J1)*I_s.comp(i,j,k,l)
                        - frac_1o3*I.comp(i,j)*FF__dJ1dC.comp(k,l) - (frac_1o3/Jp2o3)*b.comp(i,j)*I.comp(k,l);
    FFFF__d2J2dCdC.comp = (frac_2o3*J2)*I_s.comp(i,j,k,l) - frac_2o3*I.comp(i,j)*FF__dJ2dC.comp(k,l)
                        + (2.0/Jp4o3)*B_s.comp(i,j,k,l) - (frac_4o3/Jp4o3)*b.comp(i,j)*I.comp(k,l);
}
//=================================================================================================
#endif //  __UTILS_CALCS4_INLINE_H__
//=================================================================================================
