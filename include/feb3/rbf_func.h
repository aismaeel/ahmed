/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __RBF_FUNC_H__
#define __RBF_FUNC_H__
//=================================================================================================
// global-support radial basis functions
inline
Real multiquadric (const Real r, const Real* c) {
    libmesh_assert(c!=NULL);
    return pow(pow2(c[0])+pow2(r), c[1]);
}
inline
Real exp_gaussian (const Real r, const Real* c) {
    libmesh_assert(c!=NULL);
    return exp(pow(c[0], c[1])*c[2]*pow2(r));
}
// local-support radial basis functions
inline
Real wu_c2 (const Real r, const Real* c) {
    libmesh_assert(c!=NULL);
    const Real x=r/c[0];
    return (x>1.0 || x<0.0) ? 0.0 : pow5(1-x)*(5*x*x*x*x+25*x*x*x+48*x*x+40*x+8);
}
inline
Real wu_c4 (const Real r, const Real* c) {
    libmesh_assert(c!=NULL);
    const Real x=r/c[0];
    return (x>1.0 || x<0.0) ? 0.0 : pow6(1-x)*(5*x*x*x*x*x+30*x*x*x*x+72*x*x*x+82*x*x+36*x+6);
}
inline
Real wendland_c2 (const Real r, const Real* c) {
    libmesh_assert(c!=NULL);
    const Real x=r/c[0];
    return (x>1.0 || x<0.0) ? 0.0 : pow4(1-x)*(4*x+1);
}
inline
Real wendland_c4 (const Real r, const Real* c) {
    libmesh_assert(c!=NULL);
    const Real x=r/c[0];
    return (x>1.0 || x<0.0) ? 0.0 : pow6(1-x)*(35*x*x+18*x+3);
}
inline
Real wendland_c6 (const Real r, const Real* c) {
    libmesh_assert(c!=NULL);
    const Real x=r/c[0];
    return (x>1.0 || x<0.0) ? 0.0 : pow8(1-x)*(32*x*x*x+25*x*x+8*x+1);
}
//=================================================================================================
#endif // __RBF_FUNC_H__
//=================================================================================================
