/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __UTILS_CALCS5_INLINE_H__
#define __UTILS_CALCS5_INLINE_H__
//=================================================================================================
inline
Real positive_only (Real (*f)(Real x), const Real x, const Real x0) {
    if ( x0 < 0.0 ) return 0.0;
    else return f(x);
}
//=================================================================================================
inline
Real flat_function (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==1 );
    const Real amp = param[0]; // amplitude of time-function
    return amp;
}
//-------------------------------------------------------------------------------------------------
inline
Real linear_function (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==1 );
    const Real amp = param[0]; // amplitude of time-function
    return amp * t;
}
inline
Real step_function (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==3 );
    const Real amp = param[0]; // amplitude of time-function
    const Real ti  = param[1]; // initialization time
    const Real td  = param[2]; // duration time
    if      ( t<=ti )    return 0.0;
    else if ( t<=ti+td ) return amp;
    else                 return 0.0;
}
inline
Real ramp_function (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==4 );
    const Real amp = param[0]; // amplitude of time-function
    const Real ti  = param[1]; // initialization time
    const Real ta  = param[2]; // arrival (max amp) time
    const Real td  = param[3]; // duration time
    if      ( t<=ti )       return 0.0;
    else if ( t<=ti+ta )    return amp * (t-ti)/ta;
    else if ( t<=ti+td )    return amp;
    else if ( t<=ti+td+ta ) return amp * (td+ta-(t-ti))/ta;
    else                    return 0.0;
}
inline
Real steep_function (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==6 );
    const Real amp = param[0]; // amplitude of time-function
    const Real ti  = param[1]; // initialization time
    const Real ta  = param[2]; // arrival (max amp) time
    const Real td  = param[3]; // duration time
    const Real S   = param[4],
               q   = param[5];
    const Real f0 = amp * pow(0.5+0.5*sin(mPI*(t-ti)/ta-mPI_2), q),
               f1 = amp,
               f2 = amp * exp(S*(ti+td-t));
    if      ( t<=ti )    return 0.0;
    else if ( t<=ti+ta ) return f0;
    else if ( t<=ti+td ) return f1;
    else                 return f2;
}
inline
Real exp_function (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==5 );
    const Real amp = param[0]; // amplitude of time-function
    const Real ti  = param[1]; // initialization time
    //const Real ta  = param[2]; // arrival (max amp) time
    const Real td  = param[3]; // duration time
    const Real S   = param[4];
    const Real f0 = 0.0,
               f1 = amp * (1.0-exp(S*(ti-t))),
               f2 = f1 * exp(S*(ti+td-t));
    if      ( t<=ti    ) return f0;
    else if ( t<=ti+td ) return f1;
    else                 return f2;
}
inline
Real gaussian_function (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==3 );
    const Real amp = param[0]; // amplitude of time-function
    const Real tp  = param[1]; // peak time
    const Real rms = param[2]; // gaussian RMS width
    return amp*exp(-0.5*pow2((t-tp)/rms));
}
inline
Real logistic_function (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==4 );
    const Real amp = param[0]; // amplitude of time-function
    const Real tm0 = param[1]; // mid-point time (increase)
    const Real tm1 = param[2]; // mid-point time (decrease)
    const Real cs  = param[3]; // curve steepness
    const Real tm = 0.5*(tm0+tm1);
    return ( t < tm ? amp/(1.0+exp(-cs*(t-tm0))) : amp/(1.0+exp(+cs*(t-tm1))) );
}
//-------------------------------------------------------------------------------------------------
inline
Real linear_function__periodic (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==2 );
    //const Real amp = param[0]; // amplitude of time-function
    const Real period = param[1]; // period
    const unsigned int n_periods = (unsigned int)(t/period);
    Real t_p = t / period - n_periods;
    t_p += fabs(t_p)<=1.0e-9 ? 1.0 : 0.0;
    return linear_function(param, t_p);
}
inline
Real step_function__periodic (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==4 );
    //const Real amp = param[0]; // amplitude of time-function
    //const Real ti  = param[1]; // initialization time
    //const Real td  = param[2]; // duration time
    const Real period = param[3]; // period
    const unsigned int n_periods = (unsigned int)(t/period);
    Real t_p = t / period - n_periods;
    t_p += fabs(t_p)<=1.0e-9 ? 1.0 : 0.0;
    return step_function(param, t_p);
}
inline
Real ramp_function__periodic (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==5 );
    //const Real amp = param[0]; // amplitude of time-function
    //const Real ti  = param[1]; // initialization time
    //const Real ta  = param[2]; // arrival (max amp) time
    //const Real td  = param[3]; // duration time
    const Real period = param[4]; // period
    const unsigned int n_periods = (unsigned int)(t/period);
    Real t_p = t / period - n_periods;
    t_p += fabs(t_p)<=1.0e-9 ? 1.0 : 0.0;
    return ramp_function(param, t_p);
}
inline
Real steep_function__periodic (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==7 );
    //const Real amp = param[0]; // amplitude of time-function
    //const Real ti  = param[1]; // initialization time
    //const Real ta  = param[2]; // arrival (max amp) time
    //const Real td  = param[3]; // duration time
    //const Real S   = param[4],
    //           q   = param[5];
    const Real period = param[6]; // period
    const unsigned int n_periods = (unsigned int)(t/period);
    Real t_p = t / period - n_periods;
    t_p += fabs(t_p)<=1.0e-9 ? 1.0 : 0.0;
    return steep_function(param, t_p);
}
inline
Real exp_function__periodic (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==6 );
    //const Real amp = param[0]; // amplitude of time-function
    //const Real ti  = param[1]; // initialization time
    //const Real ta  = param[2]; // arrival (max amp) time
    //const Real td  = param[3]; // duration time
    //const Real S   = param[4];
    const Real period = param[5]; // period
    const unsigned int n_periods = (unsigned int)(t/period);
    Real t_p = t / period - n_periods;
    t_p += fabs(t_p)<=1.0e-9 ? 1.0 : 0.0;
    return exp_function(param, t_p);
}
inline
Real gaussian_function__periodic (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==4 );
    //const Real amp = param[0]; // amplitude of time-function
    //const Real tp  = param[1]; // peak time
    //const Real rms = param[2]; // gaussian RMS width
    const Real period = param[3]; // period
    const unsigned int n_periods = (unsigned int)(t/period);
    Real t_p = t / period - n_periods;
    t_p += fabs(t_p)<=1.0e-9 ? 1.0 : 0.0;
    return gaussian_function(param, t_p);
}
inline
Real logistic_function__periodic (const std::vector<Real>& param, const Real t) {
    libmesh_assert( param.size()==5 );
    //const Real amp = param[0]; // amplitude of time-function
    //const Real tm0 = param[1]; // mid-point time (increase)
    //const Real tm1 = param[2]; // mid-point time (decrease)
    //const Real cs  = param[3]; // curve steepness
    const Real period = param[4]; // period
    const unsigned int n_periods = (unsigned int)(t/period);
    Real t_p = t / period - n_periods;
    t_p += fabs(t_p)<=1.0e-9 ? 1.0 : 0.0;
    return logistic_function(param, t_p);
}
//=================================================================================================
#endif //  __UTILS_CALCS5_INLINE_H__
//=================================================================================================
