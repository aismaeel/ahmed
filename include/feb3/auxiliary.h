/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __AUXILIARY_H__
#define __AUXILIARY_H__
//=================================================================================================
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <vector>
#include <list>
#include <map>
#include <set>
#include "blitz/array.h"
#include "libmesh/libmesh.h"
//=================================================================================================
using namespace libMesh;
//=================================================================================================
#include "libmesh/libmesh_common.h"
#include "libmesh/id_types.h"
#include "libmesh/parallel.h"
#include "libmesh/getpot.h"
#include "libmesh/parallel_implementation.h"
#include "feb3/fractions.h"
//=================================================================================================
const int SP1D = 1,
          SP2D = 2, VOIGT_SP2D = 3,
          SP3D = 3, VOIGT_SP3D = 6;
// maximum length of buffer to read and write stuff onto a pointer of character
const int buffer_len = 10001;
// various definitions of useful constants
const Real mPI   = libMesh::pi,
           m2PI  = 2.0*mPI,
           m3PI  = 3.0*mPI,
           m4PI  = 4.0*mPI,
           m6PI  = 6.0*mPI,
           mPI_2 = mPI/2.0,
           mPI_3 = mPI/3.0,
           mPI_4 = mPI/4.0,
           mPI_6 = mPI/6.0;
const Real mEPS   = exp(1.0),
           mEPS_2 = mEPS/2.0;
const Real sqrt_2   = sqrt(2.0),
           sqrt_3   = sqrt(3.0),
           sqrt_5   = sqrt(5.0),
           sqrt_6   = sqrt(6.0),
           sqrt_7   = sqrt(7.0),
           sqrt_8   = sqrt(8.0),
           sqrt_3o2 = sqrt(3.0/2.0),
           sqrt_2o3 = sqrt(2.0/3.0);
const Real Boltzmann_constant = 1.38064852e-23; // m2 kg s-2 K-1
//=================================================================================================
class OFStream {
public:
    OFStream () : _is_open(false) {}
    OFStream (const char* name, const bool rank0_only =true) { this->open(name, rank0_only); }
    //
    inline
    void open (const char* name, const bool rank0_only) {
        this->_is_open = false;
        if ( rank0_only ) { if ( libMesh::global_processor_id() != 0 ) return; }
        if ( rank0_only ) this->_name << name << std::flush;
        else              this->_name << name << '.' << libMesh::global_processor_id() << std::flush;
        this->_fs.open(this->_name.str().c_str());
        this->_fs.setf(std::ios_base::unitbuf|std::ios_base::scientific);
        this->_fs.precision(6);
        this->_is_open = true;
    }
    inline
    bool is_open () const { return this->_is_open; }
    inline
    std::ofstream& stream () { return this->_fs; }
    inline
    void print (const std::string& msg, bool on_file_only =false) {
        this->print(msg.c_str(), on_file_only);
    }
    inline
    void print (const char* msg, bool on_file_only =false) {
        if ( ! this->_is_open ) return;
        if ( ! on_file_only && libMesh::global_processor_id() == 0 ) std::cout << msg << std::flush;
        this->_fs << msg << std::flush;
    }
    inline
    void deleteme () {
        if ( this->_is_open ) remove(this->_name.str().c_str());
    }
private:
    std::ofstream _fs;
    std::stringstream _name;
    bool _is_open;
};
//-------------------------------------------------------------------------------------------------
struct GlobalTolerance {
public:
    GlobalTolerance () : epsilon(std::numeric_limits<Real>::epsilon()), geometry(1.0e-4), zero(1.0e-12),
                         solution(1.0e-3), residual(1.0e-6), energy(1.0e-12) {}
    ~GlobalTolerance () {}
public:
    Real epsilon, geometry, zero;
    Real solution, residual, energy;
};
//=================================================================================================
#define ERROR_(_msg_) \
    do { \
        std::stringstream ss1, ss2; \
        ss1 << " *** ERROR (file:" << __FILE__ << "; line:" << __LINE__ << ").\n" \
           << "     message : " << (_msg_) << ".\n" << std::flush; \
        ss2 << " *** ERROR (file:" << __FILE__ << "; line:" << __LINE__ << ").\n" \
            << "     message : " << (_msg_) << ".\n" << std::flush; \
        flog.print(ss1.str().c_str(), false); \
        ferr.print(ss2.str().c_str(), true);  \
        libmesh_error(); \
    } while (false)
#define ASSERT_(_lexpr_, _msg_) \
    do { \
        if ( !(_lexpr_) ) { \
            std::stringstream ss1, ss2; \
            ss1 << " *** ERROR (file:" << __FILE__ << "; line:" << __LINE__ << ").\n" \
               << "     message : " << (_msg_) << ".\n" << std::flush; \
            ss2 << " *** ERROR (file:" << __FILE__ << "; line:" << __LINE__ << ").\n" \
                << "     message : " << (_msg_) << ".\n" << std::flush; \
            flog.print(ss1.str().c_str(), false); \
            ferr.print(ss2.str().c_str(), true);  \
            libmesh_error(); \
        } \
    } while (false)
#define WARNING_(_msg_) \
    do { \
        std::stringstream ss1; \
        ss1 << " *** WARNING (file:" << __FILE__ << "; line:" << __LINE__ << ").\n" \
           << "     message : " << (_msg_) << ".\n" << std::flush; \
        flog.print(ss1.str().c_str(), true); \
    } while (false)
//
#define PAUSE \
    do { \
        std::stringstream ss1; \
        ss1 << " *** PAUSED (file:" <<__FILE__<< "; line:" << __LINE__ << ").\n" << std::flush; \
        flog.print(ss1.str().c_str(), false); \
        getchar(); \
    } while (false)
//
#define EXTENT1_(_arr_)  (_arr_).extent(blitz::firstDim)
#define EXTENT2_(_arr_)  (_arr_).extent(blitz::secondDim)
#define EXTENT3_(_arr_)  (_arr_).extent(blitz::thirdDim)
#define EXTENT4_(_arr_)  (_arr_).extent(blitz::fourthDim)
//=================================================================================================
#endif // __AUXILIARY_H__
//=================================================================================================
