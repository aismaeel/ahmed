/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __WOUND_HEALING_SOLVER_H__
#define __WOUND_HEALING_SOLVER_H__
//=================================================================================================
#include "feb3/domain_data.h"
#include "feb3/reaction_diffusion_solver_Xd.h"
#include "feb3/navier_cauchy_solver_3d.h"
#include "libmesh/getpot.h"
//=================================================================================================
class WoundHealingSolver : public NavierCauchySolver3D, public ReactionDiffusionSolverXD {
public:
    WoundHealingSolver (DomainData& dmn);
    ~WoundHealingSolver () {}
    //
    const std::string& get_label () const;
    const std::string get_log () const;
    //
    void init ();
    void run ();
private:
    void _apply_gravity ();
    void _wound_closure ();
    void _wound_healing ();
private:
    std::string _label;
    mutable PerfLog _log;
};
//=================================================================================================
#include "feb3/wound_healing_solver_inl.h"
//=================================================================================================
#endif // __WOUND_HEALING_SOLVER_H__
//=================================================================================================
