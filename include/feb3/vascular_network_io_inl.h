/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __VASCULAR_NETWORK_IO_INLINE_H__
#define __VASCULAR_NETWORK_IO_INLINE_H__
//=================================================================================================
inline
void write_current_vascular_network ( const std::map<dof_id_type, Point>&              new_node_to_add,
                                      const std::map<dof_id_type, Dual<dof_id_type> >& new_elem_to_add,
                                      const std::map<dof_id_type, VascularInfo>&       new_node_vi_to_add,
                                      const std::map<dof_id_type, Point>&                  existing_node_to_update,
                                      const std::map<dof_id_type, Dual<dof_id_type> >&     existing_elem_to_update,
                                      const std::map<dof_id_type, VascularInfo>&           existing_node_vi_to_update,
                                      const std::map<dof_id_type, std::set<dof_id_type> >& existing_branch2node,
                                      std::ofstream& file ) {
    // data #1: std::map<dof_id_type, Point > new_node_to_add;
    {
        const unsigned int n = new_node_to_add.size();
        file.write((char*)&n, sizeof(unsigned int));
        for ( std::map<dof_id_type, Point>::const_iterator
              ci=new_node_to_add.begin(); ci!=new_node_to_add.end(); ci++ ) {
            file.write((char*)&ci->first, sizeof(dof_id_type));
            //
            file.write((char*)&ci->second(0), sizeof(Real));
            file.write((char*)&ci->second(1), sizeof(Real));
            file.write((char*)&ci->second(2), sizeof(Real));
        }
    }
    // data #2: std::map<dof_id_type, Dual<dof_id_type> > new_elem_to_add;
    {
        const unsigned int n = new_elem_to_add.size();
        file.write((char*)&n, sizeof(unsigned int));
        for ( std::map<dof_id_type, Dual<dof_id_type> >::const_iterator
              ci=new_elem_to_add.begin(); ci!=new_elem_to_add.end(); ci++ ) {
            file.write((char*)&ci->first, sizeof(dof_id_type));
            //
            file.write((char*)&ci->second.get_first(),  sizeof(dof_id_type));
            file.write((char*)&ci->second.get_second(), sizeof(dof_id_type));
        }
    }
    // data #3: std::map<dof_id_type, VascularInfo> new_node_vi_to_add;
    {
        const unsigned int n = new_node_vi_to_add.size();
        file.write((char*)&n, sizeof(unsigned int));
        for ( std::map<dof_id_type, VascularInfo>::const_iterator
              ci=new_node_vi_to_add.begin(); ci!=new_node_vi_to_add.end(); ci++ ) {
            file.write((char*)&ci->first, sizeof(dof_id_type));
            //
            const VascularInfo* vinf = &(ci->second);
            file.write((char*)&vinf->level,  sizeof(int));
            file.write((char*)&vinf->flow,   sizeof(VascularInfo::Flow));
            file.write((char*)&vinf->type,   sizeof(VascularInfo::Type));
            file.write((char*)&vinf->status, sizeof(VascularInfo::Status));
            file.write((char*)&vinf->time_generate, sizeof(Real));
            file.write((char*)&vinf->time_branch,   sizeof(Real));
            file.write((char*)&vinf->time_mature,   sizeof(Real));
            file.write((char*)&vinf->elem3d_id, sizeof(dof_id_type));
            file.write((char*)&vinf->length,       sizeof(Real));
            file.write((char*)&vinf->total_length, sizeof(Real));
            file.write((char*)&vinf->radius,         sizeof(Real));
            file.write((char*)&vinf->thickness,      sizeof(Real));
            file.write((char*)&vinf->pore_radius,    sizeof(Real));
            file.write((char*)&vinf->pores_fraction, sizeof(Real));
            file.write((char*)&vinf->branch_id,        sizeof(unsigned int));
            file.write((char*)&vinf->parent_branch_id, sizeof(unsigned int));
            const std::set<dof_id_type>& elmls = vinf->vsc_elem;
            const unsigned int n_vsc_elem = elmls.size();
            file.write((char*)&n_vsc_elem, sizeof(unsigned int));
            for (std::set<dof_id_type>::const_iterator l=elmls.begin(); l!=elmls.end(); l++) {
                const dof_id_type jn = (*l);
                file.write((char*)&jn, sizeof(dof_id_type));
            }
            file.write((char*)&vinf->press_ratio,       sizeof(Real));
            file.write((char*)&vinf->press_ratio_cr,    sizeof(Real));
            file.write((char*)&vinf->compression_scale, sizeof(Real));
        }
    }
    // data #4: std::map<dof_id_type, Point > existing_node_to_update;
    {
        const unsigned int n = existing_node_to_update.size();
        file.write((char*)&n, sizeof(unsigned int));
        for ( std::map<dof_id_type, Point>::const_iterator
              ci=existing_node_to_update.begin(); ci!=existing_node_to_update.end(); ci++ ) {
            file.write((char*)&ci->first, sizeof(dof_id_type));
            //
            file.write((char*)&ci->second(0), sizeof(Real));
            file.write((char*)&ci->second(1), sizeof(Real));
            file.write((char*)&ci->second(2), sizeof(Real));
        }
    }
    // data #5: std::map<dof_id_type, Dual<dof_id_type> > existing_elem_to_update;
    {
        const unsigned int n = existing_elem_to_update.size();
        file.write((char*)&n, sizeof(unsigned int));
        for ( std::map<dof_id_type, Dual<dof_id_type> >::const_iterator
              ci=existing_elem_to_update.begin(); ci!=existing_elem_to_update.end(); ci++ ) {
            file.write((char*)&ci->first, sizeof(dof_id_type));
            //
            file.write((char*)&ci->second.get_first(),  sizeof(dof_id_type));
            file.write((char*)&ci->second.get_second(), sizeof(dof_id_type));
        }
    }
    // data #6: std::map<dof_id_type, VascularInfo> existing_node_vi_to_update;
    {
        const unsigned int n = existing_node_vi_to_update.size();
        file.write((char*)&n, sizeof(unsigned int));
        for ( std::map<dof_id_type, VascularInfo>::const_iterator
              ci=existing_node_vi_to_update.begin(); ci!=existing_node_vi_to_update.end(); ci++ ) {
            file.write((char*)&ci->first, sizeof(dof_id_type));
            //
            const VascularInfo* vinf = &(ci->second);
            file.write((char*)&vinf->level,  sizeof(int));
            file.write((char*)&vinf->flow,   sizeof(VascularInfo::Flow));
            file.write((char*)&vinf->type,   sizeof(VascularInfo::Type));
            file.write((char*)&vinf->status, sizeof(VascularInfo::Status));
            file.write((char*)&vinf->time_generate, sizeof(Real));
            file.write((char*)&vinf->time_branch,   sizeof(Real));
            file.write((char*)&vinf->time_mature,   sizeof(Real));
            file.write((char*)&vinf->elem3d_id, sizeof(dof_id_type));
            file.write((char*)&vinf->length,       sizeof(Real));
            file.write((char*)&vinf->total_length, sizeof(Real));
            file.write((char*)&vinf->radius,         sizeof(Real));
            file.write((char*)&vinf->thickness,      sizeof(Real));
            file.write((char*)&vinf->pore_radius,    sizeof(Real));
            file.write((char*)&vinf->pores_fraction, sizeof(Real));
            file.write((char*)&vinf->branch_id,        sizeof(unsigned int));
            file.write((char*)&vinf->parent_branch_id, sizeof(unsigned int));
            const std::set<dof_id_type>& elmls = vinf->vsc_elem;
            const unsigned int n_vsc_elem = elmls.size();
            file.write((char*)&n_vsc_elem, sizeof(unsigned int));
            for (std::set<dof_id_type>::const_iterator l=elmls.begin(); l!=elmls.end(); l++) {
                const dof_id_type jn = (*l);
                file.write((char*)&jn, sizeof(dof_id_type));
            }
            file.write((char*)&vinf->press_ratio,       sizeof(Real));
            file.write((char*)&vinf->press_ratio_cr,    sizeof(Real));
            file.write((char*)&vinf->compression_scale, sizeof(Real));
        }
    }
    // data #7: std::map<dof_id_type, std::set<dof_id_type> > existing_branch2node;
    {
        const unsigned int n = existing_branch2node.size();
        file.write((char*)&n, sizeof(unsigned int));
        for ( std::map<dof_id_type, std::set<dof_id_type> >::const_iterator
              ci=existing_branch2node.begin(); ci!=existing_branch2node.end(); ci++ ) {
            file.write((char*)&ci->first, sizeof(dof_id_type));
            //
            const std::set<dof_id_type>& nodls = ci->second;
            const unsigned int n_nodes = nodls.size();
            file.write((char*)&n_nodes, sizeof(unsigned int));
            for (std::set<dof_id_type>::const_iterator l=nodls.begin(); l!=nodls.end(); l++) {
                const dof_id_type jn = (*l);
                file.write((char*)&jn, sizeof(int));
            }
        }
    }
    // end of write up...
}
inline
void read_current_vascular_network ( std::map<dof_id_type, Point>&              new_node_to_add,
                                     std::map<dof_id_type, Dual<dof_id_type> >& new_elem_to_add,
                                     std::map<dof_id_type, VascularInfo>&       new_node_vi_to_add,
                                     std::map<dof_id_type, Point>&                  existing_node_to_update,
                                     std::map<dof_id_type, Dual<dof_id_type> >&     existing_elem_to_update,
                                     std::map<dof_id_type, VascularInfo>&           existing_node_vi_to_update,
                                     std::map<dof_id_type, std::set<dof_id_type> >& existing_branch2node,
                                     std::ifstream& file ) {
    // clear memory first
    new_node_to_add.clear();
    new_elem_to_add.clear();
    new_node_vi_to_add.clear();
    existing_node_to_update.clear();
    existing_elem_to_update.clear();
    existing_node_vi_to_update.clear();
    existing_branch2node.clear();
    // data #1: std::map<dof_id_type, Point > new_node_to_add;
    {
        unsigned int n;
        file.read((char*)&n, sizeof(unsigned int));
        for (unsigned int i=0; i<n; i++) {
            dof_id_type id;
            file.read((char*)&id, sizeof(dof_id_type));
            //
            if ( new_node_to_add.end() != new_node_to_add.find(id) ) libmesh_error();
            //
            Point xyz;
            file.read((char*)&xyz(0), sizeof(Real));
            file.read((char*)&xyz(1), sizeof(Real));
            file.read((char*)&xyz(2), sizeof(Real));
            //
            new_node_to_add.insert( std::make_pair(id, xyz) );
        }
    }
    // data #2: std::map<dof_id_type, Dual<dof_id_type> > new_elem_to_add;
    {
        unsigned int n;
        file.read((char*)&n, sizeof(unsigned int));
        for (unsigned int i=0; i<n; i++) {
            dof_id_type id;
            file.read((char*)&id, sizeof(dof_id_type));
            //
            if ( new_elem_to_add.end() != new_elem_to_add.find(id) ) libmesh_error();
            //
            dof_id_type node_0, node_1;
            file.read((char*)&node_0, sizeof(dof_id_type));
            file.read((char*)&node_1, sizeof(dof_id_type));
            Dual<dof_id_type> enod(node_0, node_1);
            //
            new_elem_to_add.insert( std::make_pair(id, enod) );
        }
    }
    // data #3: std::map<dof_id_type, VascularInfo> new_node_vi_to_add;
    {
        unsigned int n;
        file.read((char*)&n, sizeof(unsigned int));
        for (unsigned int i=0; i<n; i++) {
            dof_id_type id;
            file.read((char*)&id, sizeof(dof_id_type));
            //
            if ( new_node_vi_to_add.end() != new_node_vi_to_add.find(id) ) libmesh_error();
            //
            VascularInfo vinf;
            file.read((char*)&vinf.level,  sizeof(int));
            file.read((char*)&vinf.flow,   sizeof(VascularInfo::Flow));
            file.read((char*)&vinf.type,   sizeof(VascularInfo::Type));
            file.read((char*)&vinf.status, sizeof(VascularInfo::Status));
            file.read((char*)&vinf.time_generate, sizeof(Real));
            file.read((char*)&vinf.time_branch,   sizeof(Real));
            file.read((char*)&vinf.time_mature,   sizeof(Real));
            file.read((char*)&vinf.elem3d_id, sizeof(dof_id_type));
            file.read((char*)&vinf.length,       sizeof(Real));
            file.read((char*)&vinf.total_length, sizeof(Real));
            file.read((char*)&vinf.radius,         sizeof(Real));
            file.read((char*)&vinf.thickness,      sizeof(Real));
            file.read((char*)&vinf.pore_radius,    sizeof(Real));
            file.read((char*)&vinf.pores_fraction, sizeof(Real));
            file.read((char*)&vinf.branch_id,        sizeof(unsigned int));
            file.read((char*)&vinf.parent_branch_id, sizeof(unsigned int));
            unsigned int n_vsc_elem;
            file.read((char*)&n_vsc_elem, sizeof(unsigned int));
            vinf.vsc_elem.clear();
            for (unsigned int l=0; l<n_vsc_elem; l++) {
                dof_id_type jn;
                file.read((char*)&jn, sizeof(dof_id_type));
                vinf.vsc_elem.insert(jn);
            }
            file.read((char*)&vinf.press_ratio,       sizeof(Real));
            file.read((char*)&vinf.press_ratio_cr,    sizeof(Real));
            file.read((char*)&vinf.compression_scale, sizeof(Real));
            //
            new_node_vi_to_add.insert( std::make_pair(id, vinf) );
        }
    }
    // data #4: std::map<dof_id_type, Point > existing_node_to_update;
    {
        unsigned int n;
        file.read((char*)&n, sizeof(unsigned int));
        for (unsigned int i=0; i<n; i++) {
            dof_id_type id;
            file.read((char*)&id, sizeof(dof_id_type));
            //
            if ( existing_node_to_update.end() != existing_node_to_update.find(id) ) libmesh_error();
            //
            Point xyz;
            file.read((char*)&xyz(0), sizeof(Real));
            file.read((char*)&xyz(1), sizeof(Real));
            file.read((char*)&xyz(2), sizeof(Real));
            //
            existing_node_to_update.insert( std::make_pair(id, xyz) );
        }
    }
    // data #5: std::map<dof_id_type, Dual<dof_id_type> > existing_elem_to_update;
    {
        unsigned int n;
        file.read((char*)&n, sizeof(unsigned int));
        for (unsigned int i=0; i<n; i++) {
            dof_id_type id;
            file.read((char*)&id, sizeof(dof_id_type));
            //
            if ( existing_elem_to_update.end() != existing_elem_to_update.find(id) ) libmesh_error();
            //
            dof_id_type node_0, node_1;
            file.read((char*)&node_0, sizeof(dof_id_type));
            file.read((char*)&node_1, sizeof(dof_id_type));
            Dual<dof_id_type> enod(node_0, node_1);
            //
            existing_elem_to_update.insert( std::make_pair(id, enod) );
        }
    }
    // data #6: std::map<dof_id_type, VascularInfo> existing_node_vi_to_update;
    {
        unsigned int n;
        file.read((char*)&n, sizeof(unsigned int));
        for (unsigned int i=0; i<n; i++) {
            dof_id_type id;
            file.read((char*)&id, sizeof(dof_id_type));
            //
            if ( existing_node_vi_to_update.end() != existing_node_vi_to_update.find(id) ) libmesh_error();
            //
            VascularInfo vinf;
            file.read((char*)&vinf.level,  sizeof(int));
            file.read((char*)&vinf.flow,   sizeof(VascularInfo::Flow));
            file.read((char*)&vinf.type,   sizeof(VascularInfo::Type));
            file.read((char*)&vinf.status, sizeof(VascularInfo::Status));
            file.read((char*)&vinf.time_generate, sizeof(Real));
            file.read((char*)&vinf.time_branch,   sizeof(Real));
            file.read((char*)&vinf.time_mature,   sizeof(Real));
            file.read((char*)&vinf.elem3d_id, sizeof(dof_id_type));
            file.read((char*)&vinf.length,       sizeof(Real));
            file.read((char*)&vinf.total_length, sizeof(Real));
            file.read((char*)&vinf.radius,         sizeof(Real));
            file.read((char*)&vinf.thickness,      sizeof(Real));
            file.read((char*)&vinf.pore_radius,    sizeof(Real));
            file.read((char*)&vinf.pores_fraction, sizeof(Real));
            file.read((char*)&vinf.branch_id,        sizeof(unsigned int));
            file.read((char*)&vinf.parent_branch_id, sizeof(unsigned int));
            unsigned int n_vsc_elem;
            file.read((char*)&n_vsc_elem, sizeof(unsigned int));
            vinf.vsc_elem.clear();
            for (unsigned int l=0; l<n_vsc_elem; l++) {
                dof_id_type jn;
                file.read((char*)&jn, sizeof(dof_id_type));
                vinf.vsc_elem.insert(jn);
            }
            file.read((char*)&vinf.press_ratio,       sizeof(Real));
            file.read((char*)&vinf.press_ratio_cr,    sizeof(Real));
            file.read((char*)&vinf.compression_scale, sizeof(Real));
            //
            existing_node_vi_to_update.insert( std::make_pair(id, vinf) );
        }
    }
    // data #7: std::map<dof_id_type, std::set<dof_id_type> > existing_branch2node;
    {
        unsigned int n;
        file.read((char*)&n, sizeof(unsigned int));
        for (unsigned int i=0; i<n; i++) {
            dof_id_type id;
            file.read((char*)&id, sizeof(dof_id_type));
            //
            if ( existing_branch2node.end() != existing_branch2node.find(id) ) libmesh_error();
            //
            unsigned int n_nodes;
            file.read((char*)&n_nodes, sizeof(unsigned int));
            std::set<dof_id_type> nodls;
            for (unsigned int l=0; l<n_nodes; l++) {
                dof_id_type jn;
                file.read((char*)&jn, sizeof(int));
                nodls.insert(jn);
            }
            //
            existing_branch2node.insert( std::make_pair(id, nodls) );
        }
    }
    // end of reading...
}
//=================================================================================================
#endif // __VASCULAR_NETWORK_IO_INLINE_H__
//=================================================================================================
