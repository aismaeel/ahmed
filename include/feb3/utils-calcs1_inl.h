/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __UTILS_CALCS1_INLINE_H__
#define __UTILS_CALCS1_INLINE_H__
//=================================================================================================
inline
Real trace (const Dyadic2D& A) {
    return A(0,0)+A(1,1);
}
inline
Real determinant (const Dyadic2D& A) {
    return A(0,0)*A(1,1)-A(0,1)*A(1,0);
}
inline
Dyadic2D transpose (const Dyadic2D& A) {
    Dyadic2D A_t;
    A_t.comp = A(0,0), A(1,0),
               A(0,1), A(1,1);
    return A_t;
}
inline
Dyadic2D inverse (const Dyadic2D& A) {
    const Real det_A = determinant(A);
    libmesh_assert( det_A > 1.0e-12 );
    Dyadic2D A_inv;
    A_inv.comp = A(1,1)/det_A,-A(0,1)/det_A,
                -A(1,0)/det_A, A(0,0)/det_A;
    return A_inv;
}
inline
Real product (const Vector2D& A, const Vector2D& B) {
    return A(0)*B(0)+A(1)*B(1);
}
inline
Real operator* (const Vector2D& A, const Vector2D& B) {
    return product(A, B);
}
inline
Real magnitude (const Vector2D& A) {
    return sqrt(product(A, A));
}
inline
Vector2D unit (const Vector2D& A) {
    const Real mag_A = magnitude(A);
    Vector2D R;
    R.comp = A(0)/mag_A, A(1)/mag_A;
    return R;
}
inline
Real inner_product (const Dyadic2D& A, const Dyadic2D& B) {
    return A(0,0)*B(0,0)+A(0,1)*B(1,0)
          +A(1,0)*B(0,1)+A(1,1)*B(1,1);
}
inline
Real outer_product (const Dyadic2D& A, const Dyadic2D& B) {
    return A(0,0)*B(0,0)+A(0,1)*B(0,1)
          +A(1,0)*B(1,0)+A(1,1)*B(1,1);
}
inline
Dyadic2D scale (const Real& s, const Dyadic2D& A) {
    Dyadic2D R;
    R.comp = s*A(0,0), s*A(0,1),
             s*A(1,0), s*A(1,1);
    return R;
}
inline
Vector2D vector (const Real& x0, const Real& x1) {
    Vector2D R;
    R.comp = x0, x1;
    return R;
}
inline
bool are_equal (const Vector2D& A, const Vector2D& B, Real tol =1.0e-6) {
    return ( fabs(A(0)-B(0))<=tol && fabs(A(1)-B(1))<=tol );
}
inline
bool are_equal (const Dyadic2D& A, const Dyadic2D& B, Real tol =1.0e-6) {
    return ( fabs(A(0,0)-B(0,0))<=tol && fabs(A(0,1)-B(0,1))<=tol &&
             fabs(A(1,0)-B(1,0))<=tol && fabs(A(1,1)-B(1,1))<=tol );
}
//-------------------------------------------------------------------------------------------------
inline
Real trace (const Dyadic3D& A) {
    return A(0,0)+A(1,1)+A(2,2);
}
inline
Real determinant (const Dyadic3D& A) {
    return A(0,0)*(A(1,1)*A(2,2)-A(1,2)*A(2,1))
          -A(0,1)*(A(1,0)*A(2,2)-A(1,2)*A(2,0))
          +A(0,2)*(A(1,0)*A(2,1)-A(1,1)*A(2,0));
}
inline
Dyadic3D transpose (const Dyadic3D& A) {
    Dyadic3D A_t;
    A_t.comp = A(0,0), A(1,0), A(2,0),
               A(0,1), A(1,1), A(2,1),
               A(0,2), A(1,2), A(2,2);
    return A_t;
}
inline
Dyadic3D inverse (const Dyadic3D& A) {
    const Real det_A = determinant(A);
    libmesh_assert( det_A > 1.0e-12 );
    Dyadic3D A_inv;
    A_inv.comp = (A(1,1)*A(2,2)-A(2,1)*A(1,2))/det_A,-(A(0,1)*A(2,2)-A(2,1)*A(0,2))/det_A, (A(0,1)*A(1,2)-A(1,1)*A(0,2))/det_A,
                -(A(1,0)*A(2,2)-A(2,0)*A(1,2))/det_A, (A(0,0)*A(2,2)-A(2,0)*A(0,2))/det_A,-(A(0,0)*A(1,2)-A(1,0)*A(0,2))/det_A,
                 (A(1,0)*A(2,1)-A(2,0)*A(1,1))/det_A,-(A(0,0)*A(2,1)-A(2,0)*A(0,1))/det_A, (A(0,0)*A(1,1)-A(1,0)*A(0,1))/det_A;
    return A_inv;
}
inline
Real product (const Vector3D& A, const Vector3D& B) {
    return A(0)*B(0)+A(1)*B(1)+A(2)*B(2);
}
inline
Real operator* (const Vector3D& A, const Vector3D& B) {
    return product(A, B);
}
inline
Real magnitude (const Vector3D& A) {
    return sqrt(product(A, A));
}
inline
Vector3D unit (const Vector3D& A) {
    const Real mag_A = magnitude(A);
    Vector3D R;
    R.comp = A(0)/mag_A, A(1)/mag_A, A(2)/mag_A;
    return R;
}
inline
Real inner_product (const Dyadic3D& A, const Dyadic3D& B) {
    return A(0,0)*B(0,0)+A(0,1)*B(1,0)+A(0,2)*B(2,0)
          +A(1,0)*B(0,1)+A(1,1)*B(1,1)+A(1,2)*B(2,1)
          +A(2,0)*B(0,2)+A(2,1)*B(1,2)+A(2,2)*B(2,2);
}
inline
Real outer_product (const Dyadic3D& A, const Dyadic3D& B) {
    return A(0,0)*B(0,0)+A(0,1)*B(0,1)+A(0,2)*B(0,2)
          +A(1,0)*B(1,0)+A(1,1)*B(1,1)+A(1,2)*B(1,2)
          +A(2,0)*B(2,0)+A(2,1)*B(2,1)+A(2,2)*B(2,2);
}
inline
Dyadic3D scale (const Real& s, const Dyadic3D& A) {
    Dyadic3D R;
    R.comp = s*A(0,0), s*A(0,1), s*A(0,2),
             s*A(1,0), s*A(1,1), s*A(1,2),
             s*A(2,0), s*A(2,1), s*A(2,2);
    return R;
}
inline
Vector3D vector (const Real& x0, const Real& x1, const Real& x2) {
    Vector3D R;
    R.comp = x0, x1, x2;
    return R;
}
inline
bool are_equal (const Vector3D& A, const Vector3D& B, Real tol =1.0e-6) {
    return ( fabs(A(0)-B(0))<=tol && fabs(A(1)-B(1))<=tol && fabs(A(2)-B(2))<=tol );
}
inline
bool are_equal (const Dyadic3D& A, const Dyadic3D& B, Real tol =1.0e-6) {
    return ( fabs(A(0,0)-B(0,0))<=tol && fabs(A(0,1)-B(0,1))<=tol && fabs(A(0,2)-B(0,2))<=tol &&
             fabs(A(1,0)-B(1,0))<=tol && fabs(A(1,1)-B(1,1))<=tol && fabs(A(1,2)-B(1,2))<=tol &&
             fabs(A(2,0)-B(2,0))<=tol && fabs(A(2,1)-B(2,1))<=tol && fabs(A(2,2)-B(2,2))<=tol );
}
//-------------------------------------------------------------------------------------------------
inline
Vector3D cross (const Vector3D& u, const Vector3D& v) {
    Vector3D p;
    p.comp = (u(1)*v(2)-u(2)*v(1)), -(u(0)*v(2)-u(2)*v(0)), (u(0)*v(1)-u(1)*v(0));
    return p;
}
inline
Real product (const Vector3D& L, const Dyadic3D& D, const Vector3D& R) {
    return L(0)*(D(0,0)*R(0)+D(0,1)*R(1)+D(0,2)*R(2))
          +L(1)*(D(1,0)*R(0)+D(1,1)*R(1)+D(1,2)*R(2))
          +L(2)*(D(2,0)*R(0)+D(2,1)*R(1)+D(2,2)*R(2));
}
inline
Vector3D vec (const RealGradient& grad) {
    Vector3D R;
    R.comp = grad(0), grad(1), grad(2);
    return R;
}
inline
RealGradient vec (const Vector3D& v) {
    RealGradient R(v(0), v(1), v(2));
    return R;
}
inline
Dyadic3D tensor (const Vector3D& A, const Vector3D& B) {
    Dyadic3D R;
    R.comp = A(0)*B(0), A(0)*B(1), A(0)*B(2),
             A(1)*B(0), A(1)*B(1), A(1)*B(2),
             A(2)*B(0), A(2)*B(1), A(2)*B(2);
    return R;
}
inline
Dyadic3D tensor (const Vector3D& A) {
    return tensor(A, A);
}
inline
Triadic3D tensor (const Vector3D& A, const Vector3D& B, const Vector3D& C) {
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    Triadic3D R;
    R.comp = A.comp(i)*B.comp(j)*C.comp(k);
    return R;
}
inline
Quindic3D tensor (const Vector3D& A, const Vector3D& B, const Vector3D& C, const Vector3D& D) {
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    Quindic3D R;
    R.comp = A.comp(i)*B.comp(j)*C.comp(k)*D.comp(l);
    return R;
}
inline
Quindic3D tensor (const Dyadic3D& A, const Dyadic3D& B) {
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    Quindic3D R;
    R.comp = A.comp(i,j)*B.comp(k,l);
    return R;
}
inline
Dyadic3D symm (const Dyadic3D& A) {
    Dyadic3D R;
    R.comp = A(0,0),              0.5*(A(0,1)+A(1,0)), 0.5*(A(0,2)+A(2,0)),
             0.5*(A(1,0)+A(0,1)), A(1,1),              0.5*(A(1,2)+A(2,1)),
             0.5*(A(2,0)+A(0,2)), 0.5*(A(2,1)+A(1,2)), A(2,2);
    return R;
}
inline
Dyadic3D skew (const Dyadic3D& A) {
    Dyadic3D R;
    R.comp = 0.0,                 0.5*(A(0,1)-A(1,0)), 0.5*(A(0,2)-A(2,0)),
             0.5*(A(1,0)-A(0,1)), 0.0,                 0.5*(A(1,2)-A(2,1)),
             0.5*(A(2,0)-A(0,2)), 0.5*(A(2,1)-A(1,2)), 0.0;
    return R;
}
inline
Quindic3D symm_tensor (const Dyadic3D& A) {
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    Quindic3D R;
    R.comp = 0.5*(A.comp(i,k)*A.comp(j,l) + A.comp(i,l)*A.comp(j,k));
    return R;
}
inline
Quindic3D op_symm_tensor (const Dyadic3D& A) {
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    blitz::fourthIndex l;
    Quindic3D R;
    R.comp = -0.5*(A.comp(i,k)*A.comp(j,l) + A.comp(i,l)*A.comp(j,k));
    return R;
}
//-------------------------------------------------------------------------------------------------
inline
Dyadic3D tensor (const RealGradient& A, const RealGradient& B) {
    Dyadic3D R;
    R.comp = A(0)*B(0), A(0)*B(1), A(0)*B(2),
             A(1)*B(0), A(1)*B(1), A(1)*B(2),
             A(2)*B(0), A(2)*B(1), A(2)*B(2);
    return R;
}
inline
Dyadic3D tensor (const RealGradient& A) {
    return tensor(A, A);
}
//-------------------------------------------------------------------------------------------------
inline
Real first_invariant (const Dyadic3D& A) {
    return trace(A);
}
inline
Real second_invariant (const Dyadic3D& A) {
    return 0.5*(pow2(first_invariant(A))-inner_product(A, A));
}
inline
Real third_invariant (const Dyadic3D& A) {
    return determinant(A);
}
//-------------------------------------------------------------------------------------------------
inline
Dyadic3D volumetric (const Dyadic3D& A) {
    const Real tr_A = (A(0,0)+A(1,1)+A(2,2))/3.0;
    Dyadic3D A_vol;
    A_vol.comp = tr_A, 0.0,  0.0,
                 0.0,  tr_A, 0.0,
                 0.0,  0.0,  tr_A;
    return A_vol;
}
inline
Dyadic3D deviatoric (const Dyadic3D& A) {
    Dyadic3D A_dev;
    A_dev.comp = A.comp - volumetric(A).comp;
    return A_dev;
}
//-------------------------------------------------------------------------------------------------
inline
Dyadic3D identity_3d () {
    Dyadic3D I;
    I.comp = 1.0, 0.0, 0.0,
             0.0, 1.0, 0.0,
             0.0, 0.0, 1.0;
    return I;
}
inline
Dyadic3D random_diagonal (const Real vMin, const Real vMax) {
    srand(static_cast<unsigned int>(time(NULL)));
    //
    Dyadic3D R;
    R.comp = uniform_distribution(vMin, vMax), 0.0, 0.0,
             0.0, uniform_distribution(vMin, vMax), 0.0,
             0.0, 0.0, uniform_distribution(vMin, vMax);
    return R;
}
inline
Triadic3D permutation () {
    const Point e0(1.0, 0.0, 0.0),
                e1(0.0, 1.0, 0.0),
                e2(0.0, 0.0, 1.0);
    Triadic3D E;
    E.comp(0,0,0) = e0*(e0.cross(e0)); E.comp(0,0,1) = e0*(e0.cross(e1)); E.comp(0,0,2) = e0*(e0.cross(e2));
    E.comp(0,1,0) = e0*(e1.cross(e0)); E.comp(0,1,1) = e0*(e1.cross(e1)); E.comp(0,1,2) = e0*(e1.cross(e2));
    E.comp(0,2,0) = e0*(e2.cross(e0)); E.comp(0,2,1) = e0*(e2.cross(e1)); E.comp(0,2,2) = e0*(e2.cross(e2));
    E.comp(1,0,0) = e1*(e0.cross(e0)); E.comp(1,0,1) = e1*(e0.cross(e1)); E.comp(1,0,2) = e1*(e0.cross(e2));
    E.comp(1,1,0) = e1*(e1.cross(e0)); E.comp(1,1,1) = e1*(e1.cross(e1)); E.comp(1,1,2) = e1*(e1.cross(e2));
    E.comp(1,2,0) = e1*(e2.cross(e0)); E.comp(1,2,1) = e1*(e2.cross(e1)); E.comp(1,2,2) = e1*(e2.cross(e2));
    E.comp(2,0,0) = e2*(e0.cross(e0)); E.comp(2,0,1) = e2*(e0.cross(e1)); E.comp(2,0,2) = e2*(e0.cross(e2));
    E.comp(2,1,0) = e2*(e1.cross(e0)); E.comp(2,1,1) = e2*(e1.cross(e1)); E.comp(2,1,2) = e2*(e1.cross(e2));
    E.comp(2,2,0) = e2*(e2.cross(e0)); E.comp(2,2,1) = e2*(e2.cross(e1)); E.comp(2,2,2) = e2*(e2.cross(e2));
    return E;
}
//-------------------------------------------------------------------------------------------------
inline
Real min_eigen (const Dyadic3D& A, Vector3D& v) {
    std::vector<Real> eigval;
    std::vector<Vector3D> eigvec;
    eigen_solve(A, eigval, eigvec);
    v = eigvec[2];
    return eigval[2];
}
inline
Vector3D min_eigen (const Dyadic3D& A) {
    Vector3D v;
    Real l = min_eigen(A, v);
    v.comp *= l;
    return v;
}
inline
Real max_eigen (const Dyadic3D& A, Vector3D& v) {
    std::vector<Real> eigval;
    std::vector<Vector3D> eigvec;
    eigen_solve(A, eigval, eigvec);
    v = eigvec[0];
    return eigval[0];
}
inline
Vector3D max_eigen (const Dyadic3D& A) {
    Vector3D v;
    Real l = max_eigen(A, v);
    v.comp *= l;
    return v;
}
//-------------------------------------------------------------------------------------------------
inline
Dyadic3D operator* (const Real s, const Dyadic3D& m) {
    Dyadic3D R;
    R.comp = s*m(0,0), s*m(0,1), s*m(0,2),
             s*m(1,0), s*m(1,1), s*m(1,2),
             s*m(2,0), s*m(2,1), s*m(2,2);
    return R;
}
inline
Dyadic3D operator* (const Dyadic3D& m, const Real s) {
    Dyadic3D R;
    R.comp = s*m(0,0), s*m(0,1), s*m(0,2),
             s*m(1,0), s*m(1,1), s*m(1,2),
             s*m(2,0), s*m(2,1), s*m(2,2);
    return R;
}
inline
Point operator* (const Dyadic3D& m, const Point& v) {
    Point r;
    r(0) = m(0,0)*v(0)+m(0,1)*v(1)+m(0,2)*v(2);
    r(1) = m(1,0)*v(0)+m(1,1)*v(1)+m(1,2)*v(2);
    r(2) = m(2,0)*v(0)+m(2,1)*v(1)+m(2,2)*v(2);
    return r;
}
inline
Point operator/ (const Point& v, const Dyadic3D& m) {
    const Real d = determinant(m);
    libmesh_assert( d > 1.0e-12 );
    Point r;
    r(0) =  (m(1,0)*m(2,1)*v(2)-m(1,0)*m(2,2)*v(1)-m(1,1)*m(2,0)*v(2)+m(1,1)*m(2,2)*v(0)+m(1,2)*m(2,0)*v(1)-m(1,2)*m(2,1)*v(0))/d;
    r(1) = -(m(0,0)*m(2,1)*v(2)-m(0,0)*m(2,2)*v(1)-m(0,1)*m(2,0)*v(2)+m(0,1)*m(2,2)*v(0)+m(0,2)*m(2,0)*v(1)-m(0,2)*m(2,1)*v(0))/d;
    r(2) =  (m(0,0)*m(1,1)*v(2)-m(0,0)*m(1,2)*v(1)-m(0,1)*m(1,0)*v(2)+m(0,1)*m(1,2)*v(0)+m(0,2)*m(1,0)*v(1)-m(0,2)*m(1,1)*v(0))/d;
    return r;
}
//=================================================================================================
const Dyadic3D  zero_matrix = Dyadic3D();
const Dyadic3D  identity_matrix = identity_3d();
const Quindic3D identity_tensor = symm_tensor(identity_matrix);
const Triadic3D permutation_tensor = permutation();
//=================================================================================================
#endif //  __UTILS_CALCS1_INLINE_H__
//=================================================================================================
