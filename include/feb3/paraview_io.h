/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __PARAVIEW_IO_H__
#define __PARAVIEW_IO_H__
//=================================================================================================
#include "libmesh/libmesh_common.h"
#include "libmesh/mesh_input.h"
#include "libmesh/mesh_output.h"
#include "libmesh/elem.h"
#include "feb3/quadrature.h"
//=================================================================================================
namespace libMesh {
//=================================================================================================
// Forward declarations
class MeshBase;
class MeshData;
//=================================================================================================
class ParaviewIO : public MeshInput<MeshBase>, public MeshOutput<MeshBase> {
// member functions:
public:
    ParaviewIO (MeshBase& mesh, MeshData* mesh_data =NULL);
    ParaviewIO (const MeshBase& mesh, MeshData* mesh_data =NULL);
    //
    virtual
    void write_nodal_data ( const std::string& fname, const std::vector<Number>& soln,
                            const std::vector<std::string>& names );
    void write_quadrature ( const std::string& fname,
                            const std::map<dof_id_type, QuadratureInfo>& elem_qp,
                            const std::map<dof_id_type, std::map<unsigned int, QuadratureInfo> >& elem_side_qp );
    virtual void read (const std::string& );
    virtual void write (const std::string& );
    void write_boundary (const std::string& fname);
    void write_reduced_mesh    (bool doit);
    void write_refined_mesh    (bool doit);
    void write_quadrature_data (bool doit);
    void write_auxiliary_data  (bool doit);
    void hide_variable (const std::string& name);
private:
    void _write_ascii ( const std::string& fname, const std::vector<Number>* soln,
                        const std::vector<std::string>* names );
    void _cell_connectivity (const Elem* elem, std::vector<unsigned int>& vtk_cell_conn);
    unsigned int _cell_type   (const Elem* elem);
    unsigned int _cell_offset (const Elem* elem);
    bool _is_hidden_variable (const std::string& name);
// member objects:
private:
    bool _write_reduced_mesh,
         _write_refined_mesh,
         _write_quadrature_data,
         _write_auxiliary_data;
    MeshData* _mesh_data;
    std::set<std::string> _hide_vars;
};
//=================================================================================================
#include "feb3/paraview_io_inl.h"
//=================================================================================================
}
//=================================================================================================
#endif // __PARAVIEW_IO_H__
//=================================================================================================
