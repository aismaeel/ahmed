/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __MESH_IGA_H__
#define __MESH_IGA_H__
//=================================================================================================
#include "libmesh/libmesh.h"
#include "libmesh/libmesh_common.h"
#include "libmesh/parallel_object.h"
#include "libmesh/mesh.h"
#include "libmesh/equation_systems.h"
#include "feb3/nurbs.h"
//=================================================================================================
class MeshIGA : public ParallelObject {
public:
    struct IGA_PostData {
        // NURBS elements: parametric elements: cells ID
        std::vector< std::map<dof_id_type, std::set<dof_id_type> > > nurbs_pelem_cells;
        // cells of the ghost mesh: vertices coordinates (parametric space)
        std::map<dof_id_type, std::vector<Point> > cells_vertices;
        //
        EquationSystems* es;
    };
public:
// constructor and destructor
    MeshIGA (MeshBase& mesh, std::string label =std::string("geom"));
    ~MeshIGA () {}
    //
// functions to read/write access
    inline
    void set_label (const std::string& label) { this->_label = label; }
    void set_nurbs (const std::string& fname);
    void set_parametric_mesh ();
    void set_post_data ( EquationSystems* es, 
                         const unsigned int nx=5, const unsigned int ny=5, const unsigned int nz=5);
// functions for read-only access
    inline
    const std::string get_label () const { return this->_label; }
    const Mesh& get_parametric_mesh () const;
    const std::vector<NURBS>&  get_nurbs () const;
    const std::map<Node*, std::vector<NURBS*> >& get_nodes_to_nurbs () const;
    //
    std::vector<NURBS>::const_iterator nurbs_begin () const;
    std::vector<NURBS>::const_iterator nurbs_end   () const;
    std::map<Node*, std::vector<NURBS*> >::const_iterator nodes_to_nurbs_begin () const;
    std::map<Node*, std::vector<NURBS*> >::const_iterator nodes_to_nurbs_end   () const;
    //
    void print_info (std::ostream& os =std::cout) const;
public:
    // data for post-processing
    MeshIGA::IGA_PostData post_data;
protected:
    // list of all NURBS in the mesh
    std::vector<NURBS> _nurbs;
    // map of control nodes to NURBS
    std::map<Node*, std::vector<NURBS*> > _nodes_to_nurbs;
private:
    std::string _label;
    //
    MeshBase& _ctrl_msh;
    Mesh _pphys_msh;
};
//=================================================================================================
#include "feb3/mesh_iga_inl.h"
//=================================================================================================
#endif //  __MESH_IGA_H__
//=================================================================================================
