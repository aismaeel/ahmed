/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __UTILS_CALCS2_INLINE_H__
#define __UTILS_CALCS2_INLINE_H__
//=================================================================================================
inline
void push_forward (const Dyadic3D& F, const Real& J, const Dyadic3D& S2pk, Dyadic3D& Sc) {
    Sc.comp(0,0) = (F(0,0)*((F(0,0)*S2pk(0,0))+(F(0,1)*S2pk(1,0))+(F(0,2)*S2pk(2,0)))
                   +F(0,1)*((F(0,0)*S2pk(0,1))+(F(0,1)*S2pk(1,1))+(F(0,2)*S2pk(2,1)))
                   +F(0,2)*((F(0,0)*S2pk(0,2))+(F(0,1)*S2pk(1,2))+(F(0,2)*S2pk(2,2))))/J;
    Sc.comp(0,1) = (F(1,0)*((F(0,0)*S2pk(0,0))+(F(0,1)*S2pk(1,0))+(F(0,2)*S2pk(2,0)))
                   +F(1,1)*((F(0,0)*S2pk(0,1))+(F(0,1)*S2pk(1,1))+(F(0,2)*S2pk(2,1)))
                   +F(1,2)*((F(0,0)*S2pk(0,2))+(F(0,1)*S2pk(1,2))+(F(0,2)*S2pk(2,2))))/J;
    Sc.comp(0,2) = (F(2,0)*((F(0,0)*S2pk(0,0))+(F(0,1)*S2pk(1,0))+(F(0,2)*S2pk(2,0)))
                   +F(2,1)*((F(0,0)*S2pk(0,1))+(F(0,1)*S2pk(1,1))+(F(0,2)*S2pk(2,1)))
                   +F(2,2)*((F(0,0)*S2pk(0,2))+(F(0,1)*S2pk(1,2))+(F(0,2)*S2pk(2,2))))/J;
    Sc.comp(1,0) = (F(0,0)*((F(1,0)*S2pk(0,0))+(F(1,1)*S2pk(1,0))+(F(1,2)*S2pk(2,0)))
                   +F(0,1)*((F(1,0)*S2pk(0,1))+(F(1,1)*S2pk(1,1))+(F(1,2)*S2pk(2,1)))
                   +F(0,2)*((F(1,0)*S2pk(0,2))+(F(1,1)*S2pk(1,2))+(F(1,2)*S2pk(2,2))))/J;
    Sc.comp(1,1) = (F(1,0)*((F(1,0)*S2pk(0,0))+(F(1,1)*S2pk(1,0))+(F(1,2)*S2pk(2,0)))
                   +F(1,1)*((F(1,0)*S2pk(0,1))+(F(1,1)*S2pk(1,1))+(F(1,2)*S2pk(2,1)))
                   +F(1,2)*((F(1,0)*S2pk(0,2))+(F(1,1)*S2pk(1,2))+(F(1,2)*S2pk(2,2))))/J;
    Sc.comp(1,2) = (F(2,0)*((F(1,0)*S2pk(0,0))+(F(1,1)*S2pk(1,0))+(F(1,2)*S2pk(2,0)))
                   +F(2,1)*((F(1,0)*S2pk(0,1))+(F(1,1)*S2pk(1,1))+(F(1,2)*S2pk(2,1)))
                   +F(2,2)*((F(1,0)*S2pk(0,2))+(F(1,1)*S2pk(1,2))+(F(1,2)*S2pk(2,2))))/J;
    Sc.comp(2,0) = (F(0,0)*((F(2,0)*S2pk(0,0))+(F(2,1)*S2pk(1,0))+(F(2,2)*S2pk(2,0)))
                   +F(0,1)*((F(2,0)*S2pk(0,1))+(F(2,1)*S2pk(1,1))+(F(2,2)*S2pk(2,1)))
                   +F(0,2)*((F(2,0)*S2pk(0,2))+(F(2,1)*S2pk(1,2))+(F(2,2)*S2pk(2,2))))/J;
    Sc.comp(2,1) = (F(1,0)*((F(2,0)*S2pk(0,0))+(F(2,1)*S2pk(1,0))+(F(2,2)*S2pk(2,0)))
                   +F(1,1)*((F(2,0)*S2pk(0,1))+(F(2,1)*S2pk(1,1))+(F(2,2)*S2pk(2,1)))
                   +F(1,2)*((F(2,0)*S2pk(0,2))+(F(2,1)*S2pk(1,2))+(F(2,2)*S2pk(2,2))))/J;
    Sc.comp(2,2) = (F(2,0)*((F(2,0)*S2pk(0,0))+(F(2,1)*S2pk(1,0))+(F(2,2)*S2pk(2,0)))
                   +F(2,1)*((F(2,0)*S2pk(0,1))+(F(2,1)*S2pk(1,1))+(F(2,2)*S2pk(2,1)))
                   +F(2,2)*((F(2,0)*S2pk(0,2))+(F(2,1)*S2pk(1,2))+(F(2,2)*S2pk(2,2))))/J;
}
inline
Dyadic3D push_forward (const Dyadic3D& F, const Dyadic3D& S2pk) {
    const Real J = F(0,0)*(F(1,1)*F(2,2)-F(1,2)*F(2,1))
                 - F(0,1)*(F(1,0)*F(2,2)-F(1,2)*F(2,0))
                 + F(0,2)*(F(1,0)*F(2,1)-F(1,1)*F(2,0));
    Dyadic3D Sc;
    push_forward(F, J, S2pk, Sc);
    return Sc;
}
inline
void pull_backward (const Dyadic3D& Fi, const Real& J, const Dyadic3D& Sc, Dyadic3D& S2pk) {
    S2pk.comp(0,0) = (Fi(0,0)*(Fi(0,0)*Sc(0,0)+Fi(0,1)*Sc(1,0)+Fi(0,2)*Sc(2,0))
                     +Fi(0,1)*(Fi(0,0)*Sc(0,1)+Fi(0,1)*Sc(1,1)+Fi(0,2)*Sc(2,1))
                     +Fi(0,2)*(Fi(0,0)*Sc(0,2)+Fi(0,1)*Sc(1,2)+Fi(0,2)*Sc(2,2)))*J;
    S2pk.comp(0,1) = (Fi(1,0)*(Fi(0,0)*Sc(0,0)+Fi(0,1)*Sc(1,0)+Fi(0,2)*Sc(2,0))
                     +Fi(1,1)*(Fi(0,0)*Sc(0,1)+Fi(0,1)*Sc(1,1)+Fi(0,2)*Sc(2,1))
                     +Fi(1,2)*(Fi(0,0)*Sc(0,2)+Fi(0,1)*Sc(1,2)+Fi(0,2)*Sc(2,2)))*J;
    S2pk.comp(0,2) = (Fi(2,0)*(Fi(0,0)*Sc(0,0)+Fi(0,1)*Sc(1,0)+Fi(0,2)*Sc(2,0))
                     +Fi(2,1)*(Fi(0,0)*Sc(0,1)+Fi(0,1)*Sc(1,1)+Fi(0,2)*Sc(2,1))
                     +Fi(2,2)*(Fi(0,0)*Sc(0,2)+Fi(0,1)*Sc(1,2)+Fi(0,2)*Sc(2,2)))*J;
    S2pk.comp(1,0) = (Fi(0,0)*(Fi(1,0)*Sc(0,0)+Fi(1,1)*Sc(1,0)+Fi(1,2)*Sc(2,0))
                     +Fi(0,1)*(Fi(1,0)*Sc(0,1)+Fi(1,1)*Sc(1,1)+Fi(1,2)*Sc(2,1))
                     +Fi(0,2)*(Fi(1,0)*Sc(0,2)+Fi(1,1)*Sc(1,2)+Fi(1,2)*Sc(2,2)))*J;
    S2pk.comp(1,1) = (Fi(1,0)*(Fi(1,0)*Sc(0,0)+Fi(1,1)*Sc(1,0)+Fi(1,2)*Sc(2,0))
                     +Fi(1,1)*(Fi(1,0)*Sc(0,1)+Fi(1,1)*Sc(1,1)+Fi(1,2)*Sc(2,1))
                     +Fi(1,2)*(Fi(1,0)*Sc(0,2)+Fi(1,1)*Sc(1,2)+Fi(1,2)*Sc(2,2)))*J;
    S2pk.comp(1,2) = (Fi(2,0)*(Fi(1,0)*Sc(0,0)+Fi(1,1)*Sc(1,0)+Fi(1,2)*Sc(2,0))
                     +Fi(2,1)*(Fi(1,0)*Sc(0,1)+Fi(1,1)*Sc(1,1)+Fi(1,2)*Sc(2,1))
                     +Fi(2,2)*(Fi(1,0)*Sc(0,2)+Fi(1,1)*Sc(1,2)+Fi(1,2)*Sc(2,2)))*J;
    S2pk.comp(2,0) = (Fi(0,0)*(Fi(2,0)*Sc(0,0)+Fi(2,1)*Sc(1,0)+Fi(2,2)*Sc(2,0))
                     +Fi(0,1)*(Fi(2,0)*Sc(0,1)+Fi(2,1)*Sc(1,1)+Fi(2,2)*Sc(2,1))
                     +Fi(0,2)*(Fi(2,0)*Sc(0,2)+Fi(2,1)*Sc(1,2)+Fi(2,2)*Sc(2,2)))*J;
    S2pk.comp(2,1) = (Fi(1,0)*(Fi(2,0)*Sc(0,0)+Fi(2,1)*Sc(1,0)+Fi(2,2)*Sc(2,0))
                     +Fi(1,1)*(Fi(2,0)*Sc(0,1)+Fi(2,1)*Sc(1,1)+Fi(2,2)*Sc(2,1))
                     +Fi(1,2)*(Fi(2,0)*Sc(0,2)+Fi(2,1)*Sc(1,2)+Fi(2,2)*Sc(2,2)))*J;
    S2pk.comp(2,2) = (Fi(2,0)*(Fi(2,0)*Sc(0,0)+Fi(2,1)*Sc(1,0)+Fi(2,2)*Sc(2,0))
                     +Fi(2,1)*(Fi(2,0)*Sc(0,1)+Fi(2,1)*Sc(1,1)+Fi(2,2)*Sc(2,1))
                     +Fi(2,2)*(Fi(2,0)*Sc(0,2)+Fi(2,1)*Sc(1,2)+Fi(2,2)*Sc(2,2)))*J;
}
inline
Dyadic3D pull_backward (const Dyadic3D& Fi, const Dyadic3D& Sc) {
    const Real Ji = Fi(0,0)*(Fi(1,1)*Fi(2,2)-Fi(1,2)*Fi(2,1))
                  - Fi(0,1)*(Fi(1,0)*Fi(2,2)-Fi(1,2)*Fi(2,0))
                  + Fi(0,2)*(Fi(1,0)*Fi(2,1)-Fi(1,1)*Fi(2,0));
    Dyadic3D S2pk;
    pull_backward(Fi, 1.0/Ji, Sc, S2pk);
    return S2pk;
}
inline
Dyadic3D push_forward (const Vector3D* g_, const Dyadic2D& S2pk) {
    Dyadic3D Sc;
    // g_[0](i)*S2pk(0,0)*g_[0](j)+g_[0](i)*S2pk(0,1)*g_[1](j)+g_[1](i)*S2pk(1,0)*g_[0](j)+g_[1](i)*S2pk(1,1)*g_[1](j)
    Sc.comp = g_[0](0)*S2pk(0,0)*g_[0](0)+g_[0](0)*S2pk(0,1)*g_[1](0)+g_[1](0)*S2pk(1,0)*g_[0](0)+g_[1](0)*S2pk(1,1)*g_[1](0),
              g_[0](0)*S2pk(0,0)*g_[0](1)+g_[0](0)*S2pk(0,1)*g_[1](1)+g_[1](0)*S2pk(1,0)*g_[0](1)+g_[1](0)*S2pk(1,1)*g_[1](1),
              g_[0](0)*S2pk(0,0)*g_[0](2)+g_[0](0)*S2pk(0,1)*g_[1](2)+g_[1](0)*S2pk(1,0)*g_[0](2)+g_[1](0)*S2pk(1,1)*g_[1](2),
              g_[0](1)*S2pk(0,0)*g_[0](0)+g_[0](1)*S2pk(0,1)*g_[1](0)+g_[1](1)*S2pk(1,0)*g_[0](0)+g_[1](1)*S2pk(1,1)*g_[1](0),
              g_[0](1)*S2pk(0,0)*g_[0](1)+g_[0](1)*S2pk(0,1)*g_[1](1)+g_[1](1)*S2pk(1,0)*g_[0](1)+g_[1](1)*S2pk(1,1)*g_[1](1),
              g_[0](1)*S2pk(0,0)*g_[0](2)+g_[0](1)*S2pk(0,1)*g_[1](2)+g_[1](1)*S2pk(1,0)*g_[0](2)+g_[1](1)*S2pk(1,1)*g_[1](2),
              g_[0](2)*S2pk(0,0)*g_[0](0)+g_[0](2)*S2pk(0,1)*g_[1](0)+g_[1](2)*S2pk(1,0)*g_[0](0)+g_[1](2)*S2pk(1,1)*g_[1](0),
              g_[0](2)*S2pk(0,0)*g_[0](1)+g_[0](2)*S2pk(0,1)*g_[1](1)+g_[1](2)*S2pk(1,0)*g_[0](1)+g_[1](2)*S2pk(1,1)*g_[1](1),
              g_[0](2)*S2pk(0,0)*g_[0](2)+g_[0](2)*S2pk(0,1)*g_[1](2)+g_[1](2)*S2pk(1,0)*g_[0](2)+g_[1](2)*S2pk(1,1)*g_[1](2);
    return Sc;
}
inline
Dyadic3D pull_backward (const Vector3D* G_, const Dyadic2D& Sc) {
    Dyadic3D S2pk;
    // G_[0](i)*Sc(0,0)*G_[0](j)+G_[0](i)*Sc(0,1)*G_[1](j)+G_[1](i)*Sc(1,0)*G_[0](j)+G_[1](i)*Sc(1,1)*G_[1](j)
    S2pk.comp = G_[0](0)*Sc(0,0)*G_[0](0)+G_[0](0)*Sc(0,1)*G_[1](0)+G_[1](0)*Sc(1,0)*G_[0](0)+G_[1](0)*Sc(1,1)*G_[1](0),
                G_[0](0)*Sc(0,0)*G_[0](1)+G_[0](0)*Sc(0,1)*G_[1](1)+G_[1](0)*Sc(1,0)*G_[0](1)+G_[1](0)*Sc(1,1)*G_[1](1),
                G_[0](0)*Sc(0,0)*G_[0](2)+G_[0](0)*Sc(0,1)*G_[1](2)+G_[1](0)*Sc(1,0)*G_[0](2)+G_[1](0)*Sc(1,1)*G_[1](2),
                G_[0](1)*Sc(0,0)*G_[0](0)+G_[0](1)*Sc(0,1)*G_[1](0)+G_[1](1)*Sc(1,0)*G_[0](0)+G_[1](1)*Sc(1,1)*G_[1](0),
                G_[0](1)*Sc(0,0)*G_[0](1)+G_[0](1)*Sc(0,1)*G_[1](1)+G_[1](1)*Sc(1,0)*G_[0](1)+G_[1](1)*Sc(1,1)*G_[1](1),
                G_[0](1)*Sc(0,0)*G_[0](2)+G_[0](1)*Sc(0,1)*G_[1](2)+G_[1](1)*Sc(1,0)*G_[0](2)+G_[1](1)*Sc(1,1)*G_[1](2),
                G_[0](2)*Sc(0,0)*G_[0](0)+G_[0](2)*Sc(0,1)*G_[1](0)+G_[1](2)*Sc(1,0)*G_[0](0)+G_[1](2)*Sc(1,1)*G_[1](0),
                G_[0](2)*Sc(0,0)*G_[0](1)+G_[0](2)*Sc(0,1)*G_[1](1)+G_[1](2)*Sc(1,0)*G_[0](1)+G_[1](2)*Sc(1,1)*G_[1](1),
                G_[0](2)*Sc(0,0)*G_[0](2)+G_[0](2)*Sc(0,1)*G_[1](2)+G_[1](2)*Sc(1,0)*G_[0](2)+G_[1](2)*Sc(1,1)*G_[1](2);
    return S2pk;
}
//-------------------------------------------------------------------------------------------------
inline
Dual<int> indices_Voigt_2d (const int i) {
    switch ( i ) {
    case 0 : return Dual<int>(0, 0);
    case 1 : return Dual<int>(1, 1);
    case 2 : return Dual<int>(0, 1);
    default : libmesh_error();
    }
}
inline
VectorVoigt2D compress_stress (const Dyadic2D& T) {
    VectorVoigt2D Tv;
    Tv.comp = T(0,0), T(1,1), T(0,1);
    return Tv;
}
inline
VectorVoigt2D compress_strain (const Dyadic2D& T) {
    VectorVoigt2D Tv;
    Tv.comp = T(0,0), T(1,1), 2.0*T(0,1);
    return Tv;
}
inline
Dyadic2D uncompress_stress (const VectorVoigt2D& T) {
    Dyadic2D Tv;
    Tv.comp = T(0), T(2),
              T(2), T(1);
    return Tv;
}
inline
Dyadic2D uncompress_strain (const VectorVoigt2D& T) {
    Dyadic2D Tv;
    Tv.comp = T(0),     T(2)*0.5,
              T(2)*0.5, T(1);
    return Tv;
}
inline
DyadicVoigt2D compress_constitutive_tensor (const Quindic2D& T) {
    DyadicVoigt2D Tv;
    Tv.comp = T(0,0,0,0), T(0,0,1,1), T(0,0,0,1),
              T(1,1,0,0), T(1,1,1,1), T(1,1,0,1),
              T(0,1,0,0), T(0,1,1,1), T(0,1,0,1);
    return Tv;
}
//-------------------------------------------------------------------------------------------------
inline
Dual<int> indices_Voigt_3d (const int i) {
    switch ( i ) {
    case 0 : return Dual<int>(0, 0);
    case 1 : return Dual<int>(1, 1);
    case 2 : return Dual<int>(2, 2);
    case 3 : return Dual<int>(1, 2);
    case 4 : return Dual<int>(0, 2);
    case 5 : return Dual<int>(0, 1);
    default : libmesh_error();
    }
}
inline
VectorVoigt3D compress_stress (const Dyadic3D& T) {
    VectorVoigt3D Tv;
    Tv.comp = T(0,0), T(1,1), T(2,2), T(1,2), T(0,2), T(0,1);
    return Tv;
}
inline
VectorVoigt3D compress_strain (const Dyadic3D& T) {
    VectorVoigt3D Tv;
    Tv.comp = T(0,0), T(1,1), T(2,2), 2.0*T(1,2), 2.0*T(0,2), 2.0*T(0,1);
    return Tv;
}
inline
Dyadic3D uncompress_stress (const VectorVoigt3D& T) {
    Dyadic3D Tv;
    Tv.comp = T(0), T(5), T(4),
              T(5), T(1), T(3),
              T(4), T(3), T(2);
    return Tv;
}
inline
Dyadic3D uncompress_strain (const VectorVoigt3D& T) {
    Dyadic3D Tv;
    Tv.comp = T(0),     T(5)*0.5, T(4)*0.5,
              T(5)*0.5, T(1),     T(3)*0.5,
              T(4)*0.5, T(3)*0.5, T(2);
    return Tv;
}
inline
DyadicVoigt3D compress_constitutive_tensor (const Quindic3D& T) {
    DyadicVoigt3D Tv;
    Tv.comp = T(0,0,0,0), T(0,0,1,1), T(0,0,2,2), T(0,0,1,2), T(0,0,0,2), T(0,0,0,1),
              T(1,1,0,0), T(1,1,1,1), T(1,1,2,2), T(1,1,1,2), T(1,1,0,2), T(1,1,0,1),
              T(2,2,0,0), T(2,2,1,1), T(2,2,2,2), T(2,2,1,2), T(2,2,0,2), T(2,2,0,1),
              T(1,2,0,0), T(1,2,1,1), T(1,2,2,2), T(1,2,1,2), T(1,2,0,2), T(1,2,0,1),
              T(0,2,0,0), T(0,2,1,1), T(0,2,2,2), T(0,2,1,2), T(0,2,0,2), T(0,2,0,1),
              T(0,1,0,0), T(0,1,1,1), T(0,1,2,2), T(0,1,1,2), T(0,1,0,2), T(0,1,0,1);
    return Tv;
}
//-------------------------------------------------------------------------------------------------
inline
Dyadic3D left_CauchyGreen_deformation (const Dyadic3D& F) {
    Dyadic3D B;
    B.comp = F(0,0)*F(0,0)+F(0,1)*F(0,1)+F(0,2)*F(0,2), F(0,0)*F(1,0)+F(0,1)*F(1,1)+F(0,2)*F(1,2), F(0,0)*F(2,0)+F(0,1)*F(2,1)+F(0,2)*F(2,2),
             F(1,0)*F(0,0)+F(1,1)*F(0,1)+F(1,2)*F(0,2), F(1,0)*F(1,0)+F(1,1)*F(1,1)+F(1,2)*F(1,2), F(1,0)*F(2,0)+F(1,1)*F(2,1)+F(1,2)*F(2,2),
             F(2,0)*F(0,0)+F(2,1)*F(0,1)+F(2,2)*F(0,2), F(2,0)*F(1,0)+F(2,1)*F(1,1)+F(2,2)*F(1,2), F(2,0)*F(2,0)+F(2,1)*F(2,1)+F(2,2)*F(2,2);
    return B;
}
inline
Dyadic3D right_CauchyGreen_deformation (const Dyadic3D& F) {
    Dyadic3D C;
    C.comp = F(0,0)*F(0,0)+F(1,0)*F(1,0)+F(2,0)*F(2,0), F(0,0)*F(0,1)+F(1,0)*F(1,1)+F(2,0)*F(2,1), F(0,0)*F(0,2)+F(1,0)*F(1,2)+F(2,0)*F(2,2),
             F(0,1)*F(0,0)+F(1,1)*F(1,0)+F(2,1)*F(2,0), F(0,1)*F(0,1)+F(1,1)*F(1,1)+F(2,1)*F(2,1), F(0,1)*F(0,2)+F(1,1)*F(1,2)+F(2,1)*F(2,2),
             F(0,2)*F(0,0)+F(1,2)*F(1,0)+F(2,2)*F(2,0), F(0,2)*F(0,1)+F(1,2)*F(1,1)+F(2,2)*F(2,1), F(0,2)*F(0,2)+F(1,2)*F(1,2)+F(2,2)*F(2,2);
    return C;
}
inline
Dyadic3D GreenLagrange_strain (const Dyadic3D& F) {
    Dyadic3D I;
    I.comp = 1.0, 0.0, 0.0,
             0.0, 1.0, 0.0,
             0.0, 0.0, 1.0;
    Dyadic3D E;
    E.comp = 0.5*(right_CauchyGreen_deformation(F).comp - I.comp);
    return E;
}
inline
Dyadic3D EulerAlmansi_strain (const Dyadic3D& Fi) {
    Dyadic3D I;
    I.comp = 1.0, 0.0, 0.0,
             0.0, 1.0, 0.0,
             0.0, 0.0, 1.0;
    Dyadic3D e;
    e.comp = 0.5*(I.comp - right_CauchyGreen_deformation(Fi).comp);
    return e;
}
inline
Dyadic3D infinitesimal_strain_3d (const DisplacementGradient& grad_u) {
    Dyadic3D E;
    E.comp = grad_u(0,0),                   (grad_u(0,1)+grad_u(1,0))*0.5, (grad_u(0,2)+grad_u(2,0))*0.5,
             (grad_u(0,1)+grad_u(1,0))*0.5, grad_u(1,1),                   (grad_u(1,2)+grad_u(2,1))*0.5,
             (grad_u(0,2)+grad_u(2,0))*0.5, (grad_u(1,2)+grad_u(2,1))*0.5, grad_u(2,2);
    return E;
}
inline
void displacement_gradient (const Dyadic3D& F, DisplacementGradient& grad_u) {
    const Dyadic3D& I = identity_matrix;
    for (int i=0; i<SP3D; i++)
        for (int j=0; j<SP3D; j++)
            grad_u(i,j) = F(i,j) - I(i,j);
}
//-------------------------------------------------------------------------------------------------
inline
Dyadic3D deformation_gradient_3d (const DisplacementGradient& grad_u) {
    Dyadic3D F;
    F.comp = 1.0+grad_u(0,0),     grad_u(0,1),     grad_u(0,2),
                 grad_u(1,0), 1.0+grad_u(1,1),     grad_u(1,2),
                 grad_u(2,0),     grad_u(2,1), 1.0+grad_u(2,2);
    return F;
}
inline
Dyadic3D deformation_gradient_3d ( const Vector3DGradient& grad_X,
                                   const Vector3DGradient& grad_x ) {
    Blitz_DyadicReal F0(3, 2);
    F0 = grad_X(0,0), grad_X(0,1),
         grad_X(1,0), grad_X(1,1),
         grad_X(2,0), grad_X(2,1);
    Blitz_DyadicReal Fn(3, 2);
    Fn = grad_X(0,0), grad_X(0,1),
         grad_X(1,0), grad_X(1,1),
         grad_X(2,0), grad_X(2,1);
    //
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    //
    Dyadic3D Fn_F0T;
    Fn_F0T.comp = sum(Fn(i,k)*F0(j,k), k);
    Dyadic3D F0_F0T;
    F0_F0T.comp = sum(F0(i,k)*F0(j,k), k);
    const Dyadic3D inv_F0_F0T = inverse(F0_F0T);
    //
    Dyadic3D F;
    F.comp = sum(Fn_F0T.comp(i,k)*inv_F0_F0T.comp(k,j), k);
    return F;
}
inline
void polar_decomposition (const Dyadic3D& F, Dyadic3D& R, Dyadic3D& U) {
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    //
    std::vector<Real> l;
    std::vector<Vector3D> v;
    eigen_solve(right_CauchyGreen_deformation(F), l, v);
    U.comp = sqrt(l[0]) * tensor(v[0]).comp
           + sqrt(l[1]) * tensor(v[1]).comp
           + sqrt(l[2]) * tensor(v[2]).comp;
    R.comp = sum(F.comp(i,k)*inverse(U).comp(k,j), k);
}
//-------------------------------------------------------------------------------------------------
inline
void covariant_basis (const Vector3DGradient& grad_X, Vector3D* G_) {
    G_[0].comp = grad_X(0,0), grad_X(1,0), grad_X(2,0);
    G_[1].comp = grad_X(0,1), grad_X(1,1), grad_X(2,1);
}
inline
Dyadic2D metric_tensor (const Vector3D* G_) {
    Dyadic2D G;
    G.comp = product(G_[0], G_[0]), product(G_[0], G_[1]),
             product(G_[1], G_[0]), product(G_[1], G_[1]);
    return G;
}
//=================================================================================================
#endif //  __UTILS_CALCS2_INLINE_H__
//=================================================================================================
