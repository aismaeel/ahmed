/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __QUADRATURE_H__
#define __QUADRATURE_H__
//=================================================================================================
#include "feb3/utils.h"
#include "libmesh/quadrature_gauss.h"
//=================================================================================================
class NonUniform_QGauss {
public:
    NonUniform_QGauss ( const unsigned int dim, Order ord_0,
                        Order ord_1 =libMeshEnums::CONSTANT,
                        Order ord_2 =libMeshEnums::CONSTANT ) :
        _dimension(dim),
        _qgauss_0(SP1D, ord_0), _qgauss_1(SP1D, ord_1), _qgauss_2(SP1D, ord_2) {
        this->init();
    }
    NonUniform_QGauss (const unsigned int dim, const Order ord[SP3D]) :
        _dimension(dim),
        _qgauss_0(SP1D, ord[0]), _qgauss_1(SP1D, ord[1]), _qgauss_2(SP1D, ord[2]) {
        this->init();
    }
    ~NonUniform_QGauss () {}
    //
    unsigned int n_points () const { return _points.size(); }
    unsigned int get_dim () const { return _dimension; }
    const std::vector<Point>& get_points ()  const { return this->_points; }
    const std::vector<Real>&  get_weights () const { return this->_weights; }
protected:
    //
    void init ();
private:
    unsigned int _dimension;
    QGauss _qgauss_0, _qgauss_1, _qgauss_2;
    std::vector<Point> _points;
    std::vector<Real>  _weights;
};
//-------------------------------------------------------------------------------------------------
class QuadratureInfo {
public:
    enum Config {
        REFERENCE = 0,
        CURRENT = 1,
        ALL = 2
    };
    enum Level {
        LEVEL_0 = 0,
        LEVEL_1 = 1,
        LEVEL_2 = 2,
        LEVEL_3 = 3
    };
public:
    QuadratureInfo () {}
    QuadratureInfo (int n_qp, int n_bc, int n_bm, int n_aux);
    ~QuadratureInfo () {}
    //
    void init (int n_qp, int n_bc, int n_bm, int n_aux);
    void save (std::ofstream& file, Config cfg, Level lvl) const;
    void load (std::ifstream& file, Config cfg, Level lvl);
public:
    // quadrature points space vector (reference and current)
    std::vector<Point> XYZ, xyz;
    // biochemical related quadrature point information
    std::vector< std::vector<Real> > biochem;
    std::vector< std::vector<RealGradient> > grad_biochem;
    std::vector< std::vector<Real> > biochem_rate;
    // biomechanical related quadrature point information
    std::vector< std::vector<Real> > biomech;
    std::vector<Dyadic3D> deform_grad, deform_grad_elas, deform_grad_inel;
    std::vector<Dyadic3D> strain,      strain_elas,      strain_inel;
    std::vector<Dyadic3D> stress;
    std::vector<Real> pressure;
    std::vector<Vector3D> fluid_velocity;
    // three-part fibre orientation vector (reference and current)
    std::vector<Vector3D> fibre_ref[SP3D], fibre_curr[SP3D];
    // auxiliary (non-specific) quadrature data
    std::vector< std::vector<Real> > auxiliary;
    std::vector< std::vector<RealGradient> > grad_auxiliary;
    std::vector< std::vector<Real> > auxiliary_rate;
};
//=================================================================================================
#include "feb3/quadrature_inl.h"
//=================================================================================================
#endif // __QUADRATURE_H__
//=================================================================================================
