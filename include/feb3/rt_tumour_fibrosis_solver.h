/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __RT_TUMOUR_FIBROSIS_SOLVER_H__
#define __RT_TUMOUR_FIBROSIS_SOLVER_H__
//=================================================================================================
#include "feb3/domain_data.h"
#include "feb3/reaction_diffusion_solver_Xd.h"
#include "libmesh/getpot.h"
//=================================================================================================
class RtTumourFibrosisSolver : public ReactionDiffusionSolverXD {
public:
    RtTumourFibrosisSolver (DomainData& dmn);
    ~RtTumourFibrosisSolver () {}
    //
    const std::string& get_label () const;
    const std::string get_log () const;
    //
    void init ();
    void run ();
private:
    std::string _label;
    mutable PerfLog _log;
};
//=================================================================================================
#include "feb3/rt_tumour_fibrosis_solver_inl.h"
//=================================================================================================
#endif // __RT_TUMOUR_FIBROSIS_SOLVER_H__
//=================================================================================================
