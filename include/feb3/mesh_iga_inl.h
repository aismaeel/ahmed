/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __MESH_IGA_INLINE_H__
#define __MESH_IGA_INLINE_H__
//=================================================================================================
inline
const Mesh& MeshIGA::get_parametric_mesh () const {
    return this->_pphys_msh;
}
inline
const std::vector<NURBS>&  MeshIGA::get_nurbs () const {
    return this->_nurbs;
}
inline
const std::map<Node*, std::vector<NURBS*> >& MeshIGA::get_nodes_to_nurbs () const {
    return this->_nodes_to_nurbs;
}
//
inline
std::vector<NURBS>::const_iterator MeshIGA::nurbs_begin () const {
    return this->_nurbs.begin();
}
inline
std::vector<NURBS>::const_iterator MeshIGA::nurbs_end   () const {
    return this->_nurbs.end();
}
inline
std::map<Node*, std::vector<NURBS*> >::const_iterator MeshIGA::nodes_to_nurbs_begin () const {
    return this->_nodes_to_nurbs.begin();
}
inline
std::map<Node*, std::vector<NURBS*> >::const_iterator MeshIGA::nodes_to_nurbs_end   () const {
    return this->_nodes_to_nurbs.end();
}
//=================================================================================================
#endif //  __MESH_IGA_INLINE_H__
//=================================================================================================
