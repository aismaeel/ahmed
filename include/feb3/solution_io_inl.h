/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __SOLUTION_IO_INLINE_H__
#define __SOLUTION_IO_INLINE_H__
//=================================================================================================
inline
void save_solution (std::ofstream& fout, System& sys) {
    libmesh_assert(fout.is_open() && fout.good());
    //
    libmesh_assert(sys.solution->initialized());
    sys.solution->close();
    //
    const numeric_index_type global_size = sys.solution->size();
    fout.write((char*)&global_size, sizeof(numeric_index_type));
    libmesh_assert(!fout.bad());
    //
    const numeric_index_type local_size = sys.solution->local_size();
    fout.write((char*)&local_size, sizeof(numeric_index_type));
    libmesh_assert(!fout.bad());
    //
    const numeric_index_type first_local_index = sys.solution->first_local_index();
    fout.write((char*)&first_local_index, sizeof(numeric_index_type));
    libmesh_assert(!fout.bad());
    //
    const numeric_index_type last_local_index = sys.solution->last_local_index();
    fout.write((char*)&last_local_index, sizeof(numeric_index_type));
    libmesh_assert(!fout.bad());
    //
    for (numeric_index_type i=first_local_index; i<last_local_index; i++) {
        const Number component = sys.solution->el(i);
        fout.write((char*)&component, sizeof(Number));
    }
    libmesh_assert(!fout.bad());
    //
    gcomm_ptr->barrier();
}
inline
void save_solution (const char* fname, System& sys) {
    std::ofstream fout;
    save_solution(fout, sys);
}
//-------------------------------------------------------------------------------------------------
inline
void load_solution (std::ifstream& fin, System& sys) {
    libmesh_assert(fin.is_open() && fin.good());
    //
    libmesh_assert(sys.solution->initialized());
    sys.solution->zero();
    //
    numeric_index_type global_size;
    fin.read((char*)&global_size, sizeof(numeric_index_type));
    libmesh_assert(sys.solution->size()==global_size);
    libmesh_assert(!fin.bad());
    //
    numeric_index_type local_size;
    fin.read((char*)&local_size, sizeof(numeric_index_type));
    libmesh_assert(sys.solution->local_size()==local_size);
    libmesh_assert(!fin.bad());
    //
    numeric_index_type first_local_index;
    fin.read((char*)&first_local_index, sizeof(numeric_index_type));
    libmesh_assert(sys.solution->first_local_index()==first_local_index);
    libmesh_assert(!fin.bad());
    //
    numeric_index_type last_local_index;
    fin.read((char*)&last_local_index, sizeof(numeric_index_type));
    libmesh_assert(sys.solution->last_local_index()==last_local_index);
    libmesh_assert(!fin.bad());
    //
    for (numeric_index_type i=first_local_index; i<last_local_index; i++) {
        Number component;
        fin.read((char*)&component, sizeof(Number));
        sys.solution->set(i, component);
    }
    libmesh_assert(!fin.bad());
    //
    sys.solution->close();
    //
    gcomm_ptr->barrier();
}
inline
void load_solution (const char* fname, System& sys) {
    std::ifstream fin;
    load_solution(fin, sys);
}
//-------------------------------------------------------------------------------------------------
template <unsigned int I>
inline
void get_system_solution_nodal_value (const System& sys, const Node* nod, Real& soln) {
    const unsigned int J = 0;
    const dof_id_type idof = nod->dof_number(sys.number(), I, J);
    libmesh_assert( nod->n_comp(sys.number(), I) == 1 );
    soln = sys.solution->el(idof);
}
template <unsigned int I>
inline
void set_system_solution_nodal_value (System& sys, const Node* nod, const Real& soln) {
    const unsigned int J = 0;
    const dof_id_type idof = nod->dof_number(sys.number(), I, J);
    libmesh_assert( nod->n_comp(sys.number(), I) == 1 );
    sys.solution->set(idof, soln);
}
//-------------------------------------------------------------------------------------------------
template <unsigned int I>
inline
Real get_system_solution_nodal_value (const System& sys, const Node* nod, const std::vector<Number>& soln) {
    const unsigned int J = 0;
    const dof_id_type idof = nod->dof_number(sys.number(), I, J);
    libmesh_assert( nod->n_comp(sys.number(), I) == 1 );
    //
    return soln[idof];
}
//=================================================================================================
#endif // __SOLUTION_IO_INLINE_H__
//=================================================================================================
