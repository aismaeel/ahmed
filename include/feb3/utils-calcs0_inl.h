/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __UTILS_CALCS0_INLINE_H__
#define __UTILS_CALCS0_INLINE_H__
//=================================================================================================
inline Real curt (Real v) { return pow(v, frac_1o3); }
//
inline Real pow2 (Real v) { return v*v; }
inline Real pow3 (Real v) { return v*pow2(v); }
inline Real pow4 (Real v) { return pow2(pow2(v)); }
inline Real pow5 (Real v) { return v*pow4(v); }
inline Real pow6 (Real v) { return pow2(pow3(v)); }
inline Real pow7 (Real v) { return v*pow6(v); }
inline Real pow8 (Real v) { return pow2(pow4(v)); }
//
inline Real pow3rt2 (Real v) { return sqrt(pow3(v)); }
inline Real pow4rt2 (Real v) { return sqrt(pow4(v)); }
inline Real pow5rt2 (Real v) { return sqrt(pow5(v)); }
//
inline Real pow2rt3 (Real v) { return curt(pow2(v)); }
inline Real pow4rt3 (Real v) { return curt(pow4(v)); }
inline Real pow5rt3 (Real v) { return curt(pow5(v)); }
//-------------------------------------------------------------------------------------------------
inline
int sign (Real r) { return ( r>=0.0 ? +1 : -1 ); }
inline
Real degrees_to_radians (Real d) { return ( mPI*d/180.0 ); }
inline
Real radians_to_degrees (Real r) { return ( 180.0*r/mPI ); }
//
inline
Real apply_lbound (const Real& L, const Real& X) { return ( X < L ? L : X ); }
inline
Real apply_ubound (const Real& X, const Real& U) { return ( X > U ? U : X ); }
inline
Real apply_bounds (const Real& L, const Real& X, const Real& U) { return ( X < L ? L : ( X > U ? U : X ) ); }
//-------------------------------------------------------------------------------------------------
inline
int factorial (unsigned int A, int res =1) {
  if ( A == 0 || A == 1 ) return res;
  else return factorial(A-1, A*res);
}
//
inline
int uniform_distribution (const int from, const int to) {
    return ((int)rand() * (to - from)) / (int)RAND_MAX + from;
    //
    std::random_device rd;
    std::default_random_engine rgen(rd());
    std::uniform_int_distribution<int> dist(from, to);
    return dist(rgen);
}
inline
double uniform_distribution (const double from, const double to) {
    return ((double)rand() * (to - from)) / (double)RAND_MAX + from;
    //
    std::random_device rd;
    std::default_random_engine rgen(rd());
    std::uniform_real_distribution<double> dist(from, to);
    return dist(rgen);
}
//-------------------------------------------------------------------------------------------------
inline
int check_propability (const Real prop) {
    const Real res = uniform_distribution(0.0, 1.0);
    libmesh_assert(prop>=0.0);
    libmesh_assert(prop<=1.0);
    return ( res <= prop ? 1 : 0 );
}
//-------------------------------------------------------------------------------------------------
inline
void find_n_replace (std::string& source, const char* find, const char* replace) {
   size_t findLen = strlen(find);
   size_t replaceLen = strlen(replace);
   size_t pos = 0;
   while ((pos = source.find(find, pos)) != std::string::npos) {
      source.replace( pos, findLen, replace );
      pos += replaceLen;
   }
}
template <typename T>
inline
std::string number2string (const T& Number) {
    std::ostringstream ss;
    ss << Number;
    return ss.str();
}
template <typename T>
inline
T string2number (const std::string& Text) {
    std::istringstream ss(Text);
    T result;
    return ( ss >> result ? result : 0 );
}
//-------------------------------------------------------------------------------------------------
inline
std::string stringfy_number (Real Number) {
    char cs[buffer_len];
    sprintf(cs, "%.10f", Number);
    std::string s = cs;
    find_n_replace(s, ".", "P");
    return s;
}
//-------------------------------------------------------------------------------------------------
inline
void status_bar (std::ostream& os, const int x, const int n, const int barWidth =50) {
    if ( libMesh::global_processor_id() ) return;
    //
    const double progress = x / static_cast<double>(n);
    const int pos = barWidth * progress;
    os << "[";
    for (int i=0; i<barWidth; i++) {
        if (i < pos) os << "=";
        else if (i == pos) os << ">";
        else os << " ";
    }
    os << "]";
    char txt[50];
    sprintf(txt, " %.2f%%\r", progress*100.0);
    os << txt << std::flush;
    if ( n == x ) {
        for (int i=0; i<barWidth+50; i++)
            os << ' ';
        os << '\r' << std::flush;
    }
}
//-------------------------------------------------------------------------------------------------
inline
void solve_polynom (const std::vector<Real>& term, std::vector<Real>& sol, Real epsilon =1.0e-9) {
    // The form of the polynomial terms must be:
    // a[n-1] * pow(x, n) + ... + a[2] * pow(x, 2) + a[1] * x + a[0] = 0.0
    unsigned int order = term.size() - 1;
    sol.clear();
    libmesh_assert( ! term.empty() );
    libmesh_assert( term.size() >= 2 );
    for (unsigned int i=term.size()-1; i>0; i--) {
        if ( fabs(term[i]) > epsilon ) {
            order = i;
            break; // conditional jumping...
        }
    }
    const unsigned int nTerm = order + 1;
    gsl_poly_complex_workspace* w = gsl_poly_complex_workspace_alloc(nTerm);
    libmesh_assert( w != 0 );
    Real* z = 0;
    Real* a = 0;
    z = new Real [2*nTerm];
    a = new Real [nTerm];
    for (unsigned int i=0; i<nTerm; i++) {
        a[i] = term[i];
    }
    if ( GSL_SUCCESS != gsl_poly_complex_solve(a, nTerm, w, z) ) libmesh_error();
    // accept only roots that are real (i.e. imaginary part is approximately equal to zero):
    for (unsigned int i=0; i<nTerm-1; i++) {
        if ( fabs(z[2*i+1]) <= epsilon ) sol.push_back((Real)z[2*i]); // insert real root
    }
    std::sort(sol.begin(), sol.end());
    delete [] a;
    delete [] z;
    gsl_poly_complex_workspace_free(w);
}
//-------------------------------------------------------------------------------------------------
inline
void inverse (const Blitz_DyadicReal& src, Blitz_DyadicReal& res, const Real epsilon =1.0e-12) {
    const int N =src.extent(blitz::firstDim);
    if ( src.extent(blitz::secondDim) != N ) libmesh_error();
    if ( &src == &res ) libmesh_error();
    Real* Mout =(Real*) malloc(sizeof(Real)*N*N);
    const int actualsize =N;
    int i, j, k;
    Real sum, x;
    res.resize(N, N);
    // copy from source
    k = 0;
    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            Mout[k] = src(i,j);
            ++k;
        }
    }
    // Add small value to diagonal if diagonal is zero
    for (i=0; i<actualsize; i++) {
        j = i*actualsize+i;
        if (Mout[j]<epsilon && Mout[j]>-epsilon) Mout[j] = epsilon;
    }
    // normalize row with respect to [0]
    for (i=1; i<actualsize; i++) {
        Mout[i] /= Mout[0];
    }
    for (i=1; i<actualsize; i++) {
        // do a column of L
        for (j=i; j<actualsize; j++) {
            sum = 0.0;
            for (k=0; k<i; k++) {
                sum += Mout[j*actualsize+k] * Mout[k*actualsize+i];
            }
            Mout[j*actualsize+i] -= sum;
        }
        if (i == actualsize-1) continue;
        // do a row of U
        for (j=i+1; j<actualsize; j++) {
            sum = 0.0;
            for (k=0; k < i; k++) {
                sum += Mout[i*actualsize+k] * Mout[k*actualsize+j];
            }
            Mout[i*actualsize+j] = (Mout[i*actualsize+j] - sum) / Mout[i*actualsize+i];
        }
    }
    // invert L
    for (i=0; i<actualsize; i++) {
        for (j=i; j<actualsize; j++) {
            x = 1.0;
            if ( i != j ) {
                x = 0.0;
                for ( k = i; k < j; k++ ) {
                    x -= Mout[j*actualsize+k] * Mout[k*actualsize+i];
                }
            }
            Mout[j*actualsize+i] = x / Mout[j*actualsize+j];
        }
    }
    // invert U
    for (i=0; i<actualsize; i++) {
        for (j=i; j<actualsize; j++) {
            if ( i == j ) continue;
            sum = 0.0;
            for (k=i; k<j; k++ ) {
                sum += Mout[k*actualsize+j] * (i==k ? 1.0 : Mout[i*actualsize+k]);
            }
            Mout[i*actualsize+j] = -sum;
        }
    }
    // final inversion
    for (i=0; i<actualsize; i++) {
        for (j=0; j<actualsize; j++) {
            sum = 0.0;
            for (k=((i>j)?i:j); k<actualsize; k++) {
                sum += (j==k ? 1.0 : Mout[j*actualsize+k]) * Mout[k*actualsize+i];
            }
            Mout[j*actualsize+i] = sum;
        }
    }
    // copy result
    k = 0;
    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            res(i,j) = Mout[k];
            ++k;
        }
    }
    // free memory
    free(Mout);
}
//-------------------------------------------------------------------------------------------------
template<int N>
inline
void inverse (const Dyadic<Real, N>& A, Dyadic<Real, N>& A_inv, const Real epsilon =1.0e-12) {
    libmesh_assert( &A != &A_inv );
    const int actualsize = N;
    int i, j, k;
    Real sum, x;
    k = 0;
    Real* Mout =(Real*) malloc(sizeof(Real)*N*N); // copy from source
    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            Mout[k] = A.comp(i, j);
            ++k;
        }
    }
    for (i=0; i<actualsize; i++) { // add small value to diagonal if diagonal is zero
        j = i * actualsize + i;
        if ( Mout[j] < epsilon && Mout[j] > -epsilon ) Mout[j] = epsilon;
    }
    for (i=1; i<actualsize; i++) { // normalize row with respect to [0]
        Mout[i] /= Mout[0];
    }
    for (i=1; i<actualsize; i++) {
        for (j=i; j<actualsize; j++) { // do a column of L
            sum = 0.0;
            for (k=0; k<i; k++) {
                sum += Mout[j*actualsize+k] * Mout[k*actualsize+i];
            }
            Mout[j*actualsize+i] -= sum;
        }
        if (i == actualsize-1) continue;
        for (j=i+1; j<actualsize; j++) { // do a row of U
            sum = 0.0;
            for (k=0; k<i; k++) {
                sum += Mout[i*actualsize+k] * Mout[k*actualsize+j];
            }
            Mout[i*actualsize+j] = (Mout[i*actualsize+j] - sum) / Mout[i*actualsize+i];
        }
    }
    for (i=0; i<actualsize; i++) { // invert L
        for (j=i; j<actualsize; j++) {
            x = 1.0;
            if ( i != j ) {
                x = 0.0;
                for (k=i; k<j; k++ ) {
                    x -= Mout[j*actualsize+k] * Mout[k*actualsize+i];
                }
            }
            Mout[j*actualsize+i] = x / Mout[j*actualsize+j];
        }
    }
    for (i=0; i<actualsize; i++) { // invert U
        for (j=i; j<actualsize; j++) {
            if ( i == j ) continue;
            sum = 0.0;
            for (k=i; k<j; k++ ) {
                sum += Mout[k*actualsize+j] * (i==k ? 1.0 : Mout[i*actualsize+k]);
            }
            Mout[i*actualsize+j] = -sum;
        }
    }
    for (i=0; i<actualsize; i++) { // final inversion
        for (j=0; j<actualsize; j++) {
            sum = 0.0;
            for (k=((i>j)?i:j); k<actualsize; k++) {
                sum += (j==k ? 1.0 : Mout[j*actualsize+k]) * Mout[k*actualsize+i];
            }
            Mout[j*actualsize+i] = sum;
        }
    }
    k = 0; // copy result
    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            A_inv.comp(i, j) = Mout[k];
            ++k;
        }
    }
    free(Mout); // free memory
}
//-------------------------------------------------------------------------------------------------
template<int N>
inline
void eigen_solve (const Dyadic<Real, N>& A, std::vector<Real>& l, std::vector< Vector<Real, N> >& v) {
    gsl_matrix*  mat = gsl_matrix_alloc(N, N);
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            gsl_matrix_set(mat, i, j, A(i, j));
        }
    }
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            if (j == i) continue;
            const Real a = gsl_matrix_get(mat, i, j),
                       b = gsl_matrix_get(mat, j, i);
            if (fabs(a-b)>1.0e-6) libmesh_error();
        }
    }
    gsl_vector*  eval = gsl_vector_alloc(N);
    gsl_matrix*  evec = gsl_matrix_alloc(N, N);
    gsl_eigen_symmv_workspace* wrk = gsl_eigen_symmv_alloc(N);
    gsl_eigen_symmv(mat, eval, evec, wrk);
    gsl_eigen_symmv_free(wrk);
    gsl_matrix_free(mat);
    gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_DESC);
    l.resize(N);
    v.resize(N);
    for (int i=0; i<N; i++) {
        l[i] = gsl_vector_get(eval, i);
        for (int j=0; j<N; j++)
            v[i].comp(j) = gsl_matrix_get(evec, j, i);
    }
    gsl_vector_free(eval);
    gsl_matrix_free(evec);
}
//-------------------------------------------------------------------------------------------------
inline
void calc_var_dofs ( const unsigned int nvars, std::vector<dof_id_type>* dof_indices_var,
                     std::vector<unsigned int>& var_dofs ) {
    var_dofs.assign(nvars, 0);
    for (int i=0; i<nvars; i++) {
        var_dofs[i] = dof_indices_var[i].size();
    }
}
inline
void calc_offset ( const unsigned int nvars, const std::vector<unsigned int> var_dofs,
                   std::vector<unsigned int>& offset ) {
    offset.assign(nvars+1, 0);
    offset[0] = 0;
    for (int i=1; i<=nvars; i++) {
        offset[i] = offset[i-1] + var_dofs[i-1];
    }
}
//-------------------------------------------------------------------------------------------------
inline
void elem2d_transformation (const Elem* elem, Dyadic3D& mat) {
    const Point i0(1.0, 0.0, 0.0),
                i1(0.0, 1.0, 0.0),
                i2(0.0, 0.0, 1.0);
    Point e0, e1, e2;
    if ( elem->type() == libMeshEnums::TRI3 ) {
        const Point x01 = elem->point(1) - elem->point(0);
        const Point x02 = elem->point(2) - elem->point(0);
        const Point nu = x01.cross(x02);
        //
        e0 = x01 / x01.norm();
        e2 = nu / nu.norm();
        e1 = e2.cross(e0);
    } else if ( elem->type() == libMeshEnums::QUAD4 ) {
        const Point x01 = elem->point(1) - elem->point(0);
        const Point x03 = elem->point(3) - elem->point(0);
        const Point nu = x01.cross(x03);
        //
        e0 = x01 / x01.norm();
        e2 = nu / nu.norm();
        e1 = e2.cross(e0);
    } else libmesh_error();
    //
    mat.comp = (e0*i0), (e0*i1), (e0*i2),
               (e1*i0), (e1*i1), (e1*i2),
               (e2*i0), (e2*i1), (e2*i2);
}
//-------------------------------------------------------------------------------------------------
inline
Point self_scale (const Point& p1, const Point& p2) {
    Point res;
    for (unsigned int i=0; i<LIBMESH_DIM; i++)
        res(i) = p1(i) * p2(i);
    return res;
}
//-------------------------------------------------------------------------------------------------
inline
int node_present (const std::map< int, libMesh::Point >& n_map, const libMesh::Point& p) {
    for ( std::map< int, libMesh::Point >::const_iterator
          ci=n_map.begin(); ci!=n_map.end(); ci++ ) {
        if ( p == ci->second ) return ci->first;
    }
    return -1;
}
//-------------------------------------------------------------------------------------------------
inline
Point random_point_on_sphere (Real radius =1.0) {
    srand(static_cast<unsigned int>(time(NULL)));
    //
    const Real a = uniform_distribution(0.0, m2PI),
               u = uniform_distribution(-1.0, +1.0);
    return Point(radius*sqrt(1.0-u*u)*cos(a), radius*sqrt(1.0-u*u)*sin(a), radius*u);
}
//-------------------------------------------------------------------------------------------------
inline
Point project_to_plane ( const Point& in, const Point& plane_point, const Point& plane_normal,
                         Real tol =1.0e-5 ) {
    Point out;
    const Real n_DOT_q = plane_normal * in;
    if ( fabs(n_DOT_q) <= tol ) {
        out = in;
    } else {
        const Real q_p_DOT_n = (in - plane_point) * plane_normal;
        out = in - q_p_DOT_n * plane_normal;
    }
    return out;
}
inline
Point rotate_on_plane ( const Point& in, const Real& angle, const Point& plane_normal,
                        Real tol =1.0e-5 ) {
    Point out;
    const Real sin8 = sin(angle),
               cos8 = cos(angle);
    out(0) = (cos8+plane_normal(0)*plane_normal(0)*(1-cos8))*in(0)
           + (plane_normal(0)*plane_normal(1)*(1-cos8)-plane_normal(2)*sin8)*in(1)
           + (plane_normal(0)*plane_normal(2)*(1-cos8)+plane_normal(1)*sin8)*in(2);
    out(1) = (plane_normal(1)*plane_normal(0)*(1-cos8)+plane_normal(2)*sin8)*in(0)
           + (cos8+plane_normal(1)*plane_normal(1)*(1-cos8))*in(1)
           + (plane_normal(1)*plane_normal(2)*(1-cos8)-plane_normal(0)*sin8)*in(2);
    out(2) = (plane_normal(2)*plane_normal(0)*(1-cos8)-plane_normal(1)*sin8)*in(0)
           + (plane_normal(2)*plane_normal(1)*(1-cos8)+plane_normal(0)*sin8)*in(1)
           + (cos8+plane_normal(2)*plane_normal(2)*(1-cos8))*in(2);
    return out;
}
//
inline
Point rotate_about_axis (const Point& axis, const Real& angle, const Point& pnt) {
    const Point axis_n = axis.unit();
    // Rodrigues' rotation formula: https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
    const Real sin_phi = sin(0.5*angle),
               cos_phi = cos(0.5*angle),
               a = cos_phi,
               b = axis_n(0)*sin_phi,
               c = axis_n(1)*sin_phi,
               d = axis_n(2)*sin_phi;
    Real matrix[SP3D][SP3D];
    matrix[0][0] = a*a+b*b-c*c-d*d;
    matrix[0][1] = 2.0*(b*c-a*d);
    matrix[0][2] = 2.0*(b*d+a*c);
    matrix[1][0] = 2.0*(b*c+a*d);
    matrix[1][1] = a*a-b*b+c*c-d*d;
    matrix[1][2] = 2.0*(c*d-a*b);
    matrix[2][0] = 2.0*(b*d-a*c);
    matrix[2][1] = 2.0*(c*d+a*b);
    matrix[2][2] = a*a-b*b-c*c+d*d;
    return Point( matrix[0][0]*pnt(0)+matrix[0][1]*pnt(1)+matrix[0][2]*pnt(2),
                  matrix[1][0]*pnt(0)+matrix[1][1]*pnt(1)+matrix[1][2]*pnt(2),
                  matrix[2][0]*pnt(0)+matrix[2][1]*pnt(1)+matrix[2][2]*pnt(2) );
}
//-------------------------------------------------------------------------------------------------
inline
void transform_shape_deriv ( const std::vector< std::vector<RealGradient> >& dPhi_dX, const unsigned int& qp,
                             const Dyadic3D& F,
                             std::vector<RealGradient>& dPhi_dx ) {
    const unsigned int n_dofs = dPhi_dX.size();
    libmesh_assert( n_dofs );
    libmesh_assert( qp < dPhi_dX[0].size() );
    dPhi_dx.resize(n_dofs);
    const Real det_F = F(0,0)*(F(1,1)*F(2,2)-F(1,2)*F(2,1))
                     - F(0,1)*(F(1,0)*F(2,2)-F(1,2)*F(2,0))
                     + F(0,2)*(F(1,0)*F(2,1)-F(1,1)*F(2,0));
    Real F_i[SP3D][SP3D];
    F_i[0][0] =  (F(1,1)*F(2,2)-F(2,1)*F(1,2))/det_F;
    F_i[0][1] = -(F(0,1)*F(2,2)-F(2,1)*F(0,2))/det_F;
    F_i[0][2] =  (F(0,1)*F(1,2)-F(1,1)*F(0,2))/det_F;
    F_i[1][0] = -(F(1,0)*F(2,2)-F(2,0)*F(1,2))/det_F;
    F_i[1][1] =  (F(0,0)*F(2,2)-F(2,0)*F(0,2))/det_F;
    F_i[1][2] = -(F(0,0)*F(1,2)-F(1,0)*F(0,2))/det_F;
    F_i[2][0] =  (F(1,0)*F(2,1)-F(2,0)*F(1,1))/det_F;
    F_i[2][1] = -(F(0,0)*F(2,1)-F(2,0)*F(0,1))/det_F;
    F_i[2][2] =  (F(0,0)*F(1,1)-F(1,0)*F(0,1))/det_F;
    for (unsigned int i=0; i<n_dofs; i++) {
        dPhi_dx[i](0) = dPhi_dX[i][qp](0)*F_i[0][0]+dPhi_dX[i][qp](1)*F_i[1][0]+dPhi_dX[i][qp](2)*F_i[2][0];
        dPhi_dx[i](1) = dPhi_dX[i][qp](0)*F_i[0][1]+dPhi_dX[i][qp](1)*F_i[1][1]+dPhi_dX[i][qp](2)*F_i[2][1];
        dPhi_dx[i](2) = dPhi_dX[i][qp](0)*F_i[0][2]+dPhi_dX[i][qp](1)*F_i[1][2]+dPhi_dX[i][qp](2)*F_i[2][2];
    }
}
//-------------------------------------------------------------------------------------------------
inline
Real iga_transf_3d (const Elem* pelem, Point& alpha, Point& beta, const int side =-1) {
    libmesh_assert( pelem->type() == ElemType::HEX8 );
    //
    alpha = Point(0.0, 0.0, 0.0);
    beta  = Point(0.0, 0.0, 0.0);
    Point Zp_O, Zp_E;
    Real jac;
    if ( -1 == side ) {
        Zp_O = pelem->point(0);
        Zp_E = pelem->point(6);
        alpha = 0.5*Zp_E - 0.5*Zp_O;
        jac = alpha(0) * alpha(1) * alpha(2);
    } else {
        if        ( side == 0 ) {
            Zp_O = Point( pelem->point(0)(1), pelem->point(0)(0), 0.0 );
            Zp_E = Point( pelem->point(2)(1), pelem->point(2)(0), 0.0 );
        } else if ( side == 1 ) {
            Zp_O = Point( pelem->point(0)(0), pelem->point(0)(2), 0.0 );
            Zp_E = Point( pelem->point(5)(0), pelem->point(5)(2), 0.0 );
        } else if ( side == 2 ) {
            Zp_O = Point( pelem->point(1)(1), pelem->point(1)(2), 0.0 );
            Zp_E = Point( pelem->point(6)(1), pelem->point(6)(2), 0.0 );
        } else if ( side == 3 ) {
            Zp_O = Point( pelem->point(2)(0), pelem->point(2)(2), 0.0 );
            Zp_E = Point( pelem->point(7)(0), pelem->point(7)(2), 0.0 );
        } else if ( side == 4 ) {
            Zp_O = Point( pelem->point(3)(1), pelem->point(3)(2), 0.0 );
            Zp_E = Point( pelem->point(4)(1), pelem->point(4)(2), 0.0 );
        } else if ( side == 5 ) {
            Zp_O = Point( pelem->point(4)(0), pelem->point(4)(1), 0.0 );
            Zp_E = Point( pelem->point(6)(0), pelem->point(6)(1), 0.0 );
        } else libmesh_error();
        alpha = 0.5*Zp_E - 0.5*Zp_O;
        jac = alpha(0) * alpha(1);
    }
    beta = 0.5*Zp_E + 0.5*Zp_O;
    // check the jacobian
    libmesh_assert( fabs(jac) > 1.0e-5 );
    return jac;
}
inline
Real iga_transf_2d (const Elem* pelem, Point& alpha, Point& beta, const int side =-1) {
    libmesh_assert( pelem->type() == ElemType::QUAD4 );
    //
    alpha = Point(0.0, 0.0, 0.0);
    beta  = Point(0.0, 0.0, 0.0);
    Point Zp_O, Zp_E;
    Real jac;
    if ( -1 == side ) {
        Zp_O = pelem->point(0);
        Zp_E = pelem->point(2);
        alpha = 0.5*Zp_E - 0.5*Zp_O;
        jac = alpha(0) * alpha(1);
    } else {
        if        ( side == 0 ) {
            Zp_O = Point( pelem->point(0)(0), 0.0, 0.0 );
            Zp_E = Point( pelem->point(1)(0), 0.0, 0.0 );
        } else if ( side == 1 ) {
            Zp_O = Point( pelem->point(1)(1), 0.0, 0.0 );
            Zp_E = Point( pelem->point(2)(1), 0.0, 0.0 );
        } else if ( side == 2 ) {
            Zp_O = Point( pelem->point(3)(0), 0.0, 0.0 );
            Zp_E = Point( pelem->point(2)(0), 0.0, 0.0 );
        } else if ( side == 3 ) {
            Zp_O = Point( pelem->point(0)(1), 0.0, 0.0 );
            Zp_E = Point( pelem->point(3)(1), 0.0, 0.0 );
        } else libmesh_error();
        alpha = 0.5*Zp_E - 0.5*Zp_O;
        jac = alpha(0);
    }
    beta = 0.5*Zp_E + 0.5*Zp_O;
    // check the jacobian
    libmesh_assert( fabs(jac) > 1.0e-5 );
    return jac;
}
//=================================================================================================
#endif //  __UTILS_CALCS0_INLINE_H__
//=================================================================================================
