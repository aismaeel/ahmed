/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __TUMOUR_ANGIOGENESIS_CONSTITUTIVE_H__
#define __TUMOUR_ANGIOGENESIS_CONSTITUTIVE_H__
//=================================================================================================
struct TumourAngiogenesisConstitutive {
    Real (*blood_vessel_hydraulic_conductivity) ( const VascularInfo* vi,
                                                  const MaterialInfo* mi__vnr,
                                                  const MaterialInfo* mi__dmn,
                                                  FieldInfo* fld );
    Real (*lymph_vessel_hydraulic_conductivity) ( const VascularInfo* vi,
                                                  const MaterialInfo* mi__vnr,
                                                  const MaterialInfo* mi__dmn,
                                                  FieldInfo* fld );
    Real (*blood_vessel_hydraulic_permeability) ( const VascularInfo* vi,
                                                  const MaterialInfo* mi__vnr,
                                                  const MaterialInfo* mi__dmn,
                                                  FieldInfo* fld );
    Real (*lymph_vessel_hydraulic_permeability) ( const VascularInfo* vi,
                                                  const MaterialInfo* mi__vnr,
                                                  const MaterialInfo* mi__dmn,
                                                  FieldInfo* fld );
    Real (*vascular_hydraulic_permeability_2_drug) ( const VascularInfo* vi,
                                                     const MaterialInfo* mi__vnr,
                                                     const MaterialInfo* mi__dmn,
                                                     FieldInfo* fld,
                                                     const unsigned int drug_id );
    Real (*vascular_reflection_coefficient_2_drug) ( const VascularInfo* vi,
                                                     const MaterialInfo* mi__vnr,
                                                     const MaterialInfo* mi__dmn,
                                                     FieldInfo* fld,
                                                     const unsigned int drug_id );
    Real (*osmotic_pressure_difference) ( const MaterialInfo* mi__vnr,
                                          const MaterialInfo* mi__dmn,
                                          FieldInfo* fld );
    Real (*blood_vessel_stroma_tissue_ratio) ( const MaterialInfo* mi__vnr,
                                               const MaterialInfo* mi__dmn,
                                               FieldInfo* fld );
    Real (*lymph_vessel_stroma_tissue_ratio) ( const MaterialInfo* mi__vnr,
                                               const MaterialInfo* mi__dmn,
                                               FieldInfo* fld );
    Real (*stroma_cross_section) ( const MaterialInfo* mi__vnr,
                                   const MaterialInfo* mi__dmn,
                                   FieldInfo* fld );
    Real (*stroma_hydraulic_conductivity) ( const MaterialInfo* mi__vnr,
                                            const MaterialInfo* mi__dmn,
                                            FieldInfo* fld );
    Real (*stroma_diffusion_coefficient_2_drug) ( const MaterialInfo* mi__vnr,
                                                  const MaterialInfo* mi__dmn,
                                                  FieldInfo* fld,
                                                  const unsigned int drug_id );
};
//=================================================================================================
// constitutive independent function
inline
Real vascular_length ( const Node* nod,     const Point nod_U,
                       const Node* c_nod[], const Point c_nod_U[] ) {
    Real length = 0.0;
    for (unsigned int l=0; l<3; l++) {
        if ( NULL == c_nod[l] ) continue;
        length += Point((*c_nod[l])+c_nod_U[l]-(*nod)-nod_U).norm();
    }
    return length;
}
inline
Real blood_viscosity (const MaterialInfo* mi__vnr) {
    return mi__vnr->get_biochem_param(0);
}
inline
Real lymph_viscosity (const MaterialInfo* mi__vnr) {
    return mi__vnr->get_biochem_param(1);
}
inline
Real plasma_viscosity (const MaterialInfo* mi__vnr) {
    return mi__vnr->get_biochem_param(2);
}
inline
Real interstitial_fluid_viscosity (const MaterialInfo* mi__vnr) {
    return mi__vnr->get_biochem_param(3);
}
//-------------------------------------------------------------------------------------------------
//
inline
Real vascular_circumference (const VascularInfo* vi) {
    const Real vessel_radius = vi->radius;
    return m2PI*vessel_radius;
}
inline
Real vascular_cross_section (const VascularInfo* vi) {
    const Real vessel_radius = vi->radius;
    return mPI*pow2(vessel_radius);
}
//-------------------------------------------------------------------------------------------------
#ifdef _LOCAL_ARGUMENTS_
    #error "an unexpected compilation error occurred!"
#else
    #define _LOCAL_ARGUMENTS_ \
      const VascularInfo* vi,      \
      const MaterialInfo* mi__vnr, \
      const MaterialInfo* mi__dmn, \
      FieldInfo* fld
#endif
//-------------------------------------------------------------------------------------------------
// constitutive functions that return the hydraulic conductivity of the vascular wall
inline
Real blood_vessel_hydraulic_conductivity_486 ( _LOCAL_ARGUMENTS_ ) {
    const Real vessel_radius = vi->radius,
               blood_dynamic_viscosity = blood_viscosity(mi__vnr);
    return pow2(vessel_radius)/(8.0*blood_dynamic_viscosity);
}
inline
Real lymph_vessel_hydraulic_conductivity_486 ( _LOCAL_ARGUMENTS_ ) {
    const Real vessel_radius = vi->radius,
               lymph_dynamic_viscosity = lymph_viscosity(mi__vnr);
    return pow2(vessel_radius)/(8.0*lymph_dynamic_viscosity);
}
//-------------------------------------------------------------------------------------------------
// constitutive functions that return the permeability of the vascular wall
inline
Real blood_vessel_hydraulic_permeability_486 ( _LOCAL_ARGUMENTS_ ) {
    if ( VascularInfo::COLLAPSED == vi->status ) return 0.0;
    //
    const Real vessel_thickness = vi->thickness,
               vessel_pore_size = vi->pore_radius,
               vascular_pores_fraction = vi->pores_fraction;
    return vascular_pores_fraction*pow2(vessel_pore_size)
          /(8.0*plasma_viscosity(mi__vnr)*vessel_thickness);
}
inline
Real lymph_vessel_hydraulic_permeability_486 ( _LOCAL_ARGUMENTS_ ) {
    if ( VascularInfo::COLLAPSED == vi->status ) return 0.0;
    //
    const Real vessel_thickness = vi->thickness,
               vessel_pore_size = vi->pore_radius,
               vascular_pores_fraction = vi->pores_fraction;
    return vascular_pores_fraction*pow2(vessel_pore_size)
          /(8.0*interstitial_fluid_viscosity(mi__vnr)*vessel_thickness);
}
//-------------------------------------------------------------------------------------------------
// constitutive functions that return the permeability of the vascular wall with respect to
// a drug molecule or a nanoparticle
inline
Real vascular_hydraulic_permeability_2_drug_486 ( _LOCAL_ARGUMENTS_, const unsigned int drug_id ) {
    if ( VascularInfo::COLLAPSED == vi->status ) return 0.0;
    // see: https://link.springer.com/article/10.1007/s10237-015-0682-0
    const Real thickness = vi->thickness,
               pores_fraction = vi->pores_fraction,
               pore_radius = vi->pore_radius;
    //
    Real molecule_radius = 0.0;
    if      ( drug_id == 0 ) molecule_radius = mi__vnr->get_biochem_param(204);
    else if ( drug_id == 1 ) molecule_radius = mi__vnr->get_biochem_param(205);
    else libmesh_error();
    //
    const Real lambda = molecule_radius / pore_radius;
    if ( lambda >= 1.0 ) return 0.0;
    const Real lambda_[] = { 1.0, lambda, pow2(lambda), pow3(lambda), pow4(lambda) };
    const Real delta_[] = { 1.0, (1.0-lambda), pow2(1.0-lambda) };
    //
    const Real F = delta_[2];
    const Real a_[] = { 0, -1.216667E+00,  1.533591E+00, -2.250830E+01, -5.617000E+00, -3.363000E-01, -1.216000E+00, 1.647000E+00 };
    const Real c_ = 3.140489E+01*pow(1.0-lambda, -2.5);
    //
    const Real K_t = c_*(1.0+a_[1]*delta_[1]+a_[2]*delta_[2])
                   + (a_[3]*lambda_[0]+a_[4]*lambda_[1]+a_[5]*lambda_[2]+a_[6]*lambda_[3]+a_[7]*lambda_[4]);
    // hydrodynamic interactions of the drug
    const Real H = m6PI*F/K_t;
    //
    const Real absolute_temperature = 310.0; // Kelvin
    Real fluid_viscosity;
    if      ( VascularInfo::BLOOD == vi->flow ) fluid_viscosity = blood_viscosity(mi__vnr);
    else if ( VascularInfo::LYMPH == vi->flow ) fluid_viscosity = lymph_viscosity(mi__vnr);
    else libmesh_error();
    // Stokes-Einstein equation: drug diffusion coefficient in free_solution at 310K
    const Real D_o = Boltzmann_constant*absolute_temperature / (m6PI*fluid_viscosity*molecule_radius);
    //
    return pores_fraction*H*D_o/thickness;
}
//-------------------------------------------------------------------------------------------------
// constitutive functions that return the reflection coefficient of the vascular wall with respect to
// a drug molecule or a nanoparticle
inline
Real vascular_reflection_coefficient_2_drug_486 ( _LOCAL_ARGUMENTS_, const unsigned int drug_id ) {
    if ( VascularInfo::COLLAPSED == vi->status ) return 1.0;
    // see: https://link.springer.com/article/10.1007/s10237-015-0682-0
    const Real pore_radius = vi->pore_radius;
    //
    Real molecule_radius = 0.0;
    if      ( drug_id == 0 ) molecule_radius = mi__vnr->get_biochem_param(204);
    else if ( drug_id == 1 ) molecule_radius = mi__vnr->get_biochem_param(205);
    else libmesh_error();
    //
    const Real lambda = molecule_radius / pore_radius;
    if ( lambda >= 1.0 ) return 1.0;
    const Real lambda_[] = { 1.0, lambda, pow2(lambda), pow3(lambda), pow4(lambda) };
    const Real delta_[] = { 1.0, (1.0-lambda), pow2(1.0-lambda) };
    //
    const Real F = delta_[2];
    const Real a_[] = { 0, -1.216667E+00,  1.533591E+00, -2.250830E+01, -5.617000E+00, -3.363000E-01, -1.216000E+00, 1.647000E+00 };
    const Real b_[] = { 0,  1.166667E-01, -4.418651E-02,  4.018000E+00, -3.978800E+00, -1.921500E+00,  4.392000E+00, 5.006000E+00 };
    const Real c_ = 3.140489E+01*pow(1.0-lambda, -2.5);
    //
    const Real K_t = c_*(1.0+a_[1]*delta_[1]+a_[2]*delta_[2])
                   + (a_[3]*lambda_[0]+a_[4]*lambda_[1]+a_[5]*lambda_[2]+a_[6]*lambda_[3]+a_[7]*lambda_[4]);
    const Real K_s = c_*(1.0+b_[1]*delta_[1]+b_[2]*delta_[2])
                   + (b_[3]*lambda_[0]+b_[4]*lambda_[1]+b_[5]*lambda_[2]+b_[6]*lambda_[3]+b_[7]*lambda_[4]);
    // electrostatic interactions of the drug
    const Real W = (2.0-F)*F*K_s/K_t;
    //
    if ( W > 1.0 ) return 0.0;
    return 1.0-W;
}
//-------------------------------------------------------------------------------------------------
#undef _LOCAL_ARGUMENTS_
//-------------------------------------------------------------------------------------------------
#ifdef _LOCAL_ARGUMENTS_
    #error "an unexpected compilation error occurred!"
#else
    #define _LOCAL_ARGUMENTS_ \
      const MaterialInfo* mi__vnr, \
      const MaterialInfo* mi__dmn, \
      FieldInfo* fld
#endif
//-------------------------------------------------------------------------------------------------
// constitutive functions that return the vascular density over (a finite volume of) the stroma
inline
Real blood_vessel_stroma_ratio_486 ( _LOCAL_ARGUMENTS_ ) {
    return mi__dmn->get_biochem_param(63);
}
//
inline
Real lymph_vessel_stroma_ratio_486 ( _LOCAL_ARGUMENTS_ ) {
    return mi__dmn->get_biochem_param(64);
}
//-------------------------------------------------------------------------------------------------
inline
Real stroma_cross_section_486 ( _LOCAL_ARGUMENTS_ ) {
    const Real vascular_mean_radius = 0.5*(mi__vnr->get_biochem_param(52)
                                          +mi__vnr->get_biochem_param(53)),
               vascular_density_ratio = blood_vessel_stroma_ratio_486(mi__vnr, mi__dmn, fld);
    return m2PI*vascular_mean_radius/vascular_density_ratio;
}
//-------------------------------------------------------------------------------------------------
// constitutive functions that return the pressure difference between the interstitial fluid
// and the plasma fluid
inline
Real osmotic_pressure_difference_486 ( _LOCAL_ARGUMENTS_ ) {
    const Real osmotic_reflection_coeff = mi__dmn->get_biochem_param(60),
               osmotic_press__plasma = mi__dmn->get_biochem_param(61),
               osmotic_press__intfld = mi__dmn->get_biochem_param(62);
    return osmotic_reflection_coeff*(osmotic_press__plasma-osmotic_press__intfld);
}
//-------------------------------------------------------------------------------------------------
// constitutive functions that return the hydraulic conductivity of the stroma
inline
Real stroma_hydraulic_conductivity_486 ( _LOCAL_ARGUMENTS_ ) {
    const Real kappa = mi__dmn->get_biochem_param(70),
               scale = mi__dmn->get_biochem_param(71);
    const Real ecm = apply_bounds(0.001, fld->biochem[3], 1.0);
    if      ( scale > 0.0 ) return ( scale*kappa)*ecm;
    else if ( scale < 0.0 ) return (-scale*kappa)/ecm;
    else                    return kappa;
}
//-------------------------------------------------------------------------------------------------
// constitutive functions that return the diffusion coefficient of a drug molecule or a nanoparticle
// with respect to the stroma
inline
Real stroma_diffusion_coefficient_2_drug_486 ( _LOCAL_ARGUMENTS_, const unsigned int drug_id ) {
    Real molecule_radius = 0.0;
    if      ( drug_id == 0 ) molecule_radius = mi__vnr->get_biochem_param(204);
    else if ( drug_id == 1 ) molecule_radius = mi__vnr->get_biochem_param(205);
    else libmesh_error();
    //
    const Real absolute_temperature = 310.0;
    const Real fluid_viscosity = interstitial_fluid_viscosity(mi__vnr);
    // Stokes-Einstein equation
    return Boltzmann_constant*absolute_temperature/(m6PI*fluid_viscosity*molecule_radius);
}
//-------------------------------------------------------------------------------------------------
#undef _LOCAL_ARGUMENTS_
//=================================================================================================
#endif // __TUMOUR_ANGIOGENESIS_CONSTITUTIVE_H__
//=================================================================================================
