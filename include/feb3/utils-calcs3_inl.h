/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __UTILS_CALCS3_INLINE_H__
#define __UTILS_CALCS3_INLINE_H__
//=================================================================================================
inline
Dyadic3D mul_im_mj (const Dyadic3D& A, const Dyadic3D& B) {
    Dyadic3D R;
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    R.comp = sum(A.comp(i,k)*B.comp(k,j), k);
    return R;
}
inline
Dyadic3D mul_mi_mj (const Dyadic3D& A, const Dyadic3D& B) {
    Dyadic3D R;
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    R.comp = sum(A.comp(k,i)*B.comp(k,j), k);
    return R;
}
inline
Dyadic3D mul_im_jm (const Dyadic3D& A, const Dyadic3D& B) {
    Dyadic3D R;
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    R.comp = sum(A.comp(i,k)*B.comp(j,k), k);
    return R;
}
inline
Dyadic3D mul_mi_jm (const Dyadic3D& A, const Dyadic3D& B) {
    Dyadic3D R;
    blitz::firstIndex  i;
    blitz::secondIndex j;
    blitz::thirdIndex  k;
    R.comp = sum(A.comp(k,i)*B.comp(j,k), k);
    return R;
}
inline
Vector3D mul_im_m (const Dyadic3D& A, const Vector3D& B) {
    Vector3D R;
    blitz::firstIndex  i;
    blitz::secondIndex j;
    R.comp = sum(A.comp(i,j)*B.comp(j), j);
    return R;
}
inline
Vector3D mul_m_mi (const Vector3D& B, const Dyadic3D& A) {
    Vector3D R;
    blitz::firstIndex  i;
    blitz::secondIndex j;
    R.comp = sum(B.comp(j)*A.comp(j,i), j);
    return R;
}
inline
Dyadic3D mul_ijmn_mn (const Quindic3D& Q, const Dyadic3D& D) {
    Dyadic3D R;
    for (int i=0; i<SP3D; i++) {
        for (int j=0; j<SP3D; j++) {
            Dyadic3D Qd;
            Qd.comp = Q(i,j,0,0), Q(i,j,0,1), Q(i,j,0,2),
                      Q(i,j,1,0), Q(i,j,1,1), Q(i,j,1,2),
                      Q(i,j,2,0), Q(i,j,2,1), Q(i,j,2,2);
            R.comp(i,j) = outer_product(Qd, D);
        }
    }
    return R;
}
inline
Dyadic3D mul_mn_mnij (const Dyadic3D& D, const Quindic3D& Q) {
    Dyadic3D R;
    for (int i=0; i<SP3D; i++) {
        for (int j=0; j<SP3D; j++) {
            Dyadic3D Qd;
            Qd.comp = Q(0,0,i,j), Q(0,1,i,j), Q(0,2,i,j),
                      Q(1,0,i,j), Q(1,1,i,j), Q(1,2,i,j),
                      Q(2,0,i,j), Q(2,1,i,j), Q(2,2,i,j);
            R.comp(i,j) = outer_product(D, Qd);
        }
    }
    return R;
}
inline
Quindic3D mul_mjkl_im (const Quindic3D& Q, const Dyadic3D& D) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++)
        for (int j=0; j<SP3D; j++)
            for (int k=0; k<SP3D; k++)
                for (int l=0; l<SP3D; l++)
                    R.comp(i,j,k,l) = Q(0,j,k,l)*D(i,0)+Q(1,j,k,l)*D(i,1)+Q(2,j,k,l)*D(i,2);
    return R;
}
inline
Quindic3D mul_imkl_jm (const Quindic3D& Q, const Dyadic3D& D) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++)
        for (int j=0; j<SP3D; j++)
            for (int k=0; k<SP3D; k++)
                for (int l=0; l<SP3D; l++)
                    R.comp(i,j,k,l) = Q(i,0,k,l)*D(j,0)+Q(i,1,k,l)*D(j,1)+Q(i,2,k,l)*D(j,2);
    return R;
}
inline
Quindic3D mul_ijml_km (const Quindic3D& Q, const Dyadic3D& D) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++)
        for (int j=0; j<SP3D; j++)
            for (int k=0; k<SP3D; k++)
                for (int l=0; l<SP3D; l++)
                    R.comp(i,j,k,l) = Q(i,j,0,l)*D(k,0)+Q(i,j,1,l)*D(k,1)+Q(i,j,2,l)*D(k,2);
    return R;
}
inline
Quindic3D mul_ijkm_lm (const Quindic3D& Q, const Dyadic3D& D) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++)
        for (int j=0; j<SP3D; j++)
            for (int k=0; k<SP3D; k++)
                for (int l=0; l<SP3D; l++)
                    R.comp(i,j,k,l) = Q(i,j,k,0)*D(l,0)+Q(i,j,k,1)*D(l,1)+Q(i,j,k,2)*D(l,2);
    return R;
}
inline
Dyadic3D mul_m_imjn_n (const Vector3D& A, const Quindic3D& Q, const Vector3D& B) {
    Dyadic3D R;
    for (int i=0; i<SP3D; i++) {
        for (int j=0; j<SP3D; j++) {
            R.comp(i,j) = A(0)*Q(i,0,j,0)*B(0)+A(0)*Q(i,0,j,1)*B(1)+A(0)*Q(i,0,j,2)*B(2)
                         +A(1)*Q(i,1,j,0)*B(0)+A(1)*Q(i,1,j,1)*B(1)+A(1)*Q(i,1,j,2)*B(2)
                         +A(2)*Q(i,2,j,0)*B(0)+A(2)*Q(i,2,j,1)*B(1)+A(2)*Q(i,2,j,2)*B(2);
        }
    }
    return R;
}
inline
Quindic3D mul_ijmn_mnkl (const Quindic3D& A, const Quindic3D& B) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++) {
        for (int j=0; j<SP3D; j++) {
            Dyadic3D Ad;
            Ad.comp = A(i,j,0,0), A(i,j,0,1), A(i,j,0,2),
                      A(i,j,1,0), A(i,j,1,1), A(i,j,1,2),
                      A(i,j,2,0), A(i,j,2,1), A(i,j,2,2);
            for (int k=0; k<SP3D; k++) {
                for (int l=0; l<SP3D; l++) {
                    Dyadic3D Bd;
                    Bd.comp = B(0,0,k,l), B(0,1,k,l), B(0,2,k,l),
                              B(1,0,k,l), B(1,1,k,l), B(1,2,k,l),
                              B(2,0,k,l), B(2,1,k,l), B(2,2,k,l);
                    R.comp(i,j,k,l) = outer_product(Ad, Bd);
                }
            }
        }
    }
    return R;
}
inline
Dyadic3D mul_ijm_m (const Triadic3D& T, const Vector3D& V) {
    Dyadic3D O;
    O.comp(0,0) = T(0,0,0)*V(0)+T(0,0,1)*V(1)+T(0,0,2)*V(2);
    O.comp(0,1) = T(0,1,0)*V(0)+T(0,1,1)*V(1)+T(0,1,2)*V(2);
    O.comp(0,2) = T(0,2,0)*V(0)+T(0,2,1)*V(1)+T(0,2,2)*V(2);
    O.comp(1,0) = T(1,0,0)*V(0)+T(1,0,1)*V(1)+T(1,0,2)*V(2);
    O.comp(1,1) = T(1,1,0)*V(0)+T(1,1,1)*V(1)+T(1,1,2)*V(2);
    O.comp(1,2) = T(1,2,0)*V(0)+T(1,2,1)*V(1)+T(1,2,2)*V(2);
    O.comp(2,0) = T(2,0,0)*V(0)+T(2,0,1)*V(1)+T(2,0,2)*V(2);
    O.comp(2,1) = T(2,1,0)*V(0)+T(2,1,1)*V(1)+T(2,1,2)*V(2);
    O.comp(2,2) = T(2,2,0)*V(0)+T(2,2,1)*V(1)+T(2,2,2)*V(2);
    return O;
}
inline
Dyadic3D mul_imj_m (const Triadic3D& T, const Vector3D& V) {
    Dyadic3D O;
    O.comp(0,0) = T(0,0,0)*V(0)+T(0,1,0)*V(1)+T(0,2,0)*V(2);
    O.comp(0,1) = T(0,0,1)*V(0)+T(0,1,1)*V(1)+T(0,2,1)*V(2);
    O.comp(0,2) = T(0,0,2)*V(0)+T(0,1,2)*V(1)+T(0,2,2)*V(2);
    O.comp(1,0) = T(1,0,0)*V(0)+T(1,1,0)*V(1)+T(1,2,0)*V(2);
    O.comp(1,1) = T(1,0,1)*V(0)+T(1,1,1)*V(1)+T(1,2,1)*V(2);
    O.comp(1,2) = T(1,0,2)*V(0)+T(1,1,2)*V(1)+T(1,2,2)*V(2);
    O.comp(2,0) = T(2,0,0)*V(0)+T(2,1,0)*V(1)+T(2,2,0)*V(2);
    O.comp(2,1) = T(2,0,1)*V(0)+T(2,1,1)*V(1)+T(2,2,1)*V(2);
    O.comp(2,2) = T(2,0,2)*V(0)+T(2,1,2)*V(1)+T(2,2,2)*V(2);
    return O;
}
inline
Dyadic3D mul_mij_m (const Triadic3D& T, const Vector3D& V) {
    Dyadic3D O;
    O.comp(0,0) = T(0,0,0)*V(0)+T(1,0,0)*V(1)+T(2,0,0)*V(2);
    O.comp(0,1) = T(0,0,1)*V(0)+T(1,0,1)*V(1)+T(2,0,1)*V(2);
    O.comp(0,2) = T(0,0,2)*V(0)+T(1,0,2)*V(1)+T(2,0,2)*V(2);
    O.comp(1,0) = T(0,1,0)*V(0)+T(1,1,0)*V(1)+T(2,1,0)*V(2);
    O.comp(1,1) = T(0,1,1)*V(0)+T(1,1,1)*V(1)+T(2,1,1)*V(2);
    O.comp(1,2) = T(0,1,2)*V(0)+T(1,1,2)*V(1)+T(2,1,2)*V(2);
    O.comp(2,0) = T(0,2,0)*V(0)+T(1,2,0)*V(1)+T(2,2,0)*V(2);
    O.comp(2,1) = T(0,2,1)*V(0)+T(1,2,1)*V(1)+T(2,2,1)*V(2);
    O.comp(2,2) = T(0,2,2)*V(0)+T(1,2,2)*V(1)+T(2,2,2)*V(2);
    return O;
}
inline
Triadic3D mul_ijm_mk (const Triadic3D& C, const Dyadic3D& A) {
    Triadic3D R;
    for (int i=0; i<SP3D; i++)
        for (int j=0; j<SP3D; j++)
            for (int k=0; k<SP3D; k++)
                R.comp(i,j,k) = C(i,j,0)*A(0,k)+C(i,j,1)*A(1,k)+C(i,j,2)*A(2,k);
    return R;
}
inline
Triadic3D mul_ijm_km (const Triadic3D& C, const Dyadic3D& A) {
    Triadic3D R;
    for (int i=0; i<SP3D; i++)
        for (int j=0; j<SP3D; j++)
            for (int k=0; k<SP3D; k++)
                R.comp(i,j,k) = C(i,j,0)*A(k,0)+C(i,j,1)*A(k,1)+C(i,j,2)*A(k,2);
    return R;
}
inline
Vector3D mul_mn_mni (const Dyadic3D& A, const Triadic3D& C) {
    Vector3D R;
    for (int i=0; i<SP3D; i++) {
        Dyadic3D Cd;
        Cd.comp = C(0,0,i), C(0,1,i), C(0,2,i),
                  C(1,0,i), C(1,1,i), C(1,2,i),
                  C(2,0,i), C(2,1,i), C(2,2,i);
        R.comp(i) = outer_product(A, Cd);
    }
    return R;
}
inline
Vector3D mul_imn_mn (const Triadic3D& C, const Dyadic3D& A) {
    Vector3D R;
    for (int i=0; i<SP3D; i++) {
        Dyadic3D Cd;
        Cd.comp = C(i,0,0), C(i,0,1), C(i,0,2),
                  C(i,1,0), C(i,1,1), C(i,1,2),
                  C(i,2,0), C(i,2,1), C(i,2,2);
        R.comp(i) = outer_product(Cd, A);
    }
    return R;
}
inline
Triadic3D mul_imn_mnjk (const Triadic3D& C, const Quindic3D& Q) {
    Triadic3D R;
    for (int i=0; i<SP3D; i++) {
        Dyadic3D Cd;
        Cd.comp = C(i,0,0), C(i,0,1), C(i,0,2),
                  C(i,1,0), C(i,1,1), C(i,1,2),
                  C(i,2,0), C(i,2,1), C(i,2,2);
        for (int j=0; j<SP3D; j++) {
            for (int k=0; k<SP3D; k++) {
                Dyadic3D Qd;
                Qd.comp = Q(0,0,j,k), Q(0,1,j,k), Q(0,2,j,k),
                          Q(1,0,j,k), Q(1,1,j,k), Q(1,2,j,k),
                          Q(2,0,j,k), Q(2,1,j,k), Q(2,2,j,k);
                R.comp(i,j,k) = outer_product(Cd, Qd);
            }
        }
    }
    return R;
}
inline
Triadic3D mul_mni_mnjk (const Triadic3D& C, const Quindic3D& Q) {
    Triadic3D R;
    for (int i=0; i<SP3D; i++) {
        Dyadic3D Cd;
        Cd.comp = C(0,0,i), C(0,1,i), C(0,2,i),
                  C(1,0,i), C(1,1,i), C(1,2,i),
                  C(2,0,i), C(2,1,i), C(2,2,i);
        for (int j=0; j<SP3D; j++) {
            for (int k=0; k<SP3D; k++) {
                Dyadic3D Qd;
                Qd.comp = Q(0,0,j,k), Q(0,1,j,k), Q(0,2,j,k),
                          Q(1,0,j,k), Q(1,1,j,k), Q(1,2,j,k),
                          Q(2,0,j,k), Q(2,1,j,k), Q(2,2,j,k);
                R.comp(i,j,k) = outer_product(Cd, Qd);
            }
        }
    }
    return R;
}
inline
Dyadic3D mul_imn_mnj (const Triadic3D& A, const Triadic3D& B) {
    Dyadic3D R;
    for (int i=0; i<SP3D; i++) {
        Dyadic3D Ad;
        Ad.comp = A(i,0,0), A(i,0,1), A(i,0,2),
                  A(i,1,0), A(i,1,1), A(i,1,2),
                  A(i,2,0), A(i,2,1), A(i,2,2);
        for (int j=0; j<SP3D; j++) {
            Dyadic3D Bd;
            Bd.comp = B(0,0,j), B(0,1,j), B(0,2,j),
                      B(1,0,j), B(1,1,j), B(1,2,j),
                      B(2,0,j), B(2,1,j), B(2,2,j);
            R.comp(i,j) = outer_product(Ad, Bd);
        }
    }
    return R;
}
inline
Dyadic3D squared (const Dyadic3D& A) {
    return mul_im_mj(A, A);
}
//-------------------------------------------------------------------------------------------------
inline
Quindic3D mul_ikmjln_mn (const Sextic3D& S, const Dyadic3D& D) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++) {
        for (int j=0; j<SP3D; j++) {
            for (int k=0; k<SP3D; k++) {
                for (int l=0; l<SP3D; l++) {
                    Dyadic3D Sd;
                    Sd.comp = S(i,k,0,j,l,0), S(i,k,0,j,l,1), S(i,k,0,j,l,2),
                              S(i,k,1,j,l,0), S(i,k,1,j,l,1), S(i,k,1,j,l,2),
                              S(i,k,2,j,l,0), S(i,k,2,j,l,1), S(i,k,2,j,l,2);
                    R.comp(i,j,k,l) = outer_product(Sd, D);
                }
            }
        }
    }
    return R;
}
inline
Quindic3D mul_ilmjkn_mn (const Sextic3D& S, const Dyadic3D& D) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++) {
        for (int j=0; j<SP3D; j++) {
            for (int k=0; k<SP3D; k++) {
                for (int l=0; l<SP3D; l++) {
                    Dyadic3D Sd;
                    Sd.comp = S(i,l,0,j,k,0), S(i,l,0,j,k,1), S(i,l,0,j,k,2),
                              S(i,l,1,j,k,0), S(i,l,1,j,k,1), S(i,l,1,j,k,2),
                              S(i,l,2,j,k,0), S(i,l,2,j,k,1), S(i,l,2,j,k,2);
                    R.comp(i,j,k,l) = outer_product(Sd, D);
                }
            }
        }
    }
    return R;
}
inline
Quindic3D mul_jkmiln_mn (const Sextic3D& S, const Dyadic3D& D) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++) {
        for (int j=0; j<SP3D; j++) {
            for (int k=0; k<SP3D; k++) {
                for (int l=0; l<SP3D; l++) {
                    Dyadic3D Sd;
                    Sd.comp = S(j,k,0,i,l,0), S(j,k,0,i,l,1), S(j,k,0,i,l,2),
                              S(j,k,1,i,l,0), S(j,k,1,i,l,1), S(j,k,1,i,l,2),
                              S(j,k,2,i,l,0), S(j,k,2,i,l,1), S(j,k,2,i,l,2);
                    R.comp(i,j,k,l) = outer_product(Sd, D);
                }
            }
        }
    }
    return R;
}
inline
Quindic3D mul_jlmikn_mn (const Sextic3D& S, const Dyadic3D& D) {
    Quindic3D R;
    for (int i=0; i<SP3D; i++) {
        for (int j=0; j<SP3D; j++) {
            for (int k=0; k<SP3D; k++) {
                for (int l=0; l<SP3D; l++) {
                    Dyadic3D Sd;
                    Sd.comp = S(j,l,0,i,k,0), S(j,l,0,i,k,1), S(j,l,0,i,k,2),
                              S(j,l,1,i,k,0), S(j,l,1,i,k,1), S(j,l,1,i,k,2),
                              S(j,l,2,i,k,0), S(j,l,2,i,k,1), S(j,l,2,i,k,2);
                    R.comp(i,j,k,l) = outer_product(Sd, D);
                }
            }
        }
    }
    return R;
}
//-------------------------------------------------------------------------------------------------
inline
Vector2D mul_im_m (const Dyadic2D& A, const Vector2D& B) {
    Vector2D R;
    blitz::firstIndex  i;
    blitz::secondIndex j;
    R.comp = sum(A.comp(i,j)*B.comp(j), j);
    return R;
}
inline
Vector2D mul_m_mi (const Vector2D& B, const Dyadic2D& A) {
    Vector2D R;
    blitz::firstIndex  i;
    blitz::secondIndex j;
    R.comp = sum(B.comp(j)*A.comp(j,i), j);
    return R;
}
inline
Dyadic2D mul_m_imnj_n (const Vector2D& L, const Quindic2D& Q, const Vector2D& R) {
    Dyadic2D O;
    O.comp(0,0) = L(0)*Q(0,0,0,0)*R(0)+L(0)*Q(0,0,1,0)*R(1)+L(1)*Q(0,1,0,0)*R(0)+L(1)*Q(0,1,1,0)*R(1);
    O.comp(0,1) = L(0)*Q(0,0,0,1)*R(0)+L(0)*Q(0,0,1,1)*R(1)+L(1)*Q(0,1,0,1)*R(0)+L(1)*Q(0,1,1,1)*R(1);
    O.comp(1,0) = L(0)*Q(1,0,0,0)*R(0)+L(0)*Q(1,0,1,0)*R(1)+L(1)*Q(1,1,0,0)*R(0)+L(1)*Q(1,1,1,0)*R(1);
    O.comp(1,1) = L(0)*Q(1,0,0,1)*R(0)+L(0)*Q(1,0,1,1)*R(1)+L(1)*Q(1,1,0,1)*R(0)+L(1)*Q(1,1,1,1)*R(1);
    return O;
}
inline
Dyadic2D mul_mi_mn_nj (const Dyadic2D& L, const Dyadic2D& D, const Dyadic2D& R) {
    Dyadic2D O;
    for (int i=0; i<SP2D; i++) {
        for (int j=0; j<SP2D; j++) {
            O.comp(i,j) = L(0,i)*D(0,0)*R(0,j)+L(0,i)*D(0,1)*R(1,j)+L(1,i)*D(1,0)*R(0,j)+L(1,i)*D(1,1)*R(1,j);
        }
    }
    return O;
}
inline
Dyadic2D mul_mi_mn_jn (const Dyadic2D& L, const Dyadic2D& D, const Dyadic2D& R) {
    Dyadic2D O;
    for (int i=0; i<SP2D; i++) {
        for (int j=0; j<SP2D; j++) {
            O.comp(i,j) = L(0,i)*D(0,0)*R(j,0)+L(0,i)*D(0,1)*R(j,1)+L(1,i)*D(1,0)*R(j,0)+L(1,i)*D(1,1)*R(j,1);
        }
    }
    return O;
}
inline
Dyadic2D mul_mj_mn_ni (const Dyadic2D& L, const Dyadic2D& D, const Dyadic2D& R) {
    Dyadic2D O;
    for (int i=0; i<SP2D; i++) {
        for (int j=0; j<SP2D; j++) {
            O.comp(i,j) = L(0,j)*D(0,0)*R(0,i)+L(0,j)*D(0,1)*R(1,i)+L(1,j)*D(1,0)*R(0,i)+L(1,j)*D(1,1)*R(1,i);
        }
    }
    return O;
}
inline
Dyadic2D mul_mj_mn_in (const Dyadic2D& L, const Dyadic2D& D, const Dyadic2D& R) {
    Dyadic2D O;
    for (int i=0; i<SP2D; i++) {
        for (int j=0; j<SP2D; j++) {
            O.comp(i,j) = L(0,j)*D(0,0)*R(i,0)+L(0,j)*D(0,1)*R(i,1)+L(1,j)*D(1,0)*R(i,0)+L(1,j)*D(1,1)*R(i,1);
        }
    }
    return O;
}
inline
Dyadic3D mul_im_mn_nj (const Blitz_DyadicReal& F, const Dyadic2D& C) {
    Blitz_DyadicReal F_C(3,2);
    F_C(0,0) = F(0,0)*C(0,0)+F(0,1)*C(1,0);
    F_C(0,1) = F(0,0)*C(0,1)+F(0,1)*C(1,1);
    F_C(1,0) = F(1,0)*C(0,0)+F(1,1)*C(1,0);
    F_C(1,1) = F(1,0)*C(0,1)+F(1,1)*C(1,1);
    F_C(2,0) = F(2,0)*C(0,0)+F(2,1)*C(1,0);
    F_C(2,1) = F(2,0)*C(0,1)+F(2,1)*C(1,1);
    Dyadic3D F_C_FT;
    F_C_FT.comp(0,0) = F_C(0,0)*F(0,0)+F_C(0,1)*F(0,1);
    F_C_FT.comp(0,1) = F_C(0,0)*F(1,0)+F_C(0,1)*F(1,1);
    F_C_FT.comp(0,2) = F_C(0,0)*F(2,0)+F_C(0,1)*F(2,1);
    F_C_FT.comp(1,0) = F_C(1,0)*F(0,0)+F_C(1,1)*F(0,1);
    F_C_FT.comp(1,1) = F_C(1,0)*F(1,0)+F_C(1,1)*F(1,1);
    F_C_FT.comp(1,2) = F_C(1,0)*F(2,0)+F_C(1,1)*F(2,1);
    F_C_FT.comp(2,0) = F_C(2,0)*F(0,0)+F_C(2,1)*F(0,1);
    F_C_FT.comp(2,1) = F_C(2,0)*F(1,0)+F_C(2,1)*F(1,1);
    F_C_FT.comp(2,2) = F_C(2,0)*F(2,0)+F_C(2,1)*F(2,1);
    return F_C_FT;
}
//=================================================================================================
#endif //  __UTILS_CALCS3_INLINE_H__
//=================================================================================================
