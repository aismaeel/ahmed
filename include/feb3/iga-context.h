/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __NURBS_CONTEXT_H__
#define __NURBS_CONTEXT_H__
//=================================================================================================
#include "feb3/mesh_iga.h"
#include "libmesh/system.h"
#include "libmesh/sparsity_pattern.h"
//=================================================================================================
class IGA_Context {
public:
    IGA_Context () : _system(NULL), _msh_iga(NULL) {}
    IGA_Context (System& sys, MeshIGA* msh) : _system(&sys), _msh_iga(msh) {}
    ~IGA_Context () {}
    //
    inline
    void init (System& sys, MeshIGA* msh) { this->_system = &sys; this->_msh_iga = msh; }
    inline
    System&  get_system () { return *this->_system; }
    inline
    MeshIGA& get_mesh_IGA () { return *this->_msh_iga; }
    //
    inline
    const System&  get_system () const { return *this->_system; }
    inline
    const MeshIGA& get_mesh_IGA () const { return *this->_msh_iga; }
    //
    void get_var_list (std::vector<unsigned short int>& vars) const;
    void get_dof_indices ( const NURBS* nurbs,
                           std::vector<dof_id_type>& dofs, std::vector<dof_id_type>* dofs_var,
                           std::vector<unsigned int>* vn =NULL) const;
    //
private:
    System* _system;
    MeshIGA* _msh_iga;
};
//=================================================================================================
void send_list_func ( std::vector<dof_id_type>&, void* );
void sparsity_func ( SparsityPattern::Graph&,
                     std::vector<dof_id_type>&, std::vector<dof_id_type>&, void* );
//non-member scalar print function
inline
void print_helper (std::ostream& os, const IGA_Context* ctx) {
    libmesh_error();
}
//=================================================================================================
#endif //  __NURBS_CONTEXT_H__
//=================================================================================================
