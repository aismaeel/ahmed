/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __PARAVIEW_IO_INLINE_H__
#define __PARAVIEW_IO_INLINE_H__
//=================================================================================================
inline
ParaviewIO::ParaviewIO (MeshBase& mesh, MeshData* mesh_data) :
    MeshInput<MeshBase> (mesh), MeshOutput<MeshBase>(mesh), _mesh_data(mesh_data) {
    this->_write_reduced_mesh = false;
    this->_write_refined_mesh = this->_write_auxiliary_data = true;
    this->_write_quadrature_data = false;
}
inline
ParaviewIO::ParaviewIO (const MeshBase& mesh, MeshData* mesh_data) :
    MeshOutput<MeshBase>(mesh), _mesh_data(mesh_data) {
    this->_write_reduced_mesh = false;
    this->_write_refined_mesh = this->_write_auxiliary_data = true;
    this->_write_quadrature_data = false;
}
//-------------------------------------------------------------------------------------------------
inline
void ParaviewIO::write_nodal_data ( const std::string& fname, const std::vector<Number>& soln,
                                    const std::vector<std::string>& names ) {
    if ( libMesh::global_processor_id() == 0 ) this->_write_ascii(fname, &soln, &names);
}
inline
void ParaviewIO::read (const std::string& name) { libmesh_error(); }
inline
void ParaviewIO::write (const std::string& name) { libmesh_error(); }
inline
void ParaviewIO::write_reduced_mesh    (bool doit) { this->_write_reduced_mesh = doit; }
inline
void ParaviewIO::write_refined_mesh    (bool doit) { this->_write_refined_mesh = doit; }
inline
void ParaviewIO::write_quadrature_data (bool doit) { this->_write_quadrature_data = doit; }
inline
void ParaviewIO::write_auxiliary_data  (bool doit) { this->_write_auxiliary_data = doit; }
inline
void ParaviewIO::hide_variable (const std::string& name) { this->_hide_vars.insert(name); }
inline
void ParaviewIO::_cell_connectivity (const Elem* elem, std::vector<unsigned int>& c_conn) {
    elem->connectivity(0, VTK, c_conn);
}
inline
unsigned int ParaviewIO::_cell_type (const Elem* elem) {
    unsigned int c_typ = 0; // initialize to something to avoid compiler warning
    switch ( elem->type() ) {
    case EDGE2 :    c_typ = 3;  break; //VTK_LINE
    case EDGE3 :    c_typ = 21; break; //VTK_QUADRATIC_EDGE
    case TRI3 :     c_typ = 5;  break; //VTK_TRIANGLE
    case TRI6 :     c_typ = 22; break; //VTK_QUADRATIC_TRIANGLE
    case QUAD4 :    c_typ = 9;  break; //VTK_QUAD
    case QUAD8 :    c_typ = 23; break; //VTK_QUADRATIC_QUAD
    case QUAD9 :    c_typ = 28; break; //VTK_BIQUADRATIC_QUAD
    case TET4 :     c_typ = 10; break; //VTK_TETRA
    case TET10 :    c_typ = 24; break; //VTK_QUADRATIC_TETRA
    case HEX8 :     c_typ = 12; break; //VTK_HEXAHEDRON
    case HEX20 :    c_typ = 25; break; //VTK_QUADRATIC_HEXAHEDRON
    case HEX27 :    c_typ = 33; break; //VTK_BIQUADRATIC_QUADRATIC_HEXAHEDRON
    case PRISM6 :   c_typ = 13; break; //VTK_WEDGE
    case PRISM15 :  c_typ = 26; break; //VTK_QUADRATIC_WEDGE
    case PRISM18 :  c_typ = 32; break; //VTK_BIQUADRATIC_QUADRATIC_WEDGE
    case PYRAMID5 : c_typ = 14; break; //VTK_PYRAMID
    default : libmesh_error();
    }
    return c_typ;
}
inline
unsigned int ParaviewIO::_cell_offset (const Elem* elem) {
    std::vector<unsigned int> conn;
    this->_cell_connectivity(elem, conn);
    return conn.size();
}
inline
bool ParaviewIO::_is_hidden_variable (const std::string& name) {
    for ( std::set<std::string>::const_iterator
          citer=this->_hide_vars.begin(); citer!=this->_hide_vars.end(); citer++ ) {
        if ( (*citer) == name ) return true;
    }
    return false;
}
//=================================================================================================
#endif // __PARAVIEW_IO_INLINE_H__
//=================================================================================================
