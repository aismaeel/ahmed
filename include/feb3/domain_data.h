/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __DOMAIN_DATA_H__
#define __DOMAIN_DATA_H__
//=================================================================================================
#include "libmesh/equation_systems.h"
#include "feb3/quadrature.h"
//=================================================================================================
class DomainData {
public:
    DomainData (EquationSystems& es) : _equation_systems(es) {}
    ~DomainData () {}
    //
    void prepare_mesh ();
    void init ();
    void init_quadrature ();
    void save_quadrature ( std::ofstream& fout,
                           QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) const;
    void load_quadrature ( std::ifstream& fin,
                           QuadratureInfo::Config cfg, QuadratureInfo::Level lvl );
    void save_quadrature ( const char* fname,
                           QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) const;
    void load_quadrature ( const char* fname,
                           QuadratureInfo::Config cfg, QuadratureInfo::Level lvl );
    void gather_quadrature ( const std::vector<std::string>& fname_ls,
                             QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) const;
    // access the equation systems
    inline
    const EquationSystems& get_equation_systems () const { return this->_equation_systems; }
    inline
    EquationSystems&       get_equation_systems () { return this->_equation_systems; }
    // access the finite element mesh
    inline
    const MeshBase& get_mesh () const { return this->_equation_systems.get_mesh(); }
    inline
    MeshBase&       get_mesh () { return this->_equation_systems.get_mesh(); }
    //
    void find_neighbor_elem (const Node* n, std::vector<const Elem*>& elem_conn) const;
    void find_neighbor_node (const Node* n, std::vector<const Node*>& node_conn) const;
public:
    // list of nodes and to which active elements they belong
    std::map<dof_id_type, std::set<dof_id_type> > node2elem;
    std::map<dof_id_type, std::map<dof_id_type, SetUInt> > node2elemside;
    // list of boundary conditions (surfaces & nodal)
    std::map<boundary_id_type, BoundaryConditionInfo> bcs;
    std::map<dof_id_type, BoundaryConditionInfo> nodal_bcs;
    // list of materials and regions
    std::map<material_id_type, MaterialInfo> mater;
    std::map<region_id_type, RegionInfo> regn;
    // list of quadrature points data of elements and side elements (global and local)
    mutable std::map<dof_id_type, QuadratureInfo> elem_qi;
    std::map<dof_id_type, QuadratureInfo> elem_qi_local;
    mutable std::map<dof_id_type, std::map<unsigned int, QuadratureInfo> > elem_side_qi;
    std::map<dof_id_type, std::map<unsigned int, QuadratureInfo> > elem_side_qi_local;
    // list of nodes in damaged and healthy regions (global and local ones)
    std::set<dof_id_type> damaged_node,       healthy_node,       interface_node;
    std::set<dof_id_type> damaged_node_local, healthy_node_local, interface_node_local;
    // body- and nodal-force information data
    BodyForceInfo bforce;
    NodalForceInfo nforce;
private:
    EquationSystems& _equation_systems;
    // map container of the node to element connectivity, valid only for FEM
    std::vector< std::vector<const Elem*> > _nodes_to_elem_map;
};
//-------------------------------------------------------------------------------------------------
class DomainData_VascularNetwork {
public:
    DomainData_VascularNetwork (EquationSystems& es, EquationSystems& es_3d) : _equation_systems(es), _equation_systems_3d(es_3d) {}
    ~DomainData_VascularNetwork () {}
    //
    void prepare_mesh ();
    void init ();
    void save_vasculature (std::ofstream& fout) const;
    void load_vasculature (std::ifstream& fin);
    void save_vasculature (const char* fname) const;
    void load_vasculature (const char* fname);
    void save_all (const char* fname) const;
    void load_all (const char* fname);
    // access the equation systems
    inline
    const EquationSystems& get_equation_systems () const { return this->_equation_systems; }
    inline
    EquationSystems&       get_equation_systems () { return this->_equation_systems; }
    // access the finite element mesh
    inline
    const MeshBase& get_mesh () const { return this->_equation_systems.get_mesh(); }
    inline
    MeshBase&       get_mesh () { return this->_equation_systems.get_mesh(); }
    // output the finite element mesh in Gmsh format
    void save_mesh (const char* fname) const;
private:
    void _init_self_p0 ();
    void _init_self ();
    void _init_extern ();
public:
    // list of boundary (in-/outlet) vascular nodes
    std::set<dof_id_type> node_inlet,
                          node_outlet;
    // list of active 3D elements with associated vascular nodes
    std::map<dof_id_type, std::set<dof_id_type> > elem3d__node;
    // list of vascular nodes with associated vascular information
    std::map<dof_id_type, VascularInfo> node_vi;
    // list of branches with associated vascular nodes
    std::map<dof_id_type, std::set<dof_id_type> > branch__node;
private:
    EquationSystems& _equation_systems;
    EquationSystems& _equation_systems_3d;
};
//-------------------------------------------------------------------------------------------------
class DomainData_SolidCoupledWithVascularNetwork {
public:
    enum Type {
        VESSEL = 101,
        STROMA = 102,
        INTERFACE = 200
    };
public:
    DomainData_SolidCoupledWithVascularNetwork (EquationSystems& es, EquationSystems& es_3d) : _equation_systems(es), _equation_systems_3d(es_3d) {}
    ~DomainData_SolidCoupledWithVascularNetwork () {}
    //
    void prepare_mesh (const DomainData_VascularNetwork& dmn_vn);
    void init (const DomainData_VascularNetwork& dmn_vn);
    // access the equation systems
    inline
    const EquationSystems& get_equation_systems () const { return this->_equation_systems; }
    inline
    EquationSystems&       get_equation_systems () { return this->_equation_systems; }
    // access the finite element mesh
    inline
    const MeshBase& get_mesh () const { return this->_equation_systems.get_mesh(); }
    inline
    MeshBase&       get_mesh () { return this->_equation_systems.get_mesh(); }
public:
    // stromal nodes (3D mesh) -> coupled-mesh nodes ...and vice versa
    std::map<dof_id_type, dof_id_type> strm_node__cm_strm_node;
    std::map<dof_id_type, dof_id_type> cm_strm_node__strm_node;
    // vascular nodes (VascularNetwork mesh) -> coupled-mesh nodes ...and vice versa
    std::map<dof_id_type, dof_id_type> vasc_node__cm_vasc_node;
    std::map<dof_id_type, dof_id_type> cm_vasc_node__vasc_node;
    // list of coupled-mesh stromal nodes situated on the boundary of the analysis domain
    std::set<dof_id_type> cm_strm_node__boundary;
    // coupled-mesh stromal nodes -> coupled-mesh stromal and vascular (neighbor) nodes
    std::map<dof_id_type, std::set<pair_dof_id_type> > cm_strm_node__cm_strm_node_segm;
    std::map<dof_id_type, std::set<pair_dof_id_type> > cm_strm_node__cm_vasc_node_segm;
    // coupled-mesh stromal elements -> subdomain ID (3D mesh)
    std::map<dof_id_type, subdomain_id_type> cm_strm_elem__subdmnid;
    // coupled-mesh vascular elements -> vascular elements (VascularNetwork mesh) ...and vice versa
    std::map<dof_id_type, dof_id_type> cm_vasc_elem__vasc_elem;
    std::map<dof_id_type, dof_id_type> vasc_elem__cm_vasc_elem;
    // coupled-mesh vascular nodes -> coupled-mesh interface elements
    std::map<dof_id_type, std::set<dof_id_type> > cm_vasc_node__cm_intf_elem;
private:
    EquationSystems& _equation_systems;
    EquationSystems& _equation_systems_3d;
};
//=================================================================================================
inline
void coupled_membrane_0d (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&) {}
inline
void coupled_solid_0d    (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&) {}
//=================================================================================================
namespace healthy {
//=================================================================================================
// routines to calculate the Cauchy stress & the Euleriean tangent stiffness tensor (INVERSE ANALYSIS)
void polynomial_2d_inverse     (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
void polynomial_3d_inverse_std (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_inverse_mod (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_inverse_2f  (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
// routines to calculate the 2nd Piola-Kirchhoff stress & the Lagrangian tangent stiffness tensor (FORWARD ANALYSIS)
void polynomial_2d_forward     (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
void polynomial_3d_forward_std (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_forward_mod (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_forward_2f  (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
//=================================================================================================
}
//=================================================================================================
namespace dynamic {
//=================================================================================================
// routines to calculate the 2nd Piola-Kirchhoff stress & the Lagrangian tangent stiffness tensor (FORWARD ANALYSIS)
void polynomial_2d_forward     (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
void polynomial_3d_forward_std (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_forward_mod (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_forward_2f  (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
//=================================================================================================
}
//=================================================================================================
namespace damaged {
//=================================================================================================
// routines to calculate the 2nd Piola-Kirchhoff stress & the Lagrangian tangent stiffness tensor (INVERSE ANALYSIS)
void polynomial_2d_inverse     (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
void polynomial_3d_inverse_std (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_inverse_mod (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_inverse_2f  (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
// routines to calculate the 2nd Piola-Kirchhoff stress & the Lagrangian tangent stiffness tensor (FORWARD ANALYSIS)
void polynomial_2d_forward     (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
void polynomial_3d_forward_std (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_forward_mod (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void polynomial_3d_forward_2f  (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
//=================================================================================================
}
//=================================================================================================
namespace invitro_tumoroid_growth {
//=================================================================================================
void damaged_region_711 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void healthy_region_711 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void damaged_region_813 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void healthy_region_813 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void damaged_region_1015(const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void healthy_region_1015(const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
//=================================================================================================
}
//=================================================================================================
namespace radiotherapy_tumour_fibrosis {
//=================================================================================================
void damaged_region_311 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void healthy_region_311 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
//=================================================================================================
}
//=================================================================================================
namespace wound_healing {
//=================================================================================================
void damage_model_511 (const MaterialInfo&, FieldInfo&);
void damage_model_611 (const MaterialInfo&, FieldInfo&);
//
void damaged_tissue_511 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void healthy_tissue_511 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void damaged_tissue_611 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void healthy_tissue_611 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
// routines to calculate the 2nd Piola-Kirchhoff stress & the Lagrangian tangent stiffness tensor (FORWARD ANALYSIS)
void contraction_membrane_511 (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
void contraction_solid_511    (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
void contraction_membrane_611 (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
void contraction_solid_611    (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
//=================================================================================================
}
//=================================================================================================
namespace tumour_angiogenesis {
//=================================================================================================
void damage_model_486 (const MaterialInfo&, FieldInfo&);
//
void damaged_tissue_486 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
void healthy_tissue_486 (const MaterialInfo&, const FieldInfo&, Blitz_VectorReal&, Blitz_VectorGradient&, Blitz_DyadicReal&, Blitz_VectorPoint&, Blitz_DyadicReal&, Blitz_DyadicGradient&);
//
void inelastic_deform_membrane_486 (const MaterialInfo&, const FieldInfo&);
void inelastic_deform_solid_486    (const MaterialInfo&, const FieldInfo&);
// routines to calculate the 2nd Piola-Kirchhoff stress & the Lagrangian tangent stiffness tensor (FORWARD ANALYSIS)
void growth_membrane_486 (const MaterialInfo&, const FieldInfo&, Dyadic2D&, Quindic2D&);
void growth_solid_486    (const MaterialInfo&, const FieldInfo&, Dyadic3D&, Quindic3D&, Dyadic3D&, Real&, Dyadic3D&, Real&);
//
void growth_vascular_486 (const MaterialInfo&, const FieldInfo&, const VascularInfo&, Displacement&);
//
void remodel_vascular_XYZ (const MaterialInfo&, VascularInfo&, const Real*);
//=================================================================================================
}
//=================================================================================================
#endif // __DOMAIN_DATA_H__
//=================================================================================================
