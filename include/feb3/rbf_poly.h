/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __RBF_POLY_H__
#define __RBF_POLY_H__
//=================================================================================================
#define X   p(0)
#define Y   p(1)
#define Z   p(2)
//=================================================================================================
inline
void linear_polynomial (const Point& p, Blitz_VectorReal& pm) {
    pm.resize(4);
    pm = 1.0, X, Y, Z;
}
inline
void bilinear_polynomial (const Point& p, Blitz_VectorReal& pm) {
    pm.resize(7);
    pm = 1.0, X, Y, Z, X*Y, X*Z, Y*Z;
}
inline
void quadratic_polynomial (const Point& p, Blitz_VectorReal& pm) {
    pm.resize(10);
    pm = 1.0, X, Y, Z, X*Y, X*Z, Y*Z, X*X, Y*Y, Z*Z;
}
inline
void biquadratic_polynomial (const Point& p, Blitz_VectorReal& pm) {
    pm.resize(16);
    pm = 1.0, X, Y, Z, X*Y, X*Z, Y*Z, X*X, Y*Y, Z*Z,
         X*X*Y, X*Y*Y, X*X*Z, X*Z*Z, Y*Y*Z, Y*Z*Z;
}
inline
void cubic_polynomial (const Point& p, Blitz_VectorReal& pm) {
    pm.resize(19);
    pm = 1.0, X, Y, Z, X*Y, X*Z, Y*Z, X*X, Y*Y, Z*Z,
         X*X*Y, X*Y*Y, X*X*Z, X*Z*Z, Y*Y*Z, Y*Z*Z, X*X*X, Y*Y*Y, Z*Z*Z;
}
//=================================================================================================
#undef X
#undef Y
#undef Z
//=================================================================================================
#endif // __RBF_POLY_H__
//=================================================================================================
