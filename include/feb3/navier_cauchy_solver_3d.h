/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __NAVIER_CAUCHY_SOLVER_3D_H__
#define __NAVIER_CAUCHY_SOLVER_3D_H__
//=================================================================================================
#include "libmesh/equation_systems.h"
#include "feb3/domain_data.h"
#include "libmesh/perf_log.h"
#include "feb3/fem-context.h"
//=================================================================================================
class NavierCauchySolver3D {
public:
    NavierCauchySolver3D (DomainData& dmn);
    ~NavierCauchySolver3D () {}
    //
    const std::string& get_label () const;
    const std::string get_log () const;
    const DomainData& get_domain () const;
    //
    void init ();
    void run ();
    void get_displacements (const std::string&, std::vector< std::vector<Real> >&, IndexPair&) const;
    void get_pressure      (const std::string&, std::vector< std::vector<Real> >&, IndexPair&) const;
    void save_restart (std::ofstream& fout) const;
    void load_restart ();
protected:
    void _copy_params ();
    void _set_initial ();
    void _assemble_system__FWD_U  ();
    void _assemble_system__FWD_UP ();
    void _assemble_system__INV_U  ();
    void _assemble_system__INV_UP ();
    void _solve ();
    int  _check_convergence () const;
    void _reset_increment (bool failed =false);
    void _set_quad_info__FWD_U  ();
    void _set_quad_info__FWD_UP ();
    void _set_quad_info__INV_U  ();
    void _set_quad_info__INV_UP ();
    void _calc_gradients ();
private:
    void _assemble_system_essential_bcs ();
private:
    std::string _label;
    mutable PerfLog _log;
protected:
    DomainData& _dmn;
public:
    Parameters par_list;
};
//=================================================================================================
#include "feb3/navier_cauchy_solver_3d_inl.h"
//=================================================================================================
#endif // __NAVIER_CAUCHY_SOLVER_3D_H__
//=================================================================================================
