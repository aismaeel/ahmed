/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __NURBS_INLINE_H__
#define __NURBS_INLINE_H__
//=================================================================================================
// functions to read/write access
inline
dof_id_type& NURBS::set_id () {
    return this->_id;
}
inline
subdomain_id_type& NURBS::set_subdomain_id () {
    return this->_sbd_id;
}
inline
dof_id_type& NURBS::set_processor_id () {
    return this->_processor_id;
}
inline
Node*& NURBS::set_ctrl_node (int i, int j, int k) {
    return this->_ctrl_pnt(i, j, k);
}
inline
Real&  NURBS::set_weight   (int i, int j, int k) {
    return this->_weight(i, j, k);
}
inline
void NURBS::set_knot_vec (int d, const Blitz_VectorReal& kv) {
    libmesh_assert( d>=0 && d<SP3D );
    this->_knot_vec[d].free();
    this->_knot_vec[d].resize(kv.size());
    this->_knot_vec[d] = kv;
    // set the non-repeated knot vector container
    std::set<Real> knot_v;
    for (int i=0; i<this->_knot_vec[d].size(); i++) {
        knot_v.insert( this->_knot_vec[d](i) );
    }
    this->_nonrepeated_knot_vec[d].free();
    this->_nonrepeated_knot_vec[d].resize(knot_v.size());
    int j = 0;
    for (std::set<Real>::const_iterator ci=knot_v.begin(); ci!=knot_v.end(); ci++) {
        this->_nonrepeated_knot_vec[d](j) = *ci;
        ++j;
    }
}
inline
void NURBS::set_to_rational    () { this->_is_rational = true; }
inline
void NURBS::set_to_nonrational () { this->_is_rational = false; }
//
template<int DIM>
inline
void NURBS::unclamp_begin () {
    this->_open_begin[DIM] = false;
}
template<int DIM>
inline
void NURBS::unclamp_end () {
    this->_open_end[DIM] = false;
}
// functions for read-only access
inline
dof_id_type NURBS::id () const {
    return this->_id;
}
inline
subdomain_id_type NURBS::subdomain_id () const {
    return this->_sbd_id;
}
inline
dof_id_type NURBS::processor_id () const {
    return this->_processor_id;
}
inline
unsigned int NURBS::dimension () const {
    return this->_dim;
}
inline
bool NURBS::is_rational () const {
    return _is_rational;
}
inline
int NURBS::order (int d) const {
    libmesh_assert( d>=0 && d<SP3D );
    return this->_order[d];
}
inline
unsigned int NURBS::n_ctrl_pnt (int d) const {
    libmesh_assert( d>=0 && d<SP3D );
    return this->_ctrl_pnt.extent(d);
}
inline
unsigned int NURBS::n_knots () const {
    if      ( this->dimension() == SP1D ) return this->n_knots(0);
    else if ( this->dimension() == SP2D ) return this->n_knots(0)*this->n_knots(1);
    else if ( this->dimension() == SP3D ) return this->n_knots(0)*this->n_knots(1)*this->n_knots(2);
    else libmesh_error();
}
inline
unsigned int NURBS::n_knots (int d) const {
    libmesh_assert( d>=0 && d<SP3D );
    return this->_nonrepeated_knot_vec[d].extent(0);
}
inline
unsigned int NURBS::all_knots () const {
    if      ( this->dimension() == SP1D ) return this->all_knots(0);
    else if ( this->dimension() == SP2D ) return this->all_knots(0)*this->all_knots(1);
    else if ( this->dimension() == SP3D ) return this->all_knots(0)*this->all_knots(1)*this->all_knots(2);
    else libmesh_error();
}
inline
unsigned int NURBS::all_knots (int d) const {
    libmesh_assert( d>=0 && d<SP3D );
    return this->_knot_vec[d].extent(0);
}
inline
unsigned int NURBS::span (int d) const {
    return this->n_knots(d)-1;
}
inline
unsigned int NURBS::size () const {
    return this->_idx_table.size();
}
//
inline
Node* NURBS::get_ctrl_node (int i, int j, int k) const {
    return this->_ctrl_pnt(i, j, k);
}
inline
const Point& NURBS::ctrl_pnt (int i, int j, int k) const {
    return *(this->get_ctrl_node(i, j, k));
}
inline
Real NURBS::weight (int i, int j, int k) const {
    return this->_weight(i, j, k);
}
//
inline
Node* NURBS::get_ctrl_node_indexed (int a) const {
    libmesh_assert( a>=0 && a<this->size() );
    return this->get_ctrl_node(this->index<0>(a), this->index<1>(a), this->index<2>(a));
}
inline
const Point& NURBS::ctrl_pnt_indexed (int a) const {
    libmesh_assert( a>=0 && a<this->size() );
    return this->ctrl_pnt(this->index<0>(a), this->index<1>(a), this->index<2>(a));
}
inline
Real NURBS::weight_indexed (int a) const {
    libmesh_assert( a>=0 && a<this->size() );
    return this->weight(this->index<0>(a), this->index<1>(a), this->index<2>(a));
}
inline
const Blitz_VectorReal& NURBS::get_knot_vec (int d) const {
    libmesh_assert( d>=0 && d<SP3D );
    return this->_knot_vec[d];
}
inline
void NURBS::get_knot_net (blitz::Array<Point, SP3D>& net) const {
    net.free();
    net.resize(this->n_knots(0), this->n_knots(1), this->n_knots(2));
    // loop over all non-repeated knots
    for (int K=0; K<this->n_knots(2); K++)
        for (int J=0; J<this->n_knots(1); J++)
            for (int I=0; I<this->n_knots(0); I++)
                net(I,J,K) = this->nonrepeated_knot(I, J, K);
}
//
template <int DIM>
inline
int NURBS::index (int a) const {
    libmesh_assert( a >= 0 && a < this->size() );
    return this->_idx_table[a](DIM);
}
template <int DIM>
inline
Real NURBS::knot (int i) const {
    return this->_knot_vec[DIM](i);
}
inline
Point NURBS::nonrepeated_knot (int i, int j, int k) const {
    if ( i < 0 || i >= this->n_knots(0) ) libmesh_error();
    if ( j < 0 || j >= this->n_knots(1) ) libmesh_error();
    if ( k < 0 || k >= this->n_knots(2) ) libmesh_error();
    return Point( this->_nonrepeated_knot_vec[0](i),
                  this->_nonrepeated_knot_vec[1](j),
                  this->_nonrepeated_knot_vec[2](k) );
}
//-------------------------------------------------------------------------------------------------
inline
unsigned int NURBS::n_sides () const {
    switch ( this->dimension() ) {
    case SP1D :
        return 2;
    case SP2D :
        return 4;
    case SP3D :
        return 6;
    default :
        libmesh_error();
    }
}
inline
void NURBS::set_neighbor (const unsigned int s, NURBS* nurbs) {
    libmesh_assert( s >= this->n_sides() );
    this->_neighbor[s] = nurbs;
}
inline
void NURBS::set_boundary_id (const unsigned int s, boundary_id_type b_id) {
    libmesh_assert( s >= this->n_sides() );
    this->_boundary_id[s] = b_id;
}
inline
bool NURBS::has_neighbor (const NURBS* nurbs) const {
    for (unsigned int s=0; s<this->n_sides(); s++) {
        if ( this->_neighbor[s] == nurbs ) return true;
    }
    return false;
}
inline
NURBS* NURBS::neighbor (const unsigned int s) const {
    libmesh_assert( s >= this->n_sides() );
    return this->_neighbor[s];
}
inline
boundary_id_type NURBS::boundary_id (const unsigned int s) const {
    libmesh_assert( s >= this->n_sides() );
    return this->_boundary_id[s];
}
//-------------------------------------------------------------------------------------------------
template <int DIM>
inline
Real NURBS::_basef_0th_deriv (Real crd, int i) const {
    return this->_basef_0th_deriv<DIM>(crd, i, this->order(DIM));
}
template <int DIM>
inline
Real NURBS::_basef_1st_deriv (Real crd, int i) const {
    return this->_basef_1st_deriv<DIM>(crd, i, this->order(DIM));
}
template <int DIM>
inline
Real NURBS::_basef_2nd_deriv (Real crd, int i) const {
    return this->_basef_2nd_deriv<DIM>(crd, i, this->order(DIM));
}
template <int DIM>
inline
Real NURBS::_basef_3rd_deriv (Real crd, int i) const {
    return this->_basef_3rd_deriv<DIM>(crd, i, this->order(DIM));
}
//-------------------------------------------------------------------------------------------------
template <int DIM>
inline
Real NURBS::_basef_0th_deriv (Real crd, int i, int p) const {
    libmesh_assert( i >= 0 && i < this->n_ctrl_pnt(DIM) );
    libmesh_assert( p >= static_cast<int>(CONSTANT) && p <= static_cast<int>(this->order(DIM)) );
    libmesh_assert( this->_open_begin[DIM] && this->_open_end[DIM] );
    //
    if ( p == static_cast<int>(CONSTANT) ) {
        if ( fabs(crd-knot<DIM>(all_knots(DIM)-1)) <= 1.0e-5 )
            return ( i < this->n_ctrl_pnt(DIM)-1 ? 0.0 : 1.0 );
        return ( ( crd >= knot<DIM>(i) && crd < knot<DIM>(i+1) ) ? 1.0 : 0.0 );
    } else {
        const Real denom_1 = (knot<DIM>(i)     - knot<DIM>(i+p));
        const Real denom_2 = (knot<DIM>(i+p+1) - knot<DIM>(i+1));
        const Real frac_1 = (fabs(denom_1)<=_tolerance) ? 0.0 : ((knot<DIM>(i)     - crd)/denom_1);
        const Real frac_2 = (fabs(denom_2)<=_tolerance) ? 0.0 : ((knot<DIM>(i+p+1) - crd)/denom_2);
        return ( frac_1 * _basef_0th_deriv<DIM>(crd,i+0,p-1)
               + frac_2 * _basef_0th_deriv<DIM>(crd,i+1,p-1) );
    }
}
template <int DIM>
inline
Real NURBS::_basef_1st_deriv (Real crd, int i, int p) const {
    libmesh_assert( i >= 0 && i < this->n_ctrl_pnt(DIM) );
    libmesh_assert( p >= static_cast<int>(CONSTANT) && p <= static_cast<int>(this->order(DIM)) );
    libmesh_assert( this->_open_begin[DIM] && this->_open_end[DIM] );
    //
    if ( p == static_cast<int>(CONSTANT) ) {
        return 0.0;
    } else {
        const Real denom_1 = (knot<DIM>(i)     - knot<DIM>(i+p));
        const Real denom_2 = (knot<DIM>(i+p+1) - knot<DIM>(i+1));
        const Real frac_1 = (fabs(denom_1)<=_tolerance) ? 0.0 : ((knot<DIM>(i)     - crd)/denom_1);
        const Real frac_2 = (fabs(denom_2)<=_tolerance) ? 0.0 : ((knot<DIM>(i+p+1) - crd)/denom_2);
        const Real frac_3 = (fabs(denom_1)<=_tolerance) ? 0.0 : (-1.0/denom_1);
        const Real frac_4 = (fabs(denom_2)<=_tolerance) ? 0.0 : (-1.0/denom_2);
        return ( frac_1 * _basef_1st_deriv<DIM>(crd,i+0,p-1)
               + frac_2 * _basef_1st_deriv<DIM>(crd,i+1,p-1)
               + frac_3 * _basef_0th_deriv<DIM>(crd,i+0,p-1)
               + frac_4 * _basef_0th_deriv<DIM>(crd,i+1,p-1) );
    }
}
template <int DIM>
inline
Real NURBS::_basef_2nd_deriv (Real crd, int i, int p) const {
    libmesh_assert( i >= 0 && i < this->n_ctrl_pnt(DIM) );
    libmesh_assert( p >= static_cast<int>(CONSTANT) && p <= static_cast<int>(this->order(DIM)) );
    libmesh_assert( this->_open_begin[DIM] && this->_open_end[DIM] );
    //
    if ( p == static_cast<int>(CONSTANT) ||
         p == static_cast<int>(FIRST)    ) {
        return 0.0;
    } else {
        const Real denom_1 = (knot<DIM>(i)     - knot<DIM>(i+p));
        const Real denom_2 = (knot<DIM>(i+p+1) - knot<DIM>(i+1));
        const Real frac_1 = (fabs(denom_1)<=_tolerance) ? 0.0 : ((knot<DIM>(i)     - crd)/denom_1);
        const Real frac_2 = (fabs(denom_2)<=_tolerance) ? 0.0 : ((knot<DIM>(i+p+1) - crd)/denom_2);
        const Real frac_3 = (fabs(denom_1)<=_tolerance) ? 0.0 : (-2.0/denom_1);
        const Real frac_4 = (fabs(denom_2)<=_tolerance) ? 0.0 : (-2.0/denom_2);
        return ( frac_1 * _basef_2nd_deriv<DIM>(crd,i+0,p-1)
               + frac_2 * _basef_2nd_deriv<DIM>(crd,i+1,p-1)
               + frac_3 * _basef_1st_deriv<DIM>(crd,i+0,p-1)
               + frac_4 * _basef_1st_deriv<DIM>(crd,i+1,p-1) );
    }
}
template <int DIM>
inline
Real NURBS::_basef_3rd_deriv (Real crd, int i, int p) const {
    libmesh_assert( i >= 0 && i < this->n_ctrl_pnt(DIM) );
    libmesh_assert( p >= static_cast<int>(CONSTANT) && p <= static_cast<int>(this->order(DIM)) );
    libmesh_assert( this->_open_begin[DIM] && this->_open_end[DIM] );
    //
    if ( p == static_cast<int>(CONSTANT) ||
         p == static_cast<int>(FIRST)    ||
         p == static_cast<int>(SECOND)   ) {
        return 0.0;
    } else {
        const Real denom_1 = (knot<DIM>(i)     - knot<DIM>(i+p));
        const Real denom_2 = (knot<DIM>(i+p+1) - knot<DIM>(i+1));
        const Real frac_1 = (fabs(denom_1)<=_tolerance) ? 0.0 : ((knot<DIM>(i)     - crd)/denom_1);
        const Real frac_2 = (fabs(denom_2)<=_tolerance) ? 0.0 : ((knot<DIM>(i+p+1) - crd)/denom_2);
        const Real frac_3 = (fabs(denom_1)<=_tolerance) ? 0.0 : (-3.0/denom_1);
        const Real frac_4 = (fabs(denom_2)<=_tolerance) ? 0.0 : (-3.0/denom_2);
        return ( frac_1 * _basef_3rd_deriv<DIM>(crd,i+0,p-1)
               + frac_2 * _basef_3rd_deriv<DIM>(crd,i+1,p-1)
               + frac_3 * _basef_2nd_deriv<DIM>(crd,i+0,p-1)
               + frac_4 * _basef_2nd_deriv<DIM>(crd,i+1,p-1) );
    }
}
//-------------------------------------------------------------------------------------------------
inline
const Point& NURBS::get_xyz () const {
    this->_calculate_coord = true;
    return this->_xyz;
}
inline
const std::vector<Real>& NURBS::get_phi () const {
    return this->_phi;
}
inline
const std::vector<Vector3D>& NURBS::get_dphi () const {
    this->_calculate_jacobian = true;
    for (int L=0; L<this->dimension(); L++)
        this->_calculate_dphidz[L] = true;
    this->_calculate_dphi = true;
    return this->_dphi;
}
inline
const std::vector<Dyadic3D>& NURBS::get_d2phi () const {
    this->_calculate_jacobian_2 = true;
    for (int L=0; L<this->dimension(); L++)
        for (int M=0; M<this->dimension(); M++)
            this->_calculate_d2phidz2[L][M] = true;
    this->_calculate_d2phi = true;
    return this->_d2phi;
}
inline
const std::vector<Triadic3D>& NURBS::get_d3phi () const {
    this->_calculate_jacobian_3 = true;
    for (int L=0; L<this->dimension(); L++)
        for (int M=0; M<this->dimension(); M++)
            for (int N=0; N<this->dimension(); N++)
                this->_calculate_d3phidz3[L][M][N] = true;
    this->_calculate_d3phi = true;
    return this->_d3phi;
}
inline
const Real& NURBS::get_jacobian () const {
    NURBS::get_dphi();
    this->_calculate_jacobian = true;
    return this->_jac_det;
}
//
template <int I>
inline
const std::vector<Real>& NURBS::get_dphidz   () const {
    this->_calculate_dphidz[I] = true;
    return this->_dphidz[I];
}
template <int I, int J>
inline
const std::vector<Real>& NURBS::get_d2phidz2 () const {
    this->_calculate_d2phidz2[I][J] = true;
    return this->_d2phidz2[I][J];
}
template <int I, int J, int K>
inline
const std::vector<Real>& NURBS::get_d3phidz3 () const {
    this->_calculate_d3phidz3[I][J][K] = true;
    return this->_d3phidz3[I][J][K];
}
//-------------------------------------------------------------------------------------------------
inline
void NURBS::_rationalize_phi () const {
    this->_sum__w_N = 0.0;
    for (int a=0; a<this->size(); a++) {
        this->_sum__w_N += this->_phi[a] * this->weight_indexed(a);
    }
    //
    for (int a=0; a<this->size(); a++) {
        this->_phi[a] *= this->weight_indexed(a) / this->_sum__w_N;
    }
}
inline
void NURBS::_rationalize_dphi (int L) const {
    this->_sum__w_dNdz[L] = 0.0;
    for (int a=0; a<this->size(); a++) {
        this->_sum__w_dNdz[L] += this->_dphidz[L][a] * this->weight_indexed(a);
    }
    //
    for (int a=0; a<this->size(); a++) {
        this->_dphidz[L][a] *= this->weight_indexed(a) / this->_sum__w_N;
        this->_dphidz[L][a] -= ( this->_phi[a] * this->_sum__w_dNdz[L] )
                               / this->_sum__w_N;
    }
}
inline
void NURBS::_rationalize_d2phi (int L, int M) const {
    this->_sum__w_d2Ndz2[L][M] = 0.0;
    for (int a=0; a<this->size(); a++) {
        this->_sum__w_d2Ndz2[L][M] += this->_d2phidz2[L][M][a] * this->weight_indexed(a);
    }
    for (int a=0; a<this->size(); a++) {
        this->_d2phidz2[L][M][a] *= this->weight_indexed(a) / this->_sum__w_N;
        this->_d2phidz2[L][M][a] -= ( this->_phi[a] * this->_sum__w_d2Ndz2[L][M]
                                    + this->_dphidz[L][a] * this->_sum__w_dNdz[M]
                                    + this->_dphidz[M][a] * this->_sum__w_dNdz[L] )
                                    / this->_sum__w_N;
    }
}
inline
void NURBS::_rationalize_d3phi (int L, int M, int N) const {
    this->_sum__w_d3Ndz3[L][M][N] = 0.0;
    for (int a=0; a<this->size(); a++) {
        this->_sum__w_d3Ndz3[L][M][N] += this->_d3phidz3[L][M][N][a] * this->weight_indexed(a);
    }
    for (int a=0; a<this->size(); a++) {
        this->_d3phidz3[L][M][N][a] *= this->weight_indexed(a) / this->_sum__w_N;
        this->_d3phidz3[L][M][N][a] -= ( this->_phi[a] * this->_sum__w_d3Ndz3[L][M][N]
                                       + this->_dphidz[L][a] * this->_sum__w_d2Ndz2[M][N]
                                       + this->_dphidz[M][a] * this->_sum__w_d2Ndz2[L][N]
                                       + this->_dphidz[N][a] * this->_sum__w_d2Ndz2[L][M]
                                       + this->_d2phidz2[L][M][a] * this->_sum__w_dNdz[N]
                                       + this->_d2phidz2[L][N][a] * this->_sum__w_dNdz[M]
                                       + this->_d2phidz2[M][N][a] * this->_sum__w_dNdz[L] )
                                       / this->_sum__w_N;
    }
}
//=================================================================================================
#endif //  __NURBS_INLINE_H__
//=================================================================================================

