/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __IDA_H__
#define __IDA_H__
//=================================================================================================
#include "feb3/auxiliary.h"
#include "libmesh/point.h"
#include <vector>
//=================================================================================================
class InverseDistanceAlgorithm {
public:
    explicit InverseDistanceAlgorithm (const Real c =1.0) : _is_init(false), _coeff(c), _pnt(NULL) {}
    ~InverseDistanceAlgorithm () {}
    //
    void init (const std::vector<Point>& pnt);
    void calc (const Point& apnt, std::vector<Real>& phi) const;
    void calc (const std::vector<Point>& apnt, std::vector< std::vector<Real> >& phi) const;
    template <class X>
    void interp (const Point& apnt, const std::vector<X>& in, X& out) const {
        std::vector<Real> phi;
        this->calc(apnt, phi);
        libmesh_assert( phi.size() == in.size() );
        for (unsigned int i=0; i<phi.size(); i++)
            out += phi[i] * in[i];
    }
private:
    bool _is_init;
    Real _coeff;
    const std::vector<Point>* _pnt;
};
//=================================================================================================
#endif // __IDA_H__
//=================================================================================================
