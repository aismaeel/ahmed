/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __VASCULAR_NETWORK_AUX_INLINE_H__
#define __VASCULAR_NETWORK_AUX_INLINE_H__
//=================================================================================================
inline
void enforce_collapse_nodes ( const std::set<dof_id_type>& scanned_vascular_nodes,
                              std::map<dof_id_type, VascularInfo>& node_vi ) {
    for ( std::map<dof_id_type, VascularInfo>::iterator
          map_ci=node_vi.begin(); map_ci!=node_vi.end(); map_ci++ ) {
        const dof_id_type node_id = map_ci->first;
        VascularInfo* vi = &(map_ci->second);
        //
        if ( scanned_vascular_nodes.end() == scanned_vascular_nodes.find(node_id) ) continue;
        //
        vi->status = VascularInfo::COLLAPSED;
        vi->compression_scale = 1000.0;
    }
}
//-------------------------------------------------------------------------------------------------
inline
void find_inlet_or_outlet ( const MeshBase& msh_1d,
                            const dof_id_type& this_node_id,
                            std::map<dof_id_type, VascularInfo>& node_vi,
                            std::set<dof_id_type>& scanned_vascular_nodes,
                            dof_id_type& inout_node_id ) {
    ASSERT_(node_vi.end()!=node_vi.find(this_node_id), "fatal internal error occurred");
    VascularInfo& vi = node_vi.find(this_node_id)->second;
    //
    if ( VascularInfo::INLET == vi.type || VascularInfo::OUTLET == vi.type ) {
        inout_node_id = this_node_id;
        return;
    }
    //
    if ( scanned_vascular_nodes.end() != scanned_vascular_nodes.find(this_node_id) ) return;
    scanned_vascular_nodes.insert( this_node_id );
    //
    if ( DofObject::invalid_id != inout_node_id ) return;
    //
    if ( VascularInfo::COLLAPSED == vi.status ) return;
    //
    int n_vsc_elem = 0;
    ASSERT_(!vi.vsc_elem.empty(), "fatal internal error occurred");
    for ( std::set<dof_id_type>::const_iterator
          set_ci=vi.vsc_elem.begin(); set_ci!=vi.vsc_elem.end(); set_ci++ ) {
        const Elem& elem_1d = msh_1d.elem_ref(*set_ci);
        const dof_id_type conn_node_id = edge2_other_node(elem_1d, this_node_id);
        // dof_id_type conn_node_id;
        // if      ( this_node_id == elem_1d.node_id(0) ) conn_node_id = elem_1d.node_id(1);
        // else if ( this_node_id == elem_1d.node_id(1) ) conn_node_id = elem_1d.node_id(0);
        // else libmesh_error();
        //
        ASSERT_(node_vi.end()!=node_vi.find(conn_node_id), "fatal internal error occurred");
        const VascularInfo& vi_conn = node_vi.find(conn_node_id)->second;
        //
        if ( VascularInfo::COLLAPSED == vi_conn.status ) continue;
        ++n_vsc_elem;
        //
        find_inlet_or_outlet(msh_1d, conn_node_id, node_vi, scanned_vascular_nodes, inout_node_id);
        if ( DofObject::invalid_id != inout_node_id ) return;
    }
    //
    ASSERT_(3>=n_vsc_elem, "fatal internal error occurred");
    if ( 0 == n_vsc_elem ) {
        vi.status = VascularInfo::COLLAPSED;
        vi.compression_scale = 1000.0;
    }
}
//=================================================================================================
#endif // __VASCULAR_NETWORK_AUX_INLINE_H__
//=================================================================================================
