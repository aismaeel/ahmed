/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __REACTION_DIFFUSION_SORVER_XD_H__
#define __REACTION_DIFFUSION_SORVER_XD_H__
//=================================================================================================
#include "libmesh/equation_systems.h"
#include "feb3/domain_data.h"
#include "libmesh/perf_log.h"
//=================================================================================================
class ReactionDiffusionSolverXD {
public:
    ReactionDiffusionSolverXD (DomainData& dmn);
    ~ReactionDiffusionSolverXD () {}
    //
    const std::string& get_label () const;
    const std::string get_log () const;
    const DomainData& get_domain () const;
    //
    void init ();
    void run ();
    void save_restart (std::ofstream& fout) const;
    void load_restart ();
protected:
    void _copy_params ();
    void _set_initial ();
    void _diag_matrix ();
    void _solve ();
    void _set_auxiliary ();
    void _set_quad_info ();
private:
    void _assemble_rhs ();
    void _assemble_all ();
private:
    std::string _label;
    mutable PerfLog _log;
protected:
    DomainData& _dmn;
public:
    Parameters par_list;
};
//=================================================================================================
#include "feb3/reaction_diffusion_solver_Xd_inl.h"
//=================================================================================================
#endif // __REACTION_DIFFUSION_SORVER_XD_H__
//=================================================================================================
