/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __NURBS_H__
#define __NURBS_H__
//=================================================================================================
#include "feb3/utils.h"
#include "libmesh/dof_object.h"
#include "libmesh/node.h"
//=================================================================================================
class NURBS {
public:
// constructors and destructor
    explicit
    NURBS (const dof_id_type id =DofObject::invalid_id);
    NURBS (const int ncp[SP3D], const Order ord[SP3D], const dof_id_type id =DofObject::invalid_id);
    ~NURBS ();
    //
    // basic initialisation function
    void init (const int ncp[SP3D], const Order ord[SP3D], const dof_id_type id);
    // routine to calculate various internal variables
    void reinit (const Point& crd) const;
    // print-out routine of the NURBS object
    void print_info (std::ostream& os =std::cout, bool coord_out =false, bool reduced_out = false) const;
// functions to read/write access
    dof_id_type& set_id ();
    subdomain_id_type& set_subdomain_id ();
    dof_id_type& set_processor_id ();
    // set the control point/node
    Node*& set_ctrl_node (int i, int j =0, int k =0);
    // set the weiht if NURBS is set to rational form
    Real&  set_weight    (int i, int j =0, int k =0);
    // reset the knot vector potentially to non-uniform
    void set_knot_vec (int d, const Blitz_VectorReal& kv);
    // change NURBS from rational to non-rational and vice versa
    void set_to_rational    ();
    void set_to_nonrational ();
    //
    template<int DIM>
    void unclamp_begin ();
    template<int DIM>
    void unclamp_end ();
    //
    void set_neighbor (const unsigned int s, NURBS* nurbs);
    void set_boundary_id (const unsigned int s, boundary_id_type b_id);
// functions for read-only access
    dof_id_type id () const;
    subdomain_id_type subdomain_id () const;
    dof_id_type processor_id () const;
    // dimension of NURBS object
    unsigned int dimension () const;
    // check if NURBS is rational or not
    bool is_rational () const;
    // directional order of polynomial basis
    int order (int d) const;
    // directional total number of control points
    unsigned int n_ctrl_pnt (int d) const;
    // total number of control points
    unsigned int size () const;
    // total number of unique and all knots respectively
    unsigned int n_knots   () const;
    unsigned int all_knots () const;
    // directional total number of unique and all knots respectively
    unsigned int n_knots   (int d) const;
    unsigned int all_knots (int d) const;
    // directional span
    unsigned int span (int d) const;
    // access the directional index of the control point
    template <int DIM>
    int index (int a) const;
    template <int DIM>
    Real knot (int i) const;
    // access the non-repeated knot in parametric coordinates
    Point nonrepeated_knot (int i, int j =0, int k =0) const;
    //
    const Point& ctrl_pnt (int i, int j =0, int k =0) const;
    const Point& ctrl_pnt_indexed (int a) const;
    //
    Real weight (int i, int j =0, int k =0) const;
    Real weight_indexed (int a) const;
    //
    Node* get_ctrl_node (int i, int j =0, int k =0) const;
    Node* get_ctrl_node_indexed (int a) const;
    //
    const Blitz_VectorReal& get_knot_vec (int d) const;
    void get_knot_net (blitz::Array<Point, SP3D>& net) const;
    //
    const Point&             get_xyz () const;
    const std::vector<Real>& get_phi () const;
    const Real& get_jacobian () const;
    const std::vector<Vector3D>&  get_dphi  () const;
    const std::vector<Dyadic3D>&  get_d2phi () const;
    const std::vector<Triadic3D>& get_d3phi () const;
    template <int I>
    const std::vector<Real>&   get_dphidz () const;
    template <int I, int J>
    const std::vector<Real>& get_d2phidz2 () const;
    template <int I, int J, int K>
    const std::vector<Real>& get_d3phidz3 () const;
    //
    unsigned int n_sides () const;
    UniquePtr<NURBS> build_side (const unsigned int l) const;
    //
    bool has_neighbor (const NURBS* nurbs) const;
    NURBS* neighbor (const unsigned int s) const;
    boundary_id_type boundary_id (const unsigned int s) const;
protected:
    //
    void calc_xyz (const Point& crd) const;
    void calc_phi (const Point& crd) const;
    void calc_jacobian   (const Point& crd) const;
    void calc_jacobian_2 (const Point& crd) const;
    void calc_jacobian_3 (const Point& crd) const;
    //
    void calc_dphi (const Point& crd) const;
    void calc_dphidxi   (const Point& crd) const;
    void calc_dphideta  (const Point& crd) const;
    void calc_dphidzeta (const Point& crd) const;
    //
    void calc_d2phi (const Point& crd) const;
    void calc_d2phidxidxi     (const Point& crd) const;
    void calc_d2phidxideta    (const Point& crd) const;
    void calc_d2phidxidzeta   (const Point& crd) const;
    void calc_d2phidetadxi    (const Point& crd) const;
    void calc_d2phidetadeta   (const Point& crd) const;
    void calc_d2phidetadzeta  (const Point& crd) const;
    void calc_d2phidzetadxi   (const Point& crd) const;
    void calc_d2phidzetadeta  (const Point& crd) const;
    void calc_d2phidzetadzeta (const Point& crd) const;
    //
    void calc_d3phi (const Point& crd) const;
    void calc_d3phidxidxidxi       (const Point& crd) const;
    void calc_d3phidxidxideta      (const Point& crd) const;
    void calc_d3phidxidxidzeta     (const Point& crd) const;
    void calc_d3phidxidetadxi      (const Point& crd) const;
    void calc_d3phidxidetadeta     (const Point& crd) const;
    void calc_d3phidxidetadzeta    (const Point& crd) const;
    void calc_d3phidxidzetadxi     (const Point& crd) const;
    void calc_d3phidxidzetadeta    (const Point& crd) const;
    void calc_d3phidxidzetadzeta   (const Point& crd) const;
    void calc_d3phidetadxidxi      (const Point& crd) const;
    void calc_d3phidetadxideta     (const Point& crd) const;
    void calc_d3phidetadxidzeta    (const Point& crd) const;
    void calc_d3phidetadetadxi     (const Point& crd) const;
    void calc_d3phidetadetadeta    (const Point& crd) const;
    void calc_d3phidetadetadzeta   (const Point& crd) const;
    void calc_d3phidetadzetadxi    (const Point& crd) const;
    void calc_d3phidetadzetadeta   (const Point& crd) const;
    void calc_d3phidetadzetadzeta  (const Point& crd) const;
    void calc_d3phidzetadxidxi     (const Point& crd) const;
    void calc_d3phidzetadxideta    (const Point& crd) const;
    void calc_d3phidzetadxidzeta   (const Point& crd) const;
    void calc_d3phidzetadetadxi    (const Point& crd) const;
    void calc_d3phidzetadetadeta   (const Point& crd) const;
    void calc_d3phidzetadetadzeta  (const Point& crd) const;
    void calc_d3phidzetadzetadxi   (const Point& crd) const;
    void calc_d3phidzetadzetadeta  (const Point& crd) const;
    void calc_d3phidzetadzetadzeta (const Point& crd) const;
    //
private:
    // routines to 'rationalize' the shape functions and derivatives
    void _rationalize_phi () const;
    void _rationalize_dphi  (int L) const;
    void _rationalize_d2phi (int L, int M) const;
    void _rationalize_d3phi (int L, int M, int N) const;
    // fundamental routines of basis functions and derivatives
    template <int DIM>
    Real _basef_0th_deriv (Real crd, int i, int p) const;
    template <int DIM>
    Real _basef_0th_deriv (Real crd, int i) const;
    template <int DIM>
    Real _basef_1st_deriv (Real crd, int i) const;
    template <int DIM>
    Real _basef_1st_deriv (Real crd, int i, int p) const;
    template <int DIM>
    Real _basef_2nd_deriv (Real crd, int i) const;
    template <int DIM>
    Real _basef_2nd_deriv (Real crd, int i, int p) const;
    template <int DIM>
    Real _basef_3rd_deriv (Real crd, int i) const;
    template <int DIM>
    Real _basef_3rd_deriv (Real crd, int i, int p) const;
    //
private:
    //
    static constexpr Real _tolerance = 1.0e-5;
    //
    dof_id_type _id;
    subdomain_id_type _sbd_id;
    dof_id_type _processor_id;
    unsigned int _dim;
    int _order[SP3D];
    bool _is_rational;
    bool _open_begin[SP3D], _open_end[SP3D];
    blitz::Array<Node*, SP3D> _ctrl_pnt;
    blitz::Array<Real, SP3D> _weight;
    Blitz_VectorReal _knot_vec[SP3D],
                     _nonrepeated_knot_vec[SP3D];
    std::vector< Blitz_VectorInt > _idx_table;
    NURBS** _neighbor;
    boundary_id_type* _boundary_id;
    //
protected:
    //
    mutable Point _xyz;
    mutable Real _jac_det;
    //
    mutable std::vector<Real> _phi;
    mutable std::vector<Vector3D>   _dphi;
    mutable std::vector<Dyadic3D>   _d2phi;
    mutable std::vector<Triadic3D>  _d3phi;
    mutable std::vector<Real> _dphidz[SP3D],
                              _d2phidz2[SP3D][SP3D],
                              _d3phidz3[SP3D][SP3D][SP3D];
    //
    mutable Real _sum__w_N;
    mutable Real _sum__w_dNdz[SP3D],
                 _sum__w_d2Ndz2[SP3D][SP3D],
                 _sum__w_d3Ndz3[SP3D][SP3D][SP3D];
    //
    mutable bool _calculate_dphidz[SP3D],
                 _calculate_d2phidz2[SP3D][SP3D],
                 _calculate_d3phidz3[SP3D][SP3D][SP3D];
    mutable bool _calculate_dphi,
                 _calculate_d2phi,
                 _calculate_d3phi;
    //
    mutable bool _calculate_coord;
    mutable bool _calculate_jacobian,
                 _calculate_jacobian_2,
                 _calculate_jacobian_3;
    // \partial{Cartesian coordinates} / \partial{Parametric coordinates}
    mutable blitz::Array<Real, 2> _map_dXdz;
    mutable blitz::Array<Real, 3> _map_d2Xdz2;
    mutable blitz::Array<Real, 4> _map_d3Xdz3;
    // \partial{Parametric coordinates} / \partial{Cartesian coordinates}
    mutable blitz::Array<Real, 2> _inv_map_dzdX;
    mutable blitz::Array<Real, 3> _inv_map_d2zdX2;
    mutable blitz::Array<Real, 4> _inv_map_d3zdX3;
};
//=================================================================================================
#include "feb3/nurbs_inl.h"
//=================================================================================================
#endif //  __NURBS_H__
//=================================================================================================
