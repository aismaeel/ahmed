/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __DOMAIN_DATA_INLINE_H__
#define __DOMAIN_DATA_INLINE_H__
//=================================================================================================
#include "libmesh/system.h"
#include "libmesh/mesh_base.h"
#include "libmesh/mesh_base.h"
#include "feb3/mesh_iga.h"
#include "libmesh/mesh_tools.h"
//=================================================================================================
inline
void DomainData::save_quadrature ( std::ofstream& fout,
                                   QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) const {
    libmesh_assert(fout.is_open() && fout.good());
    const Parameters& par = this->_equation_systems.parameters;
    //
    if ( par.get<bool>("FEM framework") ) {
        //
        //const System& sys = this->_equation_systems.get_system<System>(0);
        const MeshBase& msh = this->_equation_systems.get_mesh();
        //
        const MeshBase::const_element_iterator begin_el = msh.active_local_elements_begin();
        const MeshBase::const_element_iterator end_el = msh.active_local_elements_end();
        //
        const unsigned int nelem = this->elem_qi_local.size();
        fout.write((char*)&nelem, sizeof(unsigned int));
        ASSERT_(nelem == msh.n_active_local_elem(), "a fatal internal error occurred");
        //
        for (MeshBase::const_element_iterator citer_el=begin_el; citer_el!=end_el; citer_el++) {
            const Elem* elem = *citer_el;
            //
            const unsigned int elem_id = elem->id();
            fout.write((char*)&elem_id, sizeof(unsigned int));
            //
            const int elem_side = -1;
            fout.write((char*)&elem_side, sizeof(int));
            //
            libmesh_assert( this->elem_qi_local.end() != this->elem_qi_local.find(elem_id) );
            const QuadratureInfo* qpi_ptr = &(this->elem_qi_local.find(elem_id)->second);
            //
            qpi_ptr->save(fout, cfg, lvl);
        }
        //
        if ( par.get<bool>("membrane elements present") )
            WARNING_("this part of code is under development");
        //
    } else if ( par.get<bool>("IGA framework") ) {
        //
        std::map<std::string, MeshIGA>* all_meshes_iga = par.get<std::map<std::string, MeshIGA>*>("pointer to IGA parametric mesh(es)");
        //
        //const System& sys = this->_equation_systems.get_system<System>(0);
        const MeshIGA* msh_iga = &(all_meshes_iga->find(par.get<std::string>("IGA parametric mesh to use for analysis"))->second);
        const MeshBase& msh = msh_iga->get_parametric_mesh();
        //
        const MeshBase::const_element_iterator begin_el = msh.active_local_elements_begin();
        const MeshBase::const_element_iterator end_el = msh.active_local_elements_end();
        //
        const unsigned int nelem = this->elem_qi_local.size();
        fout.write((char*)&nelem, sizeof(unsigned int));
        ASSERT_(nelem == msh.n_active_local_elem(), "a fatal internal error occurred");
        //
        for (MeshBase::const_element_iterator citer_el=begin_el; citer_el!=end_el; citer_el++) {
            const Elem* elem = *citer_el;
            //
            const unsigned int elem_id = elem->id();
            fout.write((char*)&elem_id, sizeof(unsigned int));
            //
            const int elem_side = -1;
            fout.write((char*)&elem_side, sizeof(int));
            //
            libmesh_assert( this->elem_qi_local.end() != this->elem_qi_local.find(elem_id) );
            const QuadratureInfo* qpi_ptr = &(this->elem_qi_local.find(elem_id)->second);
            //
            qpi_ptr->save(fout, cfg, lvl);
        }
        //
        if ( par.get<bool>("membrane elements present") )
            WARNING_("this part of code is under development");
        //
    } else libmesh_error();
    //
    gcomm_ptr->barrier();
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData::save_quadrature ( const char* fname,
                                   QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) const {
    std::ofstream fout(fname);
    this->save_quadrature(fout, cfg, lvl);
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData::load_quadrature ( std::ifstream& fin,
                                   QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) {
    libmesh_assert(fin.is_open() && fin.good());
    const Parameters& par = this->_equation_systems.parameters;
    //
    this->elem_qi_local.clear();
    this->elem_side_qi_local.clear();
    //
    if ( par.get<bool>("FEM framework") ) {
        //
        //const System& sys = this->_equation_systems.get_system<System>(0);
        const MeshBase& msh = this->_equation_systems.get_mesh();
        //
        unsigned int nelem;
        fin.read((char*)&nelem, sizeof(unsigned int));
        //
        for (unsigned int ielem=0; ielem<nelem; ielem++) {
            //
            unsigned int elem_id;
            fin.read((char*)&elem_id, sizeof(unsigned int));
            //
            int elem_side;
            fin.read((char*)&elem_side, sizeof(int));
            ASSERT_(-1 == elem_side, "a fatal internal error occurred");
            //
            QuadratureInfo qpi;
            qpi.load(fin, cfg, lvl);
            //
            this->elem_qi_local.insert( std::make_pair(elem_id, qpi) );
        }
        //
        if ( par.get<bool>("membrane elements present") )
            WARNING_("this part of code is under development");
        //
        ASSERT_(this->elem_qi_local.size() == msh.n_active_local_elem(), "a fatal internal error occurred");
        //
    } else if ( par.get<bool>("IGA framework") ) {
        //
        std::map<std::string, MeshIGA>* all_meshes_iga = par.get<std::map<std::string, MeshIGA>*>("pointer to IGA parametric mesh(es)");
        //
        //const System& sys = this->_equation_systems.get_system<System>(0);
        const MeshIGA* msh_iga = &(all_meshes_iga->find(par.get<std::string>("IGA parametric mesh to use for analysis"))->second);
        const MeshBase& msh = msh_iga->get_parametric_mesh();
        //
        const MeshBase::const_element_iterator begin_el = msh.active_local_elements_begin();
        const MeshBase::const_element_iterator end_el = msh.active_local_elements_end();
        //
        unsigned int nelem;
        fin.read((char*)&nelem, sizeof(unsigned int));
        //
        for (unsigned int ielem=0; ielem<nelem; ielem++) {
            //
            unsigned int elem_id;
            fin.read((char*)&elem_id, sizeof(unsigned int));
            //
            int elem_side;
            fin.read((char*)&elem_side, sizeof(int));
            ASSERT_(-1 == elem_side, "a fatal internal error occurred");
            //
            QuadratureInfo qpi;
            qpi.load(fin, cfg, lvl);
            //
            this->elem_qi_local.insert( std::make_pair(elem_id, qpi) );
        }
        //
        if ( par.get<bool>("membrane elements present") )
            WARNING_("this part of code is under development");
        //
        ASSERT_(this->elem_qi_local.size() == msh.n_active_local_elem(), "a fatal internal error occurred");
        //
    } else libmesh_error();
    //
    gcomm_ptr->barrier();
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData::load_quadrature ( const char* fname,
                                   QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) {
    std::ifstream fin(fname);
    this->load_quadrature(fin, cfg, lvl);
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData::gather_quadrature ( const std::vector<std::string>& fname_ls,
                                     QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) const {
    const Parameters& par = this->_equation_systems.parameters;
    ASSERT_(libMesh::global_n_processors()==fname_ls.size(), "a fatal internal error occurred");
    //
    this->elem_qi.clear();
    this->elem_side_qi.clear();
    //
    if ( par.get<bool>("FEM framework") ) {
        //
        //const System& sys = this->_equation_systems.get_system<System>(0);
        const MeshBase& msh = this->_equation_systems.get_mesh();
        //
        for (unsigned int ifile=0; ifile<fname_ls.size(); ifile++) {
            std::ifstream fin(fname_ls[ifile].c_str());
            //
            unsigned int nelem;
            fin.read((char*)&nelem, sizeof(unsigned int));
            //
            for (unsigned int ielem=0; ielem<nelem; ielem++) {
                //
                unsigned int elem_id;
                fin.read((char*)&elem_id, sizeof(unsigned int));
                //
                int elem_side;
                fin.read((char*)&elem_side, sizeof(int));
                ASSERT_(-1 == elem_side, "a fatal internal error occurred");
                //
                QuadratureInfo qpi;
                qpi.load(fin, cfg, lvl);
                //
                this->elem_qi.insert( std::make_pair(elem_id, qpi) );
            }
            //
            if ( par.get<bool>("membrane elements present") )
                WARNING_("this part of code is under development");
            //
        }
        //
        ASSERT_(this->elem_qi.size() == msh.n_active_elem(), "a fatal internal error occurred");
        //
    } else if ( par.get<bool>("IGA framework") ) {
        //
        std::map<std::string, MeshIGA>* all_meshes_iga = par.get<std::map<std::string, MeshIGA>*>("pointer to IGA parametric mesh(es)");
        //
        //const System& sys = this->_equation_systems.get_system<System>(0);
        const MeshIGA* msh_iga = &(all_meshes_iga->find(par.get<std::string>("IGA parametric mesh to use for analysis"))->second);
        const MeshBase& msh = msh_iga->get_parametric_mesh();
        //
        const MeshBase::const_element_iterator begin_el = msh.active_local_elements_begin();
        const MeshBase::const_element_iterator end_el = msh.active_local_elements_end();
        //
        for (unsigned int ifile=0; ifile<fname_ls.size(); ifile++) {
            std::ifstream fin(fname_ls[ifile].c_str());
            //
            unsigned int nelem;
            fin.read((char*)&nelem, sizeof(unsigned int));
            //
            for (unsigned int ielem=0; ielem<nelem; ielem++) {
                //
                unsigned int elem_id;
                fin.read((char*)&elem_id, sizeof(unsigned int));
                //
                int elem_side;
                fin.read((char*)&elem_side, sizeof(int));
                ASSERT_(-1 == elem_side, "a fatal internal error occurred");
                //
                QuadratureInfo qpi;
                qpi.load(fin, cfg, lvl);
                //
                this->elem_qi.insert( std::make_pair(elem_id, qpi) );
            }
            //
            if ( par.get<bool>("membrane elements present") )
                WARNING_("this part of code is under development");
            //
        }
        //
        ASSERT_(this->elem_qi.size() == msh.n_active_elem(), "a fatal internal error occurred");
        //
    } else libmesh_error();
    //
    gcomm_ptr->barrier();
}
//=================================================================================================
inline
void DomainData::find_neighbor_elem ( const Node* n,
                                      std::vector<const Elem*>& elem_conn ) const {
    elem_conn.clear();
    elem_conn = this->_nodes_to_elem_map.at(n->id());
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData::find_neighbor_node ( const Node* n,
                                      std::vector<const Node*>& node_conn ) const {
    node_conn.clear();
    MeshTools::find_nodal_neighbors(this->get_mesh(), *n, this->_nodes_to_elem_map, node_conn);
}
//=================================================================================================
//
//
//
//=================================================================================================
inline
dof_id_type edge2_other_node (const Elem& elem_1d, const dof_id_type& this_node_id) {
    ASSERT_(libMesh::EDGE2==elem_1d.type(), "fatal internal error occurred");
    if ( this_node_id == elem_1d.node_id(0) ) return elem_1d.node_id(1);
    if ( this_node_id == elem_1d.node_id(1) ) return elem_1d.node_id(0);
    // return DofObject::invalid_id;
    libmesh_error();
}
//-------------------------------------------------------------------------------------------------
inline
void recursive_vascular_node_find ( const MeshBase& msh_1d, const dof_id_type this_node_id,
                                    std::map< dof_id_type, VascularInfo >& node_vi,
                                    const std::map< dof_id_type, std::set<dof_id_type> >& node2elem1d,
                                          std::map< dof_id_type, std::set<dof_id_type> >& branch2node,
                                    std::set<dof_id_type>& nodes_scanned,
                                    unsigned int& total_number_of_branches ) {
    // check first if this node has been already scanned
    if ( nodes_scanned.end() != nodes_scanned.find(this_node_id) ) return;
    nodes_scanned.insert(this_node_id);

    VascularInfo* this_node_vinfo = &(node_vi.find(this_node_id)->second);
    if ( NULL == this_node_vinfo ) libmesh_error();
    const std::set<dof_id_type>& elem_ls = node2elem1d.find(this_node_id)->second;

    if ( elem_ls.size() == 1 && VascularInfo::INLET == this_node_vinfo->type ) {
        const Elem& elem0 = msh_1d.elem_ref(*(elem_ls.begin()));
        const dof_id_type other_node0_id = edge2_other_node(elem0, this_node_id);

        this_node_vinfo->branch_id = total_number_of_branches;

        std::map< dof_id_type, std::set<dof_id_type> >::iterator iter = branch2node.find(this_node_vinfo->branch_id);
        if ( branch2node.end() == iter ) {
            std::set<dof_id_type> node_list__tmp;
            node_list__tmp.insert( this_node_id );
            branch2node.insert( std::make_pair(this_node_vinfo->branch_id, node_list__tmp) );
        } else {
            iter->second.insert( this_node_id );
        }

        recursive_vascular_node_find(msh_1d, other_node0_id, node_vi, node2elem1d, branch2node, nodes_scanned, total_number_of_branches);

    } else if ( elem_ls.size() == 1 && VascularInfo::OUTLET == this_node_vinfo->type ) {
        const Elem& elem0 = msh_1d.elem_ref(*(elem_ls.begin()));
        const dof_id_type other_node0_id = edge2_other_node(elem0, this_node_id);

        this_node_vinfo->branch_id = node_vi.find( other_node0_id )->second.branch_id;

        std::map< dof_id_type, std::set<dof_id_type> >::iterator iter = branch2node.find(this_node_vinfo->branch_id);
        if ( branch2node.end() == iter ) {
            std::set<dof_id_type> node_list__tmp;
            node_list__tmp.insert( this_node_id );
            branch2node.insert( std::make_pair(this_node_vinfo->branch_id, node_list__tmp) );
        } else {
            iter->second.insert( this_node_id );
        }

        recursive_vascular_node_find(msh_1d, other_node0_id, node_vi, node2elem1d, branch2node, nodes_scanned, total_number_of_branches);

    } else if ( elem_ls.size() == 2 && (VascularInfo::SPROUT == this_node_vinfo->type || VascularInfo::MATURE == this_node_vinfo->type) ) {
        const Elem& elem0 = msh_1d.elem_ref(*(  elem_ls.begin()));
        const Elem& elem1 = msh_1d.elem_ref(*(++elem_ls.begin()));
        const dof_id_type other_node0_id = edge2_other_node(elem0, this_node_id);
        const dof_id_type other_node1_id = edge2_other_node(elem1, this_node_id);

        this_node_vinfo->branch_id = ( node_vi.find( other_node0_id )->second.branch_id
                                     + node_vi.find( other_node1_id )->second.branch_id ) / 2;

        std::map< dof_id_type, std::set<dof_id_type> >::iterator iter = branch2node.find(this_node_vinfo->branch_id);
        if ( branch2node.end() == iter ) {
            std::set<dof_id_type> node_list__tmp;
            node_list__tmp.insert( this_node_id );
            branch2node.insert( std::make_pair(this_node_vinfo->branch_id, node_list__tmp) );
        } else {
            iter->second.insert( this_node_id );
        }

        recursive_vascular_node_find(msh_1d, other_node0_id, node_vi, node2elem1d, branch2node, nodes_scanned, total_number_of_branches);
        recursive_vascular_node_find(msh_1d, other_node1_id, node_vi, node2elem1d, branch2node, nodes_scanned, total_number_of_branches);

    } else if ( elem_ls.size() == 3 && VascularInfo::BRANCH == this_node_vinfo->type ) {
        const Elem& elem0 = msh_1d.elem_ref(*(      elem_ls.begin()));
        const Elem& elem1 = msh_1d.elem_ref(*(    ++elem_ls.begin()));
        const Elem& elem2 = msh_1d.elem_ref(*(++(++elem_ls.begin())));
        const dof_id_type other_node0_id = edge2_other_node(elem0, this_node_id);
        const dof_id_type other_node1_id = edge2_other_node(elem1, this_node_id);
        const dof_id_type other_node2_id = edge2_other_node(elem2, this_node_id);

        this_node_vinfo->branch_id = ( node_vi.find( other_node0_id )->second.branch_id
                                     + node_vi.find( other_node1_id )->second.branch_id
                                     + node_vi.find( other_node2_id )->second.branch_id ) / 3;

        std::map< dof_id_type, std::set<dof_id_type> >::iterator iter = branch2node.find(this_node_vinfo->branch_id);
        if ( branch2node.end() == iter ) {
            std::set<dof_id_type> node_list__tmp;
            node_list__tmp.insert( this_node_id );
            branch2node.insert( std::make_pair(this_node_vinfo->branch_id, node_list__tmp) );
        } else {
            iter->second.insert( this_node_id );
        }

        recursive_vascular_node_find(msh_1d, other_node0_id, node_vi, node2elem1d, branch2node, nodes_scanned, total_number_of_branches);
        recursive_vascular_node_find(msh_1d, other_node1_id, node_vi, node2elem1d, branch2node, nodes_scanned, total_number_of_branches);
        recursive_vascular_node_find(msh_1d, other_node2_id, node_vi, node2elem1d, branch2node, nodes_scanned, total_number_of_branches);

    } else libmesh_error();
}
//=================================================================================================
inline
void DomainData_VascularNetwork::save_vasculature (std::ofstream& fout) const {
    const Parameters& par = this->get_equation_systems().parameters;
    //
    const unsigned int n_branches = par.get<unsigned int>("total number of branches");
    fout.write((char*)&n_branches, sizeof(unsigned int));
    //
    const unsigned int n_nodes = this->node_vi.size();
    fout.write((char*)&n_nodes, sizeof(unsigned int));
    //
    for ( std::map<dof_id_type, VascularInfo>::const_iterator
          citer=this->node_vi.begin(); citer!=this->node_vi.end(); citer++ ) {
        const dof_id_type node_id = citer->first;
        fout.write((char*)&node_id, sizeof(dof_id_type));
        //
        const VascularInfo* vi_ptr = &(citer->second);
        //
        fout.write((char*)&vi_ptr->level, sizeof(int));
        const int flow = static_cast<int>(vi_ptr->flow);
        fout.write((char*)&flow, sizeof(int));
        const int type = static_cast<int>(vi_ptr->type);
        fout.write((char*)&type, sizeof(int));
        const int status = static_cast<int>(vi_ptr->status);
        fout.write((char*)&status, sizeof(int));
        //
        fout.write((char*)&vi_ptr->time_generate, sizeof(Real));
        fout.write((char*)&vi_ptr->time_branch, sizeof(Real));
        fout.write((char*)&vi_ptr->time_mature, sizeof(Real));
        fout.write((char*)&vi_ptr->length, sizeof(Real));
        fout.write((char*)&vi_ptr->total_length, sizeof(Real));
        fout.write((char*)&vi_ptr->radius, sizeof(Real));
        fout.write((char*)&vi_ptr->thickness, sizeof(Real));
        fout.write((char*)&vi_ptr->pore_radius, sizeof(Real));
        fout.write((char*)&vi_ptr->pores_fraction, sizeof(Real));
        fout.write((char*)&vi_ptr->elem3d_id, sizeof(dof_id_type));
        fout.write((char*)&vi_ptr->branch_id, sizeof(unsigned int));
        fout.write((char*)&vi_ptr->parent_branch_id, sizeof(unsigned int));
        //
        const unsigned int n_vsc_elem = vi_ptr->vsc_elem.size();
        fout.write((char*)&n_vsc_elem, sizeof(unsigned int));
        for ( std::set<dof_id_type>::const_iterator
              c=vi_ptr->vsc_elem.begin(); c!=vi_ptr->vsc_elem.end(); c++ ) {
            const dof_id_type conn_node = (*c);
            fout.write((char*)&conn_node, sizeof(dof_id_type));
        }
        //
        fout.write((char*)&vi_ptr->press_ratio, sizeof(Real));
        fout.write((char*)&vi_ptr->press_ratio_cr, sizeof(Real));
        fout.write((char*)&vi_ptr->compression_scale, sizeof(Real));
    }
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData_VascularNetwork::save_vasculature (const char* fname) const {
    std::ofstream fout(fname);
    this->save_vasculature(fout);
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData_VascularNetwork::load_vasculature (std::ifstream& fin) {
    Parameters& par = this->get_equation_systems().parameters;
    //
    unsigned int n_branches;
    fin.read((char*)&n_branches, sizeof(unsigned int));
    par.set<unsigned int>("total number of branches") = n_branches;
    //
    unsigned int n_nodes;
    fin.read((char*)&n_nodes, sizeof(unsigned int));
    //
    for (unsigned int inode=0; inode<n_nodes; inode++) {
        dof_id_type node_id;
        fin.read((char*)&node_id, sizeof(dof_id_type));
        //
        libmesh_assert(this->node_vi.end() != this->node_vi.find(node_id));
        VascularInfo* vi_ptr = &(this->node_vi.find(node_id)->second);
        //
        fin.read((char*)&vi_ptr->level, sizeof(int));
        int flow;
        fin.read((char*)&flow, sizeof(int));
        vi_ptr->flow = static_cast<VascularInfo::Flow>(flow);
        int type;
        fin.read((char*)&type, sizeof(int));
        vi_ptr->type = static_cast<VascularInfo::Type>(type);
        int status;
        fin.read((char*)&status, sizeof(int));
        vi_ptr->status = static_cast<VascularInfo::Status>(status);
        //
        fin.read((char*)&vi_ptr->time_generate, sizeof(Real));
        fin.read((char*)&vi_ptr->time_branch, sizeof(Real));
        fin.read((char*)&vi_ptr->time_mature, sizeof(Real));
        fin.read((char*)&vi_ptr->length, sizeof(Real));
        fin.read((char*)&vi_ptr->total_length, sizeof(Real));
        fin.read((char*)&vi_ptr->radius, sizeof(Real));
        fin.read((char*)&vi_ptr->thickness, sizeof(Real));
        fin.read((char*)&vi_ptr->pore_radius, sizeof(Real));
        fin.read((char*)&vi_ptr->pores_fraction, sizeof(Real));
        fin.read((char*)&vi_ptr->elem3d_id, sizeof(dof_id_type));
        fin.read((char*)&vi_ptr->branch_id, sizeof(unsigned int));
        fin.read((char*)&vi_ptr->parent_branch_id, sizeof(unsigned int));
        //
        unsigned int n_vsc_elem;
        fin.read((char*)&n_vsc_elem, sizeof(unsigned int));
        vi_ptr->vsc_elem.clear();
        for (unsigned int i=0; i<n_vsc_elem; i++) {
            dof_id_type vsc_elem;
            fin.read((char*)&vsc_elem, sizeof(dof_id_type));
            vi_ptr->vsc_elem.insert(vsc_elem);
        }
        //
        fin.read((char*)&vi_ptr->press_ratio, sizeof(Real));
        fin.read((char*)&vi_ptr->press_ratio_cr, sizeof(Real));
        fin.read((char*)&vi_ptr->compression_scale, sizeof(Real));
    }
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData_VascularNetwork::load_vasculature (const char* fname) {
    std::ifstream fin(fname);
    this->load_vasculature(fin);
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData_VascularNetwork::save_all (const char* fname) const {
    std::ofstream fout(fname);
    //
    const Parameters& par = this->get_equation_systems().parameters;
    //
    const unsigned int n_in_nodes = this->node_inlet.size();
    fout.write((char*)&n_in_nodes, sizeof(unsigned int));
    //
    for ( std::set<dof_id_type>::const_iterator
          ci=this->node_inlet.begin(); ci!=this->node_inlet.end(); ci++ ) {
        const dof_id_type node_id = *ci;
        fout.write((char*)&node_id, sizeof(dof_id_type));
    }
    //
    const unsigned int n_out_nodes = this->node_outlet.size();
    fout.write((char*)&n_out_nodes, sizeof(unsigned int));
    //
    for ( std::set<dof_id_type>::const_iterator
          ci=this->node_outlet.begin(); ci!=this->node_outlet.end(); ci++ ) {
        const dof_id_type node_id = *ci;
        fout.write((char*)&node_id, sizeof(dof_id_type));
    }
    //
    const unsigned int n_elem3d = this->elem3d__node.size();
    fout.write((char*)&n_elem3d, sizeof(unsigned int));
    //
    for ( std::map<dof_id_type, std::set<dof_id_type> >::const_iterator
          ci=this->elem3d__node.begin(); ci!=this->elem3d__node.end(); ci++ ) {
        const dof_id_type elem3d_id = ci->first;
        fout.write((char*)&elem3d_id, sizeof(dof_id_type));
        //
        const unsigned int n_elem3d_nodes = ci->second.size();
        fout.write((char*)&n_elem3d_nodes, sizeof(unsigned int));
        //
        for ( std::set<dof_id_type>::const_iterator
              cj=ci->second.begin(); cj!=ci->second.end(); cj++ ) {
            const dof_id_type node_id = *cj;
            fout.write((char*)&node_id, sizeof(dof_id_type));
        }
    }
    //
    const unsigned int n_nodes = this->node_vi.size();
    fout.write((char*)&n_nodes, sizeof(unsigned int));
    //
    for ( std::map<dof_id_type, VascularInfo>::const_iterator
          ci=this->node_vi.begin(); ci!=this->node_vi.end(); ci++ ) {
        const dof_id_type node_id = ci->first;
        fout.write((char*)&node_id, sizeof(dof_id_type));
        //
        const VascularInfo& vi = ci->second;
        //
        fout.write((char*)&vi.level, sizeof(int));
        const int flow = static_cast<int>(vi.flow);
        fout.write((char*)&flow, sizeof(int));
        const int type = static_cast<int>(vi.type);
        fout.write((char*)&type, sizeof(int));
        const int status = static_cast<int>(vi.status);
        fout.write((char*)&status, sizeof(int));
        //
        fout.write((char*)&vi.time_generate, sizeof(Real));
        fout.write((char*)&vi.time_branch, sizeof(Real));
        fout.write((char*)&vi.time_mature, sizeof(Real));
        fout.write((char*)&vi.length, sizeof(Real));
        fout.write((char*)&vi.total_length, sizeof(Real));
        fout.write((char*)&vi.radius, sizeof(Real));
        fout.write((char*)&vi.thickness, sizeof(Real));
        fout.write((char*)&vi.pore_radius, sizeof(Real));
        fout.write((char*)&vi.pores_fraction, sizeof(Real));
        fout.write((char*)&vi.elem3d_id, sizeof(dof_id_type));
        fout.write((char*)&vi.branch_id, sizeof(unsigned int));
        fout.write((char*)&vi.parent_branch_id, sizeof(unsigned int));
        //
        const unsigned int n_vsc_elem = vi.vsc_elem.size();
        fout.write((char*)&n_vsc_elem, sizeof(unsigned int));
        //
        for ( std::set<dof_id_type>::const_iterator
              cj=vi.vsc_elem.begin(); cj!=vi.vsc_elem.end(); cj++ ) {
            const dof_id_type vsc_elem = *cj;
            fout.write((char*)&vsc_elem, sizeof(dof_id_type));
        }
        //
        fout.write((char*)&vi.press_ratio, sizeof(Real));
        fout.write((char*)&vi.press_ratio_cr, sizeof(Real));
        fout.write((char*)&vi.compression_scale, sizeof(Real));
    }
    //
    const unsigned int n_branches = par.get<unsigned int>("total number of branches");
    fout.write((char*)&n_branches, sizeof(unsigned int));
    //
    const unsigned int n_branch = this->branch__node.size();
    fout.write((char*)&n_branch, sizeof(unsigned int));
    //
    for ( std::map<dof_id_type, std::set<dof_id_type> >::const_iterator
          ci=this->branch__node.begin(); ci!=this->branch__node.end(); ci++ ) {
        const dof_id_type branch_id = ci->first;
        fout.write((char*)&branch_id, sizeof(dof_id_type));
        //
        const unsigned int n_branch_nodes = ci->second.size();
        fout.write((char*)&n_branch_nodes, sizeof(unsigned int));
        //
        for ( std::set<dof_id_type>::const_iterator
              cj=ci->second.begin(); cj!=ci->second.end(); cj++ ) {
            const dof_id_type node_id = *cj;
            fout.write((char*)&node_id, sizeof(dof_id_type));
        }
    }
    //
    ASSERT_(n_branches==n_branch, "fatal internal error occurred");
}
//-------------------------------------------------------------------------------------------------
inline
void DomainData_VascularNetwork::load_all (const char* fname) {
    std::ifstream fin(fname);
    //
    Parameters& par = this->get_equation_systems().parameters;
    //
    this->node_inlet.clear();
    this->node_outlet.clear();
    this->elem3d__node.clear();
    this->node_vi.clear();
    this->branch__node.clear();
    //
    unsigned int n_in_nodes;
    fin.read((char*)&n_in_nodes, sizeof(unsigned int));
    //
    for (unsigned int l=0; l<n_in_nodes; l++) {
        dof_id_type node_id;
        fin.read((char*)&node_id, sizeof(dof_id_type));
        //
        this->node_inlet.insert(node_id);
    }
    //
    unsigned int n_out_nodes;
    fin.read((char*)&n_out_nodes, sizeof(unsigned int));
    //
    for (unsigned int l=0; l<n_out_nodes; l++) {
        dof_id_type node_id;
        fin.read((char*)&node_id, sizeof(dof_id_type));
        //
        this->node_outlet.insert(node_id);
    }
    //
    unsigned int n_elem3d;
    fin.read((char*)&n_elem3d, sizeof(unsigned int));
    //
    for (unsigned int l=0; l<n_elem3d; l++) {
        dof_id_type elem3d_id;
        fin.read((char*)&elem3d_id, sizeof(dof_id_type));
        //
        unsigned int n_elem3d_nodes;
        fin.read((char*)&n_elem3d_nodes, sizeof(unsigned int));
        //
        std::set<dof_id_type> node_set;
        for (unsigned int n=0; n<n_elem3d_nodes; n++) {
            dof_id_type node_id;
            fin.read((char*)&node_id, sizeof(dof_id_type));
            //
            node_set.insert(node_id);
        }
        //
        this->elem3d__node.insert( std::make_pair(elem3d_id, node_set) );
    }
    //
    unsigned int n_nodes;
    fin.read((char*)&n_nodes, sizeof(unsigned int));
    //
    for (unsigned int i=0; i<n_nodes; i++) {
        dof_id_type node_id;
        fin.read((char*)&node_id, sizeof(dof_id_type));
        //
        VascularInfo vi;
        //
        fin.read((char*)&vi.level, sizeof(int));
        int flow;
        fin.read((char*)&flow, sizeof(int));
        vi.flow = static_cast<VascularInfo::Flow>(flow);
        int type;
        fin.read((char*)&type, sizeof(int));
        vi.type = static_cast<VascularInfo::Type>(type);
        int status;
        fin.read((char*)&status, sizeof(int));
        vi.status = static_cast<VascularInfo::Status>(status);
        //
        fin.read((char*)&vi.time_generate, sizeof(Real));
        fin.read((char*)&vi.time_branch, sizeof(Real));
        fin.read((char*)&vi.time_mature, sizeof(Real));
        fin.read((char*)&vi.length, sizeof(Real));
        fin.read((char*)&vi.total_length, sizeof(Real));
        fin.read((char*)&vi.radius, sizeof(Real));
        fin.read((char*)&vi.thickness, sizeof(Real));
        fin.read((char*)&vi.pore_radius, sizeof(Real));
        fin.read((char*)&vi.pores_fraction, sizeof(Real));
        fin.read((char*)&vi.elem3d_id, sizeof(dof_id_type));
        fin.read((char*)&vi.branch_id, sizeof(unsigned int));
        fin.read((char*)&vi.parent_branch_id, sizeof(unsigned int));
        //
        unsigned int n_vsc_elem;
        fin.read((char*)&n_vsc_elem, sizeof(unsigned int));
        vi.vsc_elem.clear();
        for (unsigned int n=0; n<n_vsc_elem; n++) {
            dof_id_type vsc_elem;
            fin.read((char*)&vsc_elem, sizeof(dof_id_type));
            vi.vsc_elem.insert(vsc_elem);
        }
        //
        fin.read((char*)&vi.press_ratio, sizeof(Real));
        fin.read((char*)&vi.press_ratio_cr, sizeof(Real));
        fin.read((char*)&vi.compression_scale, sizeof(Real));
        //
        this->node_vi.insert( std::make_pair(node_id, vi) );
    }
    //
    unsigned int n_branches;
    fin.read((char*)&n_branches, sizeof(unsigned int));
    par.set<unsigned int>("total number of branches") = n_branches;
    //
    unsigned int n_branch;
    fin.read((char*)&n_branch, sizeof(unsigned int));
    //
    for (unsigned int l=0; l<n_branch; l++) {
        dof_id_type branch_id;
        fin.read((char*)&branch_id, sizeof(dof_id_type));
        //
        unsigned int n_branch_nodes;
        fin.read((char*)&n_branch_nodes, sizeof(unsigned int));
        //
        std::set<dof_id_type> node_set;
        for (unsigned int n=0; n<n_branch_nodes; n++) {
            dof_id_type node_id;
            fin.read((char*)&node_id, sizeof(dof_id_type));
            //
            node_set.insert(node_id);
        }
        //
        this->branch__node.insert( std::make_pair(branch_id, node_set) );
    }
    //
    ASSERT_(n_branches==n_branch, "fatal internal error occurred");
}
//=================================================================================================
#endif // __DOMAIN_DATA_INLINE_H__
//=================================================================================================
