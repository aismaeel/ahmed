/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __DRUG_DELIVERY_SOLVER_H__
#define __DRUG_DELIVERY_SOLVER_H__
//=================================================================================================
#include "feb3/domain_data.h"
#include "libmesh/getpot.h"
//=================================================================================================
class DrugDeliverySolver {
public:
    DrugDeliverySolver (DomainData& dmn, DomainData_VascularNetwork& dmn_vn);
    ~DrugDeliverySolver () {}
    //
    const std::string& get_label () const;
    const std::string get_log () const;
    //
    void init ();
    void set_initial ();
    void injection ();
    void delivery ();
protected:
    void _copy_params ();
private:
    std::string _label;
    mutable PerfLog _log;
protected:
    DomainData& _dmn;
    DomainData_VascularNetwork& _dmn_vn;
public:
    Parameters par_list;
};
//=================================================================================================
#include "feb3/drug_delivery_solver_inl.h"
//=================================================================================================
#endif // __DRUG_DELIVERY_SOLVER_H__
//=================================================================================================
