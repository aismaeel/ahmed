/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __NAVIER_CAUCHY_SOLVER_3D_AUX_INLINE_H__
#define __NAVIER_CAUCHY_SOLVER_3D_AUX_INLINE_H__
//=================================================================================================
#include "feb3/utils.h"
//=================================================================================================
inline
Dyadic3D calc_stiffness_forward ( const RealGradient& dNa,
                                  const Quindic3D& Dtsm_uu,
                                  const Dyadic3D& S2pk,
                                  const RealGradient& dNb,
                                  const Dyadic3D& F ) {
    const Real dNa_dX = dNa(0),
               dNa_dY = dNa(1),
               dNa_dZ = dNa(2);
    const DyadicVoigt3D DUU = compress_constitutive_tensor(Dtsm_uu);
    const Real dNb_dX = dNb(0),
               dNb_dY = dNb(1),
               dNb_dZ = dNb(2);
    const Real K_uu_geo = dNb_dX*(S2pk(0,0)*dNa_dX+S2pk(1,0)*dNa_dY+S2pk(2,0)*dNa_dZ)
                        + dNb_dY*(S2pk(0,1)*dNa_dX+S2pk(1,1)*dNa_dY+S2pk(2,1)*dNa_dZ)
                        + dNb_dZ*(S2pk(0,2)*dNa_dX+S2pk(1,2)*dNa_dY+S2pk(2,2)*dNa_dZ);
    Dyadic3D K_uu;
    K_uu.comp(0,0) = (F(0,1)*dNb_dZ+F(0,2)*dNb_dY)*(DUU(5,3)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,3)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,3)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,3)*F(0,0)*dNa_dX+DUU(1,3)*F(0,1)*dNa_dY+DUU(2,3)*F(0,2)*dNa_dZ)
                    +(F(0,0)*dNb_dZ+F(0,2)*dNb_dX)*(DUU(5,4)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,4)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,4)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,4)*F(0,0)*dNa_dX+DUU(1,4)*F(0,1)*dNa_dY+DUU(2,4)*F(0,2)*dNa_dZ)
                    +(F(0,0)*dNb_dY+F(0,1)*dNb_dX)*(DUU(5,5)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,5)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,5)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,5)*F(0,0)*dNa_dX+DUU(1,5)*F(0,1)*dNa_dY+DUU(2,5)*F(0,2)*dNa_dZ)
                    +F(0,0)*dNb_dX*(DUU(5,0)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,0)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,0)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,0)*F(0,0)*dNa_dX+DUU(1,0)*F(0,1)*dNa_dY+DUU(2,0)*F(0,2)*dNa_dZ)
                    +F(0,1)*dNb_dY*(DUU(5,1)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,1)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,1)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,1)*F(0,0)*dNa_dX+DUU(1,1)*F(0,1)*dNa_dY+DUU(2,1)*F(0,2)*dNa_dZ)
                    +F(0,2)*dNb_dZ*(DUU(5,2)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,2)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,2)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,2)*F(0,0)*dNa_dX+DUU(1,2)*F(0,1)*dNa_dY+DUU(2,2)*F(0,2)*dNa_dZ)
                   + K_uu_geo;
    K_uu.comp(0,1) = (F(1,1)*dNb_dZ+F(1,2)*dNb_dY)*(DUU(5,3)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,3)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,3)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,3)*F(0,0)*dNa_dX+DUU(1,3)*F(0,1)*dNa_dY+DUU(2,3)*F(0,2)*dNa_dZ)
                    +(F(1,0)*dNb_dZ+F(1,2)*dNb_dX)*(DUU(5,4)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,4)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,4)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,4)*F(0,0)*dNa_dX+DUU(1,4)*F(0,1)*dNa_dY+DUU(2,4)*F(0,2)*dNa_dZ)
                    +(F(1,0)*dNb_dY+F(1,1)*dNb_dX)*(DUU(5,5)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,5)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,5)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,5)*F(0,0)*dNa_dX+DUU(1,5)*F(0,1)*dNa_dY+DUU(2,5)*F(0,2)*dNa_dZ)
                    +F(1,0)*dNb_dX*(DUU(5,0)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,0)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,0)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,0)*F(0,0)*dNa_dX+DUU(1,0)*F(0,1)*dNa_dY+DUU(2,0)*F(0,2)*dNa_dZ)
                    +F(1,1)*dNb_dY*(DUU(5,1)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,1)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,1)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,1)*F(0,0)*dNa_dX+DUU(1,1)*F(0,1)*dNa_dY+DUU(2,1)*F(0,2)*dNa_dZ)
                    +F(1,2)*dNb_dZ*(DUU(5,2)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,2)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,2)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,2)*F(0,0)*dNa_dX+DUU(1,2)*F(0,1)*dNa_dY+DUU(2,2)*F(0,2)*dNa_dZ);
    K_uu.comp(0,2) = (F(2,1)*dNb_dZ+F(2,2)*dNb_dY)*(DUU(5,3)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,3)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,3)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,3)*F(0,0)*dNa_dX+DUU(1,3)*F(0,1)*dNa_dY+DUU(2,3)*F(0,2)*dNa_dZ)
                    +(F(2,0)*dNb_dZ+F(2,2)*dNb_dX)*(DUU(5,4)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,4)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,4)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,4)*F(0,0)*dNa_dX+DUU(1,4)*F(0,1)*dNa_dY+DUU(2,4)*F(0,2)*dNa_dZ)
                    +(F(2,0)*dNb_dY+F(2,1)*dNb_dX)*(DUU(5,5)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,5)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,5)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,5)*F(0,0)*dNa_dX+DUU(1,5)*F(0,1)*dNa_dY+DUU(2,5)*F(0,2)*dNa_dZ)
                    +F(2,0)*dNb_dX*(DUU(5,0)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,0)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,0)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,0)*F(0,0)*dNa_dX+DUU(1,0)*F(0,1)*dNa_dY+DUU(2,0)*F(0,2)*dNa_dZ)
                    +F(2,1)*dNb_dY*(DUU(5,1)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,1)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,1)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,1)*F(0,0)*dNa_dX+DUU(1,1)*F(0,1)*dNa_dY+DUU(2,1)*F(0,2)*dNa_dZ)
                    +F(2,2)*dNb_dZ*(DUU(5,2)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUU(4,2)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUU(3,2)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUU(0,2)*F(0,0)*dNa_dX+DUU(1,2)*F(0,1)*dNa_dY+DUU(2,2)*F(0,2)*dNa_dZ);
    K_uu.comp(1,0) = (F(0,1)*dNb_dZ+F(0,2)*dNb_dY)*(DUU(5,3)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,3)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,3)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,3)*F(1,0)*dNa_dX+DUU(1,3)*F(1,1)*dNa_dY+DUU(2,3)*F(1,2)*dNa_dZ)
                    +(F(0,0)*dNb_dZ+F(0,2)*dNb_dX)*(DUU(5,4)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,4)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,4)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,4)*F(1,0)*dNa_dX+DUU(1,4)*F(1,1)*dNa_dY+DUU(2,4)*F(1,2)*dNa_dZ)
                    +(F(0,0)*dNb_dY+F(0,1)*dNb_dX)*(DUU(5,5)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,5)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,5)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,5)*F(1,0)*dNa_dX+DUU(1,5)*F(1,1)*dNa_dY+DUU(2,5)*F(1,2)*dNa_dZ)
                    +F(0,0)*dNb_dX*(DUU(5,0)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,0)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,0)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,0)*F(1,0)*dNa_dX+DUU(1,0)*F(1,1)*dNa_dY+DUU(2,0)*F(1,2)*dNa_dZ)
                    +F(0,1)*dNb_dY*(DUU(5,1)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,1)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,1)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,1)*F(1,0)*dNa_dX+DUU(1,1)*F(1,1)*dNa_dY+DUU(2,1)*F(1,2)*dNa_dZ)
                    +F(0,2)*dNb_dZ*(DUU(5,2)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,2)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,2)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,2)*F(1,0)*dNa_dX+DUU(1,2)*F(1,1)*dNa_dY+DUU(2,2)*F(1,2)*dNa_dZ);
    K_uu.comp(1,1) = (F(1,1)*dNb_dZ+F(1,2)*dNb_dY)*(DUU(5,3)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,3)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,3)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,3)*F(1,0)*dNa_dX+DUU(1,3)*F(1,1)*dNa_dY+DUU(2,3)*F(1,2)*dNa_dZ)
                    +(F(1,0)*dNb_dZ+F(1,2)*dNb_dX)*(DUU(5,4)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,4)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,4)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,4)*F(1,0)*dNa_dX+DUU(1,4)*F(1,1)*dNa_dY+DUU(2,4)*F(1,2)*dNa_dZ)
                    +(F(1,0)*dNb_dY+F(1,1)*dNb_dX)*(DUU(5,5)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,5)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,5)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,5)*F(1,0)*dNa_dX+DUU(1,5)*F(1,1)*dNa_dY+DUU(2,5)*F(1,2)*dNa_dZ)
                    +F(1,0)*dNb_dX*(DUU(5,0)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,0)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,0)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,0)*F(1,0)*dNa_dX+DUU(1,0)*F(1,1)*dNa_dY+DUU(2,0)*F(1,2)*dNa_dZ)
                    +F(1,1)*dNb_dY*(DUU(5,1)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,1)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,1)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,1)*F(1,0)*dNa_dX+DUU(1,1)*F(1,1)*dNa_dY+DUU(2,1)*F(1,2)*dNa_dZ)
                    +F(1,2)*dNb_dZ*(DUU(5,2)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,2)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,2)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,2)*F(1,0)*dNa_dX+DUU(1,2)*F(1,1)*dNa_dY+DUU(2,2)*F(1,2)*dNa_dZ)
                   + K_uu_geo;
    K_uu.comp(1,2) = (F(2,1)*dNb_dZ+F(2,2)*dNb_dY)*(DUU(5,3)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,3)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,3)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,3)*F(1,0)*dNa_dX+DUU(1,3)*F(1,1)*dNa_dY+DUU(2,3)*F(1,2)*dNa_dZ)
                    +(F(2,0)*dNb_dZ+F(2,2)*dNb_dX)*(DUU(5,4)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,4)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,4)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,4)*F(1,0)*dNa_dX+DUU(1,4)*F(1,1)*dNa_dY+DUU(2,4)*F(1,2)*dNa_dZ)
                    +(F(2,0)*dNb_dY+F(2,1)*dNb_dX)*(DUU(5,5)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,5)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,5)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,5)*F(1,0)*dNa_dX+DUU(1,5)*F(1,1)*dNa_dY+DUU(2,5)*F(1,2)*dNa_dZ)
                    +F(2,0)*dNb_dX*(DUU(5,0)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,0)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,0)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,0)*F(1,0)*dNa_dX+DUU(1,0)*F(1,1)*dNa_dY+DUU(2,0)*F(1,2)*dNa_dZ)
                    +F(2,1)*dNb_dY*(DUU(5,1)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,1)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,1)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,1)*F(1,0)*dNa_dX+DUU(1,1)*F(1,1)*dNa_dY+DUU(2,1)*F(1,2)*dNa_dZ)
                    +F(2,2)*dNb_dZ*(DUU(5,2)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUU(4,2)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUU(3,2)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUU(0,2)*F(1,0)*dNa_dX+DUU(1,2)*F(1,1)*dNa_dY+DUU(2,2)*F(1,2)*dNa_dZ);
    K_uu.comp(2,0) = (F(0,1)*dNb_dZ+F(0,2)*dNb_dY)*(DUU(5,3)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,3)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,3)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,3)*F(2,0)*dNa_dX+DUU(1,3)*F(2,1)*dNa_dY+DUU(2,3)*F(2,2)*dNa_dZ)
                    +(F(0,0)*dNb_dZ+F(0,2)*dNb_dX)*(DUU(5,4)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,4)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,4)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,4)*F(2,0)*dNa_dX+DUU(1,4)*F(2,1)*dNa_dY+DUU(2,4)*F(2,2)*dNa_dZ)
                    +(F(0,0)*dNb_dY+F(0,1)*dNb_dX)*(DUU(5,5)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,5)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,5)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,5)*F(2,0)*dNa_dX+DUU(1,5)*F(2,1)*dNa_dY+DUU(2,5)*F(2,2)*dNa_dZ)
                    +F(0,0)*dNb_dX*(DUU(5,0)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,0)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,0)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,0)*F(2,0)*dNa_dX+DUU(1,0)*F(2,1)*dNa_dY+DUU(2,0)*F(2,2)*dNa_dZ)
                    +F(0,1)*dNb_dY*(DUU(5,1)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,1)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,1)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,1)*F(2,0)*dNa_dX+DUU(1,1)*F(2,1)*dNa_dY+DUU(2,1)*F(2,2)*dNa_dZ)
                    +F(0,2)*dNb_dZ*(DUU(5,2)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,2)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,2)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,2)*F(2,0)*dNa_dX+DUU(1,2)*F(2,1)*dNa_dY+DUU(2,2)*F(2,2)*dNa_dZ);
    K_uu.comp(2,1) = (F(1,1)*dNb_dZ+F(1,2)*dNb_dY)*(DUU(5,3)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,3)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,3)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,3)*F(2,0)*dNa_dX+DUU(1,3)*F(2,1)*dNa_dY+DUU(2,3)*F(2,2)*dNa_dZ)
                    +(F(1,0)*dNb_dZ+F(1,2)*dNb_dX)*(DUU(5,4)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,4)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,4)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,4)*F(2,0)*dNa_dX+DUU(1,4)*F(2,1)*dNa_dY+DUU(2,4)*F(2,2)*dNa_dZ)
                    +(F(1,0)*dNb_dY+F(1,1)*dNb_dX)*(DUU(5,5)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,5)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,5)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,5)*F(2,0)*dNa_dX+DUU(1,5)*F(2,1)*dNa_dY+DUU(2,5)*F(2,2)*dNa_dZ)
                    +F(1,0)*dNb_dX*(DUU(5,0)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,0)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,0)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,0)*F(2,0)*dNa_dX+DUU(1,0)*F(2,1)*dNa_dY+DUU(2,0)*F(2,2)*dNa_dZ)
                    +F(1,1)*dNb_dY*(DUU(5,1)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,1)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,1)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,1)*F(2,0)*dNa_dX+DUU(1,1)*F(2,1)*dNa_dY+DUU(2,1)*F(2,2)*dNa_dZ)
                    +F(1,2)*dNb_dZ*(DUU(5,2)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,2)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,2)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,2)*F(2,0)*dNa_dX+DUU(1,2)*F(2,1)*dNa_dY+DUU(2,2)*F(2,2)*dNa_dZ);
    K_uu.comp(2,2) = (F(2,1)*dNb_dZ+F(2,2)*dNb_dY)*(DUU(5,3)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,3)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,3)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,3)*F(2,0)*dNa_dX+DUU(1,3)*F(2,1)*dNa_dY+DUU(2,3)*F(2,2)*dNa_dZ)
                    +(F(2,0)*dNb_dZ+F(2,2)*dNb_dX)*(DUU(5,4)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,4)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,4)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,4)*F(2,0)*dNa_dX+DUU(1,4)*F(2,1)*dNa_dY+DUU(2,4)*F(2,2)*dNa_dZ)
                    +(F(2,0)*dNb_dY+F(2,1)*dNb_dX)*(DUU(5,5)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,5)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,5)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,5)*F(2,0)*dNa_dX+DUU(1,5)*F(2,1)*dNa_dY+DUU(2,5)*F(2,2)*dNa_dZ)
                    +F(2,0)*dNb_dX*(DUU(5,0)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,0)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,0)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,0)*F(2,0)*dNa_dX+DUU(1,0)*F(2,1)*dNa_dY+DUU(2,0)*F(2,2)*dNa_dZ)
                    +F(2,1)*dNb_dY*(DUU(5,1)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,1)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,1)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,1)*F(2,0)*dNa_dX+DUU(1,1)*F(2,1)*dNa_dY+DUU(2,1)*F(2,2)*dNa_dZ)
                    +F(2,2)*dNb_dZ*(DUU(5,2)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUU(4,2)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUU(3,2)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUU(0,2)*F(2,0)*dNa_dX+DUU(1,2)*F(2,1)*dNa_dY+DUU(2,2)*F(2,2)*dNa_dZ)
                   + K_uu_geo;
    return K_uu;
}
inline
Dyadic3D calc_stiffness_forward ( const Vector3D& dNa,
                                  const Quindic3D& Dtsm_uu,
                                  const Dyadic3D& S2pk,
                                  const Vector3D& dNb,
                                  const Dyadic3D& F ) {
    const RealGradient dNa_(dNa(0), dNa(1), dNa(2));
    const RealGradient dNb_(dNb(0), dNb(1), dNb(2));
    return calc_stiffness_forward(dNa_, Dtsm_uu, S2pk, dNb_, F);
}
inline
Vector3D calc_stiffness_forward ( const RealGradient& dNa,
                                  const Dyadic3D& Dtsm_up,
                                  const Real& Yb,
                                  const Dyadic3D& F ) {
    const Real dNa_dX = dNa(0),
               dNa_dY = dNa(1),
               dNa_dZ = dNa(2);
    const VectorVoigt3D DUP = compress_stress(Dtsm_up);
    Vector3D K_up;
    K_up.comp(0) = Yb*(DUP(5)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)+DUP(4)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)+DUP(3)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)+DUP(0)*F(0,0)*dNa_dX+DUP(1)*F(0,1)*dNa_dY+DUP(2)*F(0,2)*dNa_dZ);
    K_up.comp(1) = Yb*(DUP(5)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)+DUP(4)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)+DUP(3)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)+DUP(0)*F(1,0)*dNa_dX+DUP(1)*F(1,1)*dNa_dY+DUP(2)*F(1,2)*dNa_dZ);
    K_up.comp(2) = Yb*(DUP(5)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)+DUP(4)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)+DUP(3)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)+DUP(0)*F(2,0)*dNa_dX+DUP(1)*F(2,1)*dNa_dY+DUP(2)*F(2,2)*dNa_dZ);
    return K_up;
}
inline
Vector3D calc_stiffness_forward ( const Vector3D& dNa,
                                  const Dyadic3D& Dtsm_up,
                                  const Real& Yb,
                                  const Dyadic3D& F ) {
    const RealGradient dNa_(dNa(0), dNa(1), dNa(2));
    return calc_stiffness_forward(dNa_, Dtsm_up, Yb, F);
}
inline
Vector3D calc_stiffness_forward ( const Real& Ya,
                                  const Dyadic3D& Dtsm_pu,
                                  const RealGradient& dNb,
                                  const Dyadic3D& F ) {
    const VectorVoigt3D DPU = compress_stress(Dtsm_pu);
    const Real dNb_dX = dNb(0),
               dNb_dY = dNb(1),
               dNb_dZ = dNb(2);
    Vector3D K_pu;
    K_pu.comp(0) = DPU(5)*Ya*(F(0,0)*dNb_dY+F(0,1)*dNb_dX)+DPU(4)*Ya*(F(0,0)*dNb_dZ+F(0,2)*dNb_dX)+DPU(3)*Ya*(F(0,1)*dNb_dZ+F(0,2)*dNb_dY)+DPU(0)*F(0,0)*Ya*dNb_dX+DPU(1)*F(0,1)*Ya*dNb_dY+DPU(2)*F(0,2)*Ya*dNb_dZ;
    K_pu.comp(1) = DPU(5)*Ya*(F(1,0)*dNb_dY+F(1,1)*dNb_dX)+DPU(4)*Ya*(F(1,0)*dNb_dZ+F(1,2)*dNb_dX)+DPU(3)*Ya*(F(1,1)*dNb_dZ+F(1,2)*dNb_dY)+DPU(0)*F(1,0)*Ya*dNb_dX+DPU(1)*F(1,1)*Ya*dNb_dY+DPU(2)*F(1,2)*Ya*dNb_dZ;
    K_pu.comp(2) = DPU(5)*Ya*(F(2,0)*dNb_dY+F(2,1)*dNb_dX)+DPU(4)*Ya*(F(2,0)*dNb_dZ+F(2,2)*dNb_dX)+DPU(3)*Ya*(F(2,1)*dNb_dZ+F(2,2)*dNb_dY)+DPU(0)*F(2,0)*Ya*dNb_dX+DPU(1)*F(2,1)*Ya*dNb_dY+DPU(2)*F(2,2)*Ya*dNb_dZ;
    return K_pu;
}
inline
Vector3D calc_stiffness_forward ( const Real& Ya,
                                  const Dyadic3D& Dtsm_pu,
                                  const Vector3D& dNb,
                                  const Dyadic3D& F ) {
    const RealGradient dNb_(dNb(0), dNb(1), dNb(2));
    return calc_stiffness_forward(Ya, Dtsm_pu, dNb_, F);
}
inline
Real calc_stiffness_forward ( const Real& Ya,
                              const Real& Dtsm_pp,
                              const Real& Yb,
                              const Dyadic3D& F ) {
    Real K_pp;
    K_pp = Ya*Dtsm_pp*Yb;
    return K_pp;
}
//-------------------------------------------------------------------------------------------------
inline
Vector3D calc_internal_force_forward ( const RealGradient& dNa,
                                       const Dyadic3D& S2pk,
                                       const Dyadic3D& F ) {
    const Real dNa_dX = dNa(0),
               dNa_dY = dNa(1),
               dNa_dZ = dNa(2);
    Vector3D F_u;
    F_u.comp(0) = S2pk(0,1)*(F(0,0)*dNa_dY+F(0,1)*dNa_dX)
                + S2pk(0,2)*(F(0,0)*dNa_dZ+F(0,2)*dNa_dX)
                + S2pk(1,2)*(F(0,1)*dNa_dZ+F(0,2)*dNa_dY)
                + F(0,0)*S2pk(0,0)*dNa_dX
                + F(0,1)*S2pk(1,1)*dNa_dY
                + F(0,2)*S2pk(2,2)*dNa_dZ;
    F_u.comp(1) = S2pk(0,1)*(F(1,0)*dNa_dY+F(1,1)*dNa_dX)
                + S2pk(0,2)*(F(1,0)*dNa_dZ+F(1,2)*dNa_dX)
                + S2pk(1,2)*(F(1,1)*dNa_dZ+F(1,2)*dNa_dY)
                + F(1,0)*S2pk(0,0)*dNa_dX
                + F(1,1)*S2pk(1,1)*dNa_dY
                + F(1,2)*S2pk(2,2)*dNa_dZ;
    F_u.comp(2) = S2pk(0,1)*(F(2,0)*dNa_dY+F(2,1)*dNa_dX)
                + S2pk(0,2)*(F(2,0)*dNa_dZ+F(2,2)*dNa_dX)
                + S2pk(1,2)*(F(2,1)*dNa_dZ+F(2,2)*dNa_dY)
                + F(2,0)*S2pk(0,0)*dNa_dX
                + F(2,1)*S2pk(1,1)*dNa_dY
                + F(2,2)*S2pk(2,2)*dNa_dZ;
    return F_u;
}
inline
Vector3D calc_internal_force_forward ( const Vector3D& dNa,
                                       const Dyadic3D& S2pk,
                                       const Dyadic3D& F ) {
    const RealGradient dNa_(dNa(0), dNa(1), dNa(2));
    return calc_internal_force_forward(dNa_, S2pk, F);
}
inline
Real calc_internal_force_forward ( const Real& Ya,
                                   const Real& Ph,
                                   const Dyadic3D& F ) {
    Real F_p;
    F_p = Ya*Ph;
    return F_p;
}
//=================================================================================================
inline
Dyadic3D calc_stiffness_forward ( const Real& dNa_dn0, const Real& dNa_dn1,
                                  const Quindic2D& Dtsm_uu,
                                  const Dyadic2D& S2pk,
                                  const Real& dNb_dn0, const Real& dNb_dn1,
                                  const Vector3D* G_, const Vector3D* g_, const Real& h ) {
    const DyadicVoigt2D DUU = compress_constitutive_tensor(Dtsm_uu);
    const VectorVoigt2D S = compress_stress(S2pk);
    Dyadic3D K_uu;
    K_uu.comp(0,0) = (dNb_dn0*(S(0)*dNa_dn0+S(2)*dNa_dn1)+dNb_dn1*(S(1)*dNa_dn1+S(2)*dNa_dn0)
                     +(dNb_dn0*g_[1](0)+dNb_dn1*g_[0](0))*(DUU(2,2)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,2)*dNa_dn0*g_[0](0)+DUU(1,2)*dNa_dn1*g_[1](0))
                     +dNb_dn0*g_[0](0)*(DUU(2,0)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,0)*dNa_dn0*g_[0](0)+DUU(1,0)*dNa_dn1*g_[1](0))
                     +dNb_dn1*g_[1](0)*(DUU(2,1)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,1)*dNa_dn0*g_[0](0)+DUU(1,1)*dNa_dn1*g_[1](0)))*h;
    K_uu.comp(0,1) = ((dNb_dn0*g_[1](1)+dNb_dn1*g_[0](1))*(DUU(2,2)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,2)*dNa_dn0*g_[0](0)+DUU(1,2)*dNa_dn1*g_[1](0))
                     +dNb_dn0*g_[0](1)*(DUU(2,0)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,0)*dNa_dn0*g_[0](0)+DUU(1,0)*dNa_dn1*g_[1](0))
                     +dNb_dn1*g_[1](1)*(DUU(2,1)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,1)*dNa_dn0*g_[0](0)+DUU(1,1)*dNa_dn1*g_[1](0)))*h;
    K_uu.comp(0,2) = ((dNb_dn0*g_[1](2)+dNb_dn1*g_[0](2))*(DUU(2,2)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,2)*dNa_dn0*g_[0](0)+DUU(1,2)*dNa_dn1*g_[1](0))
                     +dNb_dn0*g_[0](2)*(DUU(2,0)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,0)*dNa_dn0*g_[0](0)+DUU(1,0)*dNa_dn1*g_[1](0))
                     +dNb_dn1*g_[1](2)*(DUU(2,1)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,1)*dNa_dn0*g_[0](0)+DUU(1,1)*dNa_dn1*g_[1](0)))*h;
    K_uu.comp(1,0) = ((dNb_dn0*g_[1](0)+dNb_dn1*g_[0](0))*(DUU(2,2)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,2)*dNa_dn0*g_[0](1)+DUU(1,2)*dNa_dn1*g_[1](1))
                     +dNb_dn0*g_[0](0)*(DUU(2,0)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,0)*dNa_dn0*g_[0](1)+DUU(1,0)*dNa_dn1*g_[1](1))
                     +dNb_dn1*g_[1](0)*(DUU(2,1)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,1)*dNa_dn0*g_[0](1)+DUU(1,1)*dNa_dn1*g_[1](1)))*h;
    K_uu.comp(1,1) = (dNb_dn0*(S(0)*dNa_dn0+S(2)*dNa_dn1)+dNb_dn1*(S(1)*dNa_dn1+S(2)*dNa_dn0)
                     +(dNb_dn0*g_[1](1)+dNb_dn1*g_[0](1))*(DUU(2,2)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,2)*dNa_dn0*g_[0](1)+DUU(1,2)*dNa_dn1*g_[1](1))
                     +dNb_dn0*g_[0](1)*(DUU(2,0)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,0)*dNa_dn0*g_[0](1)+DUU(1,0)*dNa_dn1*g_[1](1))
                     +dNb_dn1*g_[1](1)*(DUU(2,1)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,1)*dNa_dn0*g_[0](1)+DUU(1,1)*dNa_dn1*g_[1](1)))*h;
    K_uu.comp(1,2) = ((dNb_dn0*g_[1](2)+dNb_dn1*g_[0](2))*(DUU(2,2)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,2)*dNa_dn0*g_[0](1)+DUU(1,2)*dNa_dn1*g_[1](1))
                     +dNb_dn0*g_[0](2)*(DUU(2,0)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,0)*dNa_dn0*g_[0](1)+DUU(1,0)*dNa_dn1*g_[1](1))
                     +dNb_dn1*g_[1](2)*(DUU(2,1)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,1)*dNa_dn0*g_[0](1)+DUU(1,1)*dNa_dn1*g_[1](1)))*h;
    K_uu.comp(2,0) = ((dNb_dn0*g_[1](0)+dNb_dn1*g_[0](0))*(DUU(2,2)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,2)*dNa_dn0*g_[0](2)+DUU(1,2)*dNa_dn1*g_[1](2))
                     +dNb_dn0*g_[0](0)*(DUU(2,0)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,0)*dNa_dn0*g_[0](2)+DUU(1,0)*dNa_dn1*g_[1](2))
                     +dNb_dn1*g_[1](0)*(DUU(2,1)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,1)*dNa_dn0*g_[0](2)+DUU(1,1)*dNa_dn1*g_[1](2)))*h;
    K_uu.comp(2,1) = ((dNb_dn0*g_[1](1)+dNb_dn1*g_[0](1))*(DUU(2,2)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,2)*dNa_dn0*g_[0](2)+DUU(1,2)*dNa_dn1*g_[1](2))
                     +dNb_dn0*g_[0](1)*(DUU(2,0)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,0)*dNa_dn0*g_[0](2)+DUU(1,0)*dNa_dn1*g_[1](2))
                     +dNb_dn1*g_[1](1)*(DUU(2,1)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,1)*dNa_dn0*g_[0](2)+DUU(1,1)*dNa_dn1*g_[1](2)))*h;
    K_uu.comp(2,2) = (dNb_dn0*(S(0)*dNa_dn0+S(2)*dNa_dn1)+dNb_dn1*(S(1)*dNa_dn1+S(2)*dNa_dn0)
                     +(dNb_dn0*g_[1](2)+dNb_dn1*g_[0](2))*(DUU(2,2)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,2)*dNa_dn0*g_[0](2)+DUU(1,2)*dNa_dn1*g_[1](2))
                     +dNb_dn0*g_[0](2)*(DUU(2,0)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,0)*dNa_dn0*g_[0](2)+DUU(1,0)*dNa_dn1*g_[1](2))
                     +dNb_dn1*g_[1](2)*(DUU(2,1)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,1)*dNa_dn0*g_[0](2)+DUU(1,1)*dNa_dn1*g_[1](2)))*h;
    return K_uu;
}
//-------------------------------------------------------------------------------------------------
inline
Vector3D calc_internal_force_forward ( const Real& dNa_dn0, const Real& dNa_dn1,
                                       const Dyadic2D& S2pk,
                                       const Vector3D* g_, const Real& h ) {
    const VectorVoigt2D S = compress_stress(S2pk);
    Vector3D F_u;
    F_u.comp(0) = (S(2)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+S(0)*dNa_dn0*g_[0](0)+S(1)*dNa_dn1*g_[1](0))*h;
    F_u.comp(1) = (S(2)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+S(0)*dNa_dn0*g_[0](1)+S(1)*dNa_dn1*g_[1](1))*h;
    F_u.comp(2) = (S(2)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+S(0)*dNa_dn0*g_[0](2)+S(1)*dNa_dn1*g_[1](2))*h;
    return F_u;
}
//=================================================================================================
inline
Dyadic3D calc_stiffness_inverse ( const RealGradient& dNa,
                                  const Quindic3D& Dtsm_uu,
                                  const RealGradient& dNb,
                                  const Dyadic3D& f ) {
    const Real dNa_dx = dNa(0),
               dNa_dy = dNa(1),
               dNa_dz = dNa(2);
    const DyadicVoigt3D DUU = compress_constitutive_tensor(Dtsm_uu);
    const Real dNb_dx = dNb(0),
               dNb_dy = dNb(1),
               dNb_dz = dNb(2);
    Dyadic3D K_uu;
    K_uu.comp(0,0) = f(0,0)*(dNb_dx*(DUU(0,0)*dNa_dx+DUU(4,0)*dNa_dz+DUU(5,0)*dNa_dy)+dNb_dz*(DUU(0,4)*dNa_dx+DUU(4,4)*dNa_dz+DUU(5,4)*dNa_dy)+dNb_dy*(DUU(0,5)*dNa_dx+DUU(4,5)*dNa_dz+DUU(5,5)*dNa_dy))
                    +f(0,1)*(dNb_dy*(DUU(0,1)*dNa_dx+DUU(4,1)*dNa_dz+DUU(5,1)*dNa_dy)+dNb_dz*(DUU(0,3)*dNa_dx+DUU(4,3)*dNa_dz+DUU(5,3)*dNa_dy)+dNb_dx*(DUU(0,5)*dNa_dx+DUU(4,5)*dNa_dz+DUU(5,5)*dNa_dy))
                    +f(0,2)*(dNb_dz*(DUU(0,2)*dNa_dx+DUU(4,2)*dNa_dz+DUU(5,2)*dNa_dy)+dNb_dy*(DUU(0,3)*dNa_dx+DUU(4,3)*dNa_dz+DUU(5,3)*dNa_dy)+dNb_dx*(DUU(0,4)*dNa_dx+DUU(4,4)*dNa_dz+DUU(5,4)*dNa_dy));
    K_uu.comp(0,1) = f(1,0)*(dNb_dx*(DUU(0,0)*dNa_dx+DUU(4,0)*dNa_dz+DUU(5,0)*dNa_dy)+dNb_dz*(DUU(0,4)*dNa_dx+DUU(4,4)*dNa_dz+DUU(5,4)*dNa_dy)+dNb_dy*(DUU(0,5)*dNa_dx+DUU(4,5)*dNa_dz+DUU(5,5)*dNa_dy))
                    +f(1,1)*(dNb_dy*(DUU(0,1)*dNa_dx+DUU(4,1)*dNa_dz+DUU(5,1)*dNa_dy)+dNb_dz*(DUU(0,3)*dNa_dx+DUU(4,3)*dNa_dz+DUU(5,3)*dNa_dy)+dNb_dx*(DUU(0,5)*dNa_dx+DUU(4,5)*dNa_dz+DUU(5,5)*dNa_dy))
                    +f(1,2)*(dNb_dz*(DUU(0,2)*dNa_dx+DUU(4,2)*dNa_dz+DUU(5,2)*dNa_dy)+dNb_dy*(DUU(0,3)*dNa_dx+DUU(4,3)*dNa_dz+DUU(5,3)*dNa_dy)+dNb_dx*(DUU(0,4)*dNa_dx+DUU(4,4)*dNa_dz+DUU(5,4)*dNa_dy));
    K_uu.comp(0,2) = f(2,0)*(dNb_dx*(DUU(0,0)*dNa_dx+DUU(4,0)*dNa_dz+DUU(5,0)*dNa_dy)+dNb_dz*(DUU(0,4)*dNa_dx+DUU(4,4)*dNa_dz+DUU(5,4)*dNa_dy)+dNb_dy*(DUU(0,5)*dNa_dx+DUU(4,5)*dNa_dz+DUU(5,5)*dNa_dy))
                    +f(2,1)*(dNb_dy*(DUU(0,1)*dNa_dx+DUU(4,1)*dNa_dz+DUU(5,1)*dNa_dy)+dNb_dz*(DUU(0,3)*dNa_dx+DUU(4,3)*dNa_dz+DUU(5,3)*dNa_dy)+dNb_dx*(DUU(0,5)*dNa_dx+DUU(4,5)*dNa_dz+DUU(5,5)*dNa_dy))
                    +f(2,2)*(dNb_dz*(DUU(0,2)*dNa_dx+DUU(4,2)*dNa_dz+DUU(5,2)*dNa_dy)+dNb_dy*(DUU(0,3)*dNa_dx+DUU(4,3)*dNa_dz+DUU(5,3)*dNa_dy)+dNb_dx*(DUU(0,4)*dNa_dx+DUU(4,4)*dNa_dz+DUU(5,4)*dNa_dy));
    K_uu.comp(1,0) = f(0,0)*(dNb_dx*(DUU(1,0)*dNa_dy+DUU(3,0)*dNa_dz+DUU(5,0)*dNa_dx)+dNb_dz*(DUU(1,4)*dNa_dy+DUU(3,4)*dNa_dz+DUU(5,4)*dNa_dx)+dNb_dy*(DUU(1,5)*dNa_dy+DUU(3,5)*dNa_dz+DUU(5,5)*dNa_dx))
                    +f(0,1)*(dNb_dy*(DUU(1,1)*dNa_dy+DUU(3,1)*dNa_dz+DUU(5,1)*dNa_dx)+dNb_dz*(DUU(1,3)*dNa_dy+DUU(3,3)*dNa_dz+DUU(5,3)*dNa_dx)+dNb_dx*(DUU(1,5)*dNa_dy+DUU(3,5)*dNa_dz+DUU(5,5)*dNa_dx))
                    +f(0,2)*(dNb_dz*(DUU(1,2)*dNa_dy+DUU(3,2)*dNa_dz+DUU(5,2)*dNa_dx)+dNb_dy*(DUU(1,3)*dNa_dy+DUU(3,3)*dNa_dz+DUU(5,3)*dNa_dx)+dNb_dx*(DUU(1,4)*dNa_dy+DUU(3,4)*dNa_dz+DUU(5,4)*dNa_dx));
    K_uu.comp(1,1) = f(1,0)*(dNb_dx*(DUU(1,0)*dNa_dy+DUU(3,0)*dNa_dz+DUU(5,0)*dNa_dx)+dNb_dz*(DUU(1,4)*dNa_dy+DUU(3,4)*dNa_dz+DUU(5,4)*dNa_dx)+dNb_dy*(DUU(1,5)*dNa_dy+DUU(3,5)*dNa_dz+DUU(5,5)*dNa_dx))
                    +f(1,1)*(dNb_dy*(DUU(1,1)*dNa_dy+DUU(3,1)*dNa_dz+DUU(5,1)*dNa_dx)+dNb_dz*(DUU(1,3)*dNa_dy+DUU(3,3)*dNa_dz+DUU(5,3)*dNa_dx)+dNb_dx*(DUU(1,5)*dNa_dy+DUU(3,5)*dNa_dz+DUU(5,5)*dNa_dx))
                    +f(1,2)*(dNb_dz*(DUU(1,2)*dNa_dy+DUU(3,2)*dNa_dz+DUU(5,2)*dNa_dx)+dNb_dy*(DUU(1,3)*dNa_dy+DUU(3,3)*dNa_dz+DUU(5,3)*dNa_dx)+dNb_dx*(DUU(1,4)*dNa_dy+DUU(3,4)*dNa_dz+DUU(5,4)*dNa_dx));
    K_uu.comp(1,2) = f(2,0)*(dNb_dx*(DUU(1,0)*dNa_dy+DUU(3,0)*dNa_dz+DUU(5,0)*dNa_dx)+dNb_dz*(DUU(1,4)*dNa_dy+DUU(3,4)*dNa_dz+DUU(5,4)*dNa_dx)+dNb_dy*(DUU(1,5)*dNa_dy+DUU(3,5)*dNa_dz+DUU(5,5)*dNa_dx))
                    +f(2,1)*(dNb_dy*(DUU(1,1)*dNa_dy+DUU(3,1)*dNa_dz+DUU(5,1)*dNa_dx)+dNb_dz*(DUU(1,3)*dNa_dy+DUU(3,3)*dNa_dz+DUU(5,3)*dNa_dx)+dNb_dx*(DUU(1,5)*dNa_dy+DUU(3,5)*dNa_dz+DUU(5,5)*dNa_dx))
                    +f(2,2)*(dNb_dz*(DUU(1,2)*dNa_dy+DUU(3,2)*dNa_dz+DUU(5,2)*dNa_dx)+dNb_dy*(DUU(1,3)*dNa_dy+DUU(3,3)*dNa_dz+DUU(5,3)*dNa_dx)+dNb_dx*(DUU(1,4)*dNa_dy+DUU(3,4)*dNa_dz+DUU(5,4)*dNa_dx));
    K_uu.comp(2,0) = f(0,0)*(dNb_dx*(DUU(2,0)*dNa_dz+DUU(3,0)*dNa_dy+DUU(4,0)*dNa_dx)+dNb_dz*(DUU(2,4)*dNa_dz+DUU(3,4)*dNa_dy+DUU(4,4)*dNa_dx)+dNb_dy*(DUU(2,5)*dNa_dz+DUU(3,5)*dNa_dy+DUU(4,5)*dNa_dx))
                    +f(0,1)*(dNb_dy*(DUU(2,1)*dNa_dz+DUU(3,1)*dNa_dy+DUU(4,1)*dNa_dx)+dNb_dz*(DUU(2,3)*dNa_dz+DUU(3,3)*dNa_dy+DUU(4,3)*dNa_dx)+dNb_dx*(DUU(2,5)*dNa_dz+DUU(3,5)*dNa_dy+DUU(4,5)*dNa_dx))
                    +f(0,2)*(dNb_dz*(DUU(2,2)*dNa_dz+DUU(3,2)*dNa_dy+DUU(4,2)*dNa_dx)+dNb_dy*(DUU(2,3)*dNa_dz+DUU(3,3)*dNa_dy+DUU(4,3)*dNa_dx)+dNb_dx*(DUU(2,4)*dNa_dz+DUU(3,4)*dNa_dy+DUU(4,4)*dNa_dx));
    K_uu.comp(2,1) = f(1,0)*(dNb_dx*(DUU(2,0)*dNa_dz+DUU(3,0)*dNa_dy+DUU(4,0)*dNa_dx)+dNb_dz*(DUU(2,4)*dNa_dz+DUU(3,4)*dNa_dy+DUU(4,4)*dNa_dx)+dNb_dy*(DUU(2,5)*dNa_dz+DUU(3,5)*dNa_dy+DUU(4,5)*dNa_dx))
                    +f(1,1)*(dNb_dy*(DUU(2,1)*dNa_dz+DUU(3,1)*dNa_dy+DUU(4,1)*dNa_dx)+dNb_dz*(DUU(2,3)*dNa_dz+DUU(3,3)*dNa_dy+DUU(4,3)*dNa_dx)+dNb_dx*(DUU(2,5)*dNa_dz+DUU(3,5)*dNa_dy+DUU(4,5)*dNa_dx))
                    +f(1,2)*(dNb_dz*(DUU(2,2)*dNa_dz+DUU(3,2)*dNa_dy+DUU(4,2)*dNa_dx)+dNb_dy*(DUU(2,3)*dNa_dz+DUU(3,3)*dNa_dy+DUU(4,3)*dNa_dx)+dNb_dx*(DUU(2,4)*dNa_dz+DUU(3,4)*dNa_dy+DUU(4,4)*dNa_dx));
    K_uu.comp(2,2) = f(2,0)*(dNb_dx*(DUU(2,0)*dNa_dz+DUU(3,0)*dNa_dy+DUU(4,0)*dNa_dx)+dNb_dz*(DUU(2,4)*dNa_dz+DUU(3,4)*dNa_dy+DUU(4,4)*dNa_dx)+dNb_dy*(DUU(2,5)*dNa_dz+DUU(3,5)*dNa_dy+DUU(4,5)*dNa_dx))
                    +f(2,1)*(dNb_dy*(DUU(2,1)*dNa_dz+DUU(3,1)*dNa_dy+DUU(4,1)*dNa_dx)+dNb_dz*(DUU(2,3)*dNa_dz+DUU(3,3)*dNa_dy+DUU(4,3)*dNa_dx)+dNb_dx*(DUU(2,5)*dNa_dz+DUU(3,5)*dNa_dy+DUU(4,5)*dNa_dx))
                    +f(2,2)*(dNb_dz*(DUU(2,2)*dNa_dz+DUU(3,2)*dNa_dy+DUU(4,2)*dNa_dx)+dNb_dy*(DUU(2,3)*dNa_dz+DUU(3,3)*dNa_dy+DUU(4,3)*dNa_dx)+dNb_dx*(DUU(2,4)*dNa_dz+DUU(3,4)*dNa_dy+DUU(4,4)*dNa_dx));
    return K_uu;
}
inline
Dyadic3D calc_stiffness_inverse ( const Vector3D& dNa,
                                  const Quindic3D& Dtsm_uu,
                                  const Vector3D& dNb,
                                  const Dyadic3D& f ) {
    const RealGradient dNa_(dNa(0), dNa(1), dNa(2));
    const RealGradient dNb_(dNb(0), dNb(1), dNb(2));
    return calc_stiffness_inverse(dNa_, Dtsm_uu, dNb_, f);
}
inline
Vector3D calc_stiffness_inverse ( const RealGradient& dNa,
                                  const Dyadic3D& Dtsm_up,
                                  const Real& Yb,
                                  const Dyadic3D& f ) {
    const Real dNa_dx = dNa(0),
               dNa_dy = dNa(1),
               dNa_dz = dNa(2);
    const VectorVoigt3D DUP = compress_stress(Dtsm_up);
    Vector3D K_up;
    K_up.comp(0) = Yb*(DUP(0)*dNa_dx+DUP(4)*dNa_dz+DUP(5)*dNa_dy);
    K_up.comp(1) = Yb*(DUP(1)*dNa_dy+DUP(3)*dNa_dz+DUP(5)*dNa_dx);
    K_up.comp(2) = Yb*(DUP(2)*dNa_dz+DUP(3)*dNa_dy+DUP(4)*dNa_dx);
    return K_up;
}
inline
Vector3D calc_stiffness_inverse ( const Vector3D& dNa,
                                  const Dyadic3D& Dtsm_up,
                                  const Real& Yb,
                                  const Dyadic3D& f ) {
    const RealGradient dNa_(dNa(0), dNa(1), dNa(2));
    return calc_stiffness_inverse(dNa_, Dtsm_up, Yb, f);
}
inline
Vector3D calc_stiffness_inverse ( const Real& Ya,
                                  const Dyadic3D& Dtsm_pu,
                                  const RealGradient& dNb,
                                  const Dyadic3D& f ) {
    const VectorVoigt3D DPU = compress_stress(Dtsm_pu);
    const Real dNb_dx = dNb(0),
               dNb_dy = dNb(1),
               dNb_dz = dNb(2);
    Vector3D K_pu;
    K_pu.comp(0) = f(0,0)*(DPU(0)*Ya*dNb_dx+DPU(4)*Ya*dNb_dz+DPU(5)*Ya*dNb_dy)
                 + f(0,1)*(DPU(1)*Ya*dNb_dy+DPU(3)*Ya*dNb_dz+DPU(5)*Ya*dNb_dx)
                 + f(0,2)*(DPU(2)*Ya*dNb_dz+DPU(3)*Ya*dNb_dy+DPU(4)*Ya*dNb_dx);
    K_pu.comp(1) = f(1,0)*(DPU(0)*Ya*dNb_dx+DPU(4)*Ya*dNb_dz+DPU(5)*Ya*dNb_dy)
                 + f(1,1)*(DPU(1)*Ya*dNb_dy+DPU(3)*Ya*dNb_dz+DPU(5)*Ya*dNb_dx)
                 + f(1,2)*(DPU(2)*Ya*dNb_dz+DPU(3)*Ya*dNb_dy+DPU(4)*Ya*dNb_dx);
    K_pu.comp(2) = f(2,0)*(DPU(0)*Ya*dNb_dx+DPU(4)*Ya*dNb_dz+DPU(5)*Ya*dNb_dy)
                 + f(2,1)*(DPU(1)*Ya*dNb_dy+DPU(3)*Ya*dNb_dz+DPU(5)*Ya*dNb_dx)
                 + f(2,2)*(DPU(2)*Ya*dNb_dz+DPU(3)*Ya*dNb_dy+DPU(4)*Ya*dNb_dx);
    return K_pu;
}
inline
Vector3D calc_stiffness_inverse ( const Real& Ya,
                                  const Dyadic3D& Dtsm_pu,
                                  const Vector3D& dNb,
                                  const Dyadic3D& f ) {
    const RealGradient dNb_(dNb(0), dNb(1), dNb(2));
    return calc_stiffness_inverse(Ya, Dtsm_pu, dNb_, f);
}
inline
Real calc_stiffness_inverse ( const Real& Ya,
                              const Real& Dtsm_pp,
                              const Real& Yb,
                              const Dyadic3D& f ) {
    Real K_pp;
    K_pp = Ya*Dtsm_pp*Yb;
    return K_pp;
}
//-------------------------------------------------------------------------------------------------
inline
Vector3D calc_internal_force_inverse ( const RealGradient& dNa,
                                       const Dyadic3D& Sc ) {
    const Real dNa_dx = dNa(0),
               dNa_dy = dNa(1),
               dNa_dz = dNa(2);
    Vector3D F_u;
    F_u.comp(0) = Sc(0,0)*dNa_dx+Sc(1,0)*dNa_dy+Sc(2,0)*dNa_dz;
    F_u.comp(1) = Sc(0,1)*dNa_dx+Sc(1,1)*dNa_dy+Sc(2,1)*dNa_dz;
    F_u.comp(2) = Sc(0,2)*dNa_dx+Sc(1,2)*dNa_dy+Sc(2,2)*dNa_dz;
    return F_u;
}
inline
Vector3D calc_internal_force_inverse ( const Vector3D& dNa,
                                       const Dyadic3D& Sc ) {
    const RealGradient dNa_(dNa(0), dNa(1), dNa(2));
    return calc_internal_force_inverse(dNa_, Sc);
}
inline
Real calc_internal_force_inverse ( const Real& Ya,
                                   const Real& Ph ) {
    Real F_p;
    F_p = Ya*Ph;
    return F_p;
}
//=================================================================================================
inline
Dyadic3D calc_stiffness_inverse ( const Real& dNa_dn0, const Real& dNa_dn1,
                                  const Quindic2D& Dtsm_uu,
                                  const Dyadic2D& Sc,
                                  const Real& dNb_dn0, const Real& dNb_dn1,
                                  const Vector3D* G_, const Vector3D* g_, const Real& h ) {
    const DyadicVoigt2D DUU = compress_constitutive_tensor(Dtsm_uu);
    const VectorVoigt2D S = compress_stress(Sc);
    Dyadic3D K_uu;
    K_uu.comp(0,0) = ((G_[0](0)*dNb_dn1+G_[1](0)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,2)*dNa_dn0*g_[0](0)+DUU(1,2)*dNa_dn1*g_[1](0))
                     +G_[0](0)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,0)*dNa_dn0*g_[0](0)+DUU(1,0)*dNa_dn1*g_[1](0))
                     +G_[1](0)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,1)*dNa_dn0*g_[0](0)+DUU(1,1)*dNa_dn1*g_[1](0)))*h;
    K_uu.comp(0,1) = ((G_[0](1)*dNb_dn1+G_[1](1)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,2)*dNa_dn0*g_[0](0)+DUU(1,2)*dNa_dn1*g_[1](0))
                     +G_[0](1)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,0)*dNa_dn0*g_[0](0)+DUU(1,0)*dNa_dn1*g_[1](0))
                     +G_[1](1)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,1)*dNa_dn0*g_[0](0)+DUU(1,1)*dNa_dn1*g_[1](0)))*h;
    K_uu.comp(0,2) = ((G_[0](2)*dNb_dn1+G_[1](2)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,2)*dNa_dn0*g_[0](0)+DUU(1,2)*dNa_dn1*g_[1](0))
                     +G_[0](2)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,0)*dNa_dn0*g_[0](0)+DUU(1,0)*dNa_dn1*g_[1](0))
                     +G_[1](2)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+DUU(0,1)*dNa_dn0*g_[0](0)+DUU(1,1)*dNa_dn1*g_[1](0)))*h;
    K_uu.comp(1,0) = ((G_[0](0)*dNb_dn1+G_[1](0)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,2)*dNa_dn0*g_[0](1)+DUU(1,2)*dNa_dn1*g_[1](1))
                     +G_[0](0)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,0)*dNa_dn0*g_[0](1)+DUU(1,0)*dNa_dn1*g_[1](1))
                     +G_[1](0)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,1)*dNa_dn0*g_[0](1)+DUU(1,1)*dNa_dn1*g_[1](1)))*h;
    K_uu.comp(1,1) = ((G_[0](1)*dNb_dn1+G_[1](1)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,2)*dNa_dn0*g_[0](1)+DUU(1,2)*dNa_dn1*g_[1](1))
                     +G_[0](1)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,0)*dNa_dn0*g_[0](1)+DUU(1,0)*dNa_dn1*g_[1](1))
                     +G_[1](1)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,1)*dNa_dn0*g_[0](1)+DUU(1,1)*dNa_dn1*g_[1](1)))*h;
    K_uu.comp(1,2) = ((G_[0](2)*dNb_dn1+G_[1](2)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,2)*dNa_dn0*g_[0](1)+DUU(1,2)*dNa_dn1*g_[1](1))
                     +G_[0](2)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,0)*dNa_dn0*g_[0](1)+DUU(1,0)*dNa_dn1*g_[1](1))
                     +G_[1](2)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+DUU(0,1)*dNa_dn0*g_[0](1)+DUU(1,1)*dNa_dn1*g_[1](1)))*h;
    K_uu.comp(2,0) = ((G_[0](0)*dNb_dn1+G_[1](0)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,2)*dNa_dn0*g_[0](2)+DUU(1,2)*dNa_dn1*g_[1](2))
                     +G_[0](0)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,0)*dNa_dn0*g_[0](2)+DUU(1,0)*dNa_dn1*g_[1](2))
                     +G_[1](0)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,1)*dNa_dn0*g_[0](2)+DUU(1,1)*dNa_dn1*g_[1](2)))*h;
    K_uu.comp(2,1) = ((G_[0](1)*dNb_dn1+G_[1](1)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,2)*dNa_dn0*g_[0](2)+DUU(1,2)*dNa_dn1*g_[1](2))
                     +G_[0](1)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,0)*dNa_dn0*g_[0](2)+DUU(1,0)*dNa_dn1*g_[1](2))
                     +G_[1](1)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,1)*dNa_dn0*g_[0](2)+DUU(1,1)*dNa_dn1*g_[1](2)))*h;
    K_uu.comp(2,2) = ((G_[0](2)*dNb_dn1+G_[1](2)*dNb_dn0)*(DUU(2,2)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,2)*dNa_dn0*g_[0](2)+DUU(1,2)*dNa_dn1*g_[1](2))
                     +G_[0](2)*dNb_dn0*(DUU(2,0)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,0)*dNa_dn0*g_[0](2)+DUU(1,0)*dNa_dn1*g_[1](2))
                     +G_[1](2)*dNb_dn1*(DUU(2,1)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+DUU(0,1)*dNa_dn0*g_[0](2)+DUU(1,1)*dNa_dn1*g_[1](2)))*h;
    return K_uu;
}
//-------------------------------------------------------------------------------------------------
inline
Vector3D calc_internal_force_inverse ( const Real& dNa_dn0, const Real& dNa_dn1,
                                       const Dyadic2D& Sc,
                                       const Vector3D* g_, const Real& h ) {
    const VectorVoigt2D S = compress_stress(Sc);
    Vector3D F_u;
    F_u.comp(0) = (S(2)*(dNa_dn0*g_[1](0)+dNa_dn1*g_[0](0))+S(0)*dNa_dn0*g_[0](0)+S(1)*dNa_dn1*g_[1](0))*h;
    F_u.comp(1) = (S(2)*(dNa_dn0*g_[1](1)+dNa_dn1*g_[0](1))+S(0)*dNa_dn0*g_[0](1)+S(1)*dNa_dn1*g_[1](1))*h;
    F_u.comp(2) = (S(2)*(dNa_dn0*g_[1](2)+dNa_dn1*g_[0](2))+S(0)*dNa_dn0*g_[0](2)+S(1)*dNa_dn1*g_[1](2))*h;
    return F_u;
}
//=================================================================================================
#endif // __NAVIER_CAUCHY_SOLVER_3D_AUX_INLINE_H__
//=================================================================================================
