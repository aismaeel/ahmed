/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __FEM_CONTEXT_H__
#define __FEM_CONTEXT_H__
//=================================================================================================
#include "libmesh/dof_map.h"
#include "libmesh/mesh.h"
#include "libmesh/system.h"
//=================================================================================================
class FEM_Context {
public:
    FEM_Context () : _system(NULL), _msh(NULL) {}
    FEM_Context (System& sys) : _system(&sys), _msh(&sys.get_mesh()) {}
    ~FEM_Context () {}
    //
    inline
    void init (System& sys) { this->_system = &sys; this->_msh = &sys.get_mesh(); }
    inline
    System&   get_system () { return *this->_system; }
    inline
    MeshBase& get_mesh () { return *this->_msh; }
    //
    inline
    const System&   get_system () const { return *this->_system; }
    inline
    const MeshBase& get_mesh () const { return *this->_msh; }
    //
    void get_var_list (std::vector<unsigned short int>& vars) const;
    void get_dof_indices ( const Elem* elem,
                           std::vector<dof_id_type>& dofs, std::vector<dof_id_type>* dofs_var,
                           std::vector<unsigned int>* vn =NULL) const;
    //
private:
    System* _system;
    MeshBase* _msh;
};
//=================================================================================================
//non-member scalar print function
inline
void print_helper (std::ostream& os, const FEM_Context* ctx) {
    libmesh_error();
}
//=================================================================================================
#endif //  __FEM_CONTEXT_H__
//=================================================================================================
