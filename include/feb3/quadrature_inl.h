/*************************************************************************************************

    Copyright (C) 2009-2016    Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if
    not, write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 *************************************************************************************************/

//=================================================================================================
#ifndef __QUADRATURE_INLINE_H__
#define __QUADRATURE_INLINE_H__
//=================================================================================================
inline
void NonUniform_QGauss::init () {
    unsigned int nqgp[SP3D];
    //
    if ( SP1D == this->_dimension ) {
        this->_qgauss_0.init(EDGE2);
        nqgp[0] = this->_qgauss_0.n_points();
        //
        this->_points.resize(nqgp[0]);
        this->_weights.resize(nqgp[0]);
        // temporary index
        unsigned int q = 0;
        for (unsigned int i=0; i<nqgp[0]; i++) {
            this->_points[q] = Point( this->_qgauss_0.qp(i)(0),
                                      0.0,
                                      0.0 );
            this->_weights[q] = this->_qgauss_0.w(i);
            // increment this index
            q++;
        }
        //
    } else if ( SP2D == this->_dimension ) {
        this->_qgauss_0.init(EDGE2);
        this->_qgauss_1.init(EDGE2);
        nqgp[0] = this->_qgauss_0.n_points();
        nqgp[1] = this->_qgauss_1.n_points();
        //
        this->_points.resize(nqgp[0]*nqgp[1]);
        this->_weights.resize(nqgp[0]*nqgp[1]);
        // temporary index
        unsigned int q = 0;
        for (unsigned int j=0; j<nqgp[1]; j++) {
            for (unsigned int i=0; i<nqgp[0]; i++) {
                this->_points[q] = Point( this->_qgauss_0.qp(i)(0),
                                          this->_qgauss_1.qp(j)(0),
                                          0.0 );
                this->_weights[q] = this->_qgauss_0.w(i)
                                  * this->_qgauss_1.w(j);
                // increment this index
                q++;
            }
        }
        //
    } else if ( SP3D == this->_dimension ) {
        this->_qgauss_0.init(EDGE2);
        this->_qgauss_1.init(EDGE2);
        this->_qgauss_2.init(EDGE2);
        nqgp[0] = this->_qgauss_0.n_points();
        nqgp[1] = this->_qgauss_1.n_points();
        nqgp[2] = this->_qgauss_2.n_points();
        //
        this->_points.resize(nqgp[0]*nqgp[1]*nqgp[2]);
        this->_weights.resize(nqgp[0]*nqgp[1]*nqgp[2]);
        // temporary index
        unsigned int q = 0;
        for (unsigned int k=0; k<nqgp[2]; k++) {
            for (unsigned int j=0; j<nqgp[1]; j++) {
                for (unsigned int i=0; i<nqgp[0]; i++) {
                    this->_points[q] = Point( this->_qgauss_0.qp(i)(0),
                                              this->_qgauss_1.qp(j)(0),
                                              this->_qgauss_2.qp(k)(0) );
                    this->_weights[q] = this->_qgauss_0.w(i)
                                      * this->_qgauss_1.w(j)
                                      * this->_qgauss_2.w(k);
                    // increment this index
                    q++;
                }
            }
        }
        //
    } else libmesh_error();
}
//=================================================================================================
inline
QuadratureInfo::QuadratureInfo (int n_qp, int n_bc, int n_bm, int n_aux) {
    this->init(n_qp, n_bc, n_bm, n_aux);
}
//-------------------------------------------------------------------------------------------------
inline
void QuadratureInfo::init (int n_qp, int n_bc, int n_bm, int n_aux) {
    Vector3D fibre_default;
    fibre_default.comp = sqrt(frac_1o3);
    //
    this->XYZ.resize(n_qp);
    this->xyz.resize(n_qp);
    this->biochem.resize(n_qp);
    this->grad_biochem.resize(n_qp);
    this->biochem_rate.resize(n_qp);
    for (unsigned int i=0; i<n_qp; i++) {
        this->biochem[i].assign(n_bc, 0.0);
        this->grad_biochem[i].assign(n_bc, RealGradient(0.0, 0.0, 0.0));
        this->biochem_rate[i].assign(n_bc, 0.0);
    }
    this->biomech.resize(n_qp);
    this->deform_grad.resize(n_qp);
    this->deform_grad_elas.resize(n_qp);
    this->deform_grad_inel.resize(n_qp);
    for (unsigned int i=0; i<n_qp; i++) {
        this->biomech[i].assign(n_bm, 0.0);
        this->deform_grad[i] = identity_matrix;
        this->deform_grad_elas[i] = identity_matrix;
        this->deform_grad_inel[i] = identity_matrix;
    }
    this->strain.resize(n_qp);
    this->strain_elas.resize(n_qp);
    this->strain_inel.resize(n_qp);
    this->stress.resize(n_qp);
    this->pressure.assign(n_qp, 0.0);
    this->fluid_velocity.resize(n_qp);
    for (unsigned int k=0; k<SP3D; k++) {
        this->fibre_ref[k].resize(n_qp);
        this->fibre_curr[k].resize(n_qp);
        for (unsigned int i=0; i<n_qp; i++) {
            this->fibre_ref[k][i] = fibre_default;
            this->fibre_curr[k][i] = fibre_default;
        }
    }
    this->auxiliary.resize(n_qp);
    this->grad_auxiliary.resize(n_qp);
    this->auxiliary_rate.resize(n_qp);
    for (unsigned int i=0; i<n_qp; i++) {
        this->auxiliary[i].assign(n_aux, 0.0);
        this->grad_auxiliary[i].assign(n_aux, RealGradient(0.0, 0.0, 0.0));
        this->auxiliary_rate[i].assign(n_aux, 0.0);
    }
}
//-------------------------------------------------------------------------------------------------
inline
void QuadratureInfo::save ( std::ofstream& file,
                            QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) const {
    libmesh_assert(file.is_open() && file.good());
    //
    const unsigned int n_qp = this->XYZ.size();
    file.write((char*)&n_qp, sizeof(unsigned int));
    //
    const unsigned int n_bc = this->biochem[0].size();
    file.write((char*)&n_bc, sizeof(unsigned int));
    //
    const unsigned int n_bm = this->biomech[0].size();
    file.write((char*)&n_bm, sizeof(unsigned int));
    //
    const unsigned int n_aux = this->auxiliary[0].size();
    file.write((char*)&n_aux, sizeof(unsigned int));
    //
    // loop for all quadrature points
    for (unsigned int i=0; i<n_qp; i++) {
        //
        if ( QuadratureInfo::REFERENCE == cfg || QuadratureInfo::ALL == cfg ) {
            // quadrature point (reference) vector coordinates
            file.write((char*)&this->XYZ[i](0), sizeof(Real));
            file.write((char*)&this->XYZ[i](1), sizeof(Real));
            file.write((char*)&this->XYZ[i](2), sizeof(Real));
            // fibre (reference) vector coordinates
            file.write((char*)&this->fibre_ref[0][i](0), sizeof(Real));
            file.write((char*)&this->fibre_ref[0][i](1), sizeof(Real));
            file.write((char*)&this->fibre_ref[0][i](2), sizeof(Real));
            file.write((char*)&this->fibre_ref[1][i](0), sizeof(Real));
            file.write((char*)&this->fibre_ref[1][i](1), sizeof(Real));
            file.write((char*)&this->fibre_ref[1][i](2), sizeof(Real));
            file.write((char*)&this->fibre_ref[2][i](0), sizeof(Real));
            file.write((char*)&this->fibre_ref[2][i](1), sizeof(Real));
            file.write((char*)&this->fibre_ref[2][i](2), sizeof(Real));
        }
        //
        if ( QuadratureInfo::CURRENT == cfg || QuadratureInfo::ALL == cfg ) {
            // quadrature point (current) vector coordinates
            file.write((char*)&this->xyz[i](0), sizeof(Real));
            file.write((char*)&this->xyz[i](1), sizeof(Real));
            file.write((char*)&this->xyz[i](2), sizeof(Real));
            // fibre (current) vector coordinates
            file.write((char*)&this->fibre_curr[0][i](0), sizeof(Real));
            file.write((char*)&this->fibre_curr[0][i](1), sizeof(Real));
            file.write((char*)&this->fibre_curr[0][i](2), sizeof(Real));
            file.write((char*)&this->fibre_curr[1][i](0), sizeof(Real));
            file.write((char*)&this->fibre_curr[1][i](1), sizeof(Real));
            file.write((char*)&this->fibre_curr[1][i](2), sizeof(Real));
            file.write((char*)&this->fibre_curr[2][i](0), sizeof(Real));
            file.write((char*)&this->fibre_curr[2][i](1), sizeof(Real));
            file.write((char*)&this->fibre_curr[2][i](2), sizeof(Real));
        }
        //
        for (unsigned int l=0; l<n_bc; l++) {
            file.write((char*)&this->biochem[i][l], sizeof(Real));
            file.write((char*)&this->grad_biochem[i][l](0), sizeof(Real));
            file.write((char*)&this->grad_biochem[i][l](1), sizeof(Real));
            file.write((char*)&this->grad_biochem[i][l](2), sizeof(Real));
            file.write((char*)&this->biochem_rate[i][l], sizeof(Real));
        }
        for (unsigned int l=0; l<n_bm; l++) {
            file.write((char*)&this->biomech[i][l], sizeof(Real));
        }
        for (unsigned int l=0; l<n_aux; l++) {
            file.write((char*)&this->auxiliary[i][l], sizeof(Real));
            file.write((char*)&this->grad_auxiliary[i][l](0), sizeof(Real));
            file.write((char*)&this->grad_auxiliary[i][l](1), sizeof(Real));
            file.write((char*)&this->grad_auxiliary[i][l](2), sizeof(Real));
            file.write((char*)&this->auxiliary_rate[i][l], sizeof(Real));
        }
        //
        if ( QuadratureInfo::LEVEL_1 <= lvl ) {
            // deformation gradient tensor
            file.write((char*)&this->deform_grad[i](0,0), sizeof(Real));
            file.write((char*)&this->deform_grad[i](0,1), sizeof(Real));
            file.write((char*)&this->deform_grad[i](0,2), sizeof(Real));
            file.write((char*)&this->deform_grad[i](1,0), sizeof(Real));
            file.write((char*)&this->deform_grad[i](1,1), sizeof(Real));
            file.write((char*)&this->deform_grad[i](1,2), sizeof(Real));
            file.write((char*)&this->deform_grad[i](2,0), sizeof(Real));
            file.write((char*)&this->deform_grad[i](2,1), sizeof(Real));
            file.write((char*)&this->deform_grad[i](2,2), sizeof(Real));
            // elastic (recoverable) deformation gradient tensor
            file.write((char*)&this->deform_grad_elas[i](0,0), sizeof(Real));
            file.write((char*)&this->deform_grad_elas[i](0,1), sizeof(Real));
            file.write((char*)&this->deform_grad_elas[i](0,2), sizeof(Real));
            file.write((char*)&this->deform_grad_elas[i](1,0), sizeof(Real));
            file.write((char*)&this->deform_grad_elas[i](1,1), sizeof(Real));
            file.write((char*)&this->deform_grad_elas[i](1,2), sizeof(Real));
            file.write((char*)&this->deform_grad_elas[i](2,0), sizeof(Real));
            file.write((char*)&this->deform_grad_elas[i](2,1), sizeof(Real));
            file.write((char*)&this->deform_grad_elas[i](2,2), sizeof(Real));
            // inelastic (irrecoverable) deformation gradient tensor
            file.write((char*)&this->deform_grad_inel[i](0,0), sizeof(Real));
            file.write((char*)&this->deform_grad_inel[i](0,1), sizeof(Real));
            file.write((char*)&this->deform_grad_inel[i](0,2), sizeof(Real));
            file.write((char*)&this->deform_grad_inel[i](1,0), sizeof(Real));
            file.write((char*)&this->deform_grad_inel[i](1,1), sizeof(Real));
            file.write((char*)&this->deform_grad_inel[i](1,2), sizeof(Real));
            file.write((char*)&this->deform_grad_inel[i](2,0), sizeof(Real));
            file.write((char*)&this->deform_grad_inel[i](2,1), sizeof(Real));
            file.write((char*)&this->deform_grad_inel[i](2,2), sizeof(Real));
            // stress tensor in Voigt form
            file.write((char*)&this->stress[i](0,0), sizeof(Real));
            file.write((char*)&this->stress[i](1,1), sizeof(Real));
            file.write((char*)&this->stress[i](2,2), sizeof(Real));
            file.write((char*)&this->stress[i](1,2), sizeof(Real));
            file.write((char*)&this->stress[i](0,2), sizeof(Real));
            file.write((char*)&this->stress[i](0,1), sizeof(Real));
            // hydrostatic pressure
            file.write((char*)&this->pressure[i], sizeof(Real));
            // fluid flow velocity
            file.write((char*)&this->fluid_velocity[i](0), sizeof(Real));
            file.write((char*)&this->fluid_velocity[i](1), sizeof(Real));
            file.write((char*)&this->fluid_velocity[i](2), sizeof(Real));
        }
        //
        if ( QuadratureInfo::LEVEL_2 <= lvl ) {
            // strain tensor in Voigt form
            file.write((char*)&this->strain[i](0,0), sizeof(Real));
            file.write((char*)&this->strain[i](1,1), sizeof(Real));
            file.write((char*)&this->strain[i](2,2), sizeof(Real));
            file.write((char*)&this->strain[i](1,2), sizeof(Real));
            file.write((char*)&this->strain[i](0,2), sizeof(Real));
            file.write((char*)&this->strain[i](0,1), sizeof(Real));
            // elastic (recoverable) strain tensor in Voigt form
            file.write((char*)&this->strain_elas[i](0,0), sizeof(Real));
            file.write((char*)&this->strain_elas[i](1,1), sizeof(Real));
            file.write((char*)&this->strain_elas[i](2,2), sizeof(Real));
            file.write((char*)&this->strain_elas[i](1,2), sizeof(Real));
            file.write((char*)&this->strain_elas[i](0,2), sizeof(Real));
            file.write((char*)&this->strain_elas[i](0,1), sizeof(Real));
            // inelastic (irrecoverable) strain tensor in Voigt form
            file.write((char*)&this->strain_inel[i](0,0), sizeof(Real));
            file.write((char*)&this->strain_inel[i](1,1), sizeof(Real));
            file.write((char*)&this->strain_inel[i](2,2), sizeof(Real));
            file.write((char*)&this->strain_inel[i](1,2), sizeof(Real));
            file.write((char*)&this->strain_inel[i](0,2), sizeof(Real));
            file.write((char*)&this->strain_inel[i](0,1), sizeof(Real));
        }
        //
        // end of quadrature points loop...
    }
}
//-------------------------------------------------------------------------------------------------
inline
void QuadratureInfo::load ( std::ifstream& file,
                            QuadratureInfo::Config cfg, QuadratureInfo::Level lvl ) {
    libmesh_assert(file.is_open() && file.good());
    //
    unsigned int n_qp = 0;
    file.read((char*)&n_qp, sizeof(unsigned int));
    //
    unsigned int n_bc = 0;
    file.read((char*)&n_bc, sizeof(unsigned int));
    //
    unsigned int n_bm = 0;
    file.read((char*)&n_bm, sizeof(unsigned int));
    //
    unsigned int n_aux = 0;
    file.read((char*)&n_aux, sizeof(unsigned int));
    //
    this->init(n_qp, n_bc, n_bm, n_aux);
    //
    // loop for all quadrature points
    for (unsigned int i=0; i<n_qp; i++) {
        //
        if ( QuadratureInfo::REFERENCE == cfg || QuadratureInfo::ALL == cfg ) {
            // quadrature point (reference) vector coordinates
            file.read((char*)&this->XYZ[i](0), sizeof(Real));
            file.read((char*)&this->XYZ[i](1), sizeof(Real));
            file.read((char*)&this->XYZ[i](2), sizeof(Real));
            // fibre (reference) vector coordinates
            file.read((char*)&this->fibre_ref[0][i](0), sizeof(Real));
            file.read((char*)&this->fibre_ref[0][i](1), sizeof(Real));
            file.read((char*)&this->fibre_ref[0][i](2), sizeof(Real));
            file.read((char*)&this->fibre_ref[1][i](0), sizeof(Real));
            file.read((char*)&this->fibre_ref[1][i](1), sizeof(Real));
            file.read((char*)&this->fibre_ref[1][i](2), sizeof(Real));
            file.read((char*)&this->fibre_ref[2][i](0), sizeof(Real));
            file.read((char*)&this->fibre_ref[2][i](1), sizeof(Real));
            file.read((char*)&this->fibre_ref[2][i](2), sizeof(Real));
        }
        //
        if ( QuadratureInfo::CURRENT == cfg || QuadratureInfo::ALL == cfg ) {
            // quadrature point (current) vector coordinates
            file.read((char*)&this->xyz[i](0), sizeof(Real));
            file.read((char*)&this->xyz[i](1), sizeof(Real));
            file.read((char*)&this->xyz[i](2), sizeof(Real));
            // fibre (current) vector coordinates
            file.read((char*)&this->fibre_curr[0][i](0), sizeof(Real));
            file.read((char*)&this->fibre_curr[0][i](1), sizeof(Real));
            file.read((char*)&this->fibre_curr[0][i](2), sizeof(Real));
            file.read((char*)&this->fibre_curr[1][i](0), sizeof(Real));
            file.read((char*)&this->fibre_curr[1][i](1), sizeof(Real));
            file.read((char*)&this->fibre_curr[1][i](2), sizeof(Real));
            file.read((char*)&this->fibre_curr[2][i](0), sizeof(Real));
            file.read((char*)&this->fibre_curr[2][i](1), sizeof(Real));
            file.read((char*)&this->fibre_curr[2][i](2), sizeof(Real));
        } else {
            // quadrature point (current) vector coordinates
            this->xyz[i] = this->XYZ[i];
            // fibre (current) vector coordinates
            this->fibre_curr[0][i] = this->fibre_ref[0][i];
            this->fibre_curr[1][i] = this->fibre_ref[1][i];
            this->fibre_curr[2][i] = this->fibre_ref[2][i];
        }
        //
        for (unsigned int l=0; l<n_bc; l++) {
            file.read((char*)&this->biochem[i][l], sizeof(Real));
            file.read((char*)&this->grad_biochem[i][l](0), sizeof(Real));
            file.read((char*)&this->grad_biochem[i][l](1), sizeof(Real));
            file.read((char*)&this->grad_biochem[i][l](2), sizeof(Real));
            file.read((char*)&this->biochem_rate[i][l], sizeof(Real));
        }
        for (unsigned int l=0; l<n_bm; l++) {
            file.read((char*)&this->biomech[i][l], sizeof(Real));
        }
        for (unsigned int l=0; l<n_aux; l++) {
            file.read((char*)&this->auxiliary[i][l], sizeof(Real));
            file.read((char*)&this->grad_auxiliary[i][l](0), sizeof(Real));
            file.read((char*)&this->grad_auxiliary[i][l](1), sizeof(Real));
            file.read((char*)&this->grad_auxiliary[i][l](2), sizeof(Real));
            file.read((char*)&this->auxiliary_rate[i][l], sizeof(Real));
        }
        //
        if ( QuadratureInfo::LEVEL_1 <= lvl ) {
            // deformation gradient tensor
            file.read((char*)&this->deform_grad[i](0,0), sizeof(Real));
            file.read((char*)&this->deform_grad[i](0,1), sizeof(Real));
            file.read((char*)&this->deform_grad[i](0,2), sizeof(Real));
            file.read((char*)&this->deform_grad[i](1,0), sizeof(Real));
            file.read((char*)&this->deform_grad[i](1,1), sizeof(Real));
            file.read((char*)&this->deform_grad[i](1,2), sizeof(Real));
            file.read((char*)&this->deform_grad[i](2,0), sizeof(Real));
            file.read((char*)&this->deform_grad[i](2,1), sizeof(Real));
            file.read((char*)&this->deform_grad[i](2,2), sizeof(Real));
            // elastic (recoverable) deformation gradient tensor
            file.read((char*)&this->deform_grad_elas[i](0,0), sizeof(Real));
            file.read((char*)&this->deform_grad_elas[i](0,1), sizeof(Real));
            file.read((char*)&this->deform_grad_elas[i](0,2), sizeof(Real));
            file.read((char*)&this->deform_grad_elas[i](1,0), sizeof(Real));
            file.read((char*)&this->deform_grad_elas[i](1,1), sizeof(Real));
            file.read((char*)&this->deform_grad_elas[i](1,2), sizeof(Real));
            file.read((char*)&this->deform_grad_elas[i](2,0), sizeof(Real));
            file.read((char*)&this->deform_grad_elas[i](2,1), sizeof(Real));
            file.read((char*)&this->deform_grad_elas[i](2,2), sizeof(Real));
            // inelastic (irrecoverable) deformation gradient tensor
            file.read((char*)&this->deform_grad_inel[i](0,0), sizeof(Real));
            file.read((char*)&this->deform_grad_inel[i](0,1), sizeof(Real));
            file.read((char*)&this->deform_grad_inel[i](0,2), sizeof(Real));
            file.read((char*)&this->deform_grad_inel[i](1,0), sizeof(Real));
            file.read((char*)&this->deform_grad_inel[i](1,1), sizeof(Real));
            file.read((char*)&this->deform_grad_inel[i](1,2), sizeof(Real));
            file.read((char*)&this->deform_grad_inel[i](2,0), sizeof(Real));
            file.read((char*)&this->deform_grad_inel[i](2,1), sizeof(Real));
            file.read((char*)&this->deform_grad_inel[i](2,2), sizeof(Real));
            // stress tensor in Voigt form
            file.read((char*)&this->stress[i](0,0), sizeof(Real));
            file.read((char*)&this->stress[i](1,1), sizeof(Real));
            file.read((char*)&this->stress[i](2,2), sizeof(Real));
            file.read((char*)&this->stress[i](1,2), sizeof(Real)); this->stress[i].comp(2,1) = this->stress[i](1,2);
            file.read((char*)&this->stress[i](0,2), sizeof(Real)); this->stress[i].comp(2,0) = this->stress[i](0,2);
            file.read((char*)&this->stress[i](0,1), sizeof(Real)); this->stress[i].comp(1,0) = this->stress[i](0,1);
            // hydrostatic pressure
            file.read((char*)&this->pressure[i], sizeof(Real));
            // fluid flow velocity
            file.read((char*)&this->fluid_velocity[i](0), sizeof(Real));
            file.read((char*)&this->fluid_velocity[i](1), sizeof(Real));
            file.read((char*)&this->fluid_velocity[i](2), sizeof(Real));
        }
        //
        if ( QuadratureInfo::LEVEL_2 <= lvl ) {
            // strain tensor in Voigt form
            file.read((char*)&this->strain[i](0,0), sizeof(Real));
            file.read((char*)&this->strain[i](1,1), sizeof(Real));
            file.read((char*)&this->strain[i](2,2), sizeof(Real));
            file.read((char*)&this->strain[i](1,2), sizeof(Real)); this->strain[i].comp(2,1) = this->strain[i](1,2);
            file.read((char*)&this->strain[i](0,2), sizeof(Real)); this->strain[i].comp(2,0) = this->strain[i](0,2);
            file.read((char*)&this->strain[i](0,1), sizeof(Real)); this->strain[i].comp(1,0) = this->strain[i](0,1);
            // elastic (recoverable) strain tensor in Voigt form
            file.read((char*)&this->strain_elas[i](0,0), sizeof(Real));
            file.read((char*)&this->strain_elas[i](1,1), sizeof(Real));
            file.read((char*)&this->strain_elas[i](2,2), sizeof(Real));
            file.read((char*)&this->strain_elas[i](1,2), sizeof(Real)); this->strain_elas[i].comp(2,1) = this->strain_elas[i](1,2);
            file.read((char*)&this->strain_elas[i](0,2), sizeof(Real)); this->strain_elas[i].comp(2,0) = this->strain_elas[i](0,2);
            file.read((char*)&this->strain_elas[i](0,1), sizeof(Real)); this->strain_elas[i].comp(1,0) = this->strain_elas[i](0,1);
            // inelastic (irrecoverable) strain tensor in Voigt form
            file.read((char*)&this->strain_inel[i](0,0), sizeof(Real));
            file.read((char*)&this->strain_inel[i](1,1), sizeof(Real));
            file.read((char*)&this->strain_inel[i](2,2), sizeof(Real));
            file.read((char*)&this->strain_inel[i](1,2), sizeof(Real)); this->strain_inel[i].comp(2,1) = this->strain_inel[i](1,2);
            file.read((char*)&this->strain_inel[i](0,2), sizeof(Real)); this->strain_inel[i].comp(2,0) = this->strain_inel[i](0,2);
            file.read((char*)&this->strain_inel[i](0,1), sizeof(Real)); this->strain_inel[i].comp(1,0) = this->strain_inel[i](0,1);
        }
        //
        // end of quadrature points loop...
    }
}
//=================================================================================================
#endif // __QUADRATURE_INLINE_H__
//=================================================================================================
